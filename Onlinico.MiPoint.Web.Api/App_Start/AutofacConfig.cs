﻿using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Autofac.Integration.SignalR;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Onlinico.MiPoint.Web.Api.App_Start;
using Onlinico.MiPoint.Web.Api.Hubs;
using Onlinico.MiPoint.Web.Api.Hubs.Interfaces;
using Onlinico.MiPoint.Web.Api.Hubs.Wrappers;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;
using Onlinico.MiPoint.Web.Service.Services.Api;
using Onlinico.MiPoint.Web.Service.Services.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Services.Global;
using Onlinico.MiPoint.Web.Service.Services.Global.External;
using Owin;

namespace Onlinico.MiPoint.Web.Api
{
    public static class AutofacConfig
    {
        public static IContainer RegisterContainer(IAppBuilder app)
        {

            var builder = new ContainerBuilder();

            // Register your MVC controllers.s
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //Register your Api controllers.s
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            RegisterTypes(builder, app);


            // OPTIONAL: Register web abstractions like HttpContextBase.
            //builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            //builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            //builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            DependencyResolver.SetResolver(new Autofac.Integration.Mvc.AutofacDependencyResolver(container)); //Set the MVC DependencyResolver
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver((IContainer)container); //Set the WebApi DependencyResolver
            GlobalHost.DependencyResolver = new Autofac.Integration.SignalR.AutofacDependencyResolver(container); //Ser the SignalR DependencyResolver

            return container;
        }

        private static void RegisterTypes(ContainerBuilder builder, IAppBuilder app)
        {
            // REGISTER DEPENDENCIES
            builder.RegisterType<MiPointDbContext>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerRequest();
            builder.Register(c => new UserStore<User>(c.Resolve<MiPointDbContext>())).AsImplementedInterfaces();
            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).As<IAuthenticationManager>().InstancePerRequest();
            builder.Register<IDataProtectionProvider>(c => app.GetDataProtectionProvider()).InstancePerRequest();


            //builder.Register(c => new IdentityFactoryOptions<ApplicationUserManager>
            //{
            //    DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("MiPoint​DpapiDataProtectionProvider")
            //}).InstancePerRequest();

            //GLOBAL
            RegisterHubs(builder);

            RegistreGlobalTypes(builder);

            RegisterTypesWeb(builder);

            RegisterTypesApiVersion1(builder);

            RegisterTypesApiVersion2(builder);

            RegisterTypesApiVersion3(builder);

            RegisterTypesApiVersion4(builder);

            RegisterTypesApiAgentVersion1(builder);
        }

        private static void RegisterHubs(ContainerBuilder builder)
        {
            // Register your SignalR hubs.
            builder.RegisterHubs(Assembly.GetExecutingAssembly());
        }

        private static void RegistreGlobalTypes(ContainerBuilder builder)
        {
            builder.RegisterType<RequestService>().As<IRequestService>();
            builder.RegisterType<Logger.Logger>().As<Logger.Interfaces.ILogger>();
            builder.RegisterType<Configuration>().As<Core.Interfaces.IConfiguration>();
            builder.RegisterType<MemoryCacher>().As<IMemoryCacher>();
            builder.RegisterType<AuthService>().As<IAuthService>();
            builder.RegisterType<EmailService>().As<IEmailService>();
            builder.RegisterType<SmsService>().As<ISmsService>();
            builder.RegisterType<AgentManager>().As<IAgentManager>().SingleInstance();
        }

        private static void RegisterTypesWeb(ContainerBuilder builder)
        {
            builder.RegisterType<Service.Services.Web.HistoryService>().As<Service.Interfaces.Web.IHistoryService>();
            builder.RegisterType<Service.Services.Web.SessionLogService>().As<Service.Interfaces.Web.ISessionLogService>();
            builder.RegisterType<Service.Services.Web.UserService>().As<Service.Interfaces.Web.IUserService>();
            builder.RegisterType<Service.Services.Web.MerchantService>().As<Service.Interfaces.Web.IMerchantService>();
            builder.RegisterType<Service.Services.Web.LocationService>().As<Service.Interfaces.Web.ILocationService>();
            builder.RegisterType<OmnivoreService>().As<IOmnivoreService>();
            builder.RegisterType<Service.Services.Web.InviteService>().As<Service.Interfaces.Web.IInviteService>();
            builder.RegisterType<Service.Services.Web.EmailService>().As<Service.Interfaces.Web.IEmailService>();
            builder.RegisterType<Service.Services.Web.ReceiptService>().As<Service.Interfaces.Web.IReceiptService>();
            builder.RegisterType<Service.Services.Web.PaymentService>().As<Service.Interfaces.Web.IPaymentService>();
        }

        private static void RegisterTypesApiVersion1(ContainerBuilder builder)
        {
            builder.RegisterType<Service.Services.Api.v1.LocationService>().As<Service.Interfaces.Api.v1.ILocationService>();
            builder.RegisterType<Service.Services.Api.v1.HistoryService>().As<Service.Interfaces.Api.v1.IHistoryService>();
            builder.RegisterType<Service.Services.Api.v1.LogService>().As<Service.Interfaces.Api.v1.ILogService>();
            builder.RegisterType<Service.Services.Api.v1.ReceiptService>().As<Service.Interfaces.Api.v1.IReceiptService>();
            builder.RegisterType<Service.Services.Api.v1.EmailService>().As<Service.Interfaces.Api.v1.IEmailService>();
            builder.RegisterType<Service.Services.Api.v1.SmsService>().As<Service.Interfaces.Api.v1.ISmsService>();
        }

        private static void RegisterTypesApiVersion2(ContainerBuilder builder)
        {
            builder.RegisterType<Service.Services.Api.v2.LocationService>().As<Service.Interfaces.Api.v2.ILocationService>();
            builder.RegisterType<Service.Services.Api.v2.HistoryService>().As<Service.Interfaces.Api.v2.IHistoryService>();
            builder.RegisterType<Service.Services.Api.v2.SessionLogService>().As<Service.Interfaces.Api.v2.ISessionLogService>();
            builder.RegisterType<Service.Services.Api.v2.ReceiptService>().As<Service.Interfaces.Api.v2.IReceiptService>();
            builder.RegisterType<Service.Services.Api.v2.EmailService>().As<Service.Interfaces.Api.v2.IEmailService>();
            builder.RegisterType<Service.Services.Api.v2.SmsService>().As<Service.Interfaces.Api.v2.ISmsService>();
        }

        private static void RegisterTypesApiVersion3(ContainerBuilder builder)
        {
            builder.RegisterType<Service.Services.Api.v3.LocationService>().As<Service.Interfaces.Api.v3.ILocationService>();
            builder.RegisterType<Service.Services.Api.v3.HistoryService>().As<Service.Interfaces.Api.v3.IHistoryService>();
            builder.RegisterType<Service.Services.Api.v3.SessionLogService>().As<Service.Interfaces.Api.v3.ISessionLogService>();
            builder.RegisterType<Service.Services.Api.v3.ReceiptService>().As<Service.Interfaces.Api.v3.IReceiptService>();
            builder.RegisterType<Service.Services.Api.v3.PaymentService>().As<Service.Interfaces.Api.v3.IPaymentService>();
        }

        private static void RegisterTypesApiVersion4(ContainerBuilder builder)
        {
            builder.RegisterType<Service.Services.Api.v4.LocationService>().As<Service.Interfaces.Api.v4.ILocationService>();
            builder.RegisterType<Service.Services.Api.v4.HistoryService>().As<Service.Interfaces.Api.v4.IHistoryService>();
            builder.RegisterType<Service.Services.Api.v4.SessionLogService>().As<Service.Interfaces.Api.v4.ISessionLogService>();
            builder.RegisterType<Service.Services.Api.v4.ReceiptService>().As<Service.Interfaces.Api.v4.IReceiptService>();
            builder.RegisterType<Service.Services.Api.v4.PaymentService>().As<Service.Interfaces.Api.v4.IPaymentService>();
        }

        private static void RegisterTypesApiAgentVersion1(ContainerBuilder builder)
        {
            builder.RegisterType<AgentService>().As<IAgentService>();
            builder.RegisterType<LocationService>().As<ILocationService>();

            builder.RegisterType<AgentHubPusher>().As<IAgentHubPusher>();
            builder.RegisterType<VicarPusher>().As<IVicarPusher>();
        }
    }
}