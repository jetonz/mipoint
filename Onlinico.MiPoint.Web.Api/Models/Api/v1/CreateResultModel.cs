﻿using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v1
{
    /// <summary>
    /// Create result model
    /// </summary>
    [DataContract]
    public class CreateResultModel
    {
        /// <summary>
        /// Model constructor
        /// </summary>
        public CreateResultModel()
        {
            
        }

        /// <summary>
        /// Model constructor with object id
        /// </summary>
        /// <param name="id"></param>
        public CreateResultModel(string id)
        {
            ObjectId = id;
        }

        /// <summary>
        /// Created object id
        /// </summary>
        [DataMember(Name = "objectId")]
        public string ObjectId { get; set; }
    }
}