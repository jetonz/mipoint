﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper
{
    public static class EmployeeMapper
    {
        public static EmployeeModel ToEmployeeModel(this Domain.Models.Api.v4.EmployeeModel employee)
        {
            return new EmployeeModel
            {
                Id = employee.Id,
                EmployeeId = employee.EmployeeId,
                Login = employee.Login,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                CheckName = employee.CheckName,
            };
        }
    }
}