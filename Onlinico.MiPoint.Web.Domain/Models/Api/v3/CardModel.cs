﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v3
{
    public class CardModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
