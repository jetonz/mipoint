﻿using System;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class SettingsMapper
    {
        public static LocationSettingsModel ToDomainSettings(this LocationSettings settings, string locationId, string omnivoreId)
        {
            return new LocationSettingsModel
            {
                Id = settings.Id,
                OmnivoreId = omnivoreId,
                TaxRate = settings.TaxRate,
                Tips = settings.Tips.Split(',').Select(Int32.Parse).ToArray(),
                TenderTypes = settings.TenderTypes.ToDomainTenderTypes(),
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                SessionTimeout = settings.SessionTimeout,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }
    }
}
