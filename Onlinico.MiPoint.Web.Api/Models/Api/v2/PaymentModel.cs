﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2
{
    public class PaymentModel
    {
        /// <summary>
        /// Payment id
        /// </summary>
        [JsonProperty("id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Ticket id
        /// </summary>
        [JsonProperty("ticket_id")]
        public string TicketId { get; set; }

        /// <summary>
        /// Payment amount
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }

        /// <summary>
        /// Tip
        /// </summary>
        [JsonProperty("tip")]
        public int Tip { get; set; }

        /// <summary>
        /// Payment catd type
        /// </summary>
        [JsonProperty("card_type")]
        public string CardType { get; set; }

        /// <summary>
        /// Payment items
        /// </summary>
        [JsonProperty("items")]
        public List<PaymentItem> Items { get; set; }
    }

    public class PaymentItem
    {
        /// <summary>
        /// Item id
        /// </summary>
        [JsonProperty("item_id")]
        public string Id { get; set; }

        /// <summary>
        /// Item name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Item quantity
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Item price per unit
        /// </summary>
        [JsonProperty("price")]
        public long Price { get; set; }
    }
}