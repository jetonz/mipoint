﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2
{
    public class LocationModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("address_full")]
        public string AddressFull { get; set; }

        [JsonProperty("owner")]
        public string Owner { get; set; }

        [JsonProperty("pos_type")]
        public string PosType { get; set; }
    }
}