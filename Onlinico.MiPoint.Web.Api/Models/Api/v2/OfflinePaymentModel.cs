﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2
{
    [DataContract]
    public class OfflinePaymentModel
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "orderId")]
        public string OrderId { get; set; }

        [DataMember(Name = "employeeId")]
        public string EmployeeId { get; set; }

        [DataMember(Name = "tableNumber")]
        public string TableNumber { get; set; }

        [DataMember(Name = "amount")]
        public long Amount { get; set; }

        [DataMember(Name = "tip")]
        public long Tip { get; set; }

        [DataMember(Name = "cardType")]
        public string CardType { get; set; }

        [DataMember(Name = "transactionDateTime")]
        public DateTime TransactionDateTime { get; set; }
    }
}