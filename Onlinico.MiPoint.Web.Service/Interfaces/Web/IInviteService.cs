﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface IInviteService
    {
        ResultModel<InviteModel> Find(Guid merchantId, string email);

        Task<ResultModel<UserModel>> GetInvitedUser(Guid merchantId, string email);

        ResultModel<InviteModel> Get(Guid id);

        ResultModel Add(InviteModel model);

        ResultModel AcceptInvite(Guid id, string userId);
    }
}
