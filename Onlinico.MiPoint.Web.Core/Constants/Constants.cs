﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Core.Constants
{
    public static class Constants
    {
        public const string Unauthorized = "Unauthorized";

        public const string Fingerprint = "Fingerprint";

        public const string ApiKey = "ApiKey";

        public const string ServerId = "ServerId";

        public const string ActiveUserList = "activeuserlist";
    }
}
