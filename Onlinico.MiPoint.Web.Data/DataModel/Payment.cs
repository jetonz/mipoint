﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class Payment
    {
        public Payment()
        {
            Items = new HashSet<PaymentItem>();
        }

        [Key]
        [MaxLength(36)]
        public string Id { get; set; }

        [Index]
        [Required]
        [MaxLength(50)]
        public string DeviceId { get; set; }

        [Index]
        [Required]
        [MaxLength(50)]
        public string OrderId { get; set; }
        
        [Index]
        public PaymentTypeEnum Type { get; set; }

        [Index]
        public PaymentStatusEnum Status { get; set; }

        public long Amount { get; set; }

        public long Tip { get; set; }

        public long Tax { get; set; }

        [MaxLength(36)]
        public string EmployeeCode { get; set; }

        [MaxLength(36)]
        public string TableNumber { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }
        
        public string Message { get; set; }


        public virtual Location Location { get; set; }

        public virtual PaymentReceiptInfo ReceiptInfo { get; set; }

        public virtual PaymentTransaction Transaction { get; set; }

        public virtual ICollection<PaymentItem> Items { get; set; }
    }
}
