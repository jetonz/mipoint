﻿using System;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v3
{
    public static class SettingsMapper
    {
        public static Domain.Models.Api.v3.LocationSettingsModel ToDomainSettings(this Data.DataModel.LocationSettings settings, string locationId, string omnivoreId)
        {
            return new Domain.Models.Api.v3.LocationSettingsModel
            {
                Id = settings.Id,
                OmnivoreId = omnivoreId,
                TaxRate = settings.TaxRate,
                Tips = settings.Tips.Split(',').Select(Int32.Parse).ToArray(),
                TenderTypes = settings.TenderTypes.ToDomainTenderTypes(),
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                SessionTimeout = settings.SessionTimeout,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }
    }
}
