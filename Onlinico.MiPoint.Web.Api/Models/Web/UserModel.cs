﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class UserModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string Login { get; set; }
    }

    public class RoleModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class UserMerchantRelationsModel
    {
        public string UserId { get; set; }

        public List<SelectedItem<MerchantModel>> SelectedMerchantItems { get; set; }

        public List<Guid> SelectedMerchants { get; set; }
    }
}