﻿namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class CardMapper
    {
        public static Domain.Models.Web.CardModel ToDomainCardModel(this Data.DataModel.Card card)
        {
            return new Domain.Models.Web.CardModel
            {
                Id = card.Id,
                Name = card.Name,
                Type = card.Type
            };
        }

        public static Data.DataModel.Card ToEntityCard(this Domain.Models.Web.CardModel card)
        {
            return new Data.DataModel.Card
            {
                Id = card.Id,
                Name = card.Name,
                Type = card.Type
            };
        }
    }
}
