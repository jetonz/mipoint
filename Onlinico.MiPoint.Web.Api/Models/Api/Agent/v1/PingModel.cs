﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1.Enums;

namespace Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1
{
    public class PingModel
    {
        [JsonProperty("fingerprint")]
        public string Fingerprint { get; set; }

        [JsonProperty("agentVersion")]
        public string AgentVersion { get; set; }

        [JsonProperty("status"), JsonConverter(typeof(StringEnumConverter))]
        public AgentStatusEnum Status { get; set; }
    }

    public class PongModel
    {
        [JsonProperty("availableVersion")]
        public string AvailableVersion { get; set; }

        [JsonProperty("updateUrl")]
        public string UpdateUrl { get; set; }
    }
}