﻿using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v2
{
    public static class ReceiptMapper
    {
        public static Domain.Models.Api.v2.ReceiptModel ToDomainReceiptModel(this Data.DataModel.Receipt receipt)
        {
            return new Domain.Models.Api.v2.ReceiptModel
            {
                Id = receipt.Id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToDomainPaymentModel(),
                Items = receipt.Items.ToDomainReceiptItemModels(),
            };
        }

        public static List<Domain.Models.Api.v2.ReceiptModel> ToDomainReceipts(
            this IEnumerable<Data.DataModel.Receipt> models)
        {
            return models.Select(r => r.ToDomainReceiptModel()).ToList();
        } 

        public static List<Domain.Models.Api.v2.ReceiptItemModel> ToDomainReceiptItemModels(this IEnumerable<Data.DataModel.ReceiptItem> items)
        {
            return items.Select(i => new Domain.Models.Api.v2.ReceiptItemModel
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Domain.Models.Api.v2.ReceiptPaymentModel ToDomainPaymentModel(this Data.DataModel.ReceiptPayment payment)
        {
            return new Domain.Models.Api.v2.ReceiptPaymentModel
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }

        public static Data.DataModel.Receipt ToEntityReceiptModel(this Domain.Models.Api.v2.ReceiptModel receipt)
        {
            return new Data.DataModel.Receipt
            {
                Id = receipt.Id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToEntityPaymentModel(),
                Items = receipt.Items.ToEntityReceiptItems(),
            };
        }

        public static Data.DataModel.Receipt ToEntityReceiptModel(this Domain.Models.Api.v2.ReceiptModel receipt, string id)
        {
            return new Data.DataModel.Receipt
            {
                Id = id,
                TicketId = receipt.TicketId,
                LocationId = receipt.LocationId,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToEntityPaymentModel(),
                Items = receipt.Items.ToEntityReceiptItems(),
            };
        }

        public static List<Data.DataModel.ReceiptItem> ToEntityReceiptItems(this List<Domain.Models.Api.v2.ReceiptItemModel> items)
        {
            return items.Select(i => new Data.DataModel.ReceiptItem
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Data.DataModel.ReceiptPayment ToEntityPaymentModel(this Domain.Models.Api.v2.ReceiptPaymentModel payment)
        {
            return new Data.DataModel.ReceiptPayment
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm
            };
        }
    }
}
