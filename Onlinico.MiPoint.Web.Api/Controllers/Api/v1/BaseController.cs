﻿using System;
using System.Web.Http;
using System.Web.Mvc;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v1
{
    /// <summary>
    /// Base web api controller
    /// </summary>
    public class BaseController : ApiController
    {
        /// <summary>
        /// Base Omnivore url
        /// </summary>
        protected readonly string BaseUri;

        /// <summary>
        /// Logger
        /// </summary>
        protected readonly Logger.Interfaces.ILogger Logger;

        /// <summary>
        /// Base controller default constructor
        /// </summary>
        public BaseController()
        {
            BaseUri = "https://api.omnivore.io/";
            Logger = DependencyResolver.Current.GetService<Logger.Interfaces.ILogger>();
        }

        /// <summary>
        /// Base controller constructor
        /// </summary>
        /// <param name="logger">Inject logger</param>
        public BaseController(Logger.Interfaces.ILogger logger)
        {
            BaseUri = "https://api.omnivore.io/";
            Logger = logger;
        }

        /// <summary>
        /// Get current server domain
        /// </summary>
        protected Uri CurrentDomain => new Uri(Request.RequestUri.Scheme + System.Uri.SchemeDelimiter + Request.RequestUri.Host + (Request.RequestUri.IsDefaultPort ? "" : ":" + Request.RequestUri.Port));

    }
}
