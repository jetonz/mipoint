﻿namespace Onlinico.MiPoint.Web.Api.Models.Web.Validation
{
    public static class RegisterModelValidator
    {
        public static bool IsValid(this RegisterModel model)
        {
            return !string.IsNullOrEmpty(model?.Name) 
                && !string.IsNullOrEmpty(model.Login) 
                && !string.IsNullOrEmpty(model.Password);
        }
    }
}