﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore;
using Quartz;

namespace Onlinico.MiPoint.Web.Api.Jobs
{
    /// <summary>
    /// Sync employees job
    /// </summary>
    public class SyncEmployeesJob : IJob
    {
        private IOmnivoreService _omnivoreService;
        private ILocationService _locationService;
        private ILogger _logger;
        private IConfiguration _configuration;

        /// <summary>
        /// Execute job method
        /// </summary>
        /// <param name="context"></param>
        public void Execute(IJobExecutionContext context)
        {
            JobKey key = context.JobDetail.Key;

            JobDataMap dataMap = context.MergedJobDataMap;

            IContainer container = (IContainer)dataMap["Container"];

            _omnivoreService = container.Resolve<IOmnivoreService>();
            _locationService = container.Resolve<ILocationService>();
            _logger = container.Resolve<ILogger>();
            _configuration = container.Resolve<IConfiguration>();

            List<DateTime> lastSyncDateTime = (List<DateTime>)dataMap["LastSync"];

            try
            {
                var startSyncTime = lastSyncDateTime?.FirstOrDefault() ?? DateTime.UtcNow.AddMinutes(_configuration.SyncTriggerIntervalInMinutes * -1);

                lastSyncDateTime?.Clear();

                var endSyncTime = DateTime.UtcNow;

                lastSyncDateTime?.Add(endSyncTime);

                var locationsResult = _locationService.Get(startSyncTime.TimeOfDay, endSyncTime.TimeOfDay);

                if (locationsResult.Succeed && locationsResult.Object != null)
                {
                    locationsResult.Object.ForEach(l =>
                    {
                        var emploeesResult = _omnivoreService.GetEmployees(l.IsDev ? _configuration.OmnivoreDevApiKey : _configuration.OmnivoreProdApiKey, l.Identifier).Result;

                        if (emploeesResult.Succeed && emploeesResult.Object != null)
                        {
                            var syncResult = _locationService.SyncEmployees(l.Id, emploeesResult.Object.ToDomainEmployees());

                            var message = syncResult.Succeed
                                ? $"Location '{l.Id}' was successfully synchronized."
                                : "Sync employees error - " + syncResult.Error;

                            _logger.WriteMessageToLog(StringHelper.LogMessageBuilder(message));
                        }
                        else
                        {
                            _logger.WriteMessageToLog(StringHelper.ErrorBuilder("Get employees from omnivore error - " + emploeesResult.Error));
                        }

                        _locationService.UpdateLocationLastSyncDate(l.Id);
                    });
                }
                else
                {
                    _logger.WriteMessageToLog(StringHelper.ErrorBuilder("Get location for sync error - " + locationsResult.Error));
                }
            }
            catch (Exception ex)
            {
                _logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
        }
    }
}