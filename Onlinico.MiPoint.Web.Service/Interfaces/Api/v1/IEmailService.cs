﻿using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v1
{
    public interface IEmailService
    {
        Task<ResultModel> SendAsync(string address, string subject, string html);
    }
}
