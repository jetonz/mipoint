﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v1
{
    public class LocationSettingsModel
    {
        public Guid Id { get; set; }
    
        public string TenderTypeId { get; set; }

        public string TenderType { get; set; }

        public int[] Tips { get; set; }
        
        public float TaxRate { get; set; }
    }
}
