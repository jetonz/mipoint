﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class UserService : BaseService, IUserService
    {
        public UserService(ILogger logger) : base(logger)
        {
        }

        public List<UserModel> Get()
        {
            List<UserModel> result;
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var userList = context.Users.ToList();
                    var adminRole = context.Roles.FirstOrDefault(r => r.Name == "Admin");
                    if (adminRole != null)
                    {
                        result = userList.Where(u => u.Roles.All(r => r.RoleId != adminRole.Id))?.ToDomainUserModels();
                    }
                    else
                    {
                        result = userList.OrderBy(u => u.FullName)?.ToDomainUserModels();
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public ResultModel<List<MerchantModel>> GetMerchants(string userId)
        {
            var result = new ResultModel<List<MerchantModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.Find(userId);
                    if (user != null)
                    {
                        result.Object = user.Merchants?.ToDomainMerchantModels();
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid user id.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public UserModel Get(string userId)
        {
            UserModel result;
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.FirstOrDefault(u => u.Id == userId);
                    result = user?.ToDomainUserModel();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public bool IsUsernameUnique(string username)
        {
            bool result;
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.FirstOrDefault(u => u.UserName == username);
                    result = user == null;
                }
            }
            catch (Exception ex)
            {
                result = false;
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public Domain.Models.Web.ResultModel UpdateUserRelations(string userId, List<Guid> merchantIds)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.FirstOrDefault(u => u.Id == userId);
                    if (user != null)
                    {
                        var userMerchants = user.Merchants.ToList();
                        userMerchants.ForEach(m => user.Merchants.Remove(m));

                        if (merchantIds != null && merchantIds.Any())
                        {
                            var merchants = context.Merchants.Where(m => merchantIds.Contains(m.Id)).ToList();
                            merchants.ForEach(m => user.Merchants.Add(m));
                        }

                        context.SaveChanges();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid user.");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }
    }
}
