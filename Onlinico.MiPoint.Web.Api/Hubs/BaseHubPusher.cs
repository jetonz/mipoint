﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Onlinico.MiPoint.Web.Api.Hubs
{
    public class BaseHubPusher
    {
        private readonly DefaultHubManager _hubManager;

        public BaseHubPusher()
        {
            _hubManager = new DefaultHubManager(GlobalHost.DependencyResolver);
        }

        public T ResolveHub<T>() where T : class
        {
            var name = typeof (T).Name;
            var hub = _hubManager.ResolveHub(name) as T;
            return hub;
        }
    }
}