﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper
{
    public static class PaymentMapper
    {
        public static IQueryable<PaymentModel> ToApiPayments(this IEnumerable<Domain.Models.Api.v3.PaymentModel> models)
        {
            return models.Select(p => p.ToApiPaymentModel()).AsQueryable();
        }

        public static PaymentModel ToApiPaymentModel(this Domain.Models.Api.v3.PaymentModel model)
        {
            return new PaymentModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tip,
                Tax = model.Tax,
                Status = (PaymentStatusEnum) (int)model.Status,
                Type = (PaymentTypeEnum) (int)model.Type,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,

                Items = model.Items?.ToApiPaymentItems(),
                ReceiptInfo = model.ReceiptInfo?.ToApiPaymentReceptInfo(),
                Transaction = model.Transaction?.ToApiPaymentTransaction(),

            };
        }

        public static IEnumerable<PaymentItemModel> ToApiPaymentItems(this IEnumerable<Domain.Models.Api.v3.PaymentItemModel> models)
        {
            return models.Select(i => new PaymentItemModel
            {
                Id = i.Id,
                ItemId = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            });
        }

        public static PaymentReceptInfo ToApiPaymentReceptInfo(this Domain.Models.Api.v3.PaymentReceptInfoModel model)
        {
            return new PaymentReceptInfo
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }

        public static PaymentTransaction ToApiPaymentTransaction(this Domain.Models.Api.v3.PaymentTransactionModel model)
        {
            return new PaymentTransaction
            {
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = model.CardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType,
                Issued = model.Issued,//DateTime.SpecifyKind(model.Issued, DateTimeKind.Utc),
            };
        }





        public static Domain.Models.Api.v3.PaymentModel ToDomainPaymentModel(this PaymentModel model)
        {
            return new Domain.Models.Api.v3.PaymentModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tip,
                Tax = model.Tax,
                Status = (Domain.Enums.PaymentStatusEnum) (int)model.Status,
                Type = (Domain.Enums.PaymentTypeEnum) (int)model.Type,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,

                Items = model.Items?.ToDomainPaymentItems(),
                ReceiptInfo = model.ReceiptInfo?.ToDomainPaymentReceptInfo(),
                Transaction = model.Transaction?.ToDomainPaymentTransaction(),

            };
        }

        public static List<Domain.Models.Api.v3.PaymentItemModel> ToDomainPaymentItems(this IEnumerable<PaymentItemModel> models)
        {
            return models.Select(i => new Domain.Models.Api.v3.PaymentItemModel
            {
                Id = i.Id,
                ItemId = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Domain.Models.Api.v3.PaymentReceptInfoModel ToDomainPaymentReceptInfo(this PaymentReceptInfo model)
        {
            return new Domain.Models.Api.v3.PaymentReceptInfoModel
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }

        public static Domain.Models.Api.v3.PaymentTransactionModel ToDomainPaymentTransaction(this PaymentTransaction model)
        {


            return new Domain.Models.Api.v3.PaymentTransactionModel
            {
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = GetCardType(model.CardType), // model.CardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType,
                Issued = model.Issued,
            };
        }

        private static string GetCardType(string cloverCardType)
        {
            cloverCardType = cloverCardType.ToUpper();
            switch (cloverCardType)
            {
                case "VISA":
                    return "VISA";
                case "MC":
                    return "MasterCard";
                case "AMEX":
                    return "AMEX";
                case "DISCOVER":
                    return "Discover";
                default:
                    return "Other";
            }
        }
    }
}