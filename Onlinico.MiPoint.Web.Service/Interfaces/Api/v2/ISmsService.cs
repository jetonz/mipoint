﻿using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v2
{
    public interface ISmsService
    {
        Task<ResultModel> SendAsync(string phoneNumber, string message);
    }
}
