﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Data.DataModel;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.Agent.v1
{
    public static class SessionLogMapper
    {
        public static List<Log> ToLogEntities(this List<DeviceLogDto> models, string locationId)
        {
            return models.Select(m => m.ToLogEntity(locationId)).ToList();
        }

        public static Log ToLogEntity(this DeviceLogDto model, string locationId)
        {
            return new Log
            {
                LocationId = locationId,
                Id = Guid.NewGuid(),
                Model = model.Model,
                AppVersion = model.AppVersion,
                Serial = model.Serial,
                Manufacturer = model.Manufacturer,
                Fingerprint = model.Fingerprint,
                Created = model.Created,
                Content = model.Content,
            };
        }
    }
}
