﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1.Enum;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class PingModel
    {
        public string Fingerprint { get; set; }
        
        public string AgentVersion { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AgentStatusEnum Status { get; set; }
    }

    public class PongModel
    {
        public string AvailableVersion { get; set; }
        
        public string UpdateUrl { get; set; }
    }
}
