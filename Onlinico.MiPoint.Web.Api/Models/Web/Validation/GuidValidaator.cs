﻿using System;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Validation
{
    public static class GuidValidaator
    {
        public static bool IsEmpty(this Guid val)
        {
            return val == Guid.Empty;
        }
    }
}