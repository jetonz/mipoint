﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper
{
    public static class ClientSettingsMapper
    {
        public static ClientSettingsApiModel ToClientSettingsModel(this ClientSettingsModel settings)
        {
            return new ClientSettingsApiModel
            {
                ServerId = settings.ServerId,
                MerchantInfo = settings.MerchantInfo?.ToMerchantInfoApiModel(),
                TableModeSettings = settings.TableModeSettings?.ToTableModeSettingsApiModel(),
                CounterModeSettings = settings.CounterModeSettings?.ToCounterModeSettingsApiModel(),
            };
        }

        public static MerchantInfoApiModel ToMerchantInfoApiModel(this MerchantInfoModel merchant)
        {
            return new MerchantInfoApiModel
            {
                Address = merchant.Address,
                PhoneNumber = merchant.PhoneNumber,
                Website = merchant.Website,
                LogoUrl = merchant.LogoUrl,
                MerchantName = merchant.MerchantName,
            };
        }

        public static TableModeSettingsApiModel ToTableModeSettingsApiModel(this TableModeSettingsModel settings)
        {
            return new TableModeSettingsApiModel
            {
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                AutoPrintReceipt = settings.AutoPrintReceipt,
                DisplayLineItemsOnReceipt = settings.DisplayLineItemsOnReceipt,
                OnlyMyOrdersFilterEnabled = settings.OnlyMyOrdersFilterEnabled,
                PrintReceiptEnabled = settings.PrintReceiptEnabled,
                RequireSignature = settings.RequireSignature,
                SendReceiptEmailEnabled = settings.SendReceiptEmailEnabled,
                SendReceiptMessageEnabled = settings.SendReceiptMessageEnabled,
                SessionTimeOut = settings.SessionTimeOut,
                ShowReceiptScreen = settings.ShowReceiptScreen,
                ShowTipsScreen = settings.ShowTipsScreen,
                SessionTimeOutEnabled = settings.SessionTimeOutEnabled,
                ShowSignatureLineOnReceipt = settings.ShowSignatureLineOnReceipt,
                ShowTipLineOnReceipt = settings.ShowTipLineOnReceipt,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,

                TipSuggestions = settings.TipSuggestions,
            };
        }

        public static CounterModeSettingsApiModel ToCounterModeSettingsApiModel(this CounterModeSettingsModel settings)
        {
            return new CounterModeSettingsApiModel
            {
                AutoPrintReceipt = settings.AutoPrintReceipt,
                DisplayLineItemsOnReceipt = settings.DisplayLineItemsOnReceipt,
                PrintReceiptEnabled = settings.PrintReceiptEnabled,
                RequireSignature = settings.RequireSignature,
                SendReceiptEmailEnabled = settings.SendReceiptEmailEnabled,
                SendReceiptMessageEnabled = settings.SendReceiptMessageEnabled,
                ShowReceiptScreen = settings.ShowReceiptScreen,
                ShowTipsScreen = settings.ShowTipsScreen,
                ShowThanksSreen = settings.ShowThanksSreen,
                ThanksMessage = settings.ThanksMessage,
                WelcomeMessage = settings.WelcomeMessage,
                ShowTipLineOnReceipt = settings.ShowTipLineOnReceipt,
                ShowSignatureLineOnReceipt = settings.ShowSignatureLineOnReceipt,
                TipSuggestions = settings.TipSuggestions,
            };
        }
    }
}