﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class TenderTypeMapper
    {
        public static List<Models.Web.TenderTypeModel> ToViewTenderTypes(this List<Domain.Models.Web.TenderTypeModel> models, List<AvailableTenderTypeModel> avalibleTenderTypes)
        {
            var result = models.Select(t => new Models.Web.TenderTypeModel
            {
                Id = t.Id,
                Type = t.Type,
                Name = t.Name,
                Card = t.Card.Name,
                AvailableValues = avalibleTenderTypes?.Select(a => new SelectListItem
                {
                    Value = a.Type,
                    Text = a.Type + '-' + a.Name,
                    Selected = t.Type == a.Type
                }).ToList()
            });

            return result.ToList();
        }

        public static List<Domain.Models.Web.TenderTypeModel> ToDomainTenderTypes(this List<Models.Web.TenderTypeModel> tenderTypes)
        {
            return tenderTypes.Select(t => t.ToDomainTenderType()).ToList();
        }

        public static Domain.Models.Web.TenderTypeModel ToDomainTenderType(this Models.Web.TenderTypeModel tenderType)
        {
            var name = string.Empty;
            var type = string.Empty;
            var tenderTypeInfo = tenderType.Name.Split('-');

            if (tenderTypeInfo.Count() == 2)
            {
                type = tenderTypeInfo[0];
                name = tenderTypeInfo[1];
            }

            return new Domain.Models.Web.TenderTypeModel
            {
                Id = tenderType.Id,
                Name = name,
                Type = type
            };
        }

        public static List<AvailableTenderTypeModel> ToDomainAvailableTenderTypes(this List<Domain.Models.Global.Omnivore.TenderTypeModel> models)
        {
            return models.Select(m => m.ToDomainAvailableTenderTypeModel()).ToList();
        }

        public static AvailableTenderTypeModel ToDomainAvailableTenderTypeModel(this Domain.Models.Global.Omnivore.TenderTypeModel model)
        {
            return new AvailableTenderTypeModel
            {
                Name = model.Name,
                Type = model.Type,
            };
        }
    }
}