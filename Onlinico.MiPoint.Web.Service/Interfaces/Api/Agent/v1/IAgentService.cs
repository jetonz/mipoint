﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1
{
    public interface IAgentService
    {
        Task<ResultModel> RegisterConnectorAsync(RegisterModel info);

        Task<ResultModel> AddSessionLogsAsync(string locationId, List<DeviceLogDto> models);

        Task<ResultModel> AddActivitiesAsync(string locationId, List<RequestLogDto> models);

        Task<ResultModel> AddOrUpdatePaymentsAsync(string locationId, List<PaymentDto> models);

        Task<ResultModel<PaymentDto>> GetPaymentAsync(string paymentId);

        Task<ResultModel> UpdateTenderTypes(string locationId, List<TenderTypeDto> model);
    }
}
