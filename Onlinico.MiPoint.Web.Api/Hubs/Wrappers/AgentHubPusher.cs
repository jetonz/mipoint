﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Onlinico.MiPoint.Web.Api.Hubs.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;

namespace Onlinico.MiPoint.Web.Api.Hubs.Wrappers
{
    public class AgentHubPusher : BaseHubPusher, IAgentHubPusher
    {
        private readonly IHubContext _agentHubContext;
        private readonly ILocationService _locationService;

        public AgentHubPusher(ILocationService locationService)
        {
            _agentHubContext = GlobalHost.ConnectionManager.GetHubContext<AgentHub>();
            _locationService = locationService;
        }

        public void Restart(string locationId)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentHubContext.Clients.User(result.ObjectId).restart();
        }

        public void Stop(string locationId)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentHubContext.Clients.User(result.ObjectId).stop();
        }

        public void SendAgentSettings(string locationId)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
            {
                var settingsResult = _locationService.GetAgetnSettingsAsync(locationId).Result;
                if (settingsResult.Succeed && settingsResult.Object != null)
                {
                    _agentHubContext.Clients.User(result.ObjectId).receiveAgentSettings(settingsResult.Object);
                }
            }
        }

        public void SendClientSettings(string locationId)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
            {
                var settingsResult = _locationService.GetClientSettingsAsync(locationId).Result;
                if (settingsResult.Succeed && settingsResult.Object != null)
                {
                    _agentHubContext.Clients.User(result.ObjectId).receiveClientSettings(settingsResult.Object);
                }
            }
        }

        public void SendClientSettingsByMerchant(Guid merchantId)
        {
            var locationsResult = _locationService.GetLocations(merchantId);

            if (locationsResult.Succeed)
            {
                locationsResult.Object?.ForEach(l =>
                {
                    var settingsResult = _locationService.GetClientSettingsAsync(l.Id).Result;
                    if (settingsResult.Succeed && settingsResult.Object != null)
                    {
                        _agentHubContext.Clients.User(l.AgentFingerprint).receiveClientSettings(settingsResult.Object);
                    }
                });
            }
        }

        public void SendUpdateRequest(string locationId, string verion, string updateUrl)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentHubContext.Clients.User(result.ObjectId).update(verion, updateUrl);
        }

        public void SendAgentLogRequest(string locationId, DateTime dateTime)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentHubContext.Clients.User(result.ObjectId).getAgentLog(dateTime);
        }

        public void SenClientLogRequest(string locationId, DateTime dateTime)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentHubContext.Clients.User(result.ObjectId).getClientLog(dateTime);
        }
    }
}