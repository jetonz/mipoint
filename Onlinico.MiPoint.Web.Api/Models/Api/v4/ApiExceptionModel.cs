﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4
{
    public class ApiExceptionModel
    {
        public string ErrorMessage { get; set; }

        public string InternalErrorMessage { get; set; }
    }
}