﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1
{
    public class RegisterModel
    {
        [JsonProperty("apiKey")]
        [Required, MaxLength(36)]
        public string ApiKey { get; set; }

        
        [JsonProperty("fingerprint")]
        [Required, MaxLength(128)]
        public string Fingerprint { get; set; }

        [JsonProperty("agentVersion")]
        [Required, MaxLength(128)]
        public string AgentVersion { get; set; }
    }
}