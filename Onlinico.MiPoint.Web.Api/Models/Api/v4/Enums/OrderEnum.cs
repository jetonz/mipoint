﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums
{
    public enum OrderStatusEnum
    {
        Open = 0,
        PartiallyPaid = 1,
        Closed = 2,
    }

    public enum OrderPaymentStatusEnum
    {
        Processed = 1,
        Pending = 0,
        
    }
}