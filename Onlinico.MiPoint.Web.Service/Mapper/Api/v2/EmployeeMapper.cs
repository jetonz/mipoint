﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v2
{
    public static class EmployeeMapper
    {
        public static Domain.Models.Api.v2.EmployeeModel ToDomainEmployeeModel(this Data.DataModel.Employee employee)
        {
            return new Domain.Models.Api.v2.EmployeeModel
            {
                Id = employee.Id,
                EmployeeId = employee.EmployeeId,
                Login = employee.Login,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                CheckName = employee.CheckName
            };
        }
    }
}
