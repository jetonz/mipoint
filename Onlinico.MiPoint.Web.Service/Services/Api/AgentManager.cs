﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Core.Interfaces;

namespace Onlinico.MiPoint.Web.Service.Services.Api
{
    public class AgentManager : IAgentManager
    {
        private const string Vicar = "Vicar-";
        private ConcurrentDictionary<string, string> Connections { get; set; } = new ConcurrentDictionary<string, string>();

        public void Connected(string apiKey, string fingerptint)
        {
            Connections.AddOrUpdate(apiKey, fingerptint, (k, v) => v = fingerptint);
        }

        public void Reconnected(string apiKey, string fingerptint)
        {
            Connections.AddOrUpdate(apiKey, fingerptint, (k, v) => v = fingerptint);
        }

        public void Disconnected(string apiKey, string fingerptint)
        {
            if (Connections.ContainsKey(apiKey))
            {
                string outValue;
                Connections.TryRemove(apiKey, out outValue);
            }
        }

        public void VicarConnected(string apiKey, string fingerptint)
        {
            Connections.AddOrUpdate(Vicar + apiKey, fingerptint, (k, v) => v = fingerptint);
        }

        public void VicarReconnected(string apiKey, string fingerptint)
        {
            Connections.AddOrUpdate(Vicar + apiKey, fingerptint, (k, v) => v = fingerptint);
        }

        public void VicarDisconnected(string apiKey, string fingerptint)
        {
            if (Connections.ContainsKey(Vicar + apiKey))
            {
                string outValue;
                Connections.TryRemove(Vicar + apiKey, out outValue);
            }
        }

        public bool IsConnected(string apikey, string fingerprint)
        {
            if (Connections.ContainsKey(apikey))
            {
                string outValue;
                Connections.TryGetValue(apikey, out outValue);

                return outValue == fingerprint;
            }

            return false;
        }

        public bool IsVicarConnected(string apikey, string fingerprint)
        {
            if (Connections.ContainsKey(Vicar + apikey))
            {
                string outValue;
                Connections.TryGetValue(Vicar + apikey, out outValue);

                return outValue == fingerprint;
            }

            return false;
        }
    }
}
