﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiPoint.Shared.Types.Enums;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Data.DataModel;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.Agent.v1
{
    public static class PaymentMapper
    {
        public static PaymentDto ToPaymentModel(this Payment model)
        {
            return new PaymentDto
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tips = model.Tip,
                Tax = model.Tax,
                Status = (PaymentStatusEnum)(int)model.Status,
                PaymentType = (PaymentTypeEnum)(int)model.Type,
                EmployeeId = model.EmployeeCode,

                Items = model.Items?.ToDomainPaymentItems(),
                Receipt = model.ReceiptInfo?.ToDomainPaymentReceptInfo(),
                Transaction = model.Transaction?.ToPaymentTransactionModel(),
            };
        }

        public static TransactionDto ToPaymentTransactionModel(this PaymentTransaction model)
        {
            TransactionTypeEnum transactionType;
            Enum.TryParse(model.TransactionType, out transactionType);

            CardTypeEnum cardType;
            Enum.TryParse(model.CardType, out cardType);

            CardEntryTypeEnum cardEntryType;
            Enum.TryParse(model.CardEntryType, out cardEntryType);

            return new TransactionDto
            {
                TransactionId = model.TransactionId,
                TransactionType = transactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = cardType,
                ReferenceId = model.ReferenceId,
                LastFourDigits = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = cardEntryType,
                Issued = model.Issued.DateTime,
            };
        }

        public static ReceiptModelDto ToDomainPaymentReceptInfo(this PaymentReceiptInfo model)
        {
            return new ReceiptModelDto
            {
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued.DateTime,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OtherCharges = model.OrderServiceCharge,
                SubTotal = model.OrderSubTotal,
                Tax = model.OrderTax,
                Total = model.OrderTotal,
                Headers = new List<string> { model.ReceiptHeader1, model.ReceiptHeader2 },
                WebSite = model.Website,
            };
        }

        public static IEnumerable<PaymentItemDto> ToDomainPaymentItems(this IEnumerable<PaymentItem> items)
        {
            return items.Select(i => new PaymentItemDto
            {
                Id = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            });
        }


        public static List<Payment> ToPayments(this List<PaymentDto> models, Location location)
        {
            return models.Select(m => m.ToPaymentEntity(location)).ToList();
        }

        public static Payment ToPaymentEntity(this PaymentDto model, Location location)
        {
            return new Payment
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tips,
                Tax = model.Tax,
                EmployeeCode = model.EmployeeId,
                TableNumber = model.Receipt.TableName,
                Status = (Data.DataModel.Enums.PaymentStatusEnum)(int)model.Status,
                Type = (Data.DataModel.Enums.PaymentTypeEnum)((int)model.PaymentType),
                Message = model.FailReason ?? "Agent paid",

                Created = DateTime.UtcNow,// model.Created,
                Updated = DateTime.UtcNow,

                Items = model.Items?.ToPaymentItems(),
                ReceiptInfo = model.Receipt?.ToPaymentReceiptInfo(),
                Location = location,
                Transaction = model.Transaction?.ToTransaction(),
            };
        }

        public static PaymentReceiptInfo ToPaymentReceiptInfo(this ReceiptModelDto model)
        {
            return new PaymentReceiptInfo
            {
                Id = Guid.NewGuid(),
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OtherCharges,
                OrderSubTotal = model.SubTotal,
                OrderTax = model.Tax,
                OrderTotal = model.Total,
                ReceiptHeader1 = model.Headers.FirstOrDefault(),
                ReceiptHeader2 = model.Headers.LastOrDefault(),
                Website = model.WebSite,
            };
        }

        public static PaymentTransaction ToTransaction(this TransactionDto model)
        {
            return new PaymentTransaction
            {
                Id = Guid.NewGuid(),
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType.ToString(),
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = model.CardType.ToString(),
                ReferenceId = model.ReferenceId,
                Last4 = model.LastFourDigits,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType.ToString(),
                Issued = model.Issued,
            };
        }

        public static List<PaymentItem> ToPaymentItems(this IEnumerable<PaymentItemDto> items)
        {
            return items.Select(i => new PaymentItem
            {
                Id = Guid.NewGuid(),
                ItemId = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = unchecked((int)i.Quantity),
            }).ToList();
        }

        public static void Update(this Payment entity, PaymentDto model)
        {
            entity.Status = (Data.DataModel.Enums.PaymentStatusEnum)(int)model.Status;

            if (!string.IsNullOrEmpty(model.FailReason))
                entity.Message = model.FailReason;

            if (entity.Transaction == null && model.Transaction != null)
                entity.Transaction = model.Transaction.ToTransaction();
        }
    }
}
