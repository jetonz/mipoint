﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class ReceiptService : BaseService, IReceiptService
    {
        public ReceiptService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel<ReceiptModel>> GetAsync(string receiptId)
        {
            var result = new ResultModel<ReceiptModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var receipt = await context.Receipts
                                .Include(r => r.Items)
                                .Include(r => r.Payment)
                                .FirstOrDefaultAsync(r => r.Id == receiptId);
                    if (receipt != null)
                    {
                        result.Object = receipt.ToDomainReceiptModel();
                    }
                    else
                    {
                        result = new ResultModel<ReceiptModel>(false, "Invalid receiptId.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<ReceiptModel>> GetFromPaymentAsync(string paymentId)
        {
            var result = new ResultModel<ReceiptModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var payment = await context.Payments
                                .Include(r => r.Items)
                                .Include(r => r.ReceiptInfo)
                                .Include(p => p.Transaction)
                                .FirstOrDefaultAsync(r => r.Id == paymentId && r.Transaction != null);
                    if (payment != null)
                    {
                        result.Object = payment.ToDomainReceiptModel();
                    }
                    else
                    {
                        result = new ResultModel<ReceiptModel>(false, "Invalid payment id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
