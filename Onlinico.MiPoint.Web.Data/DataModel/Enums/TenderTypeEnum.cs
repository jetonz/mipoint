﻿namespace Onlinico.MiPoint.Web.Data.DataModel.Enums
{
    public enum TenderTypeEnum
    {
        //NotSet = 0,

        Cash = 101,
        Checks = 102,
        GcRedeem = 103,
        Coupon = 104,
        OnlineOrder = 105,
        WingCash = 106,

        Visa = 201,
        Mastercard = 202,
        Discover = 203,
        Amex = 204,
        
        IssuePoints = 302,
        GcardRedeem = 303,
        PpGiftRdm = 305,
    }
}
