﻿using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Onlinico.MiPoint.Web.Api.Models.Api.v3;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v3;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v3
{
    /// <summary>
    /// Application settings controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v3/locations")]
    public class LocationController : BaseController
    {
        private readonly ILocationService _locationService;

        private readonly IConfiguration _configuration;

        /// <summary>
        /// Location controller constructor
        /// </summary>
        public LocationController(Logger.Interfaces.ILogger logger, ILocationService locationService, IConfiguration configuration) : base(logger)
        {
            _locationService = locationService;
            _configuration = configuration;
        }

        
        /// <summary>
        /// Get current application settings
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/settings")]
        public LocationSettingsModel GetSettings()
        {
            var locationSettingsResult = _locationService.GetSettings(Request.Properties[_configuration.Secret].ToString());

            if (locationSettingsResult.Succeed && locationSettingsResult.Object != null)
            {
                return locationSettingsResult.Object.ToApiLocationSettingsModel();
            }
            else
            {
                var responce = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(locationSettingsResult.Error)
                };
                throw new HttpResponseException(responce);
            }
        }

        /// <summary>
        /// Get employee by employee login
        /// </summary>
        /// <param name="login">employee login</param>
        /// <returns></returns>
        [HttpGet, Route("current/employees/{login}")]
        [ResponseType(typeof(EmployeeModel))]
        public async Task<IHttpActionResult> GetEmployee(string login)
        {
            var locationId = Request.Properties[_configuration.Secret].ToString();

            var result = await _locationService.GetEmployeeAsync(locationId, login);

            return result.Succeed ? Ok(result.Object?.ToEmployeeModel()) : (IHttpActionResult)BadRequest(result.Error);
        }

        /// <summary>
        /// Agent Heart Beat
        /// </summary>
        /// <param name="lub"></param>
        /// <returns></returns>
        [HttpPost, Route("current/heartbeat")]
        public async Task<DubModel> HeartBeatAsync(LubModel lub)
        {
            var result = new Domain.Models.Api.v3.ResultModel(true);

            var clientIp = GetClientIp();

            if (!string.IsNullOrEmpty(clientIp))
            {
                return new DubModel() {UpdateUrl = clientIp};
            }
            else
            {
                var responce = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Can't get client ip address.")
                };
                throw new HttpResponseException(responce);
            }
        }

        private string GetClientIp(HttpRequestMessage request = null)
        {
            request = request ?? Request;

            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                RemoteEndpointMessageProperty prop = (RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name];
                return prop.Address;
            }
            else if (HttpContext.Current != null)
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                return null;
            }
        }

        protected override void Dispose(bool disposing)
        {
            _locationService.Dispose();
            base.Dispose(disposing);
        }
    }
}
