namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpandMerchantandHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Histories", "RequestHeaders", c => c.String());
            AddColumn("dbo.Histories", "RequestContent", c => c.String());
            AddColumn("dbo.Histories", "ResponseHeaders", c => c.String());
            AddColumn("dbo.Histories", "ResponseContent", c => c.String());
            AddColumn("dbo.Merchants", "WebSite", c => c.String());
            AddColumn("dbo.Merchants", "LogoUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Merchants", "LogoUrl");
            DropColumn("dbo.Merchants", "WebSite");
            DropColumn("dbo.Histories", "ResponseContent");
            DropColumn("dbo.Histories", "ResponseHeaders");
            DropColumn("dbo.Histories", "RequestContent");
            DropColumn("dbo.Histories", "RequestHeaders");
        }
    }
}
