﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Mapper.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Mapper.Exceptions;
using MiPoint.Shared.Types.Dto;

namespace Onlinico.MiPoint.Web.Service.Services.Api.Agent.v1
{
    public class AgentService : BaseService, IAgentService
    {
        public AgentService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel> RegisterConnectorAsync(RegisterModel info)
        {

            var result = new ResultModel(true)
            {
                Succeed = true
            };

            return await Task.FromResult(result);
        }

        public async Task<ResultModel> AddSessionLogsAsync(string locationId, List<DeviceLogDto> models)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    context.Configuration.AutoDetectChangesEnabled = false;

                    context.Logs.AddRange(models.ToLogEntities(locationId));

                    context.Configuration.AutoDetectChangesEnabled = true;

                    await context.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. InnerException: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> AddActivitiesAsync(string locationId, List<RequestLogDto> models)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == locationId);
                    if (location != null)
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;

                        context.Histories.AddRange(models.ToHistories(location));

                        context.Configuration.AutoDetectChangesEnabled = true;

                        await context.SaveChangesAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid location id.";
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. InnerException: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public async Task<ResultModel> AddOrUpdatePaymentsAsync(string locationId, List<PaymentDto> models)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == locationId);
                    if (location != null)
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;

                        var paymentsId = models.Select(p => p.Id).ToList();
                        var existPayments = context.Payments
                            .Include(p => p.Transaction)
                            .Where(p => paymentsId.Contains(p.Id)).ToList();
                        existPayments.ForEach(e =>
                        {
                            e.Update(models.First(p => p.Id == e.Id));
                            models.Remove(models.First(p => p.Id == e.Id));
                        });
                        context.Payments.AddRange(models.ToPayments(location));

                        context.Configuration.AutoDetectChangesEnabled = true;

                        await context.SaveChangesAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid location id.";
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. InnerException: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<PaymentDto>> GetPaymentAsync(string paymentId)
        {
            var result = new ResultModel<PaymentDto>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var payment = await context.Payments
                        .Include(p => p.Transaction)
                        .Include(p => p.ReceiptInfo)
                        .Include(p => p.Items)
                        .FirstOrDefaultAsync(p => p.Id == paymentId).ConfigureAwait(false);

                    result.Object = payment?.ToPaymentModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. InnerException: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> UpdateTenderTypes(string locationId, List<TenderTypeDto> model)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var locationSettings = context.LocationSettings.FirstOrDefault(s => s.Location.Id == locationId && !s.Location.IsRemoved);
                    if (locationSettings != null && model != null)
                    {
                        if (locationSettings.AvailableTenderTypes != null)
                        {
                            context.AvailableTenderTypes.RemoveRange(locationSettings.AvailableTenderTypes);
                        }

                        var avalibleTenderTypes = model.ToList().ToNewAvailableTenderTypes(locationSettings);

                        context.AvailableTenderTypes.AddRange(avalibleTenderTypes);

                        if (avalibleTenderTypes != null && model.Any(t => t.IsDefault))
                        {
                            var defaltTenderTypeId = model.First(t => t.IsDefault).Id;
                            var defaultTenderType = avalibleTenderTypes.FirstOrDefault(t => t.Type == defaltTenderTypeId);

                            if (defaultTenderType != null)
                            {
                                context.TenderTypes.RemoveRange(locationSettings.TenderTypes);

                                var cards = context.Cards.ToList();

                                var tenderTypes = cards.Select(c => new TenderType
                                {
                                    Id = Guid.NewGuid(),
                                    Card = c,
                                    Name = defaultTenderType.Name,
                                    Type = defaultTenderType.Type,
                                    Settings = locationSettings
                                });

                                context.TenderTypes.AddRange(tenderTypes);
                            }
                        }

                        await context.SaveChangesAsync();
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
