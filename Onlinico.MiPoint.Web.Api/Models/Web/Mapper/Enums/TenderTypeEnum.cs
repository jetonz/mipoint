﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Mapper.Enums
{
    public static class TenderTypeEnum
    {
        public static Dictionary<int, string> ToDictionary(this Domain.Enums.TenderTypeEnum @enum)
        {
            var type = @enum.GetType();
            return Enum.GetValues(type).Cast<int>().ToDictionary(e => e, e => Enum.GetName(type, e));
        }
    }
}