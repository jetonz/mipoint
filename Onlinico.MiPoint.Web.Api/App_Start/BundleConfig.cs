﻿using System.Web;
using System.Web.Optimization;

namespace Onlinico.MiPoint.Web.Api
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        //"~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-1.10.2.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js",
                        "~/Scripts/jquery.signalR-2.2.1.min.js",
                        "~/Scripts/hubs.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/moment-with-locales.min.js",

                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap/bootstrap-multiselect.js",
                      "~/Scripts/bootstrap/bootstrap-notify.min.js",
                      "~/Scripts/bootstrap/bootstrap-datetimepicker.min.js",

                      "~/Scripts/respond.js",
                      "~/Scripts/DataTable/jquery.dataTables.min.js",
                      "~/Scripts/DataTable/dataTables.bootstrap.min.js",
                      "~/Scripts/DataTable/dataTable.row().show().js"
                      ));

            bundles.Add(new StyleBundle("~/Content/basecss").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/bootstrap-multiselect.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/dataTables.bootstrap.min.css",
                      "~/Content/site.css"));
        }
    }
}
