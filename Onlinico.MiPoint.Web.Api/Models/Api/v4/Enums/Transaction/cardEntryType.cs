﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums.Transaction
{
    public enum CardEntryTypeEnum
    {
        SWIPED = 0,
        KEYED = 1,
        VOICE = 2,
        VAULTED = 3,
        OFFLINE_SWIPED = 4,
        OFFLINE_KEYED = 5,
        EMV_CONTACT = 6,
        EMV_CONTACTLESS = 7,
        MSD_CONTACTLESS = 8,
        PINPAD_MANUAL_ENTRY = 9
    }
}