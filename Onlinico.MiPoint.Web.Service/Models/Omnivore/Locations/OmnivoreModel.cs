﻿using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Service.Models.Omnivore.Locations
{

    public class ClockEntries
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Discounts
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Employees
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Jobs
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Menu
    {
        public string href { get; set; }
    }

    public class OrderTypes
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class PriceCheck
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class RevenueCenters
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Tables
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class TenderTypes
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Tickets
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links
    {
        //public ClockEntries clock_entries { get; set; }
        //public Discounts discounts { get; set; }
        public Employees employees { get; set; }
        //public Jobs jobs { get; set; }
        //public Menu menu { get; set; }
        //public OrderTypes order_types { get; set; }
        //public PriceCheck price_check { get; set; }
        //public RevenueCenters revenue_centers { get; set; }
        //public Self self { get; set; }
        //public Tables tables { get; set; }
        //public TenderTypes tender_types { get; set; }
        //public Tickets tickets { get; set; }
    }

    public class Address
    {
        public string city { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string street1 { get; set; }
        public string street2 { get; set; }
        public string zip { get; set; }
    }

    public class Agent
    {
        public object average_cpu { get; set; }
        public object average_memory { get; set; }
        public bool? healthy { get; set; }
        public object processes { get; set; }
    }

    public class System
    {
        public object average_cpu { get; set; }
        public object average_memory { get; set; }
        public bool? healthy { get; set; }
    }

    public class Tickets2
    {
        public int? response_time { get; set; }
        public string status { get; set; }
    }

    public class Health
    {
        public Agent agent { get; set; }
        public bool? healthy { get; set; }
        public System system { get; set; }
        public Tickets2 tickets { get; set; }
    }

    public class Location
    {
        public Links _links { get; set; }
        public Address address { get; set; }
        public string concept_name { get; set; }
        public int? created { get; set; }
        public bool? development { get; set; }
        public string display_name { get; set; }
        //public Health health { get; set; }
        public string id { get; set; }
        public int? modified { get; set; }
        public string name { get; set; }
        public string owner { get; set; }
        public string phone { get; set; }
        public string pos_type { get; set; }
        public string status { get; set; }
        public object website { get; set; }
    }

    public class Embedded
    {
        public List<Location> locations { get; set; }
    }

    public class Self2
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
    }

    public class RootObject
    {
        public Embedded _embedded { get; set; }
        //public Links2 _links { get; set; }
        public int? count { get; set; }
        //public int limit { get; set; }
    }
}
