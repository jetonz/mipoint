﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v1.External
{
    public interface IOmnivoreService
    {
        Task<List<LocationModel>> GetLocationsByApiKey(string apiKey);

        Task<List<TenderTypeModel>> GetLocationTenderTypes(string apiKey, string locationsId);
    }
}
