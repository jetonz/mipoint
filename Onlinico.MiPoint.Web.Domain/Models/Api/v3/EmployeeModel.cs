﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v3
{
    public class EmployeeModel
    {
        public Guid Id { get; set; }
        
        public string EmployeeId { get; set; }

        public string CheckName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }
    }
}
