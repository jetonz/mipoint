﻿using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper
{
    public static class LocationMapper
    {
        public static LocationModel ToApiLocationModel(this Domain.Models.Api.v3.LocationModel model)
        {
            return new LocationModel
            {
                Id = model.Identifier,
                Name = model.Name,
                Phone = model.Phone,
                Address = model.Address,
                AddressFull = model.AddressFull,
                Owner = model.Owner
            };
        }

        public static LocationSettingsModel ToApiLocationSettingsModel(this Domain.Models.Api.v3.LocationSettingsModel model)
        {
            return new LocationSettingsModel
            {
                OmnivoreId = model.OmnivoreId,
                TaxRate = model.TaxRate,
                Tips = model.Tips,
                TenderTypes = model.TenderTypes.ToDictionary(k=>k.Card.Type, v=>v.Type),
                AfterPaymentLogOut = model.AfterPaymentLogOut,
                SessionTimeout = model.SessionTimeout,
                OpenOrderLimit = model.OpenOrderLimit,
                ClosedOrderLimit = model.ClosedOrderLimit,
            };
        }
    }
}