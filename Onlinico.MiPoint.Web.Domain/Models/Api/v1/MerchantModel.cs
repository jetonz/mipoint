﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v1
{
    public class MerchantModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string OmnivoreApiKey { get; set; }

        public DateTime Created { get; set; }

        public List<LocationModel> Locations { get; set; } 
    }

    public class MerchantItemModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public bool Selected { get; set; }

        public DateTime Created { get; set; }
    }
}
