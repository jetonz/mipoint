﻿using System;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Global
{
    public interface IMemoryCacher
    {
        T GetValue<T>(string key) where T : class;

        bool Add(string key, object value, DateTimeOffset absExpiration);

        object Delete(string key);

        bool ContainsKey(string key);
    }
}
