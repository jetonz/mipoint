﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore
{
    public static class EmployeeMapper
    {
        public static List<Domain.Models.Global.Omnivore.EmployeeModel> ToDomainEmployees(this List<Service.Models.Omnivore.Employees.Employee> models)
        {
            return models.Select(e => new Domain.Models.Global.Omnivore.EmployeeModel
            {
                Id = Guid.NewGuid(),
                Login = e.login,
                CheckName = e.check_name,
                EmployeeId = e.id,
                FirstName = e.first_name,
                LastName = e.last_name,
            }).ToList();
        }

        public static List<Domain.Models.Web.EmployeeModel> ToDomainEmployees(this List<Domain.Models.Global.Omnivore.EmployeeModel> models)
        {
            return models.Select(e => new Domain.Models.Web.EmployeeModel
            {
                Id = e.Id,
                Login = e.Login,
                EmployeeId = e.EmployeeId,
                LocationIdentifier = e.LocationIdentifier,
                FirstName = e.FirstName,
                CheckName = e.CheckName,
                LastName = e.LastName,
            }).ToList();
        }
    }
}
