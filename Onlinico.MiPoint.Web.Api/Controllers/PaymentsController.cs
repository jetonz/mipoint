﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    public class PaymentsController : BaseController
    {
        private readonly IPaymentService _paymentService;

        public PaymentsController(IConfiguration configuration, IPaymentService paymentService) : base(configuration)
        {
            _paymentService = paymentService;
        }

        // GET: Payments
        public ActionResult Index(string locationId)
        {
            ViewBag.LocationId = locationId;
            return View();
        }

        public async Task<ActionResult> PaymentRows(string locationId, DateTime start, DateTime end, int timezoneOffset = 0)
        {
            var inverOffset = timezoneOffset * -1;


            var paymentResult = await _paymentService.GetAsync(locationId, start, end);
            
            return View(paymentResult.Object?.ToPaymentsViewModel());
        }

        public ActionResult PaymentRow(string paymentId)
        {
            return View();
        }

        public async Task<FileStreamResult> ExportPayments(string locationId, DateTime start, DateTime end)
        {
            var paymentsResult = await _paymentService.GetAsync(locationId, start, end);
            var result = WriteCsvToMemory(paymentsResult.Object);
            var memoryStream = new MemoryStream(result);
            return new FileStreamResult(memoryStream, "text/csv") { FileDownloadName = "export.csv" };
        }

        public byte[] WriteCsvToMemory(IEnumerable<Domain.Models.Web.PaymentModel> records)
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.WriteRecords(records);
                streamWriter.Flush();
                return memoryStream.ToArray();
            }
        }
    }
}