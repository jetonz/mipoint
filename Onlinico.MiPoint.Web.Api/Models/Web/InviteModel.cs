﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class InviteModel
    {
        public Guid Id { get; set; }

        [Required]
        public Guid MerchantId { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}