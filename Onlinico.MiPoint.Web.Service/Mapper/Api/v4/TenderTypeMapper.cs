﻿using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class TenderTypeMapper
    {
        public static List<TenderTypeModel> ToDomainTenderTypes(this IEnumerable<TenderType> tenderTypes)
        {
            return tenderTypes.Select(t => new TenderTypeModel
            {
                Id = t.Id,
                Name = t.Name,
                Type = t.Type,
                Card = t.Card.ToDomainCardModel()
            }).ToList();
        }


        public static List<TenderType> ToEntityTenderTypes(this List<TenderTypeModel> tenderTypes)
        {
            return tenderTypes.Select(t => new TenderType
            {
                Id = t.Id,
                Name = t.Name,
                Type = t.Type,
                Card = t.Card.ToEntityCard()
            }).ToList();
        }

        public static void UpdateTenderTypeEntity(this TenderType entity, TenderTypeModel model)
        {
            entity.Name = model.Name;
            entity.Type = model.Type;
        }
    }
}
