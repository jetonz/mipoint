﻿namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class TipSettingsModel
    {
        public int Min { get; set; }

        public int Max { get; set; }

        public int Default { get; set; }
    }
}