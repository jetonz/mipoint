﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v4;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v4;
using Swashbuckle.Swagger.Annotations;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v4
{
    [RoutePrefix("api/v4")]
    [Attributes.Authorize]
    public class EmployeeController : BaseController
    {
        private readonly ILocationService _locationService;

        public EmployeeController(ILogger logger, IConfiguration configuration, ILocationService locationService) : base(logger, configuration)
        {
            _locationService = locationService;
        }

        /// <summary>
        /// Get employee by employee login
        /// </summary>
        /// <param name="login">employee login</param>
        /// <returns></returns>
        [HttpGet]
        [Route("employees/login/{login}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(EmployeeModel))]
        [SwaggerResponse(HttpStatusCode.NoContent)]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
        public async Task<IHttpActionResult> GetEmployee(string login)
        {
            var result = await _locationService.GetEmployeeAsync(LocationId, login);

            if (result.Succeed)
            {
                if (result.Object != null)
                    return Ok(result.Object?.ToEmployeeModel());

                throw CreateApiException(result.Error, HttpStatusCode.NoContent);
            }
            throw CreateApiException(result.Error);
        }

        protected override void Dispose(bool disposing)
        {
            _locationService.Dispose();
            base.Dispose(disposing);
        }
    }
}
