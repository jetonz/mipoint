﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Onlinico.MiPoint.Web.Domain.Models.Global;
using Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore;
using Onlinico.MiPoint.Web.Service.Models.Omnivore.Payment.Result;

namespace Onlinico.MiPoint.Web.Service.Services.Global.External
{
    public class OmnivoreService : BaseService, IOmnivoreService
    {
        private readonly Core.Interfaces.IConfiguration _configuration;
        private readonly ILocationService _locationService;

        public OmnivoreService(ILogger logger, Core.Interfaces.IConfiguration configuration, ILocationService locationService) : base(logger)
        {
            _configuration = configuration;
            _locationService = locationService;
        }

        public async Task<ResultModel<LocationModel>> GetLocationAsync(string apiKey, string identifier,
            bool withEmployees = false)
        {
            var result = new ResultModel<LocationModel>(true);

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });

                    var requestUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{identifier}";
                    var responseMessage = await client.GetAsync(requestUrl).ConfigureAwait(false);

                    var parseLocationResult = await ParseResponceMessage<Models.Omnivore.Locations.Location>(responseMessage);
                    if (parseLocationResult.Succeed)
                    {
                        result.Object = parseLocationResult.Object?.ToDomainLocation();

                        if (result.Object != null && withEmployees)
                        {

                            var employeesUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{identifier}/employees";

                            var employeeRequestResult = await client.GetAsync(employeesUrl).ConfigureAwait(false);

                            var employeeResult = await ParseResponceMessage<Models.Omnivore.Employees.RootObject>(employeeRequestResult);

                            if (employeeResult.Succeed)
                            {
                                var mappingSchemeResult = _locationService.GetMappingSchemeByIdentifier(identifier);
                                if (mappingSchemeResult.Succeed && mappingSchemeResult.Object?.EmployeeScheme != null)
                                {
                                    employeeResult.Object?._embedded?.employees?.ForEach(e => e.ToScheme(mappingSchemeResult.Object.EmployeeScheme));
                                }

                                result.Object.Employees = employeeResult.Object?._embedded?.employees?.ToDomainEmployees();

                            }

                        }
                    }
                    else
                    {
                        result = new ResultModel<LocationModel>(false, parseLocationResult.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<LocationModel>(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel<List<LocationModel>>> GetLocationsByApiKey(string apiKey, bool withEmployees = false)
        {
            var result = new ResultModel<List<LocationModel>>(true);
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });

                    var requestUrl = $"{_configuration.BaseOmnivoreApiUrl}locations";
                    var responseMessage = await client.GetAsync(requestUrl);

                    var parseLocationResult = await ParseResponceMessage<Models.Omnivore.Locations.RootObject>(responseMessage);
                    if (parseLocationResult.Succeed)
                    {
                        result.Object = parseLocationResult.Object?._embedded?.locations?.ToDomainLocations();

                        if (result.Object != null && withEmployees)
                        {
                            var queue = new ConcurrentQueue<Domain.Models.Global.Omnivore.EmployeeModel>();

                            using (var httpClient = new HttpClient())
                            {
                                httpClient.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });

                                Task.WaitAll(result.Object.Select(l =>
                                {
                                    var employeesUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{l.Identifier}/employees";
                                    return httpClient.GetAsync(employeesUrl).ContinueWith(response =>
                                    {
                                        var responceResult = ParseResponceMessage<Models.Omnivore.Employees.RootObject>(response.Result).Result;

                                        if (responceResult.Succeed)
                                        {
                                            var mappingSchemeResult = _locationService.GetMappingSchemeByIdentifier(l.Identifier);

                                            if (mappingSchemeResult.Succeed && mappingSchemeResult.Object?.EmployeeScheme != null)
                                            {
                                                responceResult.Object?._embedded?.employees?.ForEach(e => e.ToScheme(mappingSchemeResult.Object.EmployeeScheme));
                                            }

                                            responceResult.Object?._embedded?.employees?.ToDomainEmployees().ForEach(
                                                e =>
                                                {
                                                    e.LocationIdentifier = l.Identifier;
                                                    queue.Enqueue(e);
                                                });
                                        }

                                    });
                                }).ToArray());
                            }

                            foreach (var employee in queue)
                            {
                                result.Object?.First(l => l.Identifier == employee.LocationIdentifier).Employees.Add(employee);
                            }
                        }
                    }
                    else
                    {
                        result = new ResultModel<List<LocationModel>>(false, parseLocationResult.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<List<LocationModel>>(false, ex.Message);
            }
            return result;
        }

        public async Task<ResultModel<List<EmployeeModel>>> GetEmployees(string apiKey, string omnivoreLocationId)
        {
            var result = new ResultModel<List<EmployeeModel>>(true);

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });


                    var employeesUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{omnivoreLocationId}/employees";
                    var requestResult = await httpClient.GetAsync(employeesUrl);

                    var responseResult = await ParseResponceMessage<Models.Omnivore.Employees.RootObject>(requestResult);
                    if (responseResult.Succeed)
                    {
                        var mappingSchemeResult = _locationService.GetMappingSchemeByIdentifier(omnivoreLocationId);

                        if (mappingSchemeResult.Succeed && mappingSchemeResult.Object?.EmployeeScheme != null)
                        {
                            responseResult.Object?._embedded?.employees?.ForEach(e => e.ToScheme(mappingSchemeResult.Object.EmployeeScheme));
                        }

                        result.Object = responseResult.Object?._embedded?.employees?.ToDomainEmployees();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<List<EmployeeModel>>(false, ex.Message);
            }
            return result;
        }

        private async Task<ResultModel<T>> ParseResponceMessage<T>(HttpResponseMessage responseMessage) where T : class
        {
            var result = new ResultModel<T>(true);
            switch (responseMessage.StatusCode)
            {
                case HttpStatusCode.OK:
                    var jsonContent = await responseMessage.Content.ReadAsStringAsync();

                    result.Object = JsonConvert.DeserializeObject<T>(jsonContent);
                    break;
                default:
                    result = new ResultModel<T>(false, responseMessage.StatusCode.ToString());
                    break;
            }
            return result;
        }

        public async Task<List<TenderTypeModel>> GetLocationTenderTypes(string apiKey, string locationsId)
        {
            var result = new List<TenderTypeModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });


                    var requestUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{locationsId}/tender_types";
                    var requestResult = await client.GetAsync(requestUrl);


                    switch (requestResult.StatusCode)
                    {
                        case HttpStatusCode.Created:
                        case HttpStatusCode.OK:
                            var jsonContent = await requestResult.Content.ReadAsStringAsync();
                            var omnivoreLocationTenderTypes = JsonConvert.DeserializeObject<Models.Omnivore.TenderType.TenderTipes>(jsonContent);
                            result = omnivoreLocationTenderTypes._embedded.tender_types.Select(t => new TenderTypeModel
                            {
                                Type = t.id,
                                Name = t.name
                            }).ToList();

                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = null;
            }
            return result;
        }

        public async Task<ResultModel<Models.Omnivore.Ticket.Ticket.RootObject>> GetTicketAsync(string apiKey, string locationId, string ticketId)
        {
            var result = new ResultModel<Models.Omnivore.Ticket.Ticket.RootObject>(true);

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });
                    var requestUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{locationId}/tickets/{ticketId}";
                    var responseMessage = await client.GetAsync(requestUrl);

                    var parseTicketResult = await ParseResponceMessage<Models.Omnivore.Ticket.Ticket.RootObject>(responseMessage);
                    if (parseTicketResult.Succeed)
                    {
                        result.Object = parseTicketResult.Object;
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = parseTicketResult.Error;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<Models.Omnivore.Ticket.Tickets.RootObject>> GetTicketsAsync(string apiKey, string locationId, bool isOpen, int limit)
        {
            var result = new ResultModel<Models.Omnivore.Ticket.Tickets.RootObject>(true);

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });
                    var requestUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{locationId}/tickets?where=eq(open,{isOpen.ToString().ToLower()})&limit={limit}";
                    var responseMessage = await client.GetAsync(requestUrl);

                    var parseTicketResult = await ParseResponceMessage<Models.Omnivore.Ticket.Tickets.RootObject>(responseMessage);
                    if (parseTicketResult.Succeed)
                    {
                        result.Object = parseTicketResult.Object;
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = parseTicketResult.Error;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<ThirdPartyPaymentResultModel>> SendThirdPartyPayment(string apiKey, string locationId, ThirdPartyPaymentModel model)
        {
            var result = new ResultModel<ThirdPartyPaymentResultModel>(true);

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });

                    var requestUrl = $"{_configuration.BaseOmnivoreApiUrl}locations/{locationId}/tickets/{model.TicketId}/payments/";

                    var content = JsonConvert.SerializeObject(model);

                    var responseMessage = await client.PostAsync(requestUrl, new StringContent(content));

                    var parsePaymentResult = await ParseResponceMessage<ThirdPartyPaymentResultModel>(responseMessage);

                    if (parsePaymentResult.Succeed)
                    {
                        result.Object = parsePaymentResult.Object;
                    }
                    else
                    {
                        var message = await responseMessage.Content.ReadAsStringAsync();
                        result.Succeed = false;
                        result.Error = message;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
