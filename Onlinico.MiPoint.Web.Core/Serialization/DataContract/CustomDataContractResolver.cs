﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Onlinico.MiPoint.Web.Core.Serialization.DataContract
{
    public class CustomDataContractResolver<T> : DefaultContractResolver where T : class
    {
        private readonly Dictionary<string, string> _scheme; 

        public CustomDataContractResolver(Dictionary<string, string> scheme)
        {
            _scheme = scheme;
        }

        //public static readonly CustomDataContractResolver Instance = new CustomDataContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            
            if (member.ReflectedType == typeof(T))
            {
                if (_scheme.ContainsKey(property.PropertyName))
                {
                    property.UnderlyingName = _scheme[property.PropertyName];
                }
            }
            
            //if (property.DeclaringType == typeof(T))
            //{
            //    if (property.PropertyName.Equals("LongPropertyName", StringComparison.OrdinalIgnoreCase))
            //    {
            //        property.PropertyName = "Short";
            //    }
            //}

            return property;
        }
    }
}
