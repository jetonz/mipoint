﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class InviteModel
    {
        public Guid Id { get; set; }

        public Guid MerchantId { get; set; }

        public string Email { get; set; }
    }
}
