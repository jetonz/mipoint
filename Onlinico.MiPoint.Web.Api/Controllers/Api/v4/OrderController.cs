﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData.Query;
using System.Web.UI.WebControls;
using Onlinico.MiPoint.Web.Api.Models.Api.v4;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper;
using Onlinico.MiPoint.Web.Api.Models.Omnivore.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v4;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;
using Swashbuckle.Swagger.Annotations;
using WebGrease.Css.Extensions;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v4
{
    /// <summary>
    /// Order controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v4/orders")]
    public class OrderController : BaseController
    {
        private readonly IPaymentService _paymentService;
        private readonly IOmnivoreService _omnivoreService;
        private readonly ILocationService _locationService;
        private readonly IAuthService _authService;
        private readonly IMemoryCacher _memoryCacher;

        /// <summary>
        /// Order controller constructor
        /// </summary>
        /// <param name="paymentService">Inject payment service</param>
        /// <param name="omnivoreService">Inject omnivore service</param>
        /// <param name="locationService">Inject location service</param>
        /// <param name="authService">Inject auth service</param>
        /// <param name="memoryCacher">Inject memory cacher</param>
        /// <param name="logger">Inject logger service</param>
        /// <param name="configuration">Inject configuration</param>
        public OrderController(IPaymentService paymentService, IOmnivoreService omnivoreService, ILocationService locationService, IAuthService authService, IMemoryCacher memoryCacher, ILogger logger, IConfiguration configuration) : base(logger, configuration)
        {
            _paymentService = paymentService;
            _omnivoreService = omnivoreService;
            _locationService = locationService;
            _authService = authService;
            _memoryCacher = memoryCacher;
        }

        /// <summary>
        /// Get order details(with payments)
        /// </summary>
        /// <param name="orderId">Order id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{orderId}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(OrderModel))]
        [SwaggerResponse(HttpStatusCode.NoContent)]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
        public async Task<IHttpActionResult> GetOrder(string orderId)
        {
            var result = new ResultModel<OrderModel>(true);

            var orderIdentifier = orderId?.Split('-');
            if (orderIdentifier != null && orderIdentifier.Length == 2 && !string.IsNullOrEmpty(orderIdentifier[0]) && !string.IsNullOrEmpty(orderIdentifier[1]))
            {
                var identityResult = _authService.GetIdentity(LocationId);
                if (identityResult.Succeed && identityResult.Object != null)
                {
                    var mappingSchemeResult = _locationService.GetMappingSchemes(LocationId);

                    var omnivoreTicketResult = await _omnivoreService.GetTicketAsync(identityResult.Object.OmnivoreApiKey, identityResult.Object.OmnivoreLocationId, orderIdentifier[0]);
                    if (omnivoreTicketResult.Succeed && omnivoreTicketResult.Object != null)
                    {
                        if (mappingSchemeResult.Succeed && mappingSchemeResult.Object != null)
                        {
                            omnivoreTicketResult.Object._embedded?.employee?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Employee).Scheme);

                            omnivoreTicketResult.Object._embedded?.table?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Table).Scheme);
                        }

                        var paymentsResult = await _paymentService.GetPaymentsForOrdersAsync(LocationId, new List<string> { orderId }, PaymentStatusEnum.Paid);
                        if (paymentsResult.Succeed && paymentsResult.Object != null)
                        {
                            var amountSum = paymentsResult.Object.Where(p => p.Status < PaymentStatusEnum.Paid).Select(p => p.Amount).Sum();

                            var due = omnivoreTicketResult.Object.totals.due ?? 0;

                            omnivoreTicketResult.Object.totals.due = Math.Max(0, due - amountSum);
                        }

                        result.Object = omnivoreTicketResult.Object.ToApiOrderModel(paymentsResult.Object);
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = $" Error get omnivore ticket {omnivoreTicketResult.Error}";
                    }
                }
                else
                {
                    result.Succeed = false;
                    result.Error = $" Error get identity {identityResult.Error}";
                }
            }
            else
            {
                result.Succeed = false;
                result.Error = "Invalid order id.";
            }

            if (result.Succeed)
            {
                if (result.Object != null)
                    return Ok(result.Object);

                throw CreateApiException(string.Empty, HttpStatusCode.NoContent);
            }
            else
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(result.Error));

                throw CreateApiException(result.Error);
            }
        }

        /// <summary>
        /// Get orders(without payments)
        /// </summary>
        /// <param name="status">Order filter status 0:closed, 1:open</param>
        /// <param name="queryOptions">OData query</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<ShortOrderModel>))]
        [SwaggerResponse(HttpStatusCode.NoContent)]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
        public async Task<IHttpActionResult> GetOrders(/*OrderStatusEnum status,*/ ODataQueryOptions<ShortOrderModel> queryOptions)
        {
            var result = new ResultModel<List<ShortOrderModel>>(true);
            var identityResult = _authService.GetIdentity(LocationId);
            if (identityResult.Succeed && identityResult.Object != null)
            {
                var settingsResult = await _locationService.GetSettingsAsync(LocationId);

                var mappingSchemeResult = _locationService.GetMappingSchemes(LocationId);

                if (settingsResult.Succeed && settingsResult.Object != null)
                {
                    var isOpen = queryOptions.Filter == null
                        || queryOptions.Filter.RawValue.IndexOf("Status eq 'Open'", StringComparison.OrdinalIgnoreCase) > -1
                        || queryOptions.Filter.RawValue.IndexOf("Status eq 'PartiallyPaid'", StringComparison.OrdinalIgnoreCase) > -1;
                    var limit = isOpen ? settingsResult.Object.OpenOrderLimit : settingsResult.Object.ClosedOrderLimit;

                    var omnivoreTicketsResult = await _omnivoreService.GetTicketsAsync(identityResult.Object.OmnivoreApiKey,
                            identityResult.Object.OmnivoreLocationId, isOpen, limit);

                    if (omnivoreTicketsResult.Succeed && omnivoreTicketsResult.Object != null)
                    {
                        if (mappingSchemeResult.Succeed && mappingSchemeResult.Object != null)
                        {
                            if (mappingSchemeResult.Object.Any(s => s.Type == MappingTypeEnum.Employee))
                                omnivoreTicketsResult.Object._embedded?.tickets?.ForEach(t => t._embedded?.employee?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Employee).Scheme));
                            if (mappingSchemeResult.Object.Any(s => s.Type == MappingTypeEnum.Table))
                                omnivoreTicketsResult.Object._embedded?.tickets?.ForEach(t => t._embedded?.table?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Table).Scheme));
                        }

                        var omnivoreTicketsId = omnivoreTicketsResult.Object._embedded?.tickets?.Select(t => $"{t.id}-{t.opened_at}").ToList();

                        var paymentsResult = await _paymentService.GetPaymentsForOrdersAsync(LocationId, omnivoreTicketsId, PaymentStatusEnum.Paid);

                        if (paymentsResult.Succeed && paymentsResult.Object != null)
                        {
                            omnivoreTicketsResult.Object._embedded?.tickets?.ForEach(t =>
                            {
                                var amountSum = paymentsResult.Object.Where(p => p.OrderId == $"{t.id}-{t.opened_at}" && p.Status < PaymentStatusEnum.Paid).Select(p => p.Amount).Sum();

                                var due = t.totals.due ?? 0;

                                t.totals.due = Math.Max(0, due - amountSum);
                            });
                        }
                        else
                        {
                            result.Succeed = false;
                            result.Error = $"Error get payments {paymentsResult.Error}";
                        }

                        result.Object = omnivoreTicketsResult.Object.ToApiShortOrderModel(paymentsResult.Object);//.ToApiOrderModel();

                        var cacheKey = LocationId + "-ClosedPendings";
                        switch (isOpen)
                        {
                            case false:
                                var closedPendingsCached = _memoryCacher.GetValue<List<ShortOrderModel>>(cacheKey);
                                closedPendingsCached?.ForEach(c =>
                                {
                                    if (result.Object.All(o => o.Id != c.Id))
                                    {
                                        result.Object.Add(c);
                                    }
                                });
                                break;
                            case true:
                                var closedPendings = result.Object.Where(p => p.Status == OrderStatusEnum.Closed && p.PaymentsStatus == OrderPaymentStatusEnum.Pending).ToList();
                                if (closedPendings.Any())
                                {
                                    _memoryCacher.Delete(cacheKey);
                                    _memoryCacher.Add(cacheKey, closedPendings, DateTimeOffset.UtcNow.AddMinutes(60));
                                }
                                result.Object = result.Object.Where(p => p.Status != OrderStatusEnum.Closed).ToList();
                                break;
                        }

                        //result.Object = result.Object.OrderBy(o => (int)o.Status).ThenByDescending(o => (int)o.PaymentStatus).ThenBy(o => o.Table?.Number).ThenByDescending(o => o.OpenedAt).ToList();
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = $"Error get omnivore tickets {omnivoreTicketsResult.Error}";
                    }
                }
                else
                {
                    result.Succeed = false;
                    result.Error = $"Error get settings {settingsResult.Error}";
                }
            }
            else
            {
                result.Succeed = false;
                result.Error = $"Error get identity: {identityResult.Error}";
            }

            if (result.Succeed && result.Object != null)
            {
                return Ok(queryOptions.ApplyTo(result.Object.AsQueryable()));
            }
            else
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(result.Error));
                throw CreateApiException(result.Error);
            }
        }

        protected override void Dispose(bool disposing)
        {
            _paymentService.Dispose();
            _locationService.Dispose();
            base.Dispose(disposing);
        }
    }
}
