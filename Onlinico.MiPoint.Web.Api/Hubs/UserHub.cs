﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Autofac;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Onlinico.MiPoint.Web.Api.Hubs
{
    [Authorize]
    [HubName("userHub")]
    public class UserHub : Hub
    {
        public UserHub(ILifetimeScope lifetimeScope)
        {

        }

        public override Task OnConnected()
        {
            //Groups.Add(Context.ConnectionId, "AuthorizedUsers"); //.Wait();

            return base.OnConnected();
        }
    }
}