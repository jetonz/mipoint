﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;
using Onlinico.MiPoint.Web.Service.Mapper.Global.MappingScheme;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class SchemeMapper
    {
        public static IEnumerable<MappingModel> ToMappingScheme(this IEnumerable<Data.DataModel.MappingScheme> data)
        {
            return data.Select(s => new MappingModel
            {
                Type = s.Type == MappingSchemeTypes.Employee ? MappingTypeEnum.Employee : MappingTypeEnum.Table,
                Scheme = s.Scheme.ToMappingScheme(),
            });
        }
    }
}
