﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class Card
    {
        public Card()
        {
            TenderTypes = new HashSet<TenderType>();
        }

        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }


        public virtual ICollection<TenderType> TenderTypes { get; set; }
    }
}
