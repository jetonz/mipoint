﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using WebGrease.Css.Extensions;

namespace Onlinico.MiPoint.Web.Api
{
    public class RequestHandler : DelegatingHandler
    {
        private readonly string _key;

        public RequestHandler()
        {
            _key = new App_Start.Configuration().Secret;
        }

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var isApi = Regex.IsMatch(request.RequestUri.AbsolutePath, "api/", RegexOptions.IgnoreCase);
            var isHeartbeat = Regex.IsMatch(request.RequestUri.AbsolutePath, "heartbeat", RegexOptions.IgnoreCase);
            var startDateTime = DateTime.UtcNow;
            var requestHeader = string.Empty;
            var requestContent = string.Empty;
            var responseHeader = string.Empty;
            var responseContent = string.Empty;
            var agent = string.Empty;
            var deviceId = string.Empty;

            if (isApi && !isHeartbeat)
            {
                if (request.Headers.Contains("Device-Info"))
                {
                    deviceId = request.Headers.GetValues("Device-Info").FirstOrDefault();
                }
                if (request.Headers.Contains("User-Agent"))
                {
                    agent = request.Headers.GetValues("User-Agent").FirstOrDefault();
                }
            }

            if (isApi)
            {
                request.Headers.ForEach(h => requestHeader += ReadHeader(h));
                if (request.Content != null)
                    requestContent = await request.Content?.ReadAsStringAsync();
            }

            var result = await base.SendAsync(request, cancellationToken);

            if (isApi)
            {
                result.Headers.ForEach(h => requestHeader += ReadHeader(h));
                if (result.Content != null)
                    responseContent = await result.Content.ReadAsStringAsync();
            }

            if (isApi && !isHeartbeat && result.StatusCode != HttpStatusCode.Unauthorized && request.Properties.ContainsKey(_key))
            {
                var historyService = (Service.Interfaces.Global.IRequestService)request.GetDependencyScope().GetService(typeof(Service.Interfaces.Global.IRequestService));

                var locationSecret = request.Properties[_key].ToString();
                if (!string.IsNullOrEmpty(locationSecret))
                {
                    var history = new Domain.Models.Global.RequestModel()
                    {
                        DeviceId = deviceId,
                        Agent = agent,
                        Start = startDateTime,
                        End = DateTime.UtcNow,
                        Method = request.Method.Method,
                        StatusCode = (int)result.StatusCode,
                        LocationSecret = locationSecret,
                        Url = request.RequestUri.PathAndQuery.Replace("/api/", string.Empty).Replace("omnivore/", string.Empty),
                        RequestHeaders = requestHeader,
                        RequestContent = requestContent,
                        ResponseHeaders = responseHeader,
                        ResponseContent = responseContent,
                    };
                    await historyService.AddAsync(history).ConfigureAwait(false);
                }
            }
            return result;
        }

        private string ReadHeader(KeyValuePair<string, IEnumerable<string>> header)
        {
            return header.Key + ":" + String.Join(",", header.Value) + ";";
        }
    }
}