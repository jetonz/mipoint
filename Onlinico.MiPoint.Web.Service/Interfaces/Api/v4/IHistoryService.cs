﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v4
{
    public interface IHistoryService : IBaseService
    {
        Task<ResultModel> AddAsync(HistoryModel model);
    }
}
