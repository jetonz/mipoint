﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v3;
using Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v3;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v3;
using Onlinico.MiPoint.Web.Service.Mapper.Exceptions;
using PaymentTypeEnum = Onlinico.MiPoint.Web.Data.DataModel.Enums.PaymentTypeEnum;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v3
{
    public class PaymentService : BaseService, IPaymentService
    {
        public PaymentService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel<List<PaymentModel>>> GetPaymentsForOrdersAsync(string locationId, List<string> ordersId, PaymentStatusEnum status)
        {
            var result = new ResultModel<List<PaymentModel>>(true);

            var paymentStatus = (Data.DataModel.Enums.PaymentStatusEnum)((int)status);

            try
            {
                var payments = await Context.Payments
                    .Include(p => p.Items)
                    .Include(p => p.ReceiptInfo)
                    .Include(p => p.Transaction)
                    .Where(p => p.Location.Id == locationId
                        && ordersId.Contains(p.OrderId)
                        && p.Status <= paymentStatus)
                    .ToListAsync();

                result.Object = payments?.ToDomainPayments().ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<ThirdPartyPaymentModel>> GetPaymentToThirdPartyPayment(string paymentId)
        {
            var result = new ResultModel<ThirdPartyPaymentModel>(true);

            try
            {
                var payment = Context.Payments.Find(paymentId);
                if (payment != null)
                {
                    if (payment.Type == PaymentTypeEnum.Normal)
                    {
                        var cardType = payment.Transaction.CardType;

                        var card = await Context.Cards.FirstOrDefaultAsync(c => c.Type == cardType);
                        if (card != null)
                        {
                            var tenderType = payment.Location.Settings.TenderTypes.FirstOrDefault(t => t.Card.Id == card.Id);
                            if (tenderType != null)
                            {
                                result.Object = new ThirdPartyPaymentModel
                                {
                                    TicketId = payment.OrderId.Split('-').FirstOrDefault(),
                                    Amount = payment.Amount,
                                    Tip = payment.Tip,
                                    TenderType = tenderType?.Type,
                                };
                            }
                            else
                            {
                                result.Succeed = false;
                                result.Error = "Invalid location tender type.";
                            }
                        }
                        else
                        {
                            result.Succeed = false;
                            result.Error = "Invalid card type id.";
                        }
                    }
                    else
                    {
                        //return only NORMAL payment
                    }
                }
                else
                {
                    result.Succeed = false;
                    result.Error = "Invalid payment id.";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<PaymentModel>> GetAsync(string paymentId)
        {
            var result = new ResultModel<PaymentModel>(true);
            try
            {
                var payment = await Context.Payments.FirstOrDefaultAsync(p => p.Id == paymentId);
                if (payment != null)
                {
                    result.Object = payment.ToDomainPayment();
                }
                else
                {
                    result = new ResultModel<PaymentModel>(false, "Invalid payment id.");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result = new ResultModel<PaymentModel>(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel> UpdateMessageAsync(string paymentId, string message)
        {
            var result = new ResultModel(true);
            try
            {
                var payment = await Context.Payments.FirstOrDefaultAsync(p => p.Id == paymentId);
                if (payment != null)
                {
                    payment.Message = message;

                    await Context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel<IEnumerable<PaymentModel>>> GetByLocationAsync(string locationId)
        {
            var result = new ResultModel<IEnumerable<PaymentModel>>(true);
            try
            {
                var location = await Context.Locations.FirstOrDefaultAsync(l => l.Id == locationId && !l.IsRemoved);
                if (location != null)
                {
                    result.Object = location.Payments.AsEnumerable().ToDomainPayments();
                }
                else
                {
                    result.Succeed = false;
                    result.Error = "Invalid location id.";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> AddAsync(string locationId, PaymentModel model)
        {
            var result = new ResultModel(true);
            try
            {
                var location = await Context.Locations.FirstOrDefaultAsync(l => l.Id == locationId && !l.IsRemoved);
                if (location != null)
                {
                    var paymentId = Guid.NewGuid().ToString().Replace("-", string.Empty);
                    var payment = model.ToNewPayment(location, paymentId);
                    Context.Payments.Add(payment);

                    await Context.SaveChangesAsync();

                    result.ObjectId = payment.Id;
                }
                else
                {
                    result = new ResultModel(false, "Invalid location id.");
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel> ConfirmAsync(string paymentId, PaymentTransactionModel transaction)
        {
            var result = new ResultModel(true);
            try
            {
                var payment = Context.Payments.Find(paymentId);
                if (payment != null)
                {
                    if (payment.Status == Data.DataModel.Enums.PaymentStatusEnum.New)
                    {
                        if (Context.Cards.Any(c => c.Type == transaction.CardType))
                        {
                            payment.Status = payment.Type == PaymentTypeEnum.Normal 
                                ? Data.DataModel.Enums.PaymentStatusEnum.IsProcessing
                                : Data.DataModel.Enums.PaymentStatusEnum.Paid;

                            var paymentTransaction = transaction.ToPaymentTransaction();

                            payment.Transaction = paymentTransaction;

                            await Context.SaveChangesAsync();
                        }
                        else
                        {
                            result = new ResultModel(false, "Not supported card type.");
                        }
                    }
                    else
                    {
                        result = new ResultModel(false, "Payment already paid or canceled.");
                    }
                }
                else
                {
                    result = new ResultModel(false, "Invalid payment id.");
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel> UpdateStatusAsync(string paymentId, PaymentStatusEnum paymentStatus, string message)
        {
            var result = new ResultModel(true);
            try
            {
                var payment = await Context.Payments.FirstOrDefaultAsync(p => p.Id == paymentId);
                if (payment != null)
                {
                    var status = (Data.DataModel.Enums.PaymentStatusEnum)((int)paymentStatus);
                    if (status > payment.Status)
                    {
                        payment.Status = status;
                        payment.Updated = DateTime.UtcNow;
                        payment.Message = message;

                        await Context.SaveChangesAsync();
                    }
                    else
                    {
                        result = new ResultModel(false, "Can't set lower degree payment status.");
                    }
                }
                else
                {
                    result = new ResultModel(false, "Invalid payment id.");
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel> CancelAsync(string paymentId, string message)
        {
            var result = new ResultModel(true);
            try
            {
                var payment = await Context.Payments.FirstOrDefaultAsync(p => p.Id == paymentId);
                if (payment != null)
                {
                    if (payment.Status == Data.DataModel.Enums.PaymentStatusEnum.New)
                    {
                        payment.Status = Data.DataModel.Enums.PaymentStatusEnum.Cancelled;
                        payment.Updated = DateTime.UtcNow;
                        payment.Message = message;

                        await Context.SaveChangesAsync();
                    }
                    else
                    {
                        result = new ResultModel(false, "You can cancel only the payments have not been paid.");
                    }
                }
                else
                {
                    result = new ResultModel(false, "Invalid payment id.");
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result = new ResultModel(false, error);
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }
    }
}
