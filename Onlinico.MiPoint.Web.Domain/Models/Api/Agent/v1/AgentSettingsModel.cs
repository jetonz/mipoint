﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1.Enum;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class AgentSettingsModel
    {
        public List<AgentTenderTypeModel> TenderTypes { get; set; }

        public List<AgentMappintSchemeModel> MappintScheme { get; set; }
    }

    public class AgentMappintSchemeModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public MappintSchemeEnum SchemeType { get; set; }

        public Dictionary<string, string> Scheme { get; set; }
    }

    public class AgentTenderTypeModel
    {
        public string Name { get; set; }

        public string TenderType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public CardTypeEnum CardType { get; set; }
    }
}
