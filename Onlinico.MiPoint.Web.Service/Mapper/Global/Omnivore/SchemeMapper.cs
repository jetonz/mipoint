﻿using System.Collections.Generic;
using Onlinico.MiPoint.Web.Core.Mapper;

namespace Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore
{
    public static class SchemeMapper
    {
        public static void ToScheme(this Models.Omnivore.Employees.Employee model, Dictionary<string, string> scheme)
        {
            var clon = (Models.Omnivore.Employees.Employee)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }
    }
}