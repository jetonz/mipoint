﻿using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v3
{
    public static class TenderTypeMapper
    {
        public static List<Domain.Models.Api.v3.TenderTypeModel> ToDomainTenderTypes(
            this IEnumerable<Data.DataModel.TenderType> tenderTypes)
        {
            return tenderTypes.Select(t => new Domain.Models.Api.v3.TenderTypeModel
            {
                Id = t.Id,
                Name = t.Name,
                Type = t.Type,
                Card = t.Card.ToDomainCardModel()
            }).ToList();
        }


        public static List<Data.DataModel.TenderType> ToEntityTenderTypes(
            this List<Domain.Models.Api.v3.TenderTypeModel> tenderTypes)
        {
            return tenderTypes.Select(t => new Data.DataModel.TenderType
            {
                Id = t.Id,
                Name = t.Name,
                Type = t.Type,
                Card = t.Card.ToEntityCard()
            }).ToList();
        }

        public static void UpdateTenderTypeEntity(this Data.DataModel.TenderType entity,
            Domain.Models.Api.v3.TenderTypeModel model)
        {
            entity.Name = model.Name;
            entity.Type = model.Type;
        }
    }
}
