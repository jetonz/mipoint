﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v1
{
    public class LocationService : BaseService, ILocationService
    {
        private readonly IMemoryCacher _memoryCacher;

        public LocationService(ILogger logger, IMemoryCacher memoryCacher) : base(logger)
        {
            _memoryCacher = memoryCacher;
        }

        public string GetApiKey(string locationId)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return null;
        }

        public string GetLocationIdentifier(string locationId)
        {
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId);
                    return location?.Identifier;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return null;
        }

        public List<LocationModel> GetByMerchant(Guid merchantId)
        {
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = context.Merchants.FirstOrDefault(m => m.Id == merchantId);
                    return merchant?.Locations.ToDomainLocations();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return null;
        }

        public LocationSettingsModel GetSettings(string locationId)
        {
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId);
                    return location?.Settings?.ToDomainSettings();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return null;
        }

        public ResultModel<LocationSettingsModel> UpdatteSettings(LocationSettingsModel model)
        {
            var result = new ResultModel<LocationSettingsModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var settings = context.LocationSettings.Include(s=>s.Location).FirstOrDefault(s => s.Id == model.Id);
                    if (settings != null)
                    {
                        settings.UpdateSettings(model);
                        
                        context.SaveChanges();

                        result.Object = settings?.ToDomainSettings();
                    }
                    else
                    {
                        result = new ResultModel<LocationSettingsModel>(false, "Invalid data.");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel<LocationSettingsModel>(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return result;
        }
    }
}
