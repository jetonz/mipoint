﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v3
{
    public static class LogMapper
    {
        public static Data.DataModel.Log ToEntityModel(this Domain.Models.Api.v3.LogModel model)
        {
            return new Data.DataModel.Log
            {
                Id = model.Id,
                Model = model.Model,
                Content = model.Content,
                Serial = model.Serial,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Fingerprint = model.Fingerprint,
                Created = DateTime.UtcNow,
                AppVersion = model.AppVersion,
            };
        }

        public static List<Domain.Models.Api.v3.LogModel> ToDomainLogModels(this IEnumerable<Data.DataModel.Log> models)
        {
            return models.Select(l => new Domain.Models.Api.v3.LogModel
            {
                Id = l.Id,
                Model = l.Model,
                Fingerprint = l.Fingerprint,
                LocationId = l.LocationId,
                Manufacturer = l.Manufacturer,
                Serial = l.Serial,
                Created = l.Created,
                AppVersion = l.AppVersion,
            }).ToList();
        }

        public static Domain.Models.Api.v3.LogModel ToDomainLogModel(this Data.DataModel.Log model)
        {
            return new Domain.Models.Api.v3.LogModel
            {
                Id = model.Id,
                Model = model.Model,
                Fingerprint = model.Fingerprint,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Serial = model.Serial,
                Created = model.Created,
                Content = model.Content,
                AppVersion = model.AppVersion,
            };
        }
    }
}
