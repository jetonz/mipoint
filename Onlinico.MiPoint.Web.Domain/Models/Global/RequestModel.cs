﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Global
{
    public class RequestModel
    {
        public string LocationSecret { get; set; }

        public string DeviceId { get; set; }
        
        public string Agent { get; set; }

        public string Method { get; set; }
        
        public string Url { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int StatusCode { get; set; }

        public string RequestHeaders { get; set; }

        public string RequestContent { get; set; }

        public string ResponseHeaders { get; set; }
        
        public string ResponseContent { get; set; }
    }
}
