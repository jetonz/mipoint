﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Enums.Transaction;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class PaymentMapper
    {
        public static PaymentModel ToDomainPayment(this Payment model)
        {
            return new PaymentModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tip,
                Tax = model.Tax,
                Status = (PaymentStatusEnum)((int)model.Status),
                Type = (PaymentTypeEnum)((int)model.Type),
                Message = model.Message,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,
                
                Items = model.Items?.ToDomainPaymentItems(),
                ReceiptInfo = model.ReceiptInfo?.ToDomainPaymentReceptInfo(),
                Transaction = model.Transaction?.ToDomainPaymentTransaction(),
            };
        }

        public static IEnumerable<PaymentModel> ToDomainPayments(this IEnumerable<Payment> models)
        {
            var result = models.Select(p => p.ToDomainPayment());
            return result.AsQueryable();
        }

        public static PaymentTransactionModel ToDomainPaymentTransaction(this PaymentTransaction model)
        {
            TransactionTypeEnum transactionType;
            Enum.TryParse(model.TransactionType, out transactionType);

            CardTypeEnum cardType;
            Enum.TryParse(model.CardType, out cardType);

            CardEntryTypeEnum cardEntryType;
            Enum.TryParse(model.CardEntryType, out cardEntryType);

            return new PaymentTransactionModel
            {
                TransactionId = model.TransactionId,
                TransactionType = transactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = cardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = cardEntryType,
                Issued = model.Issued,
            };
        }

        public static PaymentReceptInfoModel ToDomainPaymentReceptInfo(this PaymentReceiptInfo model)
        {
            return new PaymentReceptInfoModel
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }

        public static IEnumerable<PaymentItemModel> ToDomainPaymentItems(this IEnumerable<PaymentItem> items)
        {
            return items.Select(i => new PaymentItemModel
            {
                Id = i.Id,
                ItemId = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            });
        }

        public static Payment ToNewPayment(this PaymentModel model, Location location, string id)
        {
            return new Payment
            {
                Id = id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tip,
                Tax = model.Tax,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,
                Status = Data.DataModel.Enums.PaymentStatusEnum.New,
                Type = (Data.DataModel.Enums.PaymentTypeEnum)((int)model.Type),
                Message = model.Message,
                
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,

                Items = model.Items?.ToPaymentItems(),
                ReceiptInfo = model.ReceiptInfo?.ToPaymentReceiptInfo(),
                Location = location,
            };
        }

        public static PaymentReceiptInfo ToPaymentReceiptInfo(this PaymentReceptInfoModel model)
        {
            return new PaymentReceiptInfo
            {
                Id = model.Id == Guid.Empty ? Guid.NewGuid() : model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }

        public static PaymentTransaction ToPaymentTransaction(this PaymentTransactionModel model)
        {
            return new PaymentTransaction
            {
                Id = Guid.NewGuid(),
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType.ToString(),
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = model.CardType.ToString(),
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType.ToString(),
                Issued = model.Issued,
            };
        }

        public static List<PaymentItem> ToPaymentItems(this IEnumerable<PaymentItemModel> items)
        {
            return items.Select(i => new PaymentItem
            {
                Id = i.Id == Guid.Empty ? Guid.NewGuid() : i.Id,
                ItemId = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }
    }
}
