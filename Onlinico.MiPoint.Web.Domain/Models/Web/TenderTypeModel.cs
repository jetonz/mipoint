﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class TenderTypeModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public CardModel Card { get; set; }
    }

    public class AvailableTenderTypeModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
