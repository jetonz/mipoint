﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v1
{
    public static class LogMapper
    {
        public static Data.DataModel.Log ToEntityModel(this LogModel model)
        {
            return new Data.DataModel.Log
            {
                Id = model.Id,
                Model = model.Model,
                Content = model.Content,
                Serial = model.Serial,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Fingerprint = model.Fingerprint,
                Created = DateTime.UtcNow,
            };
        }

        public static List<LogModel> ToDomainLogModels(this IEnumerable<Data.DataModel.Log> models)
        {
            return models.Select(l => new LogModel
            {
                Id = l.Id,
                Model = l.Model,
                Fingerprint = l.Fingerprint,
                LocationId = l.LocationId,
                Manufacturer = l.Manufacturer,
                Serial = l.Serial
            }).ToList();
        }

        public static LogModel ToDomainLogModel(this Data.DataModel.Log model)
        {
            return new LogModel
            {
                Id = model.Id,
                Model = model.Model,
                Fingerprint = model.Fingerprint,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Serial = model.Serial,
                Content = model.Content
            };
        }
    }
}
