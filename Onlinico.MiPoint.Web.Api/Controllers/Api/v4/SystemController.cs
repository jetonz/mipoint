﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v4;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v4;
using Swashbuckle.Swagger.Annotations;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v4
{
    [Attributes.Authorize, RoutePrefix("api/v4")]
    public class SystemController : BaseController
    {
        private readonly ILocationService _locationService;

        public SystemController(ILogger logger, IConfiguration configuration, ILocationService locationService) : base(logger, configuration)
        {
            _locationService = locationService;
        }

        /// <summary>
        /// Get client settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("device/settings")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ClientSettingsApiModel))]
        [SwaggerResponse(HttpStatusCode.NoContent)]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
        public async Task<IHttpActionResult> GetClientSettings()
        {
            var locationSettingsResult = await _locationService.GetClientSettingsAsync(LocationId);

            if (locationSettingsResult.Succeed)
            {
                if (locationSettingsResult.Object != null)
                {
                    return Ok(locationSettingsResult.Object.ToClientSettingsModel());
                }
                else
                {
                    throw CreateApiException(string.Empty, HttpStatusCode.NoContent);
                }
            }
            else
            {
                throw CreateApiException(locationSettingsResult.Error);
            }
        }

        protected override void Dispose(bool disposing)
        {
            _locationService.Dispose();
            base.Dispose(disposing);
        }
    }
}
