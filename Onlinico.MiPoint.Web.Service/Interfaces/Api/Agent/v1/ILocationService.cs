﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1
{
    public interface ILocationService
    {
        ResultModel GetFingerprint(string locationId);

        Task<ResultModel> VerifyAsync(VerifyModel model);

        Task<ResultModel> RegisterAsync(RegisterModel model);

        ResultModel<List<LocationModel>> GetLocations(Guid merchantId);


        Task<ResultModel<SettingsDto>> GetAgetnSettingsAsync(string locationId);

        Task<ResultModel<ClientSettingsModel>> GetClientSettingsAsync(string locationId);
    }
}
