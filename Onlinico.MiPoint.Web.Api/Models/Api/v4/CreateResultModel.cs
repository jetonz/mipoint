﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4
{
    /// <summary>
    /// Create result model
    /// </summary>
    public class CreateResultModel
    {
        /// <summary>
        /// Model constructor
        /// </summary>
        public CreateResultModel()
        {
            
        }

        /// <summary>
        /// Model constructor with object id
        /// </summary>
        /// <param name="id"></param>
        public CreateResultModel(string id)
        {
            ObjectId = id;
        }

        /// <summary>
        /// Created object id
        /// </summary>
        [JsonProperty("id")]
        public string ObjectId { get; set; }
    }
}