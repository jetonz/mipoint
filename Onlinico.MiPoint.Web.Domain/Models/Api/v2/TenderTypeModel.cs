﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v2
{
    public class TenderTypeModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public CardModel Card { get; set; }
    }
}
