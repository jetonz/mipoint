﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3
{
    /// <summary>
    /// Create result model
    /// </summary>
    public class CreateResultModel
    {
        /// <summary>
        /// Model constructor
        /// </summary>
        public CreateResultModel()
        {
            
        }

        /// <summary>
        /// Model constructor with object id
        /// </summary>
        /// <param name="id"></param>
        public CreateResultModel(string id)
        {
            ObjectId = id;
        }

        /// <summary>
        /// Created object id
        /// </summary>
        [JsonProperty("objectId")]
        public string ObjectId { get; set; }
    }
}