﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Onlinico.MiPoint.Web.Logger
{
    public class Logger : Interfaces.ILogger
    {
        private readonly NLog.Logger _logger = LogManager.GetCurrentClassLogger();

        public void WriteExceptionToLog(Exception ex, string message)
        {
            _logger.Error(ex, message);
        }

        public void WriteMessageToLog(string message)
        {
            _logger.Info(message);
        }

        public void WriteTraceToLog(string message)
        {
            _logger.Trace(message);
        }

        public void WriteError(string error)
        {
            _logger.Error(error);
        }
    }
}
