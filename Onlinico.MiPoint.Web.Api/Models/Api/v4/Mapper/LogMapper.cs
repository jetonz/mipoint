﻿using System;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper
{
    public static class LogMapper
    {
        public static Domain.Models.Api.v4.LogModel ToDomainLogModel(this SessionLogModel model, string locationId)
        {
            var content = Base64Decode(model.Content);

            return new Domain.Models.Api.v4.LogModel
            {
                Id = Guid.NewGuid(),
                LocationId = locationId,
                Model = model.Model,
                Manufacturer = model.Manufacturer,
                Fingerprint = model.Fingerprint,
                Serial = model.Serial,
                Content = content,
                AppVersion = model.AppVersion,
            };
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(base64EncodedData))
            {
                try
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                    result = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                }
                catch (Exception ex)
                {
                    var test = ex.Message;
                }
            }
            return result;
        }
    }
}