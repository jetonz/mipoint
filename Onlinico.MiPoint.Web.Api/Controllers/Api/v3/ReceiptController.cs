﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v3;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Validation;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Domain.Models.Api.v3;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v3;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v3
{
    /// <summary>
    /// Receipt controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v3/receipt")]
    public class ReceiptController : BaseController
    {
        private readonly IPaymentService _paymentService;
        private readonly IEmailService _emailService;
        private readonly ISmsService _smsService;

        /// <summary>
        /// Receipt controller constructor
        /// </summary>
        /// <param name="paymentService">Inject payment service</param>
        /// <param name="emailService">Inject email service</param>
        /// <param name="smsService">Inject sms service</param>
        public ReceiptController(IPaymentService paymentService, IEmailService emailService, ISmsService smsService)
        {
            _paymentService = paymentService;
            _emailService = emailService;
            _smsService = smsService;
        }

        /// <summary>
        /// Send receipt to email/sms
        /// </summary>
        /// <param name="paymentId">Payment id</param>
        /// <param name="carrier">Carrier: 'Email' or 'Sms'</param>
        /// <param name="address">Email address or  phone number</param>
        /// <returns></returns>
        [HttpPost, Route("{paymentId}/send")]
        public async Task<IHttpActionResult> Send([FromUri]string paymentId, [FromUri]CarrierEnum carrier, [FromUri]string address)
        {
            var result = new Domain.Models.Global.ResultModel(true);
            var payment = await _paymentService.GetAsync(paymentId);

            if (payment.Succeed && payment.Object != null)
            {
                var receiptUrl = CurrentDomain + "receipt/" + payment.Object.Id;

                switch (carrier)
                {
                    case CarrierEnum.Email:
                        var htmlBodyResult = await GetReceiptHtmlAsync(receiptUrl);
                        if (htmlBodyResult.Succeed)
                        {
                            result = await _emailService.SendAsync(address, $"Receipt from {payment.Object.ReceiptInfo?.LocationName}", htmlBodyResult.ObjectId);
                        }
                        else
                        {
                            result = new Domain.Models.Global.ResultModel(false, htmlBodyResult.Error);
                        }
                        break;
                    case CarrierEnum.Sms:
                        var message = $"Receipt : {receiptUrl}";
                        result = await _smsService.SendAsync(address, message);
                        break;
                }
            }
            return result.Succeed ? Ok() : (IHttpActionResult)BadRequest(result.Error);
        }

        private async Task<ResultModel> GetReceiptHtmlAsync(string receiptUrl)
        {
            var result = new ResultModel(true);
            try
            {
                using (var client = new HttpClient())
                {
                    var requestResult = await client.GetAsync(receiptUrl);
                    switch (requestResult.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            result.ObjectId = await requestResult.Content.ReadAsStringAsync();
                            break;
                        default:
                            result = new ResultModel(false, "Cannot get receipt html page.");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            _paymentService.Dispose();
            base.Dispose(disposing);
        }
    }
}
