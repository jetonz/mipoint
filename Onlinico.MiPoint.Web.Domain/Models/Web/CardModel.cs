﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class CardModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
