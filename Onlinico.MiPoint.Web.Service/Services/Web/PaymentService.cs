﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class PaymentService : BaseService, IPaymentService
    {
        public PaymentService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel<List<PaymentModel>>> GetAsync(string locationId, DateTime from, DateTime to)
        {
            var result = new ResultModel<List<PaymentModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var payments = await context.Payments
                        .Include(p => p.Transaction)
                        .Include(p => p.Location.Settings.TenderTypes)
                        .Where(p => p.Location.Id == locationId && p.Created >= from && p.Created <= to)
                        .OrderByDescending(p => p.Created)
                        .ToListAsync();

                    result.Object = payments?.ToDomainPayments();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public async Task<ResultModel<List<PaymentModel>>> GetFailedPaymentsAsync()
        {
            var result = new ResultModel<List<PaymentModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var payments = await context.Payments
                        .Include(p => p.Transaction)
                        .Include(p => p.Location.Settings.TenderTypes)
                        .Where(p => p.Status == PaymentStatusEnum.Confirmed
                            && p.Type == PaymentTypeEnum.Normal
                            && p.Transaction != null).ToListAsync();

                    result.Object = payments?.ToDomainPayments();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public async Task<ResultModel> UpdateStatusAsync(string paymentId, Domain.Enums.PaymentStatusEnum status, string message)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var payment = await context.Payments.FirstOrDefaultAsync(p => p.Id == paymentId);
                    if (payment != null)
                    {
                        payment.Status = (PaymentStatusEnum)((int)status);
                        payment.Message = message;
                        payment.Updated = DateTime.UtcNow;

                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid paymnet id.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> UpdateMessageAsync(string paymentId, string message)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var payment = await context.Payments.FirstOrDefaultAsync(p => p.Id == paymentId);
                    if (payment != null)
                    {
                        payment.Message = message;
                        payment.Updated = DateTime.UtcNow;

                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid paymnet id.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
