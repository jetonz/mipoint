﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class EmployeeMapper
    {
        public static EmployeeModel ToDomainEmployeeModel(this Data.DataModel.Employee employee)
        {
            return new EmployeeModel
            {
                Id = employee.Id,
                EmployeeId = employee.EmployeeId,
                Login = employee.Login,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                CheckName = employee.CheckName
            };
        }
    }
}
