﻿using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class ReceiptMapper
    {
        public static ReceiptModel ToDomainReceiptModel(this Receipt receipt)
        {
            return new ReceiptModel
            {
                Id = receipt.Id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToDomainPaymentModel(),
                Items = receipt.Items.ToDomainReceiptItemModels(),
            };
        }

        public static List<ReceiptModel> ToDomainReceipts(this IEnumerable<Receipt> models)
        {
            return models.Select(r => r.ToDomainReceiptModel()).ToList();
        } 

        public static List<ReceiptItemModel> ToDomainReceiptItemModels(this IEnumerable<ReceiptItem> items)
        {
            return items.Select(i => new ReceiptItemModel
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static ReceiptPaymentModel ToDomainPaymentModel(this ReceiptPayment payment)
        {
            return new ReceiptPaymentModel
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }

        public static Receipt ToEntityReceiptModel(this ReceiptModel receipt)
        {
            return new Receipt
            {
                Id = receipt.Id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToEntityPaymentModel(),
                Items = receipt.Items.ToEntityReceiptItems(),
            };
        }

        public static Receipt ToEntityReceiptModel(this ReceiptModel receipt, string id)
        {
            return new Receipt
            {
                Id = id,
                TicketId = receipt.TicketId,
                LocationId = receipt.LocationId,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToEntityPaymentModel(),
                Items = receipt.Items.ToEntityReceiptItems(),
            };
        }

        public static List<ReceiptItem> ToEntityReceiptItems(this List<ReceiptItemModel> items)
        {
            return items.Select(i => new ReceiptItem
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static ReceiptPayment ToEntityPaymentModel(this ReceiptPaymentModel payment)
        {
            return new ReceiptPayment
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm
            };
        }
    }
}
