﻿using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Global;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Global
{
    public interface IEmailService
    {
        Task<ResultModel> SendAsync(string address, string subject, string html);
    }
}
