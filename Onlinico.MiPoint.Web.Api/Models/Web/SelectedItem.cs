﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class SelectedItem<T> where T: class 
    {
        public T Item { get; set; }
        
        public string Id { get; set; }

        public bool Selected { get; set; }
    }
}