﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Mapper.Api.Agent.v1;

namespace Onlinico.MiPoint.Web.Service.Services.Api.Agent.v1
{
    public class LocationService : BaseService, ILocationService
    {
        private const string Unauthorized = "Unauthorized";

        public LocationService(ILogger logger) : base(logger)
        {
        }

        public ResultModel GetFingerprint(string locationId)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    if (location != null)
                    {
                        result.ObjectId = location.AgentFingerprint;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public ResultModel<List<LocationModel>> GetLocations(Guid merchantId)
        {
            var result = new ResultModel<List<LocationModel>>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = context.Merchants.FirstOrDefault(m => m.Id == merchantId);
                    if (merchant != null)
                    {
                        result.Object = merchant.Locations.Where(l => l.AgentFingerprint != null && !l.IsRemoved)
                            .Select(l => new LocationModel
                            {
                                Id = l.Id,
                                AgentFingerprint = l.AgentFingerprint
                            }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> VerifyAsync(VerifyModel model)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == model.ApiKey && !l.IsRemoved);
                    if (location != null)
                    {
                        if (string.IsNullOrEmpty(location.AgentFingerprint))
                        {

                        }
                        else
                        {
                            result.Error = location.AgentFingerprint == model.Fingerprint
                                ? "Location already related with this connector."
                                : "Location already related with another connector.";
                        }
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = Unauthorized;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> RegisterAsync(RegisterModel model)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == model.ApiKey && !l.IsRemoved).ConfigureAwait(false);
                    if (location != null)
                    {
                        if (string.IsNullOrEmpty(location.AgentFingerprint))
                        {
                            location.AgentFingerprint = model.Fingerprint;
                            location.LastAgentPoll = DateTime.UtcNow;
                            location.AgentVersion = model.AgentVersion;
                            location.AgentAddress = model.AgentAddress;

                            await context.SaveChangesAsync();
                        }
                        else
                        {
                            result.Succeed = false;
                            result.Error = location.AgentFingerprint == model.Fingerprint
                                ? "Location already related with this connector."
                                : "Location already related with another connector.";
                        }
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = Unauthorized;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<SettingsDto>> GetAgetnSettingsAsync(string locationId)
        {
            var result = new ResultModel<SettingsDto>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var settings = await context.LocationSettings
                    .Include(s => s.Location)
                    .Include(s => s.TenderTypes)
                    .Include(s => s.MappingSchemes)
                    .FirstOrDefaultAsync(s => s.Location.Id == locationId && !s.Location.IsRemoved).ConfigureAwait(false);

                    result.Object = settings?.ToSettingsModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. InnerException : {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<ClientSettingsModel>> GetClientSettingsAsync(string locationId)
        {
            var result = new ResultModel<ClientSettingsModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations
                    .Include(l => l.Merchant)
                    .Include(l => l.Settings)
                    .Include(l => l.ClientCounterModeSettings)
                    .Include(l => l.ClientTableModeSettings)
                    .FirstOrDefaultAsync(l => l.Id == locationId && !l.IsRemoved).ConfigureAwait(false);
                    var settings = location?.ToClientSettingsModel();

                    result.Object = settings;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. InnerException : {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

    }
}
