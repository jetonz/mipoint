﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class HistoryMapper
    {
        public static List<HistoryModel> ToHistoryModels(this List<Data.DataModel.History> models)
        {
            return models.Select(h => h.ToHistoryModel(false)).ToList();
        } 

        public static HistoryModel ToHistoryModel(this Data.DataModel.History model, bool withContent)
        {
            return new HistoryModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                Agent = model.Agent,
                LocationSecret = model.Location.Id,
                Method = model.Method,
                Url = model.Uri,
                Start = model.Start,
                End = model.End,
                StatusCode = model.StatusCode,
                RequestHeaders = model.RequestHeaders,
                RequestContent = withContent ? model.RequestContent ?? model.Content : null,
                ResponseHeaders = model.ResponseHeaders,
                ResponseContent = withContent ? model.ResponseContent : null,
            };
        }
    }
}
