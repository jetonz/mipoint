﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Onlinico.MiPoint.Web.Api.Extensions;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums;
using Onlinico.MiPoint.Web.Domain.Enums;
using Quartz.Util;
using PaymentStatusEnum = Onlinico.MiPoint.Web.Domain.Enums.PaymentStatusEnum;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper
{
    public static class OrderMapper
    {
        public static OrderModel ToApiOrderModel(this Service.Models.Omnivore.Ticket.Ticket.RootObject model, List<Domain.Models.Api.v3.PaymentModel> payments)
        {
            return new OrderModel
            {
                OrderId = $"{model.id}-{model.opened_at}",
                Name = model.name ?? "",
                OpenedAt = (model.opened_at ?? 0).ToDateTime(),
                ClosedAt = model.closed_at == null ? (DateTime?)null : (model.closed_at ?? 0).ToDateTime(),
                EmployeeId = model._embedded?.employee?.id,
                GuestCount = model.guest_count ?? 0,
                OrderNumber = model.ticket_number ?? 0,
                HasDiscounts = model._embedded?.discounts?.Any() ?? false,
                PaymentStatus = GetOrderPaymentStatus(model.open ?? false, model.totals.due ?? 0, model.totals.total ?? 0, payments?.Where(p => p.OrderId == $"{model.id}-{model.opened_at}").ToList()),

                Payments = payments.ToApiPayments().ToList(),
                Table = model._embedded?.table?.ToApiOrderTableModel(),
                Items = model._embedded?.items?.ToApiOrderItems(),
                Total = model.totals?.ToApiOrderTotalModel(),
                Discounts = model._embedded?.discounts?.ToDiscounts(),
            };
        }

        public static OrderTotalModel ToApiOrderTotalModel(this Service.Models.Omnivore.Ticket.Ticket.Totals model)
        {
            return new OrderTotalModel
            {
                Total = model.total ?? 0,
                Tax = model.tax ?? 0,
                Due = model.due ?? 0,
                OtherCharges = model.other_charges ?? 0,
                ServiceCharges = model.service_charges ?? 0,
                SubTotal = model.sub_total ?? 0,
            };
        }

        public static OrderTableModel ToApiOrderTableModel(this Service.Models.Omnivore.Ticket.Ticket.Table model)
        {
            return new OrderTableModel
            {
                Id = model.id,
                Name = model.name,
                Available = model.available ?? false,
                Number = model.number ?? 0,
                Seats = model.seats ?? 0,
            };
        }

        public static List<OrderItemModel> ToApiOrderItems(this List<Service.Models.Omnivore.Ticket.Ticket.Item> model)
        {
            return model.Select(i => new OrderItemModel
            {
                Id = i.id,
                Name = i.name,
                Quantity = i.quantity ?? 0,
                Price = i.price_per_unit ?? 0,
                HasDiscounts = i._embedded.discounts.Any(),
                Comment = i.comment,
                Discounts = i._embedded?.discounts?.ToDiscounts(),
            }).ToList();
        }

        public static List<DiscountModel> ToDiscounts(this List<Service.Models.Omnivore.Ticket.Ticket.Discount> model)
        {
            return model.Select(i => new DiscountModel
            {
                Id = i.id,
                Name = i.name,
                Value = i.value ?? 0,
            }).ToList();
        }

        public static List<ShortOrderModel> ToApiShortOrderModel(this Service.Models.Omnivore.Ticket.Tickets.RootObject model, List<Domain.Models.Api.v3.PaymentModel> payments)
        {
            return model?._embedded?.tickets?.Select(t => new ShortOrderModel
            {
                OrderId = $"{t.id}-{t.opened_at}",
                OpenedAt = (t.opened_at ?? 0).ToDateTime(),
                EmployeeId = t._embedded?.employee?.id,
                OrderNumber = t.ticket_number ?? 0,
                Name = t.name ?? "",
                PaymentStatus = GetOrderPaymentStatus(t.open ?? false, t.totals.due ?? 0, t.totals.total ?? 0, payments?.Where(p => p.OrderId == $"{t.id}-{t.opened_at}").ToList()),

                Total = t.totals?.ToApiOrderTotalModel(),
                Table = t._embedded?.table?.ToApiOrderTableModel(),
            }).ToList();
        }

        public static OrderPaymentStatusEnum GetOrderPaymentStatus(bool isOpen, long due, long total, List<Domain.Models.Api.v3.PaymentModel> payments)
        {
            if (payments == null)
            {
                payments = new List<Domain.Models.Api.v3.PaymentModel>();
            }
            
            OrderPaymentStatusEnum result = OrderPaymentStatusEnum.Closed;
            if (isOpen)
            {
                var isPending = payments.Any(p => p.Status == PaymentStatusEnum.New || p.Status == PaymentStatusEnum.IsProcessing || p.Status == PaymentStatusEnum.Confirmed);

                if (due == total)
                {
                    result = !payments.Any() ? OrderPaymentStatusEnum.Open : OrderPaymentStatusEnum.PartiallyPaidPending;
                }
                if (due > 0 && due != total)
                {
                    result = isPending
                        ? OrderPaymentStatusEnum.PartiallyPaidPending
                        : OrderPaymentStatusEnum.PartiallyPaid;
                }
                if (due == 0 && total != 0)
                {
                    result = isPending
                        ? OrderPaymentStatusEnum.ClosedPending
                        : OrderPaymentStatusEnum.Closed;
                }
            }
            return result;
        }

        public static OrderTotalModel ToApiOrderTotalModel(this Service.Models.Omnivore.Ticket.Tickets.Totals model)
        {
            return new OrderTotalModel
            {
                Total = model.total ?? 0,
                Tax = model.tax ?? 0,
                Due = model.due ?? 0,
                OtherCharges = model.other_charges ?? 0,
                ServiceCharges = model.service_charges ?? 0,
                SubTotal = model.sub_total ?? 0,
            };
        }

        public static OrderTableModel ToApiOrderTableModel(this Service.Models.Omnivore.Ticket.Tickets.Table model)
        {
            return new OrderTableModel
            {
                Id = model.id,
                Name = model.name,
                Available = model.available ?? false,
                Number = model.number ?? 0,
                Seats = model.seats ?? 0,
            };
        }

        public static List<OrderItemModel> ToApiOrderItems(this List<Service.Models.Omnivore.Ticket.Tickets.Item> model)
        {
            return model.Select(i => new OrderItemModel
            {
                Id = i.id,
                Name = i.name,
                Quantity = i.quantity ?? 0,
                Price = i.price_per_unit ?? 0,
                HasDiscounts = i._embedded.discounts.Any(),
                Comment = i.comment,
            }).ToList();
        }
    }
}