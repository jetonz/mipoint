﻿using System;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class ReceiptPaymentModel
    {
        public Guid Id { get; set; }

        public string TransactionId { get; set; }

        public string Amount { get; set; }

        public string CreatedTime { get; set; }

        public string CardEntryType { get; set; }

        public string CardType { get; set; }

        public string Last4 { get; set; }

        public string ReferenceId { get; set; }

        public string AuthCode { get; set; }

        public string ApplicationId { get; set; }

        public string TransactionType { get; set; }

        public string Cvm { get; set; }
    }
}