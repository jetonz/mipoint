﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3
{
    [DataContract]
    public class ReceiptModel
    {
        /// <summary>
        /// Receipt id(max length 10)
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Ticket id
        /// </summary>
        [DataMember(Name = "ticketId")]
        public string TicketId { get; set; }

        /// <summary>
        /// email, sms or storage
        /// </summary>
        [DataMember(Name = "receiptType")]
        public string ReceiptType { get; set; }

        [DataMember(Name = "destination")]
        public string Destination { get; set; }

        [DataMember(Name = "locationName")]
        public string LocationName { get; set; }

        [DataMember(Name = "address1")]
        public string Address1 { get; set; }

        [DataMember(Name = "address2")]
        public string Address2 { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "cashier")]
        public string Cashier { get; set; }

        [DataMember(Name = "receiptDate")]
        public string ReceiptDate { get; set; }

        [DataMember(Name = "tipSum")]
        public long TipSum { get; set; }

        [DataMember(Name = "totalSum")]
        public long TotalSum { get; set; }

        [DataMember(Name = "taxSum")]
        public float TaxSum { get; set; }

        [DataMember(Name = "serviceChargesSum")]
        public long ServiceChargesSum { get; set; }

        [DataMember(Name = "amountToPay")]
        public long AmountToPay { get; set; }
        
        [DataMember(Name = "ticketNumber")]
        public string TicketNumber { get; set; }

        [DataMember(Name = "items")]
        public List<ReceiptItemModel> Items { get; set; }

        [DataMember(Name = "payment")]
        public ReceiptPaymentModel Payment { get; set; }
    }
}