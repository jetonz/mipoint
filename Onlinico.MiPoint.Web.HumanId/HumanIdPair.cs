﻿namespace Onlinico.MiPoint.Web.HumanId
{
    public struct HumanIdPair
    {
        public string Adjective { get; }

        public string Noun { get; }

        public HumanIdPair(string adjective, string noun)
        {
            Adjective = adjective;
            Noun = noun;
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}", Adjective, Noun);
        }

        public string ToString(string format)
        {
            return string.Format(format, Adjective, Noun);
        }
    }
}
