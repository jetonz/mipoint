﻿using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2.Mapper
{
    public static class LocationMapper
    {
        public static Models.Api.v2.LocationModel ToApiLocationModel(this Domain.Models.Api.v2.LocationModel model)
        {
            return new Models.Api.v2.LocationModel
            {
                Id = model.Identifier,
                Name = model.Name,
                Phone = model.Phone,
                Address = model.Address,
                AddressFull = model.AddressFull,
                Owner = model.Owner
            };
        }

        public static Models.Api.v2.LocationSettingsModel ToApiLocationSettingsModel(this Domain.Models.Api.v2.LocationSettingsModel model)
        {
            return new Models.Api.v2.LocationSettingsModel
            {
                OmnivoreId = model.OmnivoreId,
                TaxRate = model.TaxRate,
                Tips = model.Tips,
                TenderTypes = model.TenderTypes.ToDictionary(k=>k.Card.Type, v=>v.Type),
                AfterPaymentLogOut = model.AfterPaymentLogOut,
                SessionTimeout = model.SessionTimeout,
                OpenOrderLimit = model.OpenOrderLimit,
                ClosedOrderLimit = model.ClosedOrderLimit,
            };
        }
    }
}