﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v2
{
    public interface ILocationService
    {
        LocationSettingsModel GetSettings(string locationId);

        ResultModel<Domain.Models.Api.v2.LocationModel> Get(string locationId);

        Task<ResultModel<Domain.Models.Api.v2.LocationModel>> GetAsync(string locationId);

        Task<ResultModel<EmployeeModel>> GetEmployeeAsync(string locationId, string login);

        ResultModel<List<MappingModel>> GetMappingSchemes(string locationId);
    }
}
