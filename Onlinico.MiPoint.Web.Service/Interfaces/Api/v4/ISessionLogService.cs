﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v4
{
    public interface ISessionLogService : IBaseService
    {
        Task<ResultModel> AddAsync(LogModel model);

        ResultModel<List<LogModel>> Get();

        ResultModel<LogModel> Get(Guid logId);
    }
}
