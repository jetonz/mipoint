﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v2;
using WebGrease.Css.Extensions;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class SettingsMapper
    {
        public static Models.Web.LocationSettingsModel ToSettingsModel(this Domain.Models.Web.LocationSettingsModel settings, int invertTimeOffset = 0)
        {
            var syncDateTime = new DateTime(settings.SyncTime.Ticks).AddYears(1);

            return new Models.Web.LocationSettingsModel
            {
                Id = settings.Id,
                //TaxRate = settings.TaxRate,
                //Tips = string.Join(",", settings.Tips),
                SyncTime = syncDateTime.AddMinutes(invertTimeOffset).TimeOfDay.ToString(),
                SyncPeriod = settings.SyncPeriod,
                SessionTimeout = settings.SessionTimeout,
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }

        public static Models.Web.LocationSettingsModel ToSettingsModel(this Domain.Models.Web.LocationSettingsModel settings, List<AvailableTenderTypeModel> avalibleTenderTypes, int invertTimeOffset = 0)
        {
            var syncDateTime = new DateTime(settings.SyncTime.Ticks).AddYears(1);
            
            return new Models.Web.LocationSettingsModel
            {
                Id = settings.Id,
                //TaxRate = settings.TaxRate,
                //Tips = string.Join(",", settings.Tips),
                TenderTypes = settings.TenderTypes.ToViewTenderTypes(avalibleTenderTypes),
                SyncTime = syncDateTime.AddMinutes(invertTimeOffset).TimeOfDay.ToString(),
                SyncPeriod = settings.SyncPeriod,
                SessionTimeout = settings.SessionTimeout,
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }

        public static Domain.Models.Web.LocationSettingsModel ToDomainSettingsModel(this Models.Web.LocationSettingsModel settings)
        {
            var syncDateTime = DateTime.ParseExact(settings.SyncTime, "h:mm tt", CultureInfo.InvariantCulture);
            
            //var tips = new List<int>();
            //settings.Tips.Split(',').ForEach(s =>
            //{
            //    int tip;
            //    int.TryParse(s, out tip);
            //    if (tip != 0)
            //    {
            //        tips.Add(tip);
            //    }
            //});
            return new Domain.Models.Web.LocationSettingsModel
            {
                Id = settings.Id,
                //TaxRate = settings.TaxRate,
                //Tips = tips.ToArray(),
                TenderTypes = settings.TenderTypes.ToDomainTenderTypes(),
                SyncTime = syncDateTime.AddMinutes(settings.TimeOffset).TimeOfDay,
                SyncPeriod = settings.SyncPeriod,
                SessionTimeout = settings.SessionTimeout,
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }
    }
}