﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v4
{
    public interface ILocationService : IBaseService
    {
        Task<ResultModel<LocationSettingsModel>> GetSettingsAsync(string locationId);

        Task<ResultModel<ClientSettingsModel>> GetClientSettingsAsync(string locationId);

        ResultModel<LocationModel> Get(string locationId);

        Task<ResultModel<LocationModel>> GetAsync(string locationId);

        Task<ResultModel<EmployeeModel>> GetEmployeeAsync(string locationId, string login);

        ResultModel<List<MappingModel>> GetMappingSchemes(string locationId);
    }
}
