﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class Log
    {
        [Key]
        public Guid Id { get; set; }

        public string LocationId { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string Fingerprint { get; set; }

        public string Serial { get; set; }

        public string Content { get; set; }

        [Index]
        public DateTime Created { get; set; }

        [MaxLength(10)]
        public string AppVersion { get; set; }
    }
}
