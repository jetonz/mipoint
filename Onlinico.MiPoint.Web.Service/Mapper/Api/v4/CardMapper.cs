﻿using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class CardMapper
    {
        public static CardModel ToDomainCardModel(this Data.DataModel.Card card)
        {
            return new CardModel
            {
                Id = card.Id,
                Name = card.Name,
                Type = card.Type
            };
        }

        public static Data.DataModel.Card ToEntityCard(this CardModel card)
        {
            return new Data.DataModel.Card
            {
                Id = card.Id,
                Name = card.Name,
                Type = card.Type
            };
        }
    }
}
