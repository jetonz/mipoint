﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Quartz;
using IHistoryService = Onlinico.MiPoint.Web.Service.Interfaces.Web.IHistoryService;

namespace Onlinico.MiPoint.Web.Api.Jobs
{
    public class DataRotationJob : IJob
    {
        private ILogger _logger;
        private IConfiguration _configuration;
        private IHistoryService _historyService;
        private ISessionLogService _sessionLogService;

        public async void Execute(IJobExecutionContext context)
        {
            ResolveDependencies(context);

            _logger.WriteMessageToLog("Job DataRotationJob started to execute.");

            try
            {
                //API Activity
                var removeOldActivityResult = await _historyService.RemoveOldRecordsAsync(GetStartPoint(_configuration.ActivityRetentionPeriodInDays));
                _logger.WriteMessageToLog(removeOldActivityResult.Succeed ? "Removal old activity records was successful" : removeOldActivityResult.Error);

                var activityExpiredPoint = GetStartPoint(_configuration.ActivityContentRetentionPeriodInDays);
                var removeOldActivityContentResult = await _historyService.RemoveOldContentAsync(activityExpiredPoint.AddDays(-1).Date, activityExpiredPoint);
                _logger.WriteMessageToLog(removeOldActivityContentResult.Succeed ? "Removal old activity content was successful" : removeOldActivityContentResult.Error);


                //Session log
                var removeOldSessionLogResult = await _sessionLogService.RemoveOldRecordsAsync(GetStartPoint(_configuration.SessionLogRetentionPeriodInDays));
                _logger.WriteMessageToLog(removeOldSessionLogResult.Succeed ? "Removal old session log records was successful" : removeOldSessionLogResult.Error);

                var logExpiredPoint = GetStartPoint(_configuration.SessionLogContentRetentionPeriodInDays);
                var removeOldSessionLogContentResult = await _sessionLogService.RemoveOldContentAsync(logExpiredPoint.AddDays(-1).Date, logExpiredPoint);
                _logger.WriteMessageToLog(removeOldSessionLogContentResult.Succeed ? "Removal old session log content was successful" : removeOldSessionLogContentResult.Error);
            }
            catch (Exception ex)
            {
                _logger.WriteMessageToLog($"Unexpected critical error occurred when we execute DataRotationJob. Error: {ex.Message}, InnerException: {ex.InnerException?.InnerException?.Message}");
            }
        }

        private DateTime GetStartPoint(int retentionPeriodInDays)
        {
            return DateTime.UtcNow.AddDays(retentionPeriodInDays * -1);
        }

        private void ResolveDependencies(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.MergedJobDataMap;

            IContainer container = (IContainer)dataMap["Container"];

            _logger = container.Resolve<ILogger>();
            _configuration = container.Resolve<IConfiguration>();
            _historyService = container.Resolve<IHistoryService>();
            _sessionLogService = container.Resolve<ISessionLogService>();
        }
    }
}