﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v1
{
    public interface IMerchantService
    {
        List<MerchantModel> Get();

        MerchantModel Get(Guid merchantId);

        List<MerchantItemModel> GetMerchantItemsForUser(string userId);

        ResultModel RelateMerchantsWithUser(List<Guid> merchants, string userId);

        List<MerchantModel> GetForUser(string userId);

        ResultModel<MerchantModel> Update(MerchantModel model);

        Task<ResultModel> UpdateMerchantLocationsAsync(Guid merchantId, List<LocationModel> locations, string dictionaryPath);

        Task<ResultModel<MerchantModel>> AddAsync(MerchantModel model, string userId, string dictionaryPath);

        ResultModel RemoveMerchantsFromUser(string userId);

        Task<ResultModel> RemoveAsync(Guid id);
    }
}
