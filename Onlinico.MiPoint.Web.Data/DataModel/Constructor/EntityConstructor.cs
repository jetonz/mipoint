﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Data.DataModel.Constructor
{
    public class EntityConstructor
    {
        public static LocationSettings CreateLocationSettings(List<TenderType> tenderTypes, TimeSpan syncTime)
        {
            return new LocationSettings
            {
                Id = Guid.NewGuid(),
                TaxRate = 0,
                Tips = "15, 18, 20, 30",
                TenderTypes = tenderTypes,
                SyncTime = syncTime,
                SessionTimeout = 180,
                AfterPaymentLogOut = false,
                OpenOrderLimit = 200,
                ClosedOrderLimit = 25,
            };
        }

        public static ClientCounterModeSettings CreateClientCounterModeSettings()
        {
            return new ClientCounterModeSettings
            {
                Id = Guid.NewGuid(),
                DisplayLineItemsOnReceipt = false,
                ShowReceiptScreen = true,
                PrintReceiptEnabled = true,
                SendReceiptMessageEnabled = true,
                SendReceiptEmailEnabled = true,
                ShowThanksSreen = true,
                ThanksMessage = "Thank you for visiting {0}.",
                WelcomeMessage = null,
                TipSuggestions = "15, 18, 20, 25",
                ShowTipsScreen = true,
                RequireSignature = true,
                AutoPrintReceipt = false,
                ShowTipLineOnReceipt = true,
                ShowSignatureLineOnReceipt = false,
            };
        }

        public static ClientTableModeSettings CreateClientTableModeSettings()
        {
            return new ClientTableModeSettings
            {
                Id = Guid.NewGuid(),
                DisplayLineItemsOnReceipt = false,
                ShowReceiptScreen = true,
                PrintReceiptEnabled = true,
                SendReceiptMessageEnabled = true,
                SendReceiptEmailEnabled = true,
                TipSuggestions = "15, 18, 20, 25",
                ShowTipsScreen = true,
                RequireSignature = true,
                AutoPrintReceipt = false,
                OnlyMyOrdersFilterEnabled = true,
                AfterPaymentLogOut = true,
                SessionTimeOut = 60,
                SessionTimeOutEnabled = true,
                ShowTipLineOnReceipt = true,
                ShowSignatureLineOnReceipt = false,
            };
        }
    }
}
