using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using Onlinico.MiPoint.Web.Data.Migrations;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public static class Initializer
    {
        public static void SetDatabaseInitializer()
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MiPointDbContext, Configuration>());
        }
    }


    public partial class MiPointDbContext : IdentityDbContext<User>
    {
        public MiPointDbContext() : base("MiPointDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MiPointDbContext, Data.Migrations.Configuration>("MiPointDbContext"));
        }

        public static MiPointDbContext Create()
        {
            return new MiPointDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //this.Configuration.ProxyCreationEnabled = true;
            //this.Configuration.LazyLoadingEnabled = true;

            //Merchant

            modelBuilder.Entity<Merchant>()
                .HasMany(m => m.Locations)
                .WithRequired(l => l.Merchant)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Merchant>()
                .HasMany(m => m.Invites)
                .WithRequired(i => i.Merchant)
                .WillCascadeOnDelete(true);

            //Location

            modelBuilder.Entity<Location>()
                .HasOptional(l => l.Settings)
                //.WithRequired(s => s.Location)
                .WithOptionalDependent(l => l.Location)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Location>()
                .HasOptional(l => l.ClientCounterModeSettings)
                .WithRequired(l => l.Location)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Location>()
                .HasOptional(l => l.ClientTableModeSettings)
                .WithRequired(l => l.Location)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Location>()
                .HasMany(l => l.Histories)
                .WithRequired(h => h.Location)
                //.WithOptional(h => h.Location)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Location>()
                .HasMany(l => l.Employees)
                .WithRequired(h => h.Location)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Location>()
                .HasMany(l => l.Payments)
                .WithRequired(h => h.Location)
                .WillCascadeOnDelete(true);

            //LocationSettings

            modelBuilder.Entity<LocationSettings>()
                .HasMany(s => s.TenderTypes)
                .WithRequired(t => t.Settings)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<LocationSettings>()
                .HasMany(s => s.MappingSchemes)
                .WithRequired(t => t.Settings)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<LocationSettings>()
                .HasMany(s => s.AvailableTenderTypes)
                .WithRequired(t => t.Settings)
                .WillCascadeOnDelete(true);

            //Receipt

            modelBuilder.Entity<Receipt>()
                .HasOptional(r => r.Payment)
                .WithRequired(p => p.Receipt)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Receipt>()
                .HasMany(r => r.Items)
                .WithRequired(p => p.Receipt)
                .WillCascadeOnDelete(true);


            //Card

            modelBuilder.Entity<Card>()
                .HasMany(c => c.TenderTypes)
                .WithRequired(t => t.Card);

            //Payment
            modelBuilder.Entity<Payment>()
                .HasMany(c => c.Items)
                .WithRequired(t => t.Payment)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Payment>()
                .HasOptional(p=>p.ReceiptInfo)
                .WithRequired(t => t.Payment)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Payment>()
                .HasOptional(p => p.Transaction)
                .WithRequired(t => t.Payment)
                .WillCascadeOnDelete(true);

            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Merchant> Merchants { get; set; }
        public virtual DbSet<History> Histories { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationSettings> LocationSettings { get; set; }
        public virtual DbSet<TenderType> TenderTypes { get; set; }
        public virtual DbSet<Card> Cards { get; set; }
        public virtual DbSet<Invite> Invites { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<MappingScheme> MappingSchemes { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentItem> PaymentItems { get; set; }
        public virtual DbSet<PaymentReceiptInfo> PaymentReceiptInfos { get; set; }
        public virtual DbSet<PaymentTransaction> PaymentTransactions { get; set; }
        public virtual DbSet<AvailableTenderType> AvailableTenderTypes { get; set; }

        public virtual DbSet<Log> Logs { get; set; }

        public virtual DbSet<Receipt> Receipts { get; set; }
        public virtual DbSet<ReceiptItem> ReceiptItems { get; set; }
        public virtual DbSet<ReceiptPayment> ReceiptPayments { get; set; }

    }
}
