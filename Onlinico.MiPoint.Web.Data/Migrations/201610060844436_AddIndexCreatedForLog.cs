namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIndexCreatedForLog : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Logs", "Created");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Logs", new[] { "Created" });
        }
    }
}
