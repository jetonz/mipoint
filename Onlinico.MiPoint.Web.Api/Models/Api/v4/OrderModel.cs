﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4
{
    public class OrderModel
    {
        // "{order_number}-{order_open_time}"
        public string Id { get; set; }

        public string EmployeeId { get; set; }

        public int Number { get; set; }

        public int GuestCount { get; set; }

        public string Name { get; set; }
        
        public Enums.OrderStatusEnum Status { get; set; }
        
        public Enums.OrderPaymentStatusEnum PaymentsStatus { get; set; }
        
        public DateTime OpenedAt { get; set; }

        public DateTime? ClosedAt { get; set; }

        public bool HasPayments { get; set; }

        public OrderTotalModel Total { get; set; }

        public OrderTableModel Table { get; set; }

        public List<OrderItemModel> Items { get; set; }
        
        public List<DiscountModel> Discounts { get; set; } 
    }

    public class ShortOrderModel
    {
        // "{order_number}-{order_open_time}"
        public string Id { get; set; }

        public string EmployeeId { get; set; }

        public int Number { get; set; }

        public string Name { get; set; }

        public Enums.OrderStatusEnum Status { get; set; }

        public Enums.OrderPaymentStatusEnum PaymentsStatus { get; set; }

        public int GuestCount { get; set; }

        public DateTime OpenedAt { get; set; }

        public DateTime? ClosedAt { get; set; }

        public OrderTotalModel Total { get; set; }

        public OrderTableModel Table { get; set; }
    }

    public class OrderTotalModel
    {
        public long Due { get; set; }

        public long OtherCharges { get; set; }

        public long ServiceCharges { get; set; }

        public long ItemsCost { get; set; }

        public long Tax { get; set; }

        public long Total { get; set; }
    }

    public class OrderItemModel
    {
        public string Id { get; set; }

        public bool HasDiscounts { get; set; }

        public string Name { get; set; }

        public string Comment { get; set; }

        public long Sum { get; set; }

        public long? Tax { get; set; }

        public long Price { get; set; }

        public long Quantity { get; set; }

        public List<DiscountModel> Discounts { get; set; } 
    }

    public class DiscountModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public long Value { get; set; }
    }

    public class OrderTableModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool IsAvailable { get; set; }

        public long Number { get; set; }

        public long Seats { get; set; }
    }
}