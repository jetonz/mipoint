﻿using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Global;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Global
{
    public interface ISmsService
    {
        Task<ResultModel> SendAsync(string phoneNumber, string message);
    }
}
