﻿using System;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v4
{
    public abstract class BaseService : IBaseService
    {
        private bool _disposed;
        protected ILogger Logger;
        protected MiPointDbContext Context;

        protected BaseService(ILogger logger)
        {
            Logger = logger;
            Context = GetContextInstance();
        }

        private static MiPointDbContext GetContextInstance()
        {
            return new MiPointDbContext();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
