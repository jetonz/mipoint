﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    public class SessionLogController : BaseController
    {
        private readonly ISessionLogService _logService;

        public SessionLogController(ISessionLogService logService, IConfiguration configuration) : base(configuration)
        {
            _logService = logService;
        }

        public ActionResult SessionLogs()
        {
            return View();
        }

        /// <summary>
        /// Get session log rows
        /// </summary>
        /// <param name="start">Start datetime</param>
        /// <param name="end">End datetime</param>
        /// <param name="timezoneOffset">Client offset</param>
        /// <returns></returns>
        public async Task<ActionResult> SessionLogRows(DateTime start, DateTime end, int timezoneOffset = 0)
        {
            var inverOffset = timezoneOffset * -1;
            var logs = _logService.Get(start.AddMinutes(timezoneOffset), end.AddMinutes(timezoneOffset)).Object;
            return View(logs?.ToLogModels(inverOffset));
        }

        public ActionResult SessionLog(Guid logId, int timezoneOffset = 0)
        {
            var inverOffset = timezoneOffset * -1;

            var log = _logService.Get(logId).Object;
            return View(log?.ToLogModel(inverOffset));
        }
    }
}