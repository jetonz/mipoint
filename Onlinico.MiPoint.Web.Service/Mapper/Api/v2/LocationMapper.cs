﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Location = Onlinico.MiPoint.Web.Service.Models.Omnivore.Locations.Location;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v2
{
    public static class LocationMapper
    {
        //public static List<Domain.Models.Api.v2.LocationModel> ToDomainLocations(this List<Location> omnivoreLocations)
        //{
        //    return omnivoreLocations.Select(l => new Domain.Models.Api.v2.LocationModel
        //    {
        //        Identifier = l.id,
        //        Name = l.name,
        //        Owner = l.owner,
        //        IsDev = l.development,
        //        PosType = l.pos_type
        //    }).ToList();
        //}

        //public static List<Domain.Models.Api.v2.LocationModel> ToDomainLocations(this ICollection<Data.DataModel.Location> locations)
        //{
        //    return locations.Select(l => new Domain.Models.Api.v2.LocationModel
        //    {
        //        Id = l.Id,
        //        Identifier = l.Identifier,
        //        Name = l.Name,
        //        Owner = l.Owner,
        //        IsDev = l.IsDev,
        //        PosType = l.PosType
        //    }).ToList();
        //}

        public static Domain.Models.Api.v2.LocationModel ToDomainLocation(this Data.DataModel.Location location)
        {
            return new Domain.Models.Api.v2.LocationModel
            {
                Id = location.Id,
                Identifier = location.Identifier,
                Name = location.Name,
                Phone = location.Phone,
                Address = location.Address,
                AddressFull = location.AddressFull,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType
            };
        }

        //public static Data.DataModel.Location ToNewLocationEntity(this Domain.Models.Api.v2.LocationModel location)
        //{
        //    return new Data.DataModel.Location
        //    {
        //        Identifier = location.Identifier,
        //        Name = location.Name,
        //        Owner = location.Owner,
        //        IsDev = location.IsDev,
        //        PosType = location.PosType,
        //    };
        //}

        //public static Data.DataModel.Location ToNewLocationEntity(this Domain.Models.Api.v2.LocationModel location, string id, List<Data.DataModel.Card> cards)
        //{
        //    return new Data.DataModel.Location
        //    {
        //        Id = id,
        //        Identifier = location.Identifier,
        //        Name = location.Name,
        //        Owner = location.Owner,
        //        IsDev = location.IsDev,
        //        PosType = location.PosType,
        //        Settings = new LocationSettings
        //        {
        //            Id = Guid.NewGuid(),
        //            TaxRate = 0,
        //            Tips = "15, 18, 20, 30",
        //            TenderTypes = cards.Select(c=>new TenderType
        //            {
        //                Id = Guid.NewGuid(),
        //                Card = c
        //            }).ToList()
        //        }
        //    };
        //}

        //public static void UpdateLocationEntity(this Data.DataModel.Location location, Domain.Models.Api.v2.LocationModel domainLocation)
        //{
        //    location.Owner = domainLocation.Owner;
        //    location.Name = domainLocation.Name;
        //    location.IsDev = domainLocation.IsDev;
        //    location.PosType = domainLocation.PosType;
        //}
    }
}
