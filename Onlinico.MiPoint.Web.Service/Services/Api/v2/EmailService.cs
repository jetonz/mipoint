﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v2
{
    public class EmailService : BaseService, IEmailService
    {
        public EmailService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel> SendAsync(string address, string subject, string html)
        {
            var result = new ResultModel(true);
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(address));
                    message.Subject = subject;
                    message.Body = html;
                    message.IsBodyHtml = true;

                    await smtpClient.SendMailAsync(message);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }
            return result;
        }
    }
}
