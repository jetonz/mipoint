﻿namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class VerifyModel
    {
        public string ApiKey { get; set; }
        public string Fingerprint { get; set; }
    }
}
