﻿using System;
using System.Collections.Generic;
using System.Linq;
using MiPoint.Shared.Types.Dto;
using MiPoint.Shared.Types.Enums;
using Onlinico.MiPoint.Web.Data.DataModel;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.Agent.v1
{
    public static class ActivityMapper
    {
        public static List<History> ToHistories(this List<RequestLogDto> models, Location location)
        {
            return models.Select(m => m.ToHistoryEntity(location)).ToList();
        }

        public static History ToHistoryEntity(this RequestLogDto model, Location location)
        {
            int statusCode;
            int.TryParse(model.ResponseCode, out statusCode);

            return new History
            {
                Id = Guid.NewGuid(),
                Location = location,
                Agent = model.UserAgent,
                DeviceId = model.DeviceId,
                Start = model.Created,
                End = DateTime.UtcNow,
                Method = Enum.GetName(typeof(RequestMethodEnum), model.Method),
                StatusCode = statusCode,
                Uri = model.EndPoint,

                RequestHeaders = model.RequestHeaders,
                RequestContent = model.RequestContent,
                ResponseHeaders = model.ResponseHeaders,
                ResponseContent = model.ResponseContent,
            };
        }
    }
}
