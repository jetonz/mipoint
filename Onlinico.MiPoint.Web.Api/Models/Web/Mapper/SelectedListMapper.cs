﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class SelectedListMapper
    {
        public static List<SelectedItem<LocationModel>> ToSelectedItems(this List<LocationModel> models)
        {
            return models.Select(l => new SelectedItem<LocationModel>
            {
                Item = l,
                Selected = false,
            }).ToList();
        }

        public static List<SelectedItem<MerchantModel>> ToSelectedItems(this List<MerchantModel> models)
        {
            return models.Select(l => new SelectedItem<MerchantModel>
            {
                Item = l,
                Selected = false,
            }).ToList();
        }

        public static List<SelectedItem<MerchantModel>> ToSelectedItems(this List<MerchantModel> models, List<MerchantModel> selected)
        {
            return models.Select(l => new SelectedItem<MerchantModel>
            {
                Item = l,
                Selected = selected.Any(t=>t.Id == l.Id),
            }).ToList();
        }
    }
}