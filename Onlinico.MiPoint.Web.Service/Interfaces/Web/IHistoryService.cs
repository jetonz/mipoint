﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface IHistoryService
    {
        ResultModel<List<HistoryModel>> Get(DateTime start, DateTime end);

        ResultModel<HistoryModel> Get(Guid historyId);

        ResultModel<string> GetContent(Guid historyId);

        Task<ResultModel> RemoveOldRecordsAsync(DateTime star);

        Task<ResultModel> RemoveOldContentAsync(DateTime star, DateTime end);
    }
}
