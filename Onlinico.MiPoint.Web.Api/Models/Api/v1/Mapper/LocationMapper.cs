﻿namespace Onlinico.MiPoint.Web.Api.Models.Api.v1.Mapper
{
    public static class LocationMapper
    {
        public static v1.LocationSettingsModel ToApiLocationSettingsModel(
            this Domain.Models.Api.v1.LocationSettingsModel model)
        {
            return new v1.LocationSettingsModel
            {
                TenderTypeId = model.TenderTypeId,
                TenderType = model.TenderType,
                TaxRate = model.TaxRate,
                Tips = model.Tips
            };
        }
    }
}