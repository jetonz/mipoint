﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v4
{
    public class ClientSettingsModel
    {
        public Guid ServerId { get; set; }

        public MerchantInfoModel MerchantInfo { get; set; }

        public TableModeSettingsModel TableModeSettings { get; set; }

        public CounterModeSettingsModel CounterModeSettings { get; set; }
    }

    public class TableModeSettingsModel
    {
        public bool DisplayLineItemsOnReceipt { get; set; }

        public bool ShowReceiptScreen { get; set; }

        public bool PrintReceiptEnabled { get; set; }

        public bool SendReceiptMessageEnabled { get; set; }

        public bool SendReceiptEmailEnabled { get; set; }

        public IEnumerable<short> TipSuggestions { get; set; }//public 15, 18, 20, 25

        public bool ShowTipsScreen { get; set; }

        public bool RequireSignature { get; set; }

        public bool AutoPrintReceipt { get; set; }

        public bool OnlyMyOrdersFilterEnabled { get; set; }

        public bool AfterPaymentLogOut { get; set; }

        public int SessionTimeOut { get; set; }

        public bool SessionTimeOutEnabled { get; set; }

        public bool ShowTipLineOnReceipt { get; set; }

        public bool ShowSignatureLineOnReceipt { get; set; }

        public int OpenOrderLimit { get; set; }

        public int ClosedOrderLimit { get; set; }
    }

    public class CounterModeSettingsModel
    {
        public bool DisplayLineItemsOnReceipt { get; set; }

        public bool ShowReceiptScreen { get; set; }

        public bool PrintReceiptEnabled { get; set; }

        public bool SendReceiptMessageEnabled { get; set; }

        public bool SendReceiptEmailEnabled { get; set; }

        public bool ShowThanksSreen { get; set; }

        public string ThanksMessage { get; set; }

        public string WelcomeMessage { get; set; }

        public IEnumerable<short> TipSuggestions { get; set; }//public 15, 18, 20, 25

        public bool ShowTipsScreen { get; set; }

        public bool RequireSignature { get; set; }

        public bool AutoPrintReceipt { get; set; }

        public bool ShowTipLineOnReceipt { get; set; }

        public bool ShowSignatureLineOnReceipt { get; set; }
    }

    public class MerchantInfoModel
    {
        public string MerchantName { get; set; }

        public string PhoneNumber { get; set; }

        public string Website { get; set; }

        public string LogoUrl { get; set; }

        public string Address { get; set; }
    }
}
