﻿using System;

namespace Onlinico.MiPoint.Web.HumanId
{
    public class HumanIdEventArgs : EventArgs
    {
        public HumanIdPair NewId { get; }

        public bool IsUnique { get; set; }

        public HumanIdEventArgs(HumanIdPair newId)
        {
            NewId = newId;
            IsUnique = false;
        }
    }
}
