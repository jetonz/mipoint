﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v4
{
    public class PaymentToPayModel
    {
        public ThirdPartyPaymentModel PThirdPartyayment { get; set; }

        public PayCredentialsModel Credentials { get; set; }
    }

    public class PayCredentialsModel
    {
        public string ApiKey { get; set; }

        public string LocationId { get; set; }

        public string TicketId { get; set; }
    }
}
