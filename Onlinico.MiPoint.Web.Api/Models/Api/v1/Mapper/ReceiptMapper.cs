﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v1.Mapper
{
    public static class ReceiptMapper
    {
        public static Domain.Models.Api.v1.ReceiptModel ToDomainReceiptModel(this v1.ReceiptModel receipt)
        {
            return new Domain.Models.Api.v1.ReceiptModel
            {
                Created = DateTime.UtcNow,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,

                Payment = receipt.Payment.ToDomainPaymentModel(),
                Items = receipt.Items.ToDomainReceiptItemModels(),
            };
        }

        public static List<Domain.Models.Api.v1.ReceiptItemModel> ToDomainReceiptItemModels(this IEnumerable<v1.ReceiptItemModel> items)
        {
            return items.Select(i => new Domain.Models.Api.v1.ReceiptItemModel
            {
                Id = Guid.NewGuid(),
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Domain.Models.Api.v1.PaymentModel ToDomainPaymentModel(this v1.PaymentModel payment)
        {
            return new Domain.Models.Api.v1.PaymentModel
            {
                Id = Guid.NewGuid(),
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = "SALE",
                Cvm = "SIGNATURE VERIFIED",
            };
        }

    }
}