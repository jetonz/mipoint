﻿namespace Onlinico.MiPoint.Web.Api.Models.Api.v1.Validation
{
    public static class ReceiptValidator
    {
        public static bool IsValid(this v1.ReceiptModel receipt)
        {
            return !string.IsNullOrEmpty(receipt?.ReceiptType) 
                && (receipt.ReceiptType.ToLower() == "email" || receipt.ReceiptType.ToLower() == "sms")
                && !string.IsNullOrEmpty(receipt.Destination);
        }
    }
}