﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Core.Interfaces;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Base controller with authorize attribute
    /// </summary>
    [System.Web.Mvc.Authorize]
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// App configuration
        /// </summary>
        protected readonly IConfiguration Configuration;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration">Inject configuration</param>
        protected BaseController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Is current user admin
        /// </summary>
        protected bool IsAdmin => User.IsInRole(Configuration.AdminRole);

        protected Uri CurrentDomain => new Uri(Request.Url?.Scheme + System.Uri.SchemeDelimiter + Request.Url?.Host + (Request.Url?.IsDefaultPort ?? false ? "" : ":" + Request.Url?.Port));
    }
}