﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class TenderTypeModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Card { get; set; }


        public List<SelectListItem> AvailableValues { get; set; }
    }
}