﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class LocationSettings
    {
        public LocationSettings()
        {
            TenderTypes = new HashSet<TenderType>();

            MappingSchemes = new HashSet<MappingScheme>();
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Tips { get; set; }

        [Required]
        public float TaxRate { get; set; }
        
        public TimeSpan SyncTime { get; set; }

        public TimeSpan SyncPeriod { get; set; }

        public bool AfterPaymentLogOut { get; set; }

        // Session timeout in seconds
        public int SessionTimeout { get; set; }

        public int OpenOrderLimit { get; set; }

        public int ClosedOrderLimit { get; set; }


        public virtual Location Location { get; set; }
        
        public virtual ICollection<TenderType> TenderTypes { get; set; }

        public virtual ICollection<AvailableTenderType> AvailableTenderTypes { get; set; }

        public virtual ICollection<MappingScheme> MappingSchemes { get; set; }
    }
}
