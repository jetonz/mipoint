﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums
{
    public enum OrderStatusEnum
    {
        Closed = 0,
        Open = 1,
    }

    public enum OrderPaymentStatusEnum
    {
        Open = 0,
        PartiallyPaid = 1,
        Closed = 2,

        PartiallyPaidPending = 3,
        ClosedPending = 4,
    }
}