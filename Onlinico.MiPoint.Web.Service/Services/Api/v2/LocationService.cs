﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v2;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v2;
using Onlinico.MiPoint.Web.Service.Mapper.Global.MappingScheme;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v2
{
    public class LocationService : BaseService, ILocationService
    {
        private readonly IMemoryCacher _memoryCacher;

        public LocationService(ILogger logger, IMemoryCacher memoryCacher) : base(logger)
        {
            _memoryCacher = memoryCacher;
        }

        public async Task<ResultModel<EmployeeModel>> GetEmployeeAsync(string locationId, string login)
        {
            var result = new ResultModel<EmployeeModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var employee = await context.Employees.Include(e => e.Location).FirstOrDefaultAsync(e => e.Location.Id == locationId && e.Login == login && !e.Location.IsRemoved);

                    result.Object = employee?.ToDomainEmployeeModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<EmployeeModel>(false, ex.Message);
            }

            return result;
        }

        public ResultModel<Domain.Models.Api.v2.LocationModel> Get(string locationId)
        {
            var result = new ResultModel<Domain.Models.Api.v2.LocationModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    result.Object = location?.ToDomainLocation();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<LocationModel>(false, ex.Message);
            }
            return result;
        }

        public async Task<ResultModel<Domain.Models.Api.v2.LocationModel>> GetAsync(string locationId)
        {
            var result = new ResultModel<Domain.Models.Api.v2.LocationModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == locationId && !l.IsRemoved);
                    result.Object = location?.ToDomainLocation();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<LocationModel>(false, ex.Message);
            }
            return result;
        }

        public Domain.Models.Api.v2.LocationSettingsModel GetSettings(string locationId)
        {
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var offlineIdentifiers = Enum.GetNames(typeof(AgentIdentifiersEnum)).ToList();

                    var location = context.Locations
                        .Include(l => l.Settings.TenderTypes)
                        .FirstOrDefault(l => l.Id == locationId && !l.IsRemoved && !offlineIdentifiers.Contains(l.Identifier));
                    return location?.Settings?.ToDomainSettings(location.Id, location.Identifier);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return null;
        }

        public ResultModel<List<MappingModel>> GetMappingSchemes(string locationId)
        {
            var result = new ResultModel<List<MappingModel>>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    if (location != null)
                    {
                        if (location.Settings.MappingSchemes.Any())
                        {
                            result.Object = location.Settings.MappingSchemes.ToMappingScheme().ToList();
                        }
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid location.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
