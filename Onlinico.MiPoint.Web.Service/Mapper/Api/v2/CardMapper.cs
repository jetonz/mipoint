﻿namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v2
{
    public static class CardMapper
    {
        public static Domain.Models.Api.v2.CardModel ToDomainCardModel(this Data.DataModel.Card card)
        {
            return new Domain.Models.Api.v2.CardModel
            {
                Id = card.Id,
                Name = card.Name,
                Type = card.Type
            };
        }

        public static Data.DataModel.Card ToEntityCard(this Domain.Models.Api.v2.CardModel card)
        {
            return new Data.DataModel.Card
            {
                Id = card.Id,
                Name = card.Name,
                Type = card.Type
            };
        }
    }
}
