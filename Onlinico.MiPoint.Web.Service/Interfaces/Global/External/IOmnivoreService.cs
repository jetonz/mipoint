﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore;
using Onlinico.MiPoint.Web.Domain.Models.Global;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Global.External
{
    public interface IOmnivoreService
    {
        //Locations

        Task<ResultModel<LocationModel>> GetLocationAsync(string apiKey, string identifier, bool withEmployees = false);

        Task<ResultModel<List<LocationModel>>> GetLocationsByApiKey(string apiKey, bool withEmployees = false);

        Task<List<TenderTypeModel>> GetLocationTenderTypes(string apiKey, string locationsId);


        //Emploees

        Task<ResultModel<List<EmployeeModel>>> GetEmployees(string apiKey, string omnivoreLocationId);


        //Tickets

        Task<ResultModel<Models.Omnivore.Ticket.Ticket.RootObject>> GetTicketAsync(string apiKey, string locationId, string ticketId);

        Task<ResultModel<Models.Omnivore.Ticket.Tickets.RootObject>> GetTicketsAsync(string apiKey, string locationId, bool isOpen, int limit);


        //Payments

        Task<ResultModel<Models.Omnivore.Payment.Result.ThirdPartyPaymentResultModel>> SendThirdPartyPayment(string apiKey, string locationId, ThirdPartyPaymentModel model);
    }
}
