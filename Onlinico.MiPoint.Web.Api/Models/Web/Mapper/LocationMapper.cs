﻿using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class LocationMapper
    {
        public static List<LocationModel> ToLocationViewModelList(this List<Domain.Models.Web.LocationModel> locations)
        {
            return locations.Select(l => l.ToLocationViewModel()).ToList();
        }

        public static LocationModel ToLocationViewModel(this Domain.Models.Web.LocationModel model)
        {
            return new LocationModel
            {
                Id = model.Id,
                Identifier = model.Identifier,
                Name = model.Name,
                Owner = model.Owner,
                PosType = model.PosType,
                IsDev = model.IsDev,
                IsConfigured = model.IsConfigured,
                AgentFingerprint = model.AgentFingerprint,
                IsAgentActive = model.IsAgentActive,
                IsVicarActive = model.IsVicarActive,
                IsOffline = model.IsOffline,
            };
        }

        public static Domain.Models.Web.LocationModel ToDomainLocationModel(this OfflineLocationModel model)
        {
            return new Domain.Models.Web.LocationModel
            {
                Identifier = Domain.Enums.AgentIdentifiersEnum.Micros3700.ToString(),
                Name = model.Name,
                Owner = model.Owner,
                PosType = model.PosType,
                Phone = model.Phone,
                Address = model.Address,
                AddressFull = model.AddressFull,
            };
        }
    }
}