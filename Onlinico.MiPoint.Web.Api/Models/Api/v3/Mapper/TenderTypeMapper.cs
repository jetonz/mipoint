﻿using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper
{
    public static class TenderTypeMapper
    {
        public static List<TenderTypeModel> ToApiTenderTypes(
            this List<Domain.Models.Api.v3.TenderTypeModel> tenderTypes)
        {
            return tenderTypes.Select(t => new TenderTypeModel
            {
                Type = t.Type,
                Name = t.Name,
                Card = t.Card.Type
            }).ToList();
        } 
    }
}