﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v2
{
    public interface IPaymentService
    {
        Task<ResultModel<PaymentToPayModel>> GetPaymentToPay(Guid paymentId);

        Task<ResultModel<PaymentModel>> GetAsync(Guid paymentId);

        Task<ResultModel<List<PaymentModel>>> GetAsync(string locationId, PaymentStatusEnum paymentStatus);

        Task<ResultModel> AddAsync(string locationId, PaymentModel model);

        Task<ResultModel> UpdateStatusAsync(Guid paymentId, PaymentStatusEnum paymentStatus);

        Task<ResultModel> UpdateMessageAsync(Guid paymentId, string message);

        Task<ResultModel> DeleteAsync(Guid paymentId);

        //Task<ResultModel> UpdateAsync(PaymentModel model);
    }
}
