﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Onlinico.MiPoint.Web.Api.Hubs.Interfaces;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Api.Models.Web.Validation;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Merchant Controller
    /// </summary>
    public class MerchantController : BaseController
    {
        private readonly IMerchantService _merchantService;
        private readonly IOmnivoreService _omnivoreService;
        private readonly ILocationService _locationService;
        private readonly IAgentHubPusher _agentHubPusher;

        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="merchantService">Inject Merchant service</param>
        /// <param name="omnivoreService">Inject Omnivore service</param>
        /// <param name="locationService">Inject Location Service</param>
        /// <param name="agentHubPusher">Inject agent hub pusher</param>
        /// <param name="configuration">Inject Omnivore service</param>
        public MerchantController(IMerchantService merchantService, IOmnivoreService omnivoreService, ILocationService locationService, IAgentHubPusher agentHubPusher, Core.Interfaces.IConfiguration configuration) : base(configuration)
        {
            _merchantService = merchantService;
            _omnivoreService = omnivoreService;
            _locationService = locationService;
            _agentHubPusher = agentHubPusher;
        }

        /// <summary>
        /// Merchants page
        /// </summary>
        /// <returns>View</returns>
        public ActionResult Index()
        {
            ViewBag.IsAdmin = IsAdmin;

            return View();
        }

        /// <summary>
        /// Merchants partial page
        /// </summary>
        /// <returns>Partial View</returns>
        public ActionResult Merchants()
        {
            ViewBag.IsAdmin = IsAdmin;
            return View();
        }

        /// <summary>
        /// Get merchant datatable rows
        /// </summary>
        /// <returns></returns>
        public ActionResult MerchantRows()
        {
            ViewBag.IsAdmin = IsAdmin;

            var merchantsResult = IsAdmin
                ? _merchantService.Get()
                : _merchantService.GetForUser(User.Identity.GetUserId());

            return View(merchantsResult.Object?.ToMerchantViewModels());
        }

        /// <summary>
        /// Merchant partial page
        /// </summary>
        /// <param name="merchantId">Merchant id</param>
        /// <returns>Partial View</returns>
        public async Task<ActionResult> Users(Guid merchantId)
        {
            ViewBag.MerchantId = merchantId;
            var result = await _merchantService.GetUsersAsync(merchantId);
            return View(result.Object?.ToUserViewModelList());
        }
        
        [System.Web.Mvc.HttpGet]
        public async Task<ActionResult> MerchantForm()
        {
            var resultModel = new MerchantModel();

            var omnivoreProdLocationsResult = await _omnivoreService.GetLocationsByApiKey(Configuration.OmnivoreProdApiKey);
            var omnivoreDevLocationsResult = await _omnivoreService.GetLocationsByApiKey(Configuration.OmnivoreDevApiKey);
            if (omnivoreProdLocationsResult.Succeed)
            {
                if (omnivoreDevLocationsResult.Succeed && omnivoreDevLocationsResult.Object != null && omnivoreDevLocationsResult.Object.Any())
                {
                    omnivoreProdLocationsResult.Object.AddRange(omnivoreDevLocationsResult.Object);
                }

                var allLocations = omnivoreProdLocationsResult.Object.ToDomainLocations();

                var existLocationsResult = await _locationService.GetAsync();
                if (existLocationsResult.Succeed)
                {
                    var existinLocations = existLocationsResult.Object.Select(l => l.Identifier).ToList();

                    var locations = allLocations.Where(
                        l => existinLocations.All(ml => ml != l.Identifier)).ToList();

                    resultModel.SelectedLocations = locations.ToLocationViewModelList().ToSelectedItems();
                }
            }

            return View(resultModel);
        }

        /// <summary>
        /// Add new Merchant
        /// </summary>
        /// <param name="model">Merchant model</param>
        /// <param name="timeOffset">client time offset</param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> MerchantForm(MerchantModel model, [FromUri]int timeOffset = 0)
        {
            if (ModelState.IsValid)
            {
                var queue = new ConcurrentQueue<Domain.Models.Global.Omnivore.LocationModel>();

                if (model.Identifiers != null && model.Identifiers.Any())
                {
                    Task.WaitAll(model.Identifiers.Select(i =>
                    {
                        return _omnivoreService.GetLocationAsync(Configuration.OmnivoreProdApiKey, i, true).ContinueWith(
                            locationResult =>
                            {
                                if (locationResult.Result.Succeed && locationResult.Result.Object != null)
                                {
                                    queue.Enqueue(locationResult.Result.Object);
                                }
                                else
                                {
                                    var devLocationResult = _omnivoreService.GetLocationAsync(Configuration.OmnivoreDevApiKey, i, true).Result;
                                    if (devLocationResult.Succeed && devLocationResult.Object != null)
                                    {
                                        queue.Enqueue(devLocationResult.Object);
                                    }
                                }
                            });
                    }).ToArray());
                }
                
                string userId = null;
                if (!IsAdmin)
                {
                    userId = User.Identity.GetUserId();
                }
                var result = await _merchantService.AddAsync(model.ToDomainMerchantModel(), queue.ToList().ToDomainLocations(), userId, timeOffset);
                if (result.Succeed)
                {
                    var inviteController = DependencyResolver.Current.GetService<InviteController>();
                    inviteController.ControllerContext = ControllerContext;
                    try
                    {
                        await inviteController.InviteUser(new InviteModel
                        {
                            Email = result.Object.Email,
                            MerchantId = result.Object.Id
                        });
                    }
                    catch (Exception ex)
                    {
                        var testt = ex.Message;
                    }
                    

                    return PartialView("MerchantRow", result.Object?.ToMerchantViewModel());
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, result.Error);
                }

            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid data.");
        }

        /// <summary>
        /// Get edit merchant info page
        /// </summary>
        /// <param name="merchantId">merchant id</param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult EditMerchant(Guid merchantId)
        {
            var merchantResult = _merchantService.Get(merchantId);

            if (merchantResult.Succeed)
            {
                return View(merchantResult.Object?.ToMerchantViewModel());
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, merchantResult.Error);
            }
        }

        /// <summary>
        /// Post edit merchant info
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> EditMerchant(MerchantModel model)
        {
            var updateResult = _merchantService.Update(model.ToDomainMerchantModel());
            if (updateResult.Succeed)
            {
                ViewBag.IsAdmin = IsAdmin;

                _agentHubPusher.SendClientSettingsByMerchant(model.Id);
                return PartialView("MerchantRow", updateResult.Object?.ToMerchantViewModel());
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, updateResult.Error);
            }
        }

        /// <summary>
        /// Remove Merchant
        /// </summary>
        /// <param name="id">Merchant id</param>
        /// <returns></returns>
        [System.Web.Mvc.HttpDelete]
        public async Task<ActionResult> RemoveMerchant(Guid id)
        {
            var result = await _merchantService.RemoveAsync(id);
            return Json(result.Succeed, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Unlink user from merchant
        /// </summary>
        /// <param name="merchantId">Merchant id</param>
        /// <param name="userId">user id</param>
        /// <returns></returns>
        [System.Web.Mvc.HttpDelete]
        public async Task<ActionResult> UnlinckUser(Guid merchantId, string userId)
        {
            var result = await _merchantService.UnlinkUser(merchantId, userId);
            return Json(result.Succeed, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Synchronize merchant locations
        /// </summary>
        /// <param name="merchantId">merchant id</param>
        /// <param name="timeOffset">client time offset</param>
        /// <returns></returns>
        public async Task<JsonResult> SyncLocations(Guid merchantId, int timeOffset = 0)
        {
            var result = false;
            //var merchant = _merchantService.Get(merchantId);
            //if (merchant != null)
            //{
            //    var locationsResult = await _omnivoreService.GetLocationsByApiKey(merchant.OmnivoreApiKey, true);
            //    if (locationsResult.Succeed && locationsResult.Object != null)
            //    {
            //        var dictionaryPath = HttpContext.Server.MapPath("~/Dictionary.xml");
            //        var updateResult = await _merchantService.UpdateMerchantLocationsAsync(merchantId, locationsResult.Object.ToDomainLocations(), dictionaryPath, timeOffset);
            //        result = updateResult.Succeed;
            //    }
            //}
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}