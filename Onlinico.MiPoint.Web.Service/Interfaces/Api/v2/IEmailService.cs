﻿using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v2
{
    public interface IEmailService
    {
        Task<ResultModel> SendAsync(string address, string subject, string html);
    }
}
