﻿namespace Onlinico.MiPoint.Web.Domain.Models.Api.v1
{
    public class ResultModel
    {
        public ResultModel()
        {
            
        }

        public ResultModel(bool succeed)
        {
            Succeed = succeed;
        }

        public ResultModel(bool succeed, string error)
        {
            Succeed = succeed;
            Error = error;
        }

        public bool Succeed { get; set; }

        public string Error { get; set; }

        public string ObjectId { get; set; }
    }

    public class ResultModel<T>
    {
        public ResultModel()
        {

        }

        public ResultModel(bool succeed)
        {
            Succeed = succeed;
        }

        public ResultModel(bool succeed, string error)
        {
            Succeed = succeed;
            Error = error;
        }

        public bool Succeed { get; set; }

        public string Error { get; set; }

        public T Object { get; set; }
    }
}
