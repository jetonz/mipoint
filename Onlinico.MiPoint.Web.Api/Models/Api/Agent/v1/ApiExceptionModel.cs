﻿namespace Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1
{
    public class ApiExceptionModel
    {
        public string ErrorMessage { get; set; }

        public string InternalErrorMessage { get; set; }
    }
}