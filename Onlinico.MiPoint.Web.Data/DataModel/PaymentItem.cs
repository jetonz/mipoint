﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class PaymentItem
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(36)]
        public string ItemId { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        public int Quantity { get; set; }

        public long Price { get; set; }


        public virtual Payment Payment { get; set; }
    }
}
