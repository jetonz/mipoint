﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v4;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v4;
using Swashbuckle.Swagger.Annotations;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v4
{
    /// <summary>
    /// SessionLog controller
    /// </summary>
    [Attributes.Authorize]
    [RoutePrefix("api/v4/logs")]
    public class SessionLogController : BaseController
    {
        private readonly ISessionLogService _logService;

        /// <summary>
        /// Session log constructor
        /// </summary>
        /// <param name="logService">Inject log service</param>
        /// <param name="configuration">Inject configuration</param>
        public SessionLogController(ILogger logger, IConfiguration configuration, ISessionLogService logService) : base(logger, configuration)
        {
            _logService = logService;
        }

        /// <summary>
        /// Add MiPoint App Session Log
        /// </summary>
        /// <param name="model">Session log model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("device")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
        public async Task<IHttpActionResult> Post([FromBody]SessionLogModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _logService.AddAsync(model.ToDomainLogModel(Request.Properties[Configuration.Secret].ToString()));

                if (result.Succeed)
                    return Ok();

                throw CreateApiException(result.Error);
            }
            else
            {
                throw CreateApiException(ModelState);
            }
        }

        protected override void Dispose(bool disposing)
        {
            _logService.Dispose();
            base.Dispose(disposing);
        }
    }
}
