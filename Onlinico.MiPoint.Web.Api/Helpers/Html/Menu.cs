﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Onlinico.MiPoint.Web.Api.Helpers.Html
{
    public static class Menu
    {
        public static MvcHtmlString MenuLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName , object routValues, object htmlAttributes)
        {
            string currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            string currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");

            if (actionName == currentAction && controllerName == currentController)
            {
                var propertyClass = htmlAttributes.GetType().GetProperty("class")?.GetValue(htmlAttributes, null) ?? "";

                var propertyId = htmlAttributes.GetType().GetProperty("id")?.GetValue(htmlAttributes, null) ?? "";

                return htmlHelper.ActionLink(linkText, actionName, controllerName, routValues, new { @class = propertyClass + " selected", id = propertyId });
            }

            return htmlHelper.ActionLink(linkText, actionName, controllerName, routValues, htmlAttributes);


        }
    }
}