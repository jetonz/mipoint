﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v4
{
    public interface IReceiptService : IBaseService
    {
        Task<ResultModel<ReceiptModel>> GetAsync(string receiptId);

        Task<ResultModel<List<ReceiptModel>>> GetByTicketIdAsync(string locationId, string ticketId);

        Task<ResultModel> AddAsync(ReceiptModel model);
    }
}
