﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4
{
    [DataContract]
    public class EmployeeModel
    {
        public Guid Id { get; set; }
        
        [DataMember(Name = "id")]
        public string EmployeeId { get; set; }

        [DataMember(Name = "checkName")]
        public string CheckName { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "login")]
        public string Login { get; set; }
    }
}