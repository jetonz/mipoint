﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper
{
    public static class ReceiptMapper
    {
        public static List<Models.Api.v3.ReceiptModel> ToReceipts(
            this IEnumerable<Domain.Models.Api.v3.ReceiptModel> models)
        {
            return models.Select(r => r.ToReceiptModel()).ToList();
        } 

        public static Models.Api.v3.ReceiptModel ToReceiptModel(this Domain.Models.Api.v3.ReceiptModel receipt)
        {
            return new Models.Api.v3.ReceiptModel
            {
                TicketId = receipt.TicketId,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToPaymentModel(),
                Items = receipt.Items.ToReceiptItemModels(),
            };
        }

        public static List<Models.Api.v3.ReceiptItemModel> ToReceiptItemModels(this IEnumerable<Domain.Models.Api.v3.ReceiptItemModel> items)
        {
            return items.Select(i => new Models.Api.v3.ReceiptItemModel
            {
                Id = Guid.NewGuid(),
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Models.Api.v3.ReceiptPaymentModel ToPaymentModel(this Domain.Models.Api.v3.ReceiptPaymentModel payment)
        {
            return new Models.Api.v3.ReceiptPaymentModel
            {
                Id = Guid.NewGuid(),
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }



        public static Domain.Models.Api.v3.ReceiptModel ToDomainReceiptModel(this Models.Api.v3.ReceiptModel receipt, string locationId)
        {
            return new Domain.Models.Api.v3.ReceiptModel
            {
                TicketId = receipt.TicketId,
                LocationId = locationId,
                Created = DateTime.UtcNow,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToDomainPaymentModel(),
                Items = receipt.Items.ToDomainReceiptItemModels(),
            };
        }

        public static List<Domain.Models.Api.v3.ReceiptItemModel> ToDomainReceiptItemModels(this IEnumerable<Models.Api.v3.ReceiptItemModel> items)
        {
            return items.Select(i => new Domain.Models.Api.v3.ReceiptItemModel
            {
                Id = Guid.NewGuid(),
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Domain.Models.Api.v3.ReceiptPaymentModel ToDomainPaymentModel(this Models.Api.v3.ReceiptPaymentModel payment)
        {
            return new Domain.Models.Api.v3.ReceiptPaymentModel
            {
                Id = Guid.NewGuid(),
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }

    }
}