namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RebuildLocationIndex : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Histories", "Location_Id", "dbo.Locations");
            DropIndex("dbo.Histories", new[] { "Location_Id" });
            DropPrimaryKey("dbo.Locations");
            AlterColumn("dbo.Locations", "Id", c => c.String(nullable: false, maxLength: 36));
            AlterColumn("dbo.Locations", "Identifier", c => c.String(nullable: false, maxLength: 36));
            AlterColumn("dbo.Histories", "Location_Id", c => c.String(nullable: false, maxLength: 36));
            AddPrimaryKey("dbo.Locations", "Id");
            CreateIndex("dbo.Histories", "Location_Id");
            AddForeignKey("dbo.Histories", "Location_Id", "dbo.Locations", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Histories", "Location_Id", "dbo.Locations");
            DropIndex("dbo.Histories", new[] { "Location_Id" });
            DropPrimaryKey("dbo.Locations");
            AlterColumn("dbo.Histories", "Location_Id", c => c.String(maxLength: 50));
            AlterColumn("dbo.Locations", "Identifier", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Locations", "Id", c => c.String(nullable: false, maxLength: 50));
            AddPrimaryKey("dbo.Locations", "Id");
            CreateIndex("dbo.Histories", "Location_Id");
            AddForeignKey("dbo.Histories", "Location_Id", "dbo.Locations", "Id", cascadeDelete: true);
        }
    }
}
