﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface ILocationService
    {
        Task<ResultModel<List<LocationModel>>> GetAsync();

        Task<ResultModel<LocationModel>> GetAsync(string locationId);

        Task<ResultModel> AddAsync(Guid merchantId, List<LocationModel> locations, int timeOffset = 0);

        Task<ResultModel> RemoveAsync(string locationId);

        Task<ResultModel> UpdateAsync(LocationModel model);

        ResultModel<List<LocationModel>> Get(TimeSpan startSync, TimeSpan endSync);

        LocationSettingsModel GetSettings(string locationId);

        ResultModel<LocationSettingsModel> UpdatteSettings(LocationSettingsModel model);

        List<LocationModel> GetByMerchant(Guid merchantId);

        ResultModel SyncEmployees(string locationId, List<EmployeeModel> employees);

        ResultModel UpdateLocationLastSyncDate(string locationId);

        ResultModel<MappingSchemeModel> GetMappingScheme(string locationId);

        ResultModel UpdateMappingScheme(string locationId, MappingSchemeModel model);

        ResultModel<MappingSchemeModel> GetMappingSchemeByIdentifier(string locationSecret);

        ResultModel RemoveAgentFingerprint(string locationId);

        ResultModel<List<AvailableTenderTypeModel>> GetAvailableTenderTypes(string locationId);
    }
}
