﻿namespace Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1.Mapper
{
    public static class VerifyMapper
    {
        public static Domain.Models.Api.Agent.v1.VerifyModel ToDomainVerifyModel(this VerifyModel model)
        {
            return new Domain.Models.Api.Agent.v1.VerifyModel
            {
                Fingerprint = model.Fingerprint,
                ApiKey = model.ApiKey,
            };
        }
    }
}