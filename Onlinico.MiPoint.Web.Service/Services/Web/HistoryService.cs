﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class HistoryService : BaseService, IHistoryService
    {
        public HistoryService(ILogger logger) : base(logger)
        {
        }

        public ResultModel<List<HistoryModel>> Get(DateTime start, DateTime end)
        {
            var result = new ResultModel<List<HistoryModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    result.Object = context.Histories.AsNoTracking().Where(h => h.Start >= start && h.Start <= end)
                      .Take(1000).Select(r => new HistoryModel()
                        {
                            Id = r.Id,
                            DeviceId = r.DeviceId,
                            LocationSecret = r.Location.Id,
                            Method = r.Method,
                            Url = r.Uri,
                            Start = r.Start,
                            StatusCode = r.StatusCode,
                        }).OrderByDescending(h => h.Start).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public ResultModel<HistoryModel> Get(Guid historyId)
        {
            var result = new ResultModel<HistoryModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var history = context.Histories.Find(historyId);
                    result.Object = history?.ToHistoryModel(true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public ResultModel<string> GetContent(Guid historyId)
        {
            var result = new ResultModel<string>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var history = context.Histories.Find(historyId);
                    result.Object = history?.Content;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public async Task<ResultModel> RemoveOldRecordsAsync(DateTime star)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    context.Database.CommandTimeout = 360;
                    context.Configuration.AutoDetectChangesEnabled = false;

                    context.Histories.RemoveRange(context.Histories.Where(h => h.Start < star));
                    await context.SaveChangesAsync();

                    context.Configuration.AutoDetectChangesEnabled = true;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> RemoveOldContentAsync(DateTime star, DateTime end)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    context.Database.CommandTimeout = 360;
                    context.Configuration.AutoDetectChangesEnabled = false;

                    var histories = await context.Histories.Where(h => h.Start >= star && h.Start <= end).ToListAsync();
                    histories.ForEach(h => h.Content = null);

                    context.Configuration.AutoDetectChangesEnabled = true;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
