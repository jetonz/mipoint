﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class MerchantMapper
    {
        public static MerchantModel ToDomainMerchantModel(this Data.DataModel.Merchant model)
        {
            return new MerchantModel
            {
                Id = model.Id,
                Name = model.Name,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                Country = model.Country,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Address = model.Address,
                Created = model.Created,
                Updated = model.Updated,
                WebSite = model.WebSite,
                LogoUrl = model.LogoUrl,
            };
        }

        public static List<MerchantModel> ToDomainMerchantModels(this IEnumerable<Data.DataModel.Merchant> merchants)
        {
            return merchants.Select(m => m.ToDomainMerchantModel()).ToList();
        }

        public static Data.DataModel.Merchant ToNewMerchantEntity(this MerchantModel model)
        {
            return new Merchant
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                Country = model.Country,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Address = model.Address,
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
                IsRemoved = false,
                WebSite = model.WebSite,
                LogoUrl = model.LogoUrl,
            };
        }

        public static void UpdateEntity(this Data.DataModel.Merchant entity, MerchantModel model)
        {
            entity.Name = model.Name;
            entity.Email = model.Email;
            entity.PhoneNumber = model.PhoneNumber;
            entity.Country = model.Country;
            entity.City = model.City;
            entity.State = model.State;
            entity.Zip = model.Zip;
            entity.Address = model.Address;
            entity.Updated = DateTime.UtcNow;
            entity.WebSite = model.WebSite;
            entity.LogoUrl = model.LogoUrl;
        }
    }
}
