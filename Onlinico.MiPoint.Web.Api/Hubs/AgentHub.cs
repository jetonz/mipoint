﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Onlinico.MiPoint.Web.Api.Attributes;
using Onlinico.MiPoint.Web.Api.Hubs.Wrappers;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using MiPoint.Shared.Types.Dto;
using MiPoint.Shared.Types.Enums;

namespace Onlinico.MiPoint.Web.Api.Hubs
{
    [HubName("agentHub")]
    [AuthorizeAgentHub]
    public class AgentHub : Hub
    {
        private readonly ILocationService _locationService;
        private readonly IAgentService _agentService;
        private readonly IEmailService _emailService;
        private readonly ISmsService _smsService;
        private readonly IAgentManager _agentManager;

        public AgentHub(ILifetimeScope lifetimeScope)
        {
            _locationService = lifetimeScope.Resolve<ILocationService>();
            _agentService = lifetimeScope.Resolve<IAgentService>();
            _emailService = lifetimeScope.Resolve<IEmailService>();
            _smsService = lifetimeScope.Resolve<ISmsService>();
            _agentManager = lifetimeScope.Resolve<IAgentManager>();
        }

        public override Task OnConnected()
        {
            _agentManager.Connected(ApiKey, Fingerprint);

            new UserHubPusher().AgentStateChanged(ApiKey, "Agent-Conected");

            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            _agentManager.Reconnected(ApiKey, Fingerprint);

            new UserHubPusher().AgentStateChanged(ApiKey, "Agent-Reconnected");

            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            //You should not manually remove the user from the group when the user disconnects. This action is automatically performed by the SignalR framework.

            _agentManager.Disconnected(ApiKey, Fingerprint);

            new UserHubPusher().AgentStateChanged(ApiKey, "Agent-Disconected");

            return base.OnDisconnected(stopCalled);
        }

        [HubMethodName("getAgentSettings")]
        public async Task<SettingsDto> GetAgentSettings()
        {
            var settingsResult = await _locationService.GetAgetnSettingsAsync(ApiKey);
            if (settingsResult.Succeed)
            {
                return settingsResult.Object;
            }
            else
            {
                throw new Exception(settingsResult.Error);
            }
        }

        [HubMethodName("getClientSettings")]
        public async Task<Domain.Models.Api.Agent.v1.ClientSettingsModel> GetClientSettings()
        {
            var settingsResult = await _locationService.GetClientSettingsAsync(ApiKey);
            if (settingsResult.Succeed)
            {
                return settingsResult.Object;
            }
            else
            {
                throw new Exception(settingsResult.Error);
            }
        }

        [HubMethodName("addSessionLog")]
        public async Task<bool> AddSessionLog(List<DeviceLogDto> model)
        {
            var result = await _agentService.AddSessionLogsAsync(ApiKey, model);
            return result.Succeed;
        }

        [HubMethodName("addActivity")]
        public async Task<bool> AddActivity(List<RequestLogDto> model)
        {
            var result = await _agentService.AddActivitiesAsync(ApiKey, model);
            return result.Succeed;
        }

        [HubMethodName("addPayments")]
        public async Task<bool> AddPayments(List<PaymentDto> model)
        {
            var result = await _agentService.AddOrUpdatePaymentsAsync(ApiKey, model);
            return result.Succeed;
        }

        [HubMethodName("updateTenderTypes")]
        public async Task<bool> UpdateTenderTypes(List<TenderTypeDto> model)
        {
            var result = await _agentService.UpdateTenderTypes(ApiKey, model);
            return result.Succeed;
        }

        [HubMethodName("sendReceipt")]
        public async Task<bool> SendReceipt(SendReceiptDto model)
        {
            var result = true;
            if (model?.Payment != null)
            {
                var addOrUpdatePaymentResult = await _agentService.AddOrUpdatePaymentsAsync(ApiKey, new List<PaymentDto> { model.Payment });
                if (!addOrUpdatePaymentResult.Succeed)
                {
                    throw new Exception(addOrUpdatePaymentResult.Error);
                }

                var receiptUrl = $"{CurrentDomain}receipt/{model.Payment.Id}";

                switch (model.Carrier)
                {
                    case CarrierEnum.Email:
                        var htmlBodyResult = await GetReceiptHtmlAsync(receiptUrl).ConfigureAwait(false);
                        var sendEmailResult = await _emailService.SendAsync(model.Recipient, "Receipt MiPoint", htmlBodyResult.ObjectId).ConfigureAwait(false);

                        result = sendEmailResult.Succeed;
                        break;
                    case CarrierEnum.Sms:
                        var sendSmsResult = await _smsService.SendAsync(model.Recipient, $"Receipt : {receiptUrl}");
                        result = sendSmsResult.Succeed;
                        break;
                    default:
                        result = false;
                        break;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        private async Task<Domain.Models.Api.Agent.v1.ResultModel> GetReceiptHtmlAsync(string receiptUrl)
        {
            var result = new Domain.Models.Api.Agent.v1.ResultModel(true);
            try
            {
                using (var client = new HttpClient())
                {
                    var requestResult = await client.GetAsync(receiptUrl);
                    switch (requestResult.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            result.ObjectId = await requestResult.Content.ReadAsStringAsync();
                            break;
                        default:
                            result = new Domain.Models.Api.Agent.v1.ResultModel(false, "Cannot get receipt html page.");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                result = new Domain.Models.Api.Agent.v1.ResultModel(false, ex.Message);
            }

            return result;
        }

        private string ApiKey => Context.Request.Headers.GetValues(Core.Constants.Constants.ApiKey).FirstOrDefault();

        private string Fingerprint => Context.Request.Headers.GetValues(Core.Constants.Constants.Fingerprint).FirstOrDefault();

        protected Uri CurrentDomain => new Uri(Context.Request.Url.Scheme + System.Uri.SchemeDelimiter + Context.Request.Url.Host + (Context.Request.Url.IsDefaultPort ? "" : ":" + Context.Request.Url.Port));

    }
}