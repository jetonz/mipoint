﻿namespace Onlinico.MiPoint.Web.HumanId
{
    public class HumanId
    {
        public delegate void CheckIdForUniqueEventHandler(object sender, HumanIdEventArgs args);

        private HumanWordsDictionary _dictionary;

        public event CheckIdForUniqueEventHandler OnCheckIdForUnique;
        public string DictionaryFilePath { get; set; }

        public HumanId(string dictionaryPath = "Dictionary.xml")
        {
            _dictionary = HumanWordsDictionary.Load(dictionaryPath);
        }

        public HumanIdPair GetId()
        {
            var id = new HumanIdPair(_dictionary.GetRandomAdjective(), _dictionary.GetRandomNoon());
            if (OnCheckIdForUnique != null)
            {
                const int maxTry = 100;
                var i = 0;

                while (i <= maxTry)
                {
                    var args = new HumanIdEventArgs(id);

                    OnCheckIdForUnique(this, args);
                    if (args.IsUnique)
                        break;

                    i++;
                    if (i > maxTry)
                        throw new System.Exception("Low dictionary latency. Can't find unique pairs");
                    id = new HumanIdPair(_dictionary.GetRandomAdjective(), _dictionary.GetRandomNoon());
                }
            }
            return id;
        }
    }
}
