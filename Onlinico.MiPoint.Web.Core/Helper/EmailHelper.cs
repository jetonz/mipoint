﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Core.Helper
{
    public class EmailHelper
    {
        public static string GenerateInviteSubject(string merchantName)
        {
            return $"You have been invited for join using of the '{merchantName}' merrchant";
        }

        public static string GenerateInviteBody(string merchantName, string linkHref)
        {
            return $"You have been invited for joint using of the '{merchantName}' merchant. To accept this invitation, please, follow the <a href='{linkHref}' target='_blank'> link </a>";
        }
    }
}
