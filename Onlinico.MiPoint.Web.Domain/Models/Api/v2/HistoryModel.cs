﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v2
{
    public class HistoryModel
    {
        public Guid Id { get; set; }

        public string LocationSecret { get; set; }

        public string Method { get; set; }
        
        public string Url { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int StatusCode { get; set; }

        public string Content { get; set; }
    }
}
