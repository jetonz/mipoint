﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Service.Models.Omnivore.TenderType
{
    public class Self
    {
        public string href { get; set; }
    }

    public class Links
    {
        public Self self { get; set; }
    }

    public class TenderType
    {
        public Links _links { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Embedded
    {
        public List<TenderType> tender_types { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
    }

    public class TenderTipes
    {
        public Embedded _embedded { get; set; }

        public int? count { get; set; }
    }
}
