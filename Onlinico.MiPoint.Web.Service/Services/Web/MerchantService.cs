﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Exceptions;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class MerchantService : BaseService, IMerchantService
    {

        public MerchantService(ILogger logger) : base(logger)
        {
        }

        public ResultModel<List<MerchantModel>> Get()
        {
            var result = new ResultModel<List<MerchantModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var apps = context.Merchants.Where(m=>!m.IsRemoved).OrderByDescending(a => a.Created).ToList();
                    result.Object = apps.ToDomainMerchantModels();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public async Task<ResultModel<List<UserModel>>> GetUsersAsync(Guid merchantId)
        {
            var result = new ResultModel<List<UserModel>>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var users = await context.Users.Where(u=>u.Merchants.Any(m=>m.Id == merchantId)).ToListAsync();
                    result.Object = users?.ToDomainUserModels();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        } 

        public ResultModel<MerchantModel> Get(Guid merchantId)
        {
            var result = new ResultModel<MerchantModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = context.Merchants.FirstOrDefault(m => m.Id == merchantId && !m.IsRemoved);
                    result.Object = merchant?.ToDomainMerchantModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }
        
        public ResultModel RelateMerchantsWithUser(List<Guid> merchantIds, string userId)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.FirstOrDefault(u => u.Id == userId);
                    if (user != null)
                    {
                        var merchants = context.Merchants.Where(m => merchantIds.Contains(m.Id) && m.Users.All(u => u.Id != userId)).ToList();
                        if (merchants.Any())
                        {

                            merchants.ForEach(m => user.Merchants.Add(m));

                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid user.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }
            return result;
        }

        public ResultModel<List<MerchantModel>> GetForUser(string userId)
        {
            var result = new ResultModel<List<MerchantModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.Find(userId);
                    result.Object = user?.Merchants?.ToDomainMerchantModels();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public Domain.Models.Web.ResultModel<MerchantModel> Update(MerchantModel model)
        {
            var result = new Domain.Models.Web.ResultModel<MerchantModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = context.Merchants.FirstOrDefault(m => m.Id == model.Id);
                    if (merchant != null)
                    {
                        merchant.UpdateEntity(model);

                        context.SaveChanges();

                        model.Created = merchant.Created;

                        result.Object = model;
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public async Task<ResultModel> UpdateMerchantLocationsAsync(Guid merchantId, List<LocationModel> locations, int timeOffset = 0)
        {
            ResultModel result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    context.Database.CommandTimeout = 360;

                    var merchant = await context.Merchants.FirstOrDefaultAsync(m => m.Id == merchantId);
                    if (merchant != null)
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;
                        
                        locations.ForEach(l =>
                        {
                            var merchantLocation = merchant.Locations.FirstOrDefault(v => v.Identifier == l.Identifier);
                            if (merchantLocation != null)
                            {
                                merchantLocation.UpdateLocationEntity(l);

                                var employees = merchantLocation.Employees.ToList();

                                l.Employees?.ForEach(e =>
                                {
                                    var employee = employees.FirstOrDefault(r => r.EmployeeId == e.EmployeeId);
                                    if (employee != null)
                                    {
                                        employee.Update(e);
                                    }
                                    else
                                    {
                                        context.Employees.Add(e.ToNewEmployee(merchantLocation));
                                    }
                                });

                            }
                            else
                            {
                                var freeId = string.Empty;
                                var exit = false;
                                while (!exit)
                                {
                                    freeId = Core.StringGenerator.RandomStringKey();
                                    if (!context.Locations.Any(a => a.Id == freeId))
                                    {
                                        exit = true;
                                    }
                                }

                                var cards = context.Cards.ToList();

                                merchant.Locations.Add(l.ToNewLocationEntity(freeId, cards, timeOffset));
                            }
                        });

                        //Remove not avalible locations
                        var locationIdentifiers = locations.Select(l => l.Identifier);
                        var notAvailableLocations = merchant.Locations.Where(l => !locationIdentifiers.Contains(l.Identifier)).ToList();
                        notAvailableLocations.ForEach(l =>
                        {
                            var query = $"DELETE FROM [{context.Database.Connection.Database}].[dbo].[Histories] WHERE Location_Id = @param1";
                            context.Database.ExecuteSqlCommand(query, new SqlParameter("param1", l.Id));

                            context.Locations.Remove(l);
                        });

                        context.Configuration.AutoDetectChangesEnabled = true;

                        await context.SaveChangesAsync();

                        Logger.WriteMessageToLog(StringHelper.LogMessageBuilder("Manual locations sync was successful."));
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid merchant id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message + " Inner Exception: " + ex.InnerException?.Message));
                result = null;
            }
            return result;
        }

        public async Task<ResultModel<MerchantModel>> AddAsync(MerchantModel model, List<LocationModel> locations, string userId, int clientTimeOffset = 0)
        {
            var result = new ResultModel<MerchantModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = model.ToNewMerchantEntity();
                    context.Merchants.Add(merchant);

                    if (!string.IsNullOrEmpty(userId))
                    {
                        var user = await context.Users.FirstOrDefaultAsync(u => u.Id == userId);
                        user?.Merchants.Add(merchant);
                    }
                    var generatedKeys = new List<string>();
                    locations.ForEach(l =>
                    {
                        var freeId = string.Empty;
                        var exit = false;
                        while (!exit)
                        {
                            freeId = Core.StringGenerator.RandomStringKey();
                            if (generatedKeys.All(k => k != freeId) && !context.Locations.Any(a => a.Id == freeId))
                            {
                                generatedKeys.Add(freeId);
                                exit = true;
                            }
                        }

                        var cards = context.Cards.ToList();

                        merchant.Locations.Add(l.ToNewLocationEntity(freeId, cards, clientTimeOffset));
                    });
                    await context.SaveChangesAsync();
                    result.Object = merchant.ToDomainMerchantModel();
                }
            }
            catch (Exception ex)
            {
                result = new Domain.Models.Web.ResultModel<MerchantModel>(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public ResultModel RemoveMerchantsFromUser(string userId)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.FirstOrDefault(a => a.Id == userId);
                    if (user != null)
                    {
                        var userMerchants = user.Merchants.ToList();
                        userMerchants.ForEach(m => user.Merchants.Remove(m));
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public async Task<ResultModel> RemoveAsync(Guid id)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = await context.Merchants.Include(m=>m.Locations).FirstOrDefaultAsync(a => a.Id == id);
                    if (merchant != null)
                    {
                        merchant.IsRemoved = true;
                        foreach (var location in merchant.Locations)
                        {
                            location.IsRemoved = true;
                        }
                        await context.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message + " InnerException: " + ex.InnerException));
            }
            return result;
        }

        public async Task<ResultModel> UnlinkUser(Guid merchantId, string userId)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = await context.Merchants.FirstOrDefaultAsync(a => a.Id == merchantId);
                    if (merchant != null)
                    {
                        var user = merchant.Users.FirstOrDefault(u => u.Id == userId);
                        if (user != null)
                        {
                            merchant.Users.Remove(user);

                            await context.SaveChangesAsync();
                        }
                        else
                        {
                            result = new ResultModel(false, "Invalid user id.");
                        }
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid merchant id.");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }
    }
}
