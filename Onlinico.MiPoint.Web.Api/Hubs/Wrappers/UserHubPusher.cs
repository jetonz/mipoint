﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Onlinico.MiPoint.Web.Api.Hubs.Wrappers
{
    public class UserHubPusher
    {
        private readonly IHubContext _userHubContext;

        public UserHubPusher()
        {
            _userHubContext = GlobalHost.ConnectionManager.GetHubContext<UserHub>();
        }

        public void AgentStateChanged(string agentId, string state)
        {
            _userHubContext.Clients.All.agentStateChanged(agentId, state);
        }
    }
}