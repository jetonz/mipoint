﻿using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3
{
    [DataContract]
    public class TenderTypeModel
    {
        [DataMember(Name = "id")]
        public string Type { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
        
        [DataMember(Name = "card")]
        public string Card{ get; set; }

    }
}