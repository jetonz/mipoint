﻿namespace Onlinico.MiPoint.Web.Domain.Models.Api.v1
{
    public class TenderTypeModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
