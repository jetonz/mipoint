﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v4;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v4;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Swashbuckle.Swagger.Annotations;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v4
{
    /// <summary>
    /// Receipt controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v4")]
    public class ReceiptController : BaseController
    {
        private readonly IPaymentService _paymentService;
        private readonly IEmailService _emailService;
        private readonly ISmsService _smsService;

        /// <summary>
        /// Receipt controller constructor
        /// </summary>
        /// <param name="configuration">Inject app configuration</param>
        /// <param name="paymentService">Inject payment service</param>
        /// <param name="emailService">Inject email service</param>
        /// <param name="smsService">Inject sms service</param>
        /// <param name="logger">Inject logger</param>
        public ReceiptController(ILogger logger, IConfiguration configuration, IPaymentService paymentService, IEmailService emailService, ISmsService smsService) : base(logger, configuration)
        {
            _paymentService = paymentService;
            _emailService = emailService;
            _smsService = smsService;
        }

        /// <summary>
        /// Send receipt to email/sms
        /// </summary>
        /// <param name="paymentId">Payment id</param>
        /// <param name="carrier">Carrier: 'Email' or 'Sms'</param>
        /// <param name="address">Email address or  phone number</param>
        /// <returns></returns>
        [HttpPost]
        [Route("payments/{paymentId}/receipt/send")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
        public async Task<IHttpActionResult> Send([FromUri]string paymentId, SendReceiptModel model)
        {
            if (ModelState.IsValid)
            {
                var result = new Domain.Models.Global.ResultModel(true);
                var payment = await _paymentService.GetAsync(paymentId);

                if (payment.Succeed && payment.Object != null)
                {
                    var receiptUrl = CurrentDomain + "receipt/" + payment.Object.Id;

                    switch (model.Carrier)
                    {
                        case CarrierEnum.Email:
                            var htmlBodyResult = await GetReceiptHtmlAsync(receiptUrl);
                            if (htmlBodyResult.Succeed)
                            {
                                result = await _emailService.SendAsync(model.Recipient, $"Receipt from {payment.Object.ReceiptInfo?.LocationName}", htmlBodyResult.ObjectId);
                            }
                            else
                            {
                                result = new Domain.Models.Global.ResultModel(false, htmlBodyResult.Error);
                            }
                            break;
                        case CarrierEnum.Sms:
                            var message = $"Receipt : {receiptUrl}";
                            result = await _smsService.SendAsync(model.Recipient, message);
                            break;
                    }
                }
                if (result.Succeed)
                    return Ok();

                throw CreateApiException(result.Error);
            }
            else
            {
                throw CreateApiException(ModelState);
            }
        }

        private async Task<ResultModel> GetReceiptHtmlAsync(string receiptUrl)
        {
            var result = new ResultModel(true);
            try
            {
                using (var client = new HttpClient())
                {
                    var requestResult = await client.GetAsync(receiptUrl);
                    switch (requestResult.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            result.ObjectId = await requestResult.Content.ReadAsStringAsync();
                            break;
                        default:
                            result = new ResultModel(false, "Cannot get receipt html page.");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            _paymentService.Dispose();
            base.Dispose(disposing);
        }
    }
}
