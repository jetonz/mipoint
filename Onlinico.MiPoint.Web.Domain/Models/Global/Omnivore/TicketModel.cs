﻿namespace Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore
{
    public class TicketModel
    {
        public string Id { get; set; }

        public bool Open { get; set; }

        public long OpenedAt { get; set; }
    }
}
