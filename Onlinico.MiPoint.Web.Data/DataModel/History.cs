namespace Onlinico.MiPoint.Web.Data.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class History
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(36)]
        public string DeviceId { get; set; }

        [MaxLength(250)]
        public string Agent { get; set; }

        [Required]
        [MaxLength(10)]
        public string Method { get; set; }

        [Required]
        public string Uri { get; set; }

        [Index]
        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int StatusCode { get; set; }

        public string Content { get; set; }

        public string RequestHeaders { get; set; }

        public string RequestContent { get; set; }

        public string ResponseHeaders { get; set; }
        
        public string ResponseContent { get; set; }

        public virtual Location Location { get; set; }
    }
}
