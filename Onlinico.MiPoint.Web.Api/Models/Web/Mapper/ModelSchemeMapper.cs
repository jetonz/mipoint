﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class ModelSchemeMapper
    {
        public static Domain.Models.Web.MappingSchemeModel ToDomainMappingSchemeModel(
            this Models.Web.MappingSchemeModel model)
        {
            return new Domain.Models.Web.MappingSchemeModel
            {
                LocationId = model.LocationId,
                EmployeeScheme = model.Employees,
                TableScheme = model.Tables,
            };
        }

        public static Models.Web.MappingSchemeModel ToMappingSchemeViewModel(
            this Domain.Models.Web.MappingSchemeModel model, string locationId)
        {
            return new Models.Web.MappingSchemeModel(locationId)
            {
                LocationId = model.LocationId,
                Employees = model.EmployeeScheme,
                Tables = model.TableScheme,
            };
        }
    }
}