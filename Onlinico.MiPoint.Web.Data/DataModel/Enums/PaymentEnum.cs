﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel.Enums
{
    public enum PaymentTypeEnum
    {
        Offline = 0,
        Normal = 1
    }

    public enum PaymentStatusEnum
    {
        New = 0,

        IsProcessing = 1,

        Confirmed = 2,

        Paid = 3,

        Cancelled = 4,


        //Open = 0,

        //TryToPay = 1,

        //Paid = 2,

        //Closed = 3,

        //Canceled = 4,
    }
}
