﻿namespace Onlinico.MiPoint.Web.Domain.Models.Api.v1
{
    public class LocationModel
    {
        public string Id { get; set; }
        
        public string Identifier { get; set; }

        public string Name { get; set; }

        public string Owner { get; set; }

        public string PosType { get; set; }

        public bool IsDev { get; set; }
    }
}
