﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums
{
    public enum PaymentTypeEnum
    {
        Offline = 0,
        Normal = 1
    }

    public enum PaymentStatusEnum
    {
        Open = 0,

        TryToPay = 1,

        Paid = 2,

        Closed = 3,

        Canceled = 4,
    }
}