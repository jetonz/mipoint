﻿using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2.Mapper
{
    public static class TenderTypeMapper
    {
        public static List<Models.Api.v2.TenderTypeModel> ToApiTenderTypes(
            this List<Domain.Models.Api.v2.TenderTypeModel> tenderTypes)
        {
            return tenderTypes.Select(t => new Models.Api.v2.TenderTypeModel
            {
                Type = t.Type,
                Name = t.Name,
                Card = t.Card.Type
            }).ToList();
        } 
    }
}