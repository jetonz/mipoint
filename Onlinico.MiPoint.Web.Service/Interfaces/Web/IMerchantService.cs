﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface IMerchantService
    {
        ResultModel<List<MerchantModel>> Get();

        Task<ResultModel<List<UserModel>>> GetUsersAsync(Guid merchantId);

        ResultModel<MerchantModel> Get(Guid merchantId);

        //List<MerchantItemModel> GetMerchantItemsForUser(string userId);

        ResultModel RelateMerchantsWithUser(List<Guid> merchants, string userId);

        ResultModel<List<MerchantModel>> GetForUser(string userId);

        Domain.Models.Web.ResultModel<MerchantModel> Update(MerchantModel model);

        Task<ResultModel> UpdateMerchantLocationsAsync(Guid merchantId, List<LocationModel> locations, int timeOffset = 0);

        Task<ResultModel<MerchantModel>> AddAsync(MerchantModel model, List<LocationModel> locations, string userId, int clientTimeOffset = 0);

        ResultModel RemoveMerchantsFromUser(string userId);

        Task<ResultModel> RemoveAsync(Guid id);

        Task<ResultModel> UnlinkUser(Guid merchantId, string userId);
    }
}
