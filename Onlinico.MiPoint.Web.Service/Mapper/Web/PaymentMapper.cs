﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class PaymentMapper
    {
        public static List<PaymentModel> ToDomainPayments(this IEnumerable<Data.DataModel.Payment> models)
        {
            return models.Select(p => p.ToDomainPayment()).ToList();
        }

        public static PaymentModel ToDomainPayment(this Data.DataModel.Payment model)
        {
            var tenderType = model.Location.Settings.TenderTypes.FirstOrDefault(t => t.Card.Name == (model.Transaction?.CardType ?? null));

            return new PaymentModel
            {
                CardType = model.Transaction?.CardType,
                TenderType = tenderType?.Type,
                LocationId = model.Location.Id,
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tip,
                Tax = model.Tax,
                Status = (PaymentStatusEnum)((int)model.Status),
                Type = (PaymentTypeEnum)((int)model.Type),
                Message = model.Message,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,
                Created = model.Created,

                Items = model.Items?.ToDomainPaymentItems(),
                ReceptInfo = model.ReceiptInfo?.ToDomainPaymentReceptInfo(),
                Transaction = model.Transaction?.ToDomainPaymentTransaction(),
            };
        }
        
        public static PaymentTransactionModel ToDomainPaymentTransaction(this Data.DataModel.PaymentTransaction model)
        {
            return new PaymentTransactionModel
            {
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = model.CardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType,
                Issued = model.Issued,
            };
        }

        public static PaymentReceptInfoModel ToDomainPaymentReceptInfo(this Data.DataModel.PaymentReceiptInfo model)
        {
            return new PaymentReceptInfoModel
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }

        public static List<PaymentItemModel> ToDomainPaymentItems(this IEnumerable<Data.DataModel.PaymentItem> items)
        {
            return items.Select(i => new PaymentItemModel
            {
                Id = i.Id,
                ItemId = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }
    }
}
