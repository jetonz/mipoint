﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Domain.Enums.Transaction;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class PaymentModel
    {
        public string Id { get; set; }

        public string DeviceId { get; set; }

        public string OrderId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Domain.Enums.PaymentTypeEnum PaymentType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Domain.Enums.PaymentStatusEnum Status { get; set; }

        public long Amount { get; set; }

        public long Tips { get; set; }

        public long Tax { get; set; }

        public string EmployeeId { get; set; }

        public DateTime Created { get; set; }

        public PaymentTransactionModel Transaction { get; set; }

        public PaymentReceptInfoModel Receipt { get; set; }

        public IEnumerable<PaymentItemModel> Items { get; set; }
    }

    public class PaymentItemModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public long Price { get; set; }
    }

    public class PaymentTransactionModel
    {
        public string TransactionId { get; set; }

        public DateTimeOffset Issued { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public CardEntryTypeEnum CardEntryType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public CardTypeEnum CardType { get; set; }

        public string lastFourDigits { get; set; }

        public string ReferenceId { get; set; }

        public string AuthCode { get; set; }

        public string ApplicationId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionTypeEnum TransactionType { get; set; }

        public string Cvm { get; set; }
    }

    public class PaymentReceptInfoModel
    {
        public Guid Id { get; set; }

        public string OrderNumber { get; set; }

        public DateTimeOffset Issued { get; set; }

        public string TableName { get; set; }

        public long SubTotal { get; set; }

        public long Tax { get; set; }

        public long Total { get; set; }

        public long ServiceCharge { get; set; }

        public string Cashier { get; set; }

        public string LocationName { get; set; }

        public List<string> Headers { get; set; }

        public string PhoneNumber { get; set; }

        public string Website { get; set; }
    }
}
