﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Location = Onlinico.MiPoint.Web.Service.Models.Omnivore.Locations.Location;

namespace Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore
{
    public static class LocationMapper
    {
        public static Domain.Models.Global.Omnivore.LocationModel ToDomainLocation(this Location model)
        {
            return new Domain.Models.Global.Omnivore.LocationModel
            {
                Identifier = model.id,
                Name = model.name,
                Phone = model.phone,
                Address = model.address?.street1,
                AddressFull = $"{model.address?.city} {model.address?.state} {model.address?.zip}",
                Owner = model.owner,
                IsDev = model.development ?? false,
                PosType = model.pos_type,
                Employees = new List<Domain.Models.Global.Omnivore.EmployeeModel>(),
            };
        }

        public static List<Domain.Models.Global.Omnivore.LocationModel> ToDomainLocations(this List<Location> omnivoreLocations)
        {
            return omnivoreLocations.Select(l => l.ToDomainLocation()).ToList();
        }

        public static List<Domain.Models.Web.LocationModel> ToDomainLocations(this List<Domain.Models.Global.Omnivore.LocationModel> models)
        {
            return models.Select(l => l.ToDomainLocation()).ToList();
        }

        public static Domain.Models.Web.LocationModel ToDomainLocation(this Domain.Models.Global.Omnivore.LocationModel model)
        {
            return new Domain.Models.Web.LocationModel
            {
                Id = model.Id,
                Identifier = model.Identifier,
                Address = model.Address,
                Name = model.Name,
                Phone = model.Phone,
                AddressFull = model.AddressFull,
                IsConfigured = model.IsConfigured,
                IsDev = model.IsDev,
                Owner = model.Owner,
                PosType = model.PosType,
                Employees = model.Employees.ToDomainEmployees()
            };
        }
    }
}
