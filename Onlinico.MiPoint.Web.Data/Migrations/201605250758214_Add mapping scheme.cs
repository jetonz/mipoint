namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addmappingscheme : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MappingSchemes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Scheme = c.String(maxLength: 250),
                        Type = c.Int(nullable: false),
                        Settings_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LocationSettings", t => t.Settings_Id, cascadeDelete: true)
                .Index(t => t.Settings_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MappingSchemes", "Settings_Id", "dbo.LocationSettings");
            DropIndex("dbo.MappingSchemes", new[] { "Settings_Id" });
            DropTable("dbo.MappingSchemes");
        }
    }
}
