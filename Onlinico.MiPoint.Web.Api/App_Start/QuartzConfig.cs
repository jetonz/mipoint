﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Onlinico.MiPoint.Web.Api.Jobs;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Quartz;
using Quartz.Impl;

namespace Onlinico.MiPoint.Web.Api.App_Start
{
    public static class QuartzConfig
    {
        public static void Register(IContainer container)
        {
            var config = container.Resolve<IConfiguration>();

            ISchedulerFactory schedFact = new StdSchedulerFactory();

            IScheduler sched = schedFact.GetScheduler();

            sched.Start();

            // define the job and tie it to our SyncEmployeesJob class
            IJobDetail syncEmploeesjob = JobBuilder.Create<SyncEmployeesJob>()
                .WithIdentity("SyncEmployees", "Sync")
                .Build();
            syncEmploeesjob.JobDataMap.Put("Container", container);
            syncEmploeesjob.JobDataMap.Put("LastSync", new List<DateTime> { DateTime.UtcNow });


            IJobDetail failedPaymentResolverjob = JobBuilder.Create<FailedPaymentResolverJob>()
                .WithIdentity("FailedPaymentResolver", "Resolver")
                .Build();
            failedPaymentResolverjob.JobDataMap.Put("Container", container);
            

            // Trigger the job to run every 5 minutes
            ITrigger syncEmploeesTrigger = TriggerBuilder.Create()
              .WithIdentity("SyncLocations", "Sync")
              .StartNow()
              .WithSimpleSchedule(x => x
              .WithIntervalInMinutes(config.SyncTriggerIntervalInMinutes)
              .RepeatForever())
              .Build();

            ITrigger failedPaymentResolverTrigger = TriggerBuilder.Create()
              .WithIdentity("ResolvPayments", "Resolver")
              .StartNow()
              .WithSimpleSchedule(x => x
              .WithIntervalInMinutes(config.SyncTriggerIntervalInMinutes)
              .RepeatForever())
              .Build();
            
            sched.ScheduleJob(syncEmploeesjob, syncEmploeesTrigger);

            sched.ScheduleJob(failedPaymentResolverjob, failedPaymentResolverTrigger);
        }
    }
}