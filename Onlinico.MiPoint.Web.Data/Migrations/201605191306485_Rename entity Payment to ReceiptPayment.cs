namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameentityPaymenttoReceiptPayment : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Payments", newName: "ReceiptPayments");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ReceiptPayments", newName: "Payments");
        }
    }
}
