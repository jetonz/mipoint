﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class SessionLogService : BaseService, ISessionLogService
    {
        public SessionLogService(ILogger logger) : base(logger)
        {
        }

        public ResultModel<List<SessionLogModel>> Get(DateTime start, DateTime end)
        {
            var result = new ResultModel<List<SessionLogModel>>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    result.Object = context.Logs.AsNoTracking().Where(h => h.Created >= start && h.Created <= end)
                       .Select(r => new SessionLogModel()
                        {
                            Id = r.Id,
                            Model = r.Model,
                            Fingerprint = r.Fingerprint,
                            LocationId = r.LocationId,
                            Manufacturer = r.Manufacturer,
                            Serial = r.Serial,
                            Created = r.Created,
                      //      Content = r.Content,
                            AppVersion = r.AppVersion
                        }) .OrderByDescending(l => l.Created).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<List<SessionLogModel>>(false, ex.Message);
            }

            return result;
        }

        public ResultModel<SessionLogModel> Get(Guid logId)
        {
            var result = new ResultModel<SessionLogModel>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var log = context.Logs.FirstOrDefault(l => l.Id == logId);
                    if (log != null)
                    {
                        result.Object = log.ToDomainLogModel();
                    }
                    else
                    {
                        result = new ResultModel<SessionLogModel>(false, "Invalid log id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<SessionLogModel>(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel> RemoveOldRecordsAsync(DateTime star)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    context.Database.CommandTimeout = 360;
                    context.Configuration.AutoDetectChangesEnabled = false;

                    context.Logs.RemoveRange(context.Logs.Where(h => h.Created < star));
                    await context.SaveChangesAsync();

                    context.Configuration.AutoDetectChangesEnabled = true;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> RemoveOldContentAsync(DateTime star, DateTime end)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    context.Database.CommandTimeout = 360;
                    context.Configuration.AutoDetectChangesEnabled = false;

                    var logs = await context.Logs.Where(h => h.Created >= star && h.Created <= end).ToListAsync();
                    logs.ForEach(h => h.Content = null);

                    context.Configuration.AutoDetectChangesEnabled = true;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
