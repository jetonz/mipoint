﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Core.Interfaces;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Documentation Controller
    /// </summary>
    public class DocController : BaseController
    {
        public DocController(IConfiguration configuration) : base(configuration)
        {
        }

        /// <summary>
        /// Documentation page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var url = HttpContext.Request.Url;
            var root = url?.Scheme + "://" + url?.Authority;
            ViewBag.RootUrl = root;
            ViewBag.EndPointUrl = root + "/api/proxy/omnivore/{url}";
            ViewBag.EndPointExampleUrl = root + "/api/proxy/omnivore/0.1/locations";
            return View();
        }
    }
}