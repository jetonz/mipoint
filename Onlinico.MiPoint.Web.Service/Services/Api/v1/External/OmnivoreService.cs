﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1.External;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v1;
using Onlinico.MiPoint.Web.Service.Models.Omnivore.Locations;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v1.External
{
    public class OmnivoreService : BaseService, IOmnivoreService
    {
        private readonly Core.Interfaces.IConfiguration _configuration;

        public OmnivoreService(ILogger logger, Core.Interfaces.IConfiguration configuration) : base(logger)
        {
            _configuration = configuration;
        }

        public async Task<List<LocationModel>> GetLocationsByApiKey(string apiKey)
        {
            List<LocationModel> result;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });

                    var requestUrl = _configuration.BaseOmnivoreApiUrl + "locations";
                    var requestResult = await client.GetAsync(requestUrl);

                    switch (requestResult.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            var jsonContent = await requestResult.Content.ReadAsStringAsync();
                            var omnivoreLocations = JsonConvert.DeserializeObject<RootObject>(jsonContent);
                            result = omnivoreLocations?._embedded?.locations?.ToDomainLocations();
                            break;
                        default:
                            result = null;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(ex.Message);
                result = null;
            }
            return result;
        }

        public async Task<List<TenderTypeModel>> GetLocationTenderTypes(string apiKey, string locationsId)
        {
            List<TenderTypeModel> result = null;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add(_configuration.HeaderApiKey, new List<string> { apiKey });


                    var requestUrl = _configuration.BaseOmnivoreApiUrl + "locations/" + locationsId + "/tender_types";
                    var requestResult = await client.GetAsync(requestUrl);


                    switch (requestResult.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            var jsonContent = await requestResult.Content.ReadAsStringAsync();
                            var omnivoreLocationTenderTypes = JsonConvert.DeserializeObject<Models.Omnivore.TenderType.TenderTipes>(jsonContent);
                            result = omnivoreLocationTenderTypes._embedded.tender_types.Select(t => new TenderTypeModel
                            {
                                Id = t.id,
                                Name = t.name
                            }).ToList();

                            break;
                        default:
                            result = null;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(ex.Message);
                result = null;
            }
            return result;
        }

    }
}
