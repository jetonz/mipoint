﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class InviteService : BaseService, IInviteService
    {
        public InviteService(ILogger logger) : base(logger)
        {
        }

        public ResultModel<InviteModel> Find(Guid merchantId, string email)
        {
            var result = new ResultModel<InviteModel>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var invite = context.Invites.FirstOrDefault(i => i.Merchant.Id == merchantId && i.Email == email);
                    result.Object = invite?.ToDomainInviteModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return result;
        }

        public async Task<ResultModel<UserModel>> GetInvitedUser(Guid merchantId, string email)
        {
            var result = new ResultModel<UserModel>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = await context.Users.FirstOrDefaultAsync(u => u.UserName == email && u.Merchants.Any(m => m.Id == merchantId));
                    result.Object = user?.ToDomainUserModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }


        public ResultModel<InviteModel> Get(Guid id)
        {
            var result = new ResultModel<InviteModel>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var invite = context.Invites.FirstOrDefault(i => i.Id == id);
                    result.Object = invite?.ToDomainInviteModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<InviteModel>(false, ex.Message);
            }

            return result;
        }

        public ResultModel Add(InviteModel model)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = context.Merchants.FirstOrDefault(m => m.Id == model.MerchantId);
                    if (merchant != null)
                    {
                        var invite = merchant.Invites.FirstOrDefault(i => i.Email == model.Email);
                        if (invite != null)
                        {
                            context.Invites.Remove(invite);
                        }

                        context.Invites.Add(new Invite
                        {
                            Id = model.Id,
                            Email = model.Email,
                            Merchant = merchant,
                        });

                        context.SaveChanges();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid merchant.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public ResultModel AcceptInvite(Guid id, string userId)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var invite = context.Invites.FirstOrDefault(i => i.Id == id);
                    if (invite != null)
                    {
                        var user = context.Users.Find(userId);
                        if (user != null)
                        {
                            if (user.Merchants.All(m => m.Id != invite.Merchant.Id))
                            {
                                user.Merchants.Add(invite.Merchant);
                            }
                            else
                            {
                                //result = new ResultModel(false, "User already linked to merchant.");
                            }

                            context.Invites.Remove(invite);

                            context.SaveChanges();
                        }
                        else
                        {
                            result = new ResultModel(false, "Invalid user.");
                        }
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid invite.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }
    }
}
