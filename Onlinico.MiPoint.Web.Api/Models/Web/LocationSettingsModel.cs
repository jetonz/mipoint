﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class LocationSettingsModel
    {
        public Guid Id { get; set; }

        //public string Tips { get; set; }

        //public float TaxRate { get; set; }

        public List<TenderTypeModel> TenderTypes { get; set; }

        public string SyncTime { get; set; }

        public int TimeOffset { get; set; }

        public TimeSpan SyncPeriod { get; set; }

        public bool AfterPaymentLogOut { get; set; }

        public int SessionTimeout { get; set; }

        public int OpenOrderLimit { get; set; }

        public int ClosedOrderLimit { get; set; }
    }

    public class MappingSchemeModel
    {
        public MappingSchemeModel()
        {
            
        }

        public MappingSchemeModel(string locationId)
        {
            LocationId = locationId;

            TableModelProperties = typeof(Models.Omnivore.Tickets.Table).GetProperties().Where(p => p.Name != "_links").Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Name
            });

            EmployeeModelProperties = typeof(Models.Omnivore.Tickets.Employee).GetProperties().Where(p => p.Name != "_links").Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Name
            });
        }

        public string LocationId { get; set; }

        public IEnumerable<SelectListItem> TableModelProperties { get; set; }

        public IEnumerable<SelectListItem> EmployeeModelProperties { get; set; }
        
        public Dictionary<string, string> Employees { get; set; }

        public Dictionary<string, string> Tables { get; set; }
    }
}