﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using Newtonsoft.Json;
using Onlinico.MiPoint.Web.Api.Models.Omnivore.Mapper;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v2;
using WebGrease.Css.Extensions;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v2
{
    /// <summary>
    /// Omnivore proxy controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v2/proxy/omnivore")]
    public class OmnivoreController : v2.BaseController
    {
        //private readonly IHistoryService _historyService;
        private readonly Core.Interfaces.IConfiguration _configuration;
        //private HistoryModel _history;
        private readonly ILocationService _locationService;

        /// <summary>
        /// Omnivore controller constructor
        /// </summary>
        /// <param name="logger">Inject logger</param>
        /// <param name="historyService">Inject history service</param>
        /// <param name="configuration">Inject configuration</param>
        /// <param name="locationService">Inject location service</param>
        public OmnivoreController(Logger.Interfaces.ILogger logger, ILocationService locationService, Core.Interfaces.IConfiguration configuration/*, IHistoryService historyService*/) : base(logger)
        {
            _locationService = locationService;
            //_historyService = historyService;
            _configuration = configuration;
        }

        /// <summary>
        /// Initialize Omnivore controller
        /// </summary>
        /// <param name="controllerContext">Current controller context</param>
        //protected override void Initialize(HttpControllerContext controllerContext)
        //{
        //    base.Initialize(controllerContext);
        //    _history = new HistoryModel { Id = Guid.NewGuid(), Method = Request.Method.Method, Start = DateTime.UtcNow };
        //}

        /// <summary>
        /// Proxy GET
        /// </summary>
        /// <param name="url">Omnivore web api url</param>
        /// <returns>Http response message</returns>
        //[Route("")]
        [System.Web.Http.Route("{*url}")]
        public async Task<HttpResponseMessage> Get([FromUri]string url)
        {
            HttpResponseMessage result = null;
            using (var httpClient = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                var urlParts = url.Split('/');
                var isTicketsRequest = (urlParts.Length == 4 || (urlParts.Length == 5 && string.IsNullOrEmpty(urlParts[4]))) && urlParts[0] == "0.1" && urlParts[1].ToLower() == "locations" && urlParts[3].ToLower() == "tickets";
                var isTicketRequest = urlParts.Length >= 5 && urlParts[0] == "0.1" && urlParts[1].ToLower() == "locations" && urlParts[3].ToLower() == "tickets" && !string.IsNullOrEmpty(urlParts[4]);

                url += Request.RequestUri.Query;

                if (string.IsNullOrEmpty(Request.RequestUri.Query))
                {

                    if (urlParts.Length == 4 && urlParts[0] == "0.1" && urlParts[1].ToLower() == "locations" && urlParts[3].ToLower() == "tickets")
                    {
                        url += "?where=eq(open,true)";

                        if (urlParts[2] == "7T6ApBET") // giBg7jMi    7T6ApBET
                        {
                            url += "&limit=100";
                        }
                    }
                }
                else
                {
                    if (url.Contains("0.1/locations/7T6ApBET/tickets")) // giBg7jMi    7T6ApBET
                    {
                        url += "&limit=100";
                    }
                }


                var request = new HttpRequestMessage(HttpMethod.Get, BaseUri + url);

                Request.Headers.ForEach(h => request.Headers.TryAddWithoutValidation(h.Key, h.Value));

                try
                {
                    result = await httpClient.SendAsync(request);

                    if (isTicketsRequest && result.StatusCode == HttpStatusCode.OK)
                    {
                        var content = await result.Content.ReadAsStringAsync();

                        var tickets = JsonConvert.DeserializeObject<Models.Omnivore.Tickets.RootObject>(content);

                        var mappingSchemeResult = _locationService.GetMappingSchemes(Request.Properties[_configuration.Secret].ToString());
                        if (mappingSchemeResult.Succeed && mappingSchemeResult.Object != null)
                        {
                            if (mappingSchemeResult.Object.Any(s => s.Type == MappingTypeEnum.Employee))
                                tickets?._embedded?.tickets?.ForEach(t => t._embedded?.employee?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Employee).Scheme));
                            if (mappingSchemeResult.Object.Any(s => s.Type == MappingTypeEnum.Table))
                                tickets?._embedded?.tickets?.ForEach(t => t._embedded?.table?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Table).Scheme));
                        }

                        if (tickets != null)
                        {
                            tickets._embedded?.tickets?.ForEach(t => t.name = t.name ?? "");

                            result.Content = new StringContent(JsonConvert.SerializeObject(tickets));
                        }
                    }
                    else if (isTicketRequest && result.StatusCode == HttpStatusCode.OK)
                    {
                        var content = await result.Content.ReadAsStringAsync();

                        var ticket = JsonConvert.DeserializeObject<Models.Omnivore.Ticket.RootObject>(content);

                        var mappingSchemeResult = _locationService.GetMappingSchemes(Request.Properties[_configuration.Secret].ToString());
                        if (mappingSchemeResult.Succeed && mappingSchemeResult.Object != null)
                        {
                            ticket?._embedded?.employee?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Employee).Scheme);

                            ticket?._embedded?.table?.ToScheme(mappingSchemeResult.Object.First(s => s.Type == MappingTypeEnum.Table).Scheme);
                        }

                        if (ticket != null)
                        {
                            ticket.name = ticket.name ?? "";

                            result.Content = new StringContent(JsonConvert.SerializeObject(ticket));
                        }
                    }

                }
                catch (Exception ex)
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message + " Inner Exception Message: " + ex.InnerException?.Message));
                }
            }
            return result;
        }

        /// <summary>
        /// Proxy POST
        /// </summary>
        /// <param name="url">Omnivore web api url</param>
        /// <returns>Http response message</returns>
        [System.Web.Http.Route("{*url}")]
        public async Task<HttpResponseMessage> Post([FromUri]string url)
        {
            HttpResponseMessage result = null;
            using (var httpClient = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                url += Request.RequestUri.Query;
                var request = new HttpRequestMessage(HttpMethod.Post, BaseUri + url);
                Request.Headers.ForEach(h => request.Headers.TryAddWithoutValidation(h.Key, h.Value));
                request.Content = Request.Content;

                try
                {
                    //_history.Content = await request.Content.ReadAsStringAsync();

                    result = await httpClient.SendAsync(request);
                }
                catch (Exception ex)
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message + " Inner Exception Message: " + ex.InnerException?.Message));
                }
                //finally
                //{
                //    await AddHistoryAsync(url, result?.StatusCode).ConfigureAwait(false);
                //}
            }
            return result;
        }

        /// <summary>
        /// Proxy DELETE
        /// </summary>
        /// <param name="url">Omnivore web api url</param>
        /// <returns>Http response message</returns>
        [System.Web.Http.Route("{*url}")]
        public async Task<HttpResponseMessage> Delete([FromUri]string url)
        {
            HttpResponseMessage result = null;
            using (var httpClient = new HttpClient())
            {
                url += Request.RequestUri.Query;
                var request = new HttpRequestMessage(HttpMethod.Delete, BaseUri + url);
                Request.Headers.ForEach(h => request.Headers.TryAddWithoutValidation(h.Key, h.Value));
                try
                {
                    result = await httpClient.SendAsync(request);
                }
                catch (Exception ex)
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message + " Inner Exception Message: " + ex.InnerException?.Message));
                }
                //finally
                //{
                //    await AddHistoryAsync(url, result?.StatusCode).ConfigureAwait(false);
                //}
            }
            return result;
        }

        //private async Task AddHistoryAsync(string url, HttpStatusCode? statusCode)
        //{
        //    var locationSecret = Request.Properties[_configuration.Secret].ToString();

        //    _history.Url = url;
        //    _history.LocationSecret = locationSecret;
        //    _history.StatusCode = statusCode != null ? (int)statusCode : 400;
        //    _history.End = DateTime.UtcNow;
        //    await _historyService.AddAsync(_history).ConfigureAwait(false);
        //}

    }
}
