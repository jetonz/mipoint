﻿using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface IEmailService
    {
        Task<ResultModel> SendAsync(string address, string subject, string html);
    }
}
