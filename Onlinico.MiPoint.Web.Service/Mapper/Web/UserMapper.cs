﻿using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class UserMapper
    {
        public static UserModel ToDomainUserModel(this Data.DataModel.User entityUserModel)
        {
            return new UserModel
            {
                Id = entityUserModel.Id,
                FullName = entityUserModel.FullName,
                Login = entityUserModel.UserName,
            };
        }

        public static List<UserModel> ToDomainUserModels(this IEnumerable<Data.DataModel.User> entityUserModels)
        {
            return entityUserModels.Select(u=> new UserModel
            {
                Id = u.Id,
                FullName = u.FullName,
                Login = u.UserName,
            }).ToList();
        }

        public static Data.DataModel.User ToEntityUserModel(this UserModel domainUserModel)
        {
            return new Data.DataModel.User
            {
                Id = domainUserModel.Id,
                FullName = domainUserModel.FullName,
                UserName = domainUserModel.Login,
            };
        }

        public static void UpdateUserEntity(this Data.DataModel.User entity, UserModel model)
        {
            entity.FullName = model.FullName;
            //entity.UserName = model.Login;
        }
    }
}
