﻿using Onlinico.MiPoint.Web.Logger.Interfaces;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v1
{
    public class BaseService
    {
        protected ILogger Logger;

        public BaseService(ILogger logger)
        {
            Logger = logger;
        }
    }
}
