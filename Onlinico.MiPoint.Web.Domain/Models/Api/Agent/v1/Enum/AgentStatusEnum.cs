﻿namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1.Enum
{
    public enum AgentStatusEnum
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// NotInstalled
        /// </summary>
        NotInstalled = 1,
        /// <summary>
        /// Stopped
        /// </summary>
        Stopped = 2,
        /// <summary>
        /// StopPending
        /// </summary>
        StopPending = 3,
        /// <summary>
        /// Running
        /// </summary>
        Running = 4,
        /// <summary>
        /// StartPending
        /// </summary>
        StartPending = 5
    }
}