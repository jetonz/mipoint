﻿namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class RegisterModel
    {
        public string ApiKey { get; set; }
        public string Fingerprint { get; set; }
        public string AgentVersion { get; set; }
        public string AgentAddress { get; set; }
    }
}
