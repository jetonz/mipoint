﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class RegisterModel
    {
        public string Name { get; set; }

        public string Password { get; set; }

        public string Login { get; set; }

        public List<Guid> SelectedMerchants { get; set; }

        public List<SelectedItem<MerchantModel>> SelectedMerchantItems { get; set; }
    }
}