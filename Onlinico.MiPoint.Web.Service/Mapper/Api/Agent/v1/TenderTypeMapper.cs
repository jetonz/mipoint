﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Data.DataModel;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.Agent.v1
{
    public static class TenderTypeMapper
    {

        public static List<AvailableTenderType> ToNewAvailableTenderTypes(this List<TenderTypeDto> models, LocationSettings settings)
        {
            return models.Select(m => m.ToNewAvailableTenderType(settings)).ToList();
        }

        public static AvailableTenderType ToNewAvailableTenderType(this TenderTypeDto model, LocationSettings settings)
        {
            return new AvailableTenderType
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                Type = model.Id,
                Settings = settings,
            };
        }
    }
}
