﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Core.Interfaces;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Manage controller
    /// </summary>
    [Authorize]
    public class ManageController : BaseController
    {
        private readonly ApplicationUserManager _applicationUserManager;
        private readonly ApplicationSignInManager _applicationSignInManager;

        /// <summary>
        /// Manage controller constructor
        /// </summary>
        public ManageController(ApplicationUserManager applicationUserManager, ApplicationSignInManager applicationSignInManager, IConfiguration configuration) : base(configuration)
        {
            _applicationUserManager = applicationUserManager;
            _applicationSignInManager = applicationSignInManager;
        }

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await _applicationUserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await _applicationUserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await _applicationUserManager.GetLoginsAsync(userId),
                BrowserRemembered = false,
            };
            return View(model);
        }

        /// <summary>
        /// User Profile GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserProfile()
        {
            var user = _applicationUserManager.FindByName(User.Identity.Name);
            
            return View(new UserProfileViewModel { FullName = user.FullName, Email = user.Email });
        }

        /// <summary>
        /// User Profile POST
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserProfile(UserProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = _applicationUserManager.FindByName(User.Identity.Name);

            user.FullName = model.FullName;
            user.Email = model.Email;

            var result = _applicationUserManager.Update(user);

            if (!result.Succeeded)
            {
                AddErrors(result);
            }

            return View(model);
        }

        /// <summary>
        /// Change Password GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// Change Password POST
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await _applicationUserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await _applicationUserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await _applicationSignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        private bool HasPassword()
        {
            var user = _applicationUserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }
    }
}