﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Autofac;
using Newtonsoft.Json;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Quartz;
using WebGrease.Css.Extensions;

namespace Onlinico.MiPoint.Web.Api.Jobs
{
    /// <summary>
    /// Failed payment resolver
    /// </summary>
    public class FailedPaymentResolverJob : IJob
    {
        private IOmnivoreService _omnivoreService;
        private IPaymentService _paymentService;
        private IAuthService _authService;
        private ILogger _logger;

        public async void Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.MergedJobDataMap;

            IContainer container = (IContainer)dataMap["Container"];

            _omnivoreService = container.Resolve<IOmnivoreService>();
            _paymentService = container.Resolve<IPaymentService>();
            _authService = container.Resolve<IAuthService>();
            _logger = container.Resolve<ILogger>();

            try
            {
                var paymentResult = await _paymentService.GetFailedPaymentsAsync();

                if (paymentResult.Succeed && paymentResult.Object != null)
                {
                    var payments = paymentResult.Object.GroupBy(p => p.LocationId);

                    Task.WaitAll(payments.SelectMany(g =>
                    {
                        var tasks = new List<Task>();
                        var locationIdentityResult = _authService.GetIdentity(g.Key);
                        if (locationIdentityResult.Succeed)
                        {
                            var identity = locationIdentityResult.Object;

                            tasks.Add(ResolvPaymentsAsync(identity.OmnivoreApiKey, identity.OmnivoreLocationId, g.ToList()));
                        }
                        else
                        {
                            _logger.WriteMessageToLog(StringHelper.ErrorBuilder(locationIdentityResult.Error));
                        }

                        return tasks;

                    }).ToArray());
                }
            }
            catch (Exception ex)
            {
                _logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
        }

        private async Task<string> ResolvPaymentsAsync(string apiKey, string locationId, List<Domain.Models.Web.PaymentModel> payments)
        {
            payments.ForEach(p =>
            {
                var ticketId = p.OrderId.Split('-').First();

                var ticketResult = _omnivoreService.GetTicketAsync(apiKey, locationId, ticketId).Result;

                if (ticketResult.Succeed)
                {
                    var currentTicketId = $"{ticketResult.Object.id}-{ticketResult.Object.opened_at}";
                    if (p.OrderId == currentTicketId)
                    {
                        if (ticketResult.Object.closed_at == null)
                        {
                            if (ticketResult.Object.totals.due >= p.Amount)
                            {
                                Thread.Sleep(1000);

                                var paidResult = _omnivoreService.SendThirdPartyPayment(apiKey, locationId, p.ToThirdPartyPaymentModel()).Result;

                                if (paidResult.Succeed)
                                {
                                    var message = "Resolved : " + JsonConvert.SerializeObject(paidResult.Object);
                                    ;
                                    var updateResult = _paymentService.UpdateStatusAsync(p.Id, PaymentStatusEnum.Paid, message).Result;
                                    _logger.WriteMessageToLog(updateResult.Succeed
                                        ? StringHelper.ErrorBuilder($"Payment: {p.Id} success resolved.")
                                        : StringHelper.ErrorBuilder(updateResult.Error));
                                }
                                else
                                {
                                    var updateResult = _paymentService.UpdateMessageAsync(p.Id, paidResult.Error).Result;
                                }
                            }
                            else
                            {
                                var updateResult = _paymentService.UpdateStatusAsync(p.Id, PaymentStatusEnum.Cancelled, "Omnivore ticket due lower than payment amount.").Result;
                            }
                        }
                        else
                        {
                            var updateResult = _paymentService.UpdateStatusAsync(p.Id, PaymentStatusEnum.Cancelled, "Ticket closed on Omnivore.").Result;
                        }
                    }
                    else
                    {
                        var updateResult = _paymentService.UpdateStatusAsync(p.Id, PaymentStatusEnum.Cancelled, "Ticket number reused on Omnivore.").Result;
                    }
                }
                else
                {
                    if (ticketResult.Error == "NotFound")
                    {
                        var updateResult = _paymentService.UpdateStatusAsync(p.Id, PaymentStatusEnum.Cancelled, ticketResult.Error).Result;
                    }
                    else
                    {
                        var updateResult = _paymentService.UpdateMessageAsync(p.Id, ticketResult.Error).Result;
                    }
                }
            });

            return string.Empty;
        }
    }
}