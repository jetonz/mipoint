﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v3;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v3
{
    public interface IReceiptService : IBaseService
    {
        Task<ResultModel<Domain.Models.Api.v3.ReceiptModel>> GetAsync(string receiptId);

        Task<ResultModel<List<Domain.Models.Api.v3.ReceiptModel>>> GetByTicketIdAsync(string locationId, string ticketId);

        Task<ResultModel> AddAsync(Domain.Models.Api.v3.ReceiptModel model);
    }
}
