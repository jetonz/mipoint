﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Enums;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v2
{
    public static class PaymentMapper
    {
        //public static Domain.Models.Api.v2.PaymentModel ToDomainPayment(this Data.DataModel.Payment model)
        //{
        //    return new Domain.Models.Api.v2.PaymentModel
        //    {
        //        Id = model.Id,
        //        TicketId = model.OrderId,
        //        Amount = model.Amount,
        //        Tip = model.Tip,
        //        CardType = model.Transaction?.CardType,
        //        Created = model.Created,
        //        Updated = model.Updated,
        //        Status = (PaymentStatusEnum)((int)model.Status),
        //        Message = model.Message,
        //        Items = model.Items?.ToDomainPaymentItems()
        //    };
        //}

        //public static List<Domain.Models.Api.v2.PaymentModel> ToDomainPayments(this IEnumerable<Data.DataModel.Payment> models)
        //{
        //    var result = models.Select(p => p.ToDomainPayment()).ToList();
        //    return result;
        //}

        //public static List<Domain.Models.Api.v2.PaymentItemModel> ToDomainPaymentItems(
        //    this IEnumerable<Data.DataModel.PaymentItem> items)
        //{
        //    return items.Select(i => new Domain.Models.Api.v2.PaymentItemModel
        //    {
        //        Id = i.Id,
        //        ItemId = i.ItemId,
        //        Name = i.Name,
        //        Price = i.Price,
        //        Quantity = i.Quantity
        //    }).ToList();
        //}

        //public static List<Domain.Models.Api.v2.PaymentModel> ToDomainPaymentModel(this IEnumerable<Data.DataModel.Payment> models)
        //{
        //    return models.Select(m => new Domain.Models.Api.v2.PaymentModel
        //    {
        //        Id = m.Id,
        //        OrderId = m.OrderId,
        //        Tip = m.Tip,
        //        Amount = m.Amount,
        //        Payload = m.Payload,
        //        ErrorMessage = m.ErrorMessage,
        //        Issued = m.Issued,
        //        Created = m.Created
        //    }).ToList();
        //}

        //public static Data.DataModel.Payment ToNewPayment(this Domain.Models.Api.v2.PaymentModel model, Data.DataModel.Location location)
        //{
        //    return new Data.DataModel.Payment
        //    {
        //        Id = model.Id == Guid.Empty ? Guid.NewGuid() : model.Id,
        //        OrderId = model.TicketId,
        //        Amount = model.Amount,
        //        Tip = model.Tip,
        //        //Transaction = model.CardType,
        //        Status = (Data.DataModel.Enums.PaymentStatusEnum)((int)model.Status),
        //        Message = model.Message,
        //        Items = model.Items?.ToPaymentItems(),

        //        Created = DateTime.UtcNow,
        //        Updated = DateTime.UtcNow,

        //        Location = location,
        //    };
        //}

        //public static List<Data.DataModel.PaymentItem> ToPaymentItems(
        //    this List<Domain.Models.Api.v2.PaymentItemModel> items)
        //{
        //    return items.Select(i => new Data.DataModel.PaymentItem
        //    {
        //        Id = i.Id == Guid.Empty ? Guid.NewGuid() : i.Id,
        //        ItemId = i.ItemId,
        //        Name = i.Name,
        //        Price = i.Price,
        //        Quantity = i.Quantity
        //    }).ToList();
        //}

        //public static void Update(this Data.DataModel.Payment entity, Domain.Models.Api.v2.PaymentModel model)
        //{
        //    entity.OrderId = model.OrderId;
        //    entity.Amount = model.Amount;
        //    entity.Tip = model.Tip;
        //    entity.Payload = model.Payload;
        //    entity.ErrorMessage = model.ErrorMessage;
        //    entity.Created = DateTime.UtcNow;
        //}
    }
}
