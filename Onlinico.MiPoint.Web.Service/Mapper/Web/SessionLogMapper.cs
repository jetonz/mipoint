﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class SessionLogMapper
    {
        public static Data.DataModel.Log ToEntityModel(this Domain.Models.Web.SessionLogModel model)
        {
            return new Data.DataModel.Log
            {
                Id = model.Id,
                Model = model.Model,
                Content = model.Content,
                Serial = model.Serial,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Fingerprint = model.Fingerprint,
                Created = DateTime.UtcNow,
                AppVersion = model.AppVersion,
            };
        }

        public static List<Domain.Models.Web.SessionLogModel> ToDomainLogModels(this IEnumerable<Data.DataModel.Log> models)
        {
            return models.Select(l => l.ToDomainLogModel()).ToList();
        }

        public static Domain.Models.Web.SessionLogModel ToDomainLogModel(this Data.DataModel.Log model)
        {
            return new Domain.Models.Web.SessionLogModel
            {
                Id = model.Id,
                Model = model.Model,
                Fingerprint = model.Fingerprint,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Serial = model.Serial,
                Created = model.Created,
                Content = model.Content,
                AppVersion = model.AppVersion,
            };
        }
    }
}
