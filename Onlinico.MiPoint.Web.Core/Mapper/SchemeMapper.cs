﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Core.Mapper
{
    public static class SchemeMapper
    {
        public static void To(object from, object to, Dictionary<string, string> scheme)
        {
            foreach (KeyValuePair<string, string> entry in scheme)
            {
                if (ContainsProperty(to, entry.Key) && ContainsProperty(from, entry.Value))
                {
                    SetPropValue(to, entry.Key, GetPropValue(from, entry.Value));
                }
            }
        }

        public static bool ContainsProperty(object src, string propName)
        {
            return src.GetType().GetProperty(propName) != null;
        }

        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName)?.GetValue(src, null);
        }

        public static void SetPropValue(object src, string propName, object val)
        {
            var propertyInfo = src.GetType().GetProperty(propName);
            try
            {
                val = ChangeType(val, propertyInfo.PropertyType);

                propertyInfo.SetValue(src, val, null);
            }
            catch (Exception ex)
            {
                //TODO
                var test = ex.Message;
            }
        }

        public static object ChangeType(object value, Type conversion)
        {
            var t = conversion;

            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (value == null)
                {
                    return null;
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return Convert.ChangeType(value, t);
        }
    }
}