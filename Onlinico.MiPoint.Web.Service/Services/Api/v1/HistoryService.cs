﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v1
{
    public class HistoryService : BaseService, IHistoryService
    {
        public HistoryService(ILogger logger) : base(logger)
        {
        }
        
        public async Task<ResultModel> AddAsync(HistoryModel model)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == model.LocationSecret);
                    if (location != null)
                    {
                        var history = new History
                        {
                            Id = Guid.NewGuid(),
                            Method = model.Method,
                            Uri = model.Url,
                            Start = model.Start,
                            End = model.End,
                            StatusCode = model.StatusCode,
                            Content = model.Content,
                            Location = location
                        };

                        context.Histories.Add(history);

                        await context.SaveChangesAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        result = new ResultModel(false, "Can't find location by 'location secret'");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }
    }
}
