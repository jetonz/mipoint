﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Onlinico.MiPoint.Web.Api.Models;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    public class AccountController : BaseController
    {
        private readonly ApplicationUserManager _applicationUserManager;
        private readonly ApplicationSignInManager _applicationSignInManager;
        private readonly IInviteService _inviteService;
        private readonly IEmailService _emailService;

        public AccountController(ApplicationUserManager applicationUserManager, ApplicationSignInManager applicationSignInManager, IInviteService inviteService, IConfiguration configuration, IEmailService emailService) : base(configuration)
        {
            _applicationUserManager = applicationUserManager;
            _applicationSignInManager = applicationSignInManager;
            _inviteService = inviteService;
            _emailService = emailService;
        }

        // GET: /Account/Login
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _applicationSignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                //return View("Lockout");
                case SignInStatus.RequiresVerification:
                //return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        // POST: /Account/LogOff
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Merchant");
        }

        /// <summary>
        /// Get regist form
        /// </summary>
        /// <param name="inviteId"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.AllowAnonymous]
        public async Task<ActionResult> Register(Guid inviteId, string returnUrl)
        {
            var invite = _inviteService.Get(inviteId);
            if (invite.Object != null)
            {
                ViewBag.ReturnUrl = returnUrl;
                return View(new RegisterViewModel { Email = invite.Object.Email });
            }
            else
            {
                return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            }
        }

        /// <summary>
        /// Regist user post
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = new User { Id = Guid.NewGuid().ToString(), UserName = model.Email, Email = model.Email, FullName = model.UserName };

                var result = await _applicationUserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    result = _applicationUserManager.AddToRole(user.Id, Configuration.MerchantManagerRole);

                    if (result.Succeeded)
                    {
                        await _applicationSignInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }
            return View(model);
        }

        /// <summary>
        /// Get forgot password view
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// Submit forgot password model
        /// </summary>
        /// <param name="model">Forgot password model</param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _applicationUserManager.FindByNameAsync(model.Email);
                if (user != null /*|| !(await _applicationUserManager.IsEmailConfirmedAsync(user.Id))*/)
                {
                    string code = await _applicationUserManager.GeneratePasswordResetTokenAsync(user.Id);


                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request?.Url?.Scheme);

                    var result = await _emailService.SendAsync(user.Email, "MiPoint Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    //await _applicationUserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("ForgotPasswordConfirmation", "Account");
                }
            }
            return View(model);
        }

        /// <summary>
        /// Get Forgot Password Confirmation View
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// Reset Password GET
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View(new ResetPasswordViewModel { Code = code });
        }

        /// <summary>
        /// Reset Password POST
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _applicationUserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var result = await _applicationUserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        /// <summary>
        /// Reset Password Confirmation GET
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// MiPoint End User Licence Agreement v1.1
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [System.Web.Mvc.HttpGet]
        public ActionResult LicenceAgreement()

        {
            return View();
        }

        /// <summary>
        /// MiPoint Privacy Policy v1.1
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        [System.Web.Mvc.HttpGet]
        public ActionResult PrivacyPolicy()
        {
            return View();
        }


        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Merchant");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error?.Replace("token", "email"));
            }
        }
    }
}