﻿using System;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class MerchantItemModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool Selected { get; set; }

        public DateTime Created { get; set; }
    }
}