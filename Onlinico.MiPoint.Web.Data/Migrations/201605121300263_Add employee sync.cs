namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addemployeesync : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationSettings", "SyncTime", c => c.Time(nullable: false, precision: 7));
            AddColumn("dbo.Locations", "LastSync", c => c.DateTime(nullable: false, defaultValueSql: "getdate()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "LastSync");
            DropColumn("dbo.LocationSettings", "SyncTime");
        }
    }
}
