namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInvites : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invites",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Email = c.String(nullable: false, maxLength: 50),
                        Merchant_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Merchants", t => t.Merchant_Id, cascadeDelete: true)
                .Index(t => t.Merchant_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invites", "Merchant_Id", "dbo.Merchants");
            DropIndex("dbo.Invites", new[] { "Merchant_Id" });
            DropTable("dbo.Invites");
        }
    }
}
