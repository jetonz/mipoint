﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums.Transaction;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4
{
    public class PaymentModel
    {
        public string Id { get; set; }

        [Required]
        public string DeviceId { get; set; }

        [Required]
        public string OrderId { get; set; }

        public Domain.Enums.PaymentTypeEnum PaymentType { get; set; }

        public Domain.Enums.PaymentStatusEnum Status { get; set; }

        [Required]
        public long Amount { get; set; }

        public long Tips { get; set; }

        public long Tax { get; set; }

        public string EmployeeId { get; set; }
        
        public PaymentTransaction Transaction { get; set; }

        [Required]
        public PaymentReceptInfo Receipt { get; set; }

        public IEnumerable<PaymentItemModel> Items { get; set; }
    }

    public class ShortPaymentModel
    {
        public string Id { get; set; }

        [Required]
        public string DeviceId { get; set; }

        [Required]
        public string OrderId { get; set; }

        public Domain.Enums.PaymentTypeEnum PaymentType { get; set; }

        public Domain.Enums.PaymentStatusEnum Status { get; set; }

        [Required]
        public long Amount { get; set; }

        public long Tips { get; set; }

        public long Tax { get; set; }

        public string EmployeeId { get; set; }
        
        [Required]
        public PaymentReceptInfo Receipt { get; set; }

        public IEnumerable<PaymentItemModel> Items { get; set; }
    }

    public class PaymentItemModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public long Price { get; set; }
    }

    public class PaymentTransaction
    {
        [Required]
        public string TransactionId { get; set; }

        public DateTimeOffset Issued { get; set; }

        public CardEntryTypeEnum CardEntryType { get; set; }

        [Required]
        public CardTypeEnum CardType { get; set; }

        [Required]
        [MaxLength(4)]
        public string lastFourDigits { get; set; }

        public string ReferenceId { get; set; }

        public string AuthCode { get; set; }

        public string ApplicationId { get; set; }

        public TransactionTypeEnum TransactionType { get; set; }

        public string Cvm { get; set; }
    }

    public class PaymentReceptInfo
    {
        public Guid Id { get; set; }

        public string OrderNumber { get; set; }

        public DateTimeOffset Issued { get; set; }

        public string TableName { get; set; }

        public long SubTotal { get; set; }

        public long Tax { get; set; }

        public long Total { get; set; }

        public long ServiceCharge { get; set; }

        public string Cashier { get; set; }

        public string LocationName { get; set; }

        public List<string> Headers { get; set; }

        public string PhoneNumber { get; set; }

        public string Website { get; set; }
    }
}