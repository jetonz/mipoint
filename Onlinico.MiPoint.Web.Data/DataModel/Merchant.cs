namespace Onlinico.MiPoint.Web.Data.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Merchant
    {
        public Merchant()
        {
            Locations = new HashSet<Location>();
            Users = new HashSet<User>();
            Invites = new HashSet<Invite>();
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }

        [MaxLength(250)]
        public string Country { get; set; }

        [MaxLength(250)]
        public string City { get; set; }

        [MaxLength(250)]
        public string State { get; set; }

        [MaxLength(250)]
        public string Zip { get; set; }

        [MaxLength(250)]
        public string Address { get; set; }
        
        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Updated { get; set; }

        public bool IsRemoved { get; set; }

        public string WebSite { get; set; }

        public string LogoUrl { get; set; }


        public virtual ICollection<User> Users { get; set; } 

        public virtual ICollection<Location> Locations { get; set; }

        public virtual ICollection<Invite> Invites { get; set; }
    }
}
