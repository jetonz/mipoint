﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class MappingScheme
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(250)]
        public string Scheme { get; set; }

        public MappingSchemeTypes Type { get; set; }


        public virtual LocationSettings Settings { get; set; }
    }
}
