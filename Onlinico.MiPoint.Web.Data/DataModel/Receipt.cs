﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class Receipt
    {
        public Receipt()
        {
            Items = new HashSet<ReceiptItem>();
        }

        [Key]
        [Required]
        [MaxLength(10)]
        public string Id { get; set; }

        [Index]
        [MaxLength(36)]
        public string TicketId { get; set; }

        //Not related with location, it's important!
        [Index]
        [MaxLength(36)]
        public string LocationId { get; set; }

        public DateTime Created { get; set; }

        public string ReceiptType { get; set; }

        public string Destination { get; set; }

        public string LocationName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Phone { get; set; }

        public string Cashier { get; set; }

        public string ReceiptDate { get; set; }

        public long TipSum { get; set; }

        public long TotalSum { get; set; }

        public long AmountToPay { get; set; }

        public float TaxSum { get; set; }

        public long ServiceChargesSum { get; set; }

        public string TicketNumber { get; set; }
        

        public virtual ReceiptPayment Payment { get; set; }

        public virtual ICollection<ReceiptItem> Items { get; set; }
    }
}
