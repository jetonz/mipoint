﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Api.Models.Web.Validation;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// User controller
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IMerchantService _merchantService;
        private readonly ApplicationUserManager _applicationUserManager;

        /// <summary>
        /// User controller constructor
        /// </summary>
        /// <param name="userService">Inject user service</param>
        /// <param name="merchantService">Inject merchant service</param>
        /// <param name="configuration">Inject configuration</param>
        /// <param name="applicationUserManager">Inject userManager</param>
        public UserController(IUserService userService, IMerchantService merchantService, Core.Interfaces.IConfiguration configuration, ApplicationUserManager applicationUserManager) : base(configuration)
        {
            _userService = userService;
            _merchantService = merchantService;
            _applicationUserManager = applicationUserManager;
        }

        /// <summary>
        /// Index view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users()
        {
            return View();
        }

        public ActionResult UserRows()
        {
            return View(_userService.Get()?.ToUserViewModelList());
        }

        [HttpGet]
        public ActionResult UserRelations(string userId)
        {
            ViewBag.UserId = userId;

            var result = new UserMerchantRelationsModel { UserId = userId };

            var allMerchantsResult = _merchantService.Get();
            if (allMerchantsResult.Succeed)
            {
                var userMerchantResult = _userService.GetMerchants(userId);
                result.SelectedMerchantItems = userMerchantResult.Succeed 
                    ? allMerchantsResult.Object?.ToMerchantViewModels().ToSelectedItems(userMerchantResult.Object?.ToMerchantViewModels()) 
                    : allMerchantsResult.Object?.ToMerchantViewModels().ToSelectedItems();
            }

            return View(result);
        }

        [HttpPost]
        public ActionResult UserRelations(UserMerchantRelationsModel model)
        {
            var result = _userService.UpdateUserRelations(model.UserId, model.SelectedMerchants);
            return new HttpStatusCodeResult(result.Succeed ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
        }

        public JsonResult RemoveUser(string userId)
        {
            var result = false;
            var user = _applicationUserManager.FindById(userId);
            if (user != null)
            {
                result = _merchantService.RemoveMerchantsFromUser(userId).Succeed;
                if (result)
                {
                    result = _applicationUserManager.Delete(user).Succeeded;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Register()
        {
            var allMerchantsResult = _merchantService.Get();
            if (allMerchantsResult.Succeed)
            {
                var result = new RegisterModel()
                {
                    SelectedMerchantItems = allMerchantsResult.Object?.ToMerchantViewModels().ToSelectedItems()
                };
                return View(result);
            }
            return View("Error");
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (model.IsValid())
            {
                var createResult = await _applicationUserManager.CreateAsync(new User { UserName = model.Login, FullName = model.Name, Email = model.Login }, model.Password);
                if (createResult.Succeeded)
                {
                    var user = await _applicationUserManager.FindByNameAsync(model.Login);
                    if (user != null)
                    {
                        await _applicationUserManager.AddToRoleAsync(user.Id, Configuration.MerchantManagerRole);

                        if (model.SelectedMerchants != null && model.SelectedMerchants.Any())
                        {
                            var result = _merchantService.RelateMerchantsWithUser(model.SelectedMerchants, user.Id);
                            if (!result.Succeed)
                            {
                                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User was created, but not related with retailers.");
                            }
                        }

                        return PartialView("UserRow", user?.ToUserModel());
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Some error happened, please try again later.");
                    }
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, createResult.Errors.First().Replace("Name", "Login"));
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid user data.");
        }

        public JsonResult IsUsernameUnique(string login)
        {
            var result = _userService.IsUsernameUnique(login);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}