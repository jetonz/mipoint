﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3
{
    public class PaymentModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [Required]
        [JsonProperty("deviceId")]
        public string DeviceId { get; set; }

        [Required]
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
        
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("type")]
        public PaymentTypeEnum Type { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("status")]
        public PaymentStatusEnum Status { get; set; }

        [Required]
        [JsonProperty("amount")]
        public long Amount { get; set; }
        
        [JsonProperty("tip")]
        public long Tip { get; set; }
        
        [JsonProperty("tax")]
        public long Tax { get; set; }

        [JsonProperty("employeeCode")]
        public string EmployeeCode { get; set; }

        [JsonProperty("tableNumber")]
        public string TableNumber { get; set; }
        
        [JsonProperty("transaction")]
        public PaymentTransaction Transaction { get; set; }

        [Required]
        [JsonProperty("receiptInfo")]
        public PaymentReceptInfo ReceiptInfo { get; set; }

        [JsonProperty("items")]
        public IEnumerable<PaymentItemModel> Items { get; set; }
    }

    public class PaymentItemModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("itemId")]
        public string ItemId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }
    }

    public class PaymentTransaction
    {
        [Required]
        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }
        
        [JsonProperty("issued")]
        public DateTimeOffset Issued { get; set; }

        [JsonProperty("cardEntryType")]
        public string CardEntryType { get; set; }

        [Required]
        [JsonProperty("cardType")]
        public string CardType { get; set; }

        [Required]
        [MaxLength(4)]
        [JsonProperty("last4")]
        public string Last4 { get; set; }

        [JsonProperty("referenceId")]
        public string ReferenceId { get; set; }

        [JsonProperty("authCode")]
        public string AuthCode { get; set; }

        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }
        
        [JsonProperty("transactionType")]
        public string TransactionType { get; set; }

        [JsonProperty("cvm")]
        public string Cvm { get; set; }
    }

    public class PaymentReceptInfo
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("orderNumber")]
        public string OrderNumber { get; set; }

        [JsonProperty("issued")]
        public DateTimeOffset Issued { get; set; }

        [JsonProperty("orderSubTotal")]
        public long OrderSubTotal { get; set; }

        [JsonProperty("orderTax")]
        public long OrderTax { get; set; }

        [JsonProperty("orderTotal")]
        public long OrderTotal { get; set; }

        [JsonProperty("orderServiceCharge")]
        public long OrderServiceCharge { get; set; }

        [JsonProperty("cashier")]
        public string Cashier { get; set; }

        [JsonProperty("locationName")]
        public string LocationName { get; set; }

        [JsonProperty("receiptHeader1")]
        public string ReceiptHeader1 { get; set; }

        [JsonProperty("receiptHeader2")]
        public string ReceiptHeader2 { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }
    }
}