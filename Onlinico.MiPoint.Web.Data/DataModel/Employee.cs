﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class Employee
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(50)]
        public string EmployeeId { get; set; }

        [MaxLength(50)]
        public string CheckName { get; set; }

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [Index]
        [MaxLength(50)]
        public string Login { get; set; }


        public virtual Location Location { get; set; }
    }
}
