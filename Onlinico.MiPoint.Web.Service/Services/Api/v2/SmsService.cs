﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v2
{
    public class SmsService : BaseService, ISmsService
    {
        private const string Targeturi = "https://api.twilio.com/2010-04-01/Accounts/{0}/SMS/Messages";

        private readonly IConfiguration _configuration;

        public SmsService(ILogger logger, IConfiguration configuration) : base(logger)
        {
            _configuration = configuration;
        }



        public async Task<ResultModel> SendAsync(string to, string message)
        {
            var result = new ResultModel(true);
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = CreateBasicAuthenticationHeader(_configuration.TwilioAccountSid, _configuration.TwilioAuthToken);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var dict = new Dictionary<string, string> { { "From", _configuration.TwilioPhoneNumber }, { "To", to }, { "Body", message} };
                    var content = new FormUrlEncodedContent(dict);

                    var response = await client.PostAsync(string.Format(Targeturi, _configuration.TwilioAccountSid), content);

                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                        case HttpStatusCode.Created:

                            break;
                        default:
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var xml = new XmlDocument();
                            xml.LoadXml(responseContent);
                            var errorMessage = xml.GetElementsByTagName("Message")[0]?.InnerText;
                            result = new ResultModel(false, errorMessage);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }
            return result;
        }

        private AuthenticationHeaderValue CreateBasicAuthenticationHeader(string username, string password)
        {
            return new AuthenticationHeaderValue("Basic", System.Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(string.Format("{0}:{1}", username, password))));
        }
    }
}
