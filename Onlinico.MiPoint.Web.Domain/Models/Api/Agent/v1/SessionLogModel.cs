﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class SessionLogModel
    {
        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string Fingerprint { get; set; }

        public string Serial { get; set; }

        public string Content { get; set; }

        public string AppVersion { get; set; }

        public DateTime Created { get; set; }
    }
}
