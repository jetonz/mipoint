﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class MerchantModel
    {
        public Guid Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [Required]
        [MaxLength(250)]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Address { get; set; }

        public DateTime Created { get; set; }

        public string WebSite { get; set; }

        public string LogoUrl { get; set; }

        public List<SelectedItem<LocationModel>> SelectedLocations { get; set; } 

        public List<string> Identifiers { get; set; } 
    }
}