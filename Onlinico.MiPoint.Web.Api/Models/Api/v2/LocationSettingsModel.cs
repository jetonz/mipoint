﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2
{
    /// <summary>
    /// Location settings
    /// </summary>
    [DataContract]
    public class LocationSettingsModel
    {

        /// <summary>
        /// Omnivore location id
        /// </summary>
        [DataMember(Name = "locationId")]
        public string OmnivoreId { get; set; }

        /// <summary>
        /// Tips array
        /// </summary>
        [DataMember(Name = "tips")]
        public int[] Tips { get; set; }

        /// <summary>
        /// Tax rate
        /// </summary>
        [DataMember(Name = "taxRate")]
        public float TaxRate { get; set; }


        /// <summary>
        /// Tender type list
        /// </summary>
        [DataMember(Name = "tenderTypes")]
        public Dictionary<string, string> TenderTypes { get; set; }

        /// <summary>
        /// Log out after payment flag
        /// </summary>
        [DataMember(Name = "afterPaymentLogOut")]
        public bool AfterPaymentLogOut { get; set; }

        /// <summary>
        /// Session timeout in seconds
        /// </summary>
        [DataMember(Name = "sessionTimeout")]
        public int SessionTimeout { get; set; }

        /// <summary>
        /// Order limit
        /// </summary>
        [DataMember(Name = "orderLimit")]
        public int OpenOrderLimit { get; set; }

        /// <summary>
        /// Order limit
        /// </summary>
        [DataMember(Name = "closedOrderLimit")]
        public int ClosedOrderLimit { get; set; }
    }
}