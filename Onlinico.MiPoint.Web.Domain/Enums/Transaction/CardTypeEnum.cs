﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Enums.Transaction
{
    public enum CardTypeEnum
    {
        VISA = 0,
        MasterCard = 1,
        AMEX = 2,
        Discover = 3,
        Other = 4,
        
        //VISA = 0,
        //MC = 1,
        //AMEX = 2,
        //DISCOVER = 3,
        //DINERS_CLUB = 4,
        //JCB = 5,
        //MAESTRO = 6,
        //SOLO = 7,
        //LASER = 8,
        //CHINA_UNION_PAY = 9,
        //CARTE_BLANCHE = 10,
        //UNKNOWN = 11,
        //GIFT_CARD = 12,
        //EBT = 13
    }
}
