﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v2
{
    public class PaymentModel
    {
        public Guid Id { get; set; }

        public string TicketId { get; set; }

        public long Amount { get; set; }

        public long Tip { get; set; }

        public string CardType { get; set; }

        public Enums.PaymentStatusEnum Status { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public string Message { get; set; }

        public List<PaymentItemModel> Items { get; set; }
    }

    public class PaymentItemModel
    {
        public Guid Id { get; set; }

        public string ItemId { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public long Price { get; set; }
    }
}
