﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v1
{
    public class LogService : BaseService, ILogService
    {
        public LogService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel> AddAsync(LogModel model)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    context.Logs.Add(model.ToEntityModel());

                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public ResultModel<List<LogModel>> Get()
        {
            var result = new ResultModel<List<LogModel>>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    result.Object = context.Logs.OrderByDescending(l=>l.Created).ToDomainLogModels();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<List<LogModel>>(false, ex.Message);
            }

            return result;
        }

        public ResultModel<LogModel> Get(Guid logId)
        {
            var result = new ResultModel<LogModel>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var log = context.Logs.FirstOrDefault(l => l.Id == logId);
                    if (log != null)
                    {
                        result.Object = log.ToDomainLogModel();
                    }
                    else
                    {
                        result = new ResultModel<LogModel>(false, "Invalid log id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<LogModel>(false, ex.Message);
            }

            return result;
        }
    }
}
