﻿using System;
using System.Linq;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v1
{
    public static class SettingsMapper
    {
        public static LocationSettingsModel ToDomainSettings(this Data.DataModel.LocationSettings settings)
        {
            var tenderType = settings.TenderTypes.First(t => t.Card.Type == "Other");

            return new LocationSettingsModel
            {
                Id = settings.Id,
                TenderTypeId = tenderType.Type,
                TenderType = tenderType.Name,
                TaxRate = settings.TaxRate,
                Tips = settings.Tips.Split(',').Select(Int32.Parse).ToArray()
            };
        }

        public static Data.DataModel.LocationSettings ToEntitySettings(this LocationSettingsModel settings)
        {
            return new Data.DataModel.LocationSettings
            {
                Id = settings.Id,
                //TenderTypeId = settings.TenderTypeId,
                //TenderType = settings.TenderType,
                TaxRate = settings.TaxRate,
                Tips = string.Join(",", settings.Tips)
            };
        }

        public static void UpdateSettings(this Data.DataModel.LocationSettings entity, LocationSettingsModel settings)
        {
            //entity.TenderTypeId = settings.TenderTypeId;
            //entity.TenderType = settings.TenderType;
            entity.TaxRate = settings.TaxRate;
            entity.Tips = string.Join(",", settings.Tips);
        }
    }
}
