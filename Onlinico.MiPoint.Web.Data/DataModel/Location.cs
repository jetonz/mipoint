﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class Location
    {
        public Location()
        {
            Histories = new HashSet<History>();
            Employees = new HashSet<Employee>();
            Payments = new HashSet<Payment>();
        }

        [Key]
        [MaxLength(36)]
        public string Id { get; set; }

        [Required]
        [MaxLength(36)]
        public string Identifier { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string Phone { get; set; }

        [MaxLength(250)]
        public string Address { get; set; }

        [MaxLength(250)]
        public string AddressFull { get; set; }

        [MaxLength(250)]
        public string Owner { get; set; }

        [MaxLength(250)]
        public string PosType { get; set; }

        public bool IsDev { get; set; }

        public DateTime LastSync { get; set; }

        public bool IsRemoved { get; set; }

        [Index]
        [MaxLength(128)]
        public string AgentFingerprint { get; set; }

        public DateTime? LastAgentPoll { get; set; }

        [MaxLength(128)]
        public string AgentVersion { get; set; }

        [MaxLength(128)]
        public string AgentAddress { get; set; }


        public virtual Merchant Merchant { get; set; }

        public virtual LocationSettings Settings { get; set; }
        
        public virtual ClientCounterModeSettings ClientCounterModeSettings { get; set; }

        public virtual ClientTableModeSettings ClientTableModeSettings { get; set; }

        public virtual ICollection<History> Histories { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
    }
}
