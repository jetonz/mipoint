﻿using System;
using System.Runtime.Caching;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;

namespace Onlinico.MiPoint.Web.Service.Services.Global
{
    public class MemoryCacher : IMemoryCacher
    {
        public T GetValue<T>(string key) where T : class
        {
            var memoryCache = MemoryCache.Default;
            
            return memoryCache.Contains(key) ? memoryCache.Get(key) as T : null;
        }

        public bool Add(string key, object value, DateTimeOffset absExpiration)
        {
            var cachePolicty = new CacheItemPolicy {AbsoluteExpiration = absExpiration};

            var memoryCache = MemoryCache.Default;
            
            return memoryCache.Add(key, value, cachePolicty);

        }

        public object Delete(string key)
        {
            var memoryCache = MemoryCache.Default;
            return memoryCache.Contains(key) ? memoryCache.Remove(key) : null;
        }

        public bool ContainsKey(string key)
        {
            var memoryCache = MemoryCache.Default;
            return memoryCache.Contains(key);
        }
    }
}
