﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v2
{
    public class LocationSettingsModel
    {
        public Guid Id { get; set; }

        public string OmnivoreId { get; set; }

        public int[] Tips { get; set; }
        
        public float TaxRate { get; set; }
        
        public bool AfterPaymentLogOut { get; set; }

        public int SessionTimeout { get; set; }

        public int OpenOrderLimit { get; set; }

        public int ClosedOrderLimit { get; set; }

        public List<TenderTypeModel> TenderTypes { get; set; } 
    }
}
