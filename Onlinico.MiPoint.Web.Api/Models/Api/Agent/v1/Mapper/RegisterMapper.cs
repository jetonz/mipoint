﻿namespace Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1.Mapper
{
    public static class RegisterMapper
    {
        public static Domain.Models.Api.Agent.v1.RegisterModel ToDomainResultModel(this RegisterModel model)
        {
            return new Domain.Models.Api.Agent.v1.RegisterModel()
            {
                Fingerprint = model.Fingerprint,
                ApiKey = model.ApiKey,
                AgentVersion = model.AgentVersion,
            };
        }
    }
}