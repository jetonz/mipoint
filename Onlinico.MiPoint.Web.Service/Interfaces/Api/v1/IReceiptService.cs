﻿using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v1
{
    public interface IReceiptService
    {
        Task<ResultModel<ReceiptModel>> GetAsync(string receiptId);

        Task<ResultModel> AddAsync(ReceiptModel model);
    }
}
