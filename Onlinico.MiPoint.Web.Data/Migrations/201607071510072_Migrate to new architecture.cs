namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migratetonewarchitecture : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36),
                        DeviceId = c.String(nullable: false, maxLength: 50),
                        OrderId = c.String(nullable: false, maxLength: 50),
                        Type = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        Tip = c.Long(nullable: false),
                        Tax = c.Long(nullable: false),
                        EmployeeCode = c.String(maxLength: 36),
                        TableNumber = c.String(maxLength: 36),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        Message = c.String(),
                        Location_Id = c.String(nullable: false, maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.DeviceId)
                .Index(t => t.OrderId)
                .Index(t => t.Type)
                .Index(t => t.Status)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.PaymentItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ItemId = c.String(maxLength: 36),
                        Name = c.String(maxLength: 250),
                        Quantity = c.Int(nullable: false),
                        Price = c.Long(nullable: false),
                        Payment_Id = c.String(nullable: false, maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Payments", t => t.Payment_Id, cascadeDelete: true)
                .Index(t => t.Payment_Id);
            
            CreateTable(
                "dbo.PaymentReceiptInfoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OrderNumber = c.String(nullable: false, maxLength: 36),
                        Issued = c.DateTimeOffset(nullable: false, precision: 7),
                        OrderSubTotal = c.Long(nullable: false),
                        OrderTax = c.Long(nullable: false),
                        OrderTotal = c.Long(nullable: false),
                        OrderServiceCharge = c.Long(nullable: false),
                        Cashier = c.String(maxLength: 50),
                        LocationName = c.String(maxLength: 100),
                        ReceiptHeader1 = c.String(maxLength: 100),
                        ReceiptHeader2 = c.String(maxLength: 100),
                        PhoneNumber = c.String(maxLength: 20),
                        Website = c.String(maxLength: 50),
                        Payment_Id = c.String(nullable: false, maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Payments", t => t.Payment_Id, cascadeDelete: true)
                .Index(t => t.OrderNumber)
                .Index(t => t.Payment_Id);
            
            CreateTable(
                "dbo.PaymentTransactions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TransactionId = c.String(maxLength: 36),
                        Issued = c.DateTimeOffset(nullable: false, precision: 7),
                        CardEntryType = c.String(maxLength: 30),
                        CardType = c.String(maxLength: 30),
                        Last4 = c.String(maxLength: 4),
                        ReferenceId = c.String(maxLength: 36),
                        AuthCode = c.String(maxLength: 36),
                        ApplicationId = c.String(maxLength: 36),
                        TransactionType = c.String(maxLength: 30),
                        Cvm = c.String(maxLength: 50),
                        Payment_Id = c.String(nullable: false, maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Payments", t => t.Payment_Id, cascadeDelete: true)
                .Index(t => t.TransactionId)
                .Index(t => t.Payment_Id);
            
            AddColumn("dbo.Locations", "IsRemoved", c => c.Boolean(nullable: false));
            AddColumn("dbo.Histories", "DeviceId", c => c.String(maxLength: 36));
            AddColumn("dbo.Histories", "Agent", c => c.String(maxLength: 250));
            AddColumn("dbo.Merchants", "Email", c => c.String(maxLength: 100));
            AddColumn("dbo.Merchants", "PhoneNumber", c => c.String(maxLength: 50));
            AddColumn("dbo.Merchants", "Country", c => c.String(maxLength: 250));
            AddColumn("dbo.Merchants", "City", c => c.String(maxLength: 250));
            AddColumn("dbo.Merchants", "State", c => c.String(maxLength: 250));
            AddColumn("dbo.Merchants", "Zip", c => c.String(maxLength: 250));
            AddColumn("dbo.Merchants", "Address", c => c.String(maxLength: 250));
            AddColumn("dbo.Merchants", "Updated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Merchants", "IsRemoved", c => c.Boolean(nullable: false));
            DropColumn("dbo.Merchants", "OmnivoreApiKey");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Merchants", "OmnivoreApiKey", c => c.String(nullable: false, maxLength: 250));
            DropForeignKey("dbo.Payments", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.PaymentTransactions", "Payment_Id", "dbo.Payments");
            DropForeignKey("dbo.PaymentReceiptInfoes", "Payment_Id", "dbo.Payments");
            DropForeignKey("dbo.PaymentItems", "Payment_Id", "dbo.Payments");
            DropIndex("dbo.PaymentTransactions", new[] { "Payment_Id" });
            DropIndex("dbo.PaymentTransactions", new[] { "TransactionId" });
            DropIndex("dbo.PaymentReceiptInfoes", new[] { "Payment_Id" });
            DropIndex("dbo.PaymentReceiptInfoes", new[] { "OrderNumber" });
            DropIndex("dbo.PaymentItems", new[] { "Payment_Id" });
            DropIndex("dbo.Payments", new[] { "Location_Id" });
            DropIndex("dbo.Payments", new[] { "Status" });
            DropIndex("dbo.Payments", new[] { "Type" });
            DropIndex("dbo.Payments", new[] { "OrderId" });
            DropIndex("dbo.Payments", new[] { "DeviceId" });
            DropColumn("dbo.Merchants", "IsRemoved");
            DropColumn("dbo.Merchants", "Updated");
            DropColumn("dbo.Merchants", "Address");
            DropColumn("dbo.Merchants", "Zip");
            DropColumn("dbo.Merchants", "State");
            DropColumn("dbo.Merchants", "City");
            DropColumn("dbo.Merchants", "Country");
            DropColumn("dbo.Merchants", "PhoneNumber");
            DropColumn("dbo.Merchants", "Email");
            DropColumn("dbo.Histories", "Agent");
            DropColumn("dbo.Histories", "DeviceId");
            DropColumn("dbo.Locations", "IsRemoved");
            DropTable("dbo.PaymentTransactions");
            DropTable("dbo.PaymentReceiptInfoes");
            DropTable("dbo.PaymentItems");
            DropTable("dbo.Payments");
        }
    }
}
