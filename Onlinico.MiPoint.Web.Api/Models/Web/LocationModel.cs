﻿namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class LocationModel
    {
        public string Id { get; set; }
        
        public string Identifier { get; set; }

        public string Name { get; set; }

        public string Owner { get; set; }

        public string PosType { get; set; }

        public bool IsDev { get; set; }

        public bool IsConfigured { get; set; }

        public bool IsAgentActive { get; set; }

        public bool IsVicarActive { get; set; }

        public bool IsOffline { get; set; }

        public string AgentFingerprint { get; set; }
    }
}