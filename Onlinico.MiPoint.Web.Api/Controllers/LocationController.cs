﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Api.Hubs.Interfaces;
using Onlinico.MiPoint.Web.Api.Hubs.Wrappers;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Location controller
    /// </summary>
    public class LocationController : BaseController
    {
        private readonly ILocationService _locationService;
        private readonly IOmnivoreService _omnivoreService;
        private readonly IAuthService _authService;
        private readonly IAgentManager _agentManager;
        private readonly IAgentHubPusher _agentHubPusher;
        private readonly IVicarPusher _agentServiceHubPusher;

        /// <summary>
        /// Location controller constructor
        /// </summary>
        /// <param name="locationService">Inject location service</param>
        /// <param name="omnivoreService">Inject location service</param>
        /// <param name="authService">Inject auth service</param>
        /// <param name="agentManager">Inject agent manager</param>
        /// <param name="agentHubPusher">Inject agent hub pusher</param>
        /// <param name="agentServiceHubPusher">Inject agent service hub pusher</param>
        /// <param name="configuration">Inject configuration</param>
        public LocationController(ILocationService locationService, IOmnivoreService omnivoreService,
            IAuthService authService, IAgentManager agentManager, IAgentHubPusher agentHubPusher, IVicarPusher agentServiceHubPusher,
            IConfiguration configuration) : base(configuration)
        {
            _locationService = locationService;
            _omnivoreService = omnivoreService;
            _authService = authService;
            _agentManager = agentManager;
            _agentHubPusher = agentHubPusher;
            _agentServiceHubPusher = agentServiceHubPusher;
        }

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(Guid merchantId)
        {
            ViewBag.MerchantId = merchantId;
            return View();
        }

        /// <summary>
        /// Locations page
        /// </summary>
        /// <param name="merchantId">Merchant id</param>
        /// <returns>Зage</returns>
        public ActionResult Locations(Guid merchantId)
        {
            ViewBag.IsAdmin = IsAdmin;
            ViewBag.MerchantId = merchantId;

            return View();
        }

        /// <summary>
        /// Get datatable location rows
        /// </summary>
        /// <param name="merchantId">merchant id</param>
        /// <returns></returns>
        public ActionResult LocationRows(Guid merchantId)
        {
            ViewBag.IsAdmin = IsAdmin;

            var locationsResult = _locationService.GetByMerchant(merchantId);

            locationsResult?.ForEach(l =>
            {
                l.IsAgentActive = _agentManager.IsConnected(l.Id, l.AgentFingerprint);
                l.IsVicarActive = _agentManager.IsVicarConnected(l.Id, l.AgentFingerprint);
            });

            return View(locationsResult?.ToLocationViewModelList());
        }

        public async Task<ActionResult> GetLocationRow(string locationId)
        {
            var locationResult = await _locationService.GetAsync(locationId);
            if (locationResult.Succeed && locationResult.Object != null)
            {
                ViewBag.IsAdmin = IsAdmin;
                return PartialView("LocationRow", locationResult.Object.ToLocationViewModel());
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Create new offline location
        /// </summary>
        /// <param name="merchantId">Merchant id</param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public async Task<ActionResult> NewOfflineLocation(Guid merchantId)
        {
            return View(new OfflineLocationModel() { MerchantId = merchantId });
        }

        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> NewOfflineLocation(OfflineLocationModel model, [FromUri]int timeOffset = 0)
        {
            if (ModelState.IsValid)
            {
                var addResult = await _locationService.AddAsync(model.MerchantId, new List<Domain.Models.Web.LocationModel> { model.ToDomainLocationModel() }, timeOffset);
            }
            return View(model);
        }

        /// <summary>
        /// Get add location form
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public async Task<ActionResult> AddLocations(Guid merchantId)
        {
            ViewBag.MerchantId = merchantId;

            var omnivoreProdLocationsResult = await _omnivoreService.GetLocationsByApiKey(Configuration.OmnivoreProdApiKey);
            var omnivoreDevLocationsResult = await _omnivoreService.GetLocationsByApiKey(Configuration.OmnivoreDevApiKey);
            if (omnivoreProdLocationsResult.Succeed && omnivoreDevLocationsResult.Succeed)
            {
                if (omnivoreDevLocationsResult.Object != null && omnivoreDevLocationsResult.Object.Any())
                {
                    omnivoreProdLocationsResult.Object?.AddRange(omnivoreDevLocationsResult.Object);
                }

                var allLocations = omnivoreProdLocationsResult.Object.ToDomainLocations();

                var existLocationsResult = await _locationService.GetAsync();
                if (existLocationsResult.Succeed)
                {
                    var existinLocations = existLocationsResult.Object.Select(l => l.Identifier).ToList();

                    var locations = allLocations.Where(
                        l => existinLocations.All(ml => ml != l.Identifier)).ToList();

                    return View(locations.ToLocationViewModelList().ToSelectedItems());
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Add new locations to merchant
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="identifiers"></param>
        /// <param name="timeOffset"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> AddLocations([FromBody]Guid merchantId, [FromBody]List<string> identifiers, [FromUri]int timeOffset = 0)
        {
            var queue = new ConcurrentQueue<Domain.Models.Global.Omnivore.LocationModel>();

            if (identifiers != null && identifiers.Any())
            {
                Task.WaitAll(identifiers.Select(i =>
                {
                    return _omnivoreService.GetLocationAsync(Configuration.OmnivoreProdApiKey, i, true).ContinueWith(
                        locationResult =>
                        {
                            if (locationResult.Result.Succeed && locationResult.Result.Object != null)
                            {
                                queue.Enqueue(locationResult.Result.Object);
                            }
                            else
                            {
                                var devLocationResult = _omnivoreService.GetLocationAsync(Configuration.OmnivoreDevApiKey, i, true).Result;
                                if (devLocationResult.Succeed && devLocationResult.Object != null)
                                {
                                    queue.Enqueue(devLocationResult.Object);
                                }
                            }
                        });
                }).ToArray());

                if (queue.Count > 0)
                {
                    var result = await _locationService.AddAsync(merchantId, queue.ToList().ToDomainLocations(), timeOffset);
                    if (result.Succeed)
                    {
                        return View();
                    }
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Remove location
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public async Task<ActionResult> RemoveLocation(string locationId)
        {
            var result = await _locationService.RemoveAsync(locationId);

            return Json(result.Succeed, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Sync loaction
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public async Task<ActionResult> SyncLocation(string locationId)
        {
            var result = false;

            var identityResult = _authService.GetIdentity(locationId);
            if (identityResult.Succeed && identityResult.Object != null)
            {
                var locationResult = await _omnivoreService.GetLocationAsync(identityResult.Object.OmnivoreApiKey, identityResult.Object.OmnivoreLocationId, true);
                if (locationResult.Succeed && locationResult.Object != null)
                {
                    locationResult.Object.Id = locationId;
                    var updateResult = await _locationService.UpdateAsync(locationResult.Object.ToDomainLocation());
                    result = updateResult.Succeed;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Location settings partial page
        /// </summary>
        /// <param name="locationId">Location id</param>
        /// <param name="isOffline">Is offline location</param>
        /// <param name="timeOffset">Client time offset</param>
        /// <returns>Partial page</returns>
        public async Task<ActionResult> Settings(string locationId, bool isOffline, int timeOffset = 0)
        {
            ViewBag.LocationId = locationId;

            var locationResult = await _locationService.GetAsync(locationId);
            if (locationResult.Succeed && locationResult.Object != null)
            {
                if (!string.IsNullOrEmpty(locationResult.Object.AgentFingerprint))
                {
                    var availableTenderTypes = _locationService.GetAvailableTenderTypes(locationId);

                    var location = _locationService.GetSettings(locationId);
                    return View(location?.ToSettingsModel(availableTenderTypes.Object, timeOffset * -1));
                }

                var identityResult = _authService.GetIdentity(locationId);
                if (identityResult.Succeed)
                {
                    var avalibleTenderTypes = await _omnivoreService.GetLocationTenderTypes(identityResult.Object.OmnivoreApiKey, identityResult.Object.OmnivoreLocationId);

                    var location = _locationService.GetSettings(locationId);
                    return View(location?.ToSettingsModel(avalibleTenderTypes?.ToDomainAvailableTenderTypes(), timeOffset * -1));
                }

            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, locationResult.Error);
        }

        /// <summary>
        /// Location settings update
        /// </summary>
        /// <param name="model">Location settings model</param>
        /// <returns>Partial page</returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult Settings(string locationId, LocationSettingsModel model)
        {
            var result = _locationService.UpdatteSettings(model?.ToDomainSettingsModel());
            if (result.Succeed)
            {
                _agentHubPusher.SendAgentSettings(locationId);
                _agentHubPusher.SendClientSettings(locationId);

                return new HttpStatusCodeResult(HttpStatusCode.Created);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// Mapping scheme view
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult MappingScheme(string locationId)
        {
            var result = _locationService.GetMappingScheme(locationId);

            return View(result.Object != null ? result.Object.ToMappingSchemeViewModel(locationId) : new MappingSchemeModel(locationId));
        }

        /// <summary>
        /// Post mapping scheme form
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> MappingScheme(MappingSchemeModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _locationService.UpdateMappingScheme(model.LocationId, model.ToDomainMappingSchemeModel());
                if (result.Succeed)
                {
                    await this.SyncLocation(model.LocationId);

                    _agentHubPusher.SendAgentSettings(model.LocationId);

                    return Json(result.Succeed, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, result.Error);
                }
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid model.");
            }
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult LogRequestForm(string locationId)
        {
            return View(new LogRequestModel() { LocationId = locationId, LogDate = DateTime.UtcNow.Date });
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult LogRequestForm(LogRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.IsAgentLog)
                {
                    _agentServiceHubPusher.SendAgentLogRequest(model.LocationId, model.LogDate);
                }
                else
                {
                    _agentHubPusher.SenClientLogRequest(model.LocationId, model.LogDate);
                }

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult RestartAgent(string locationId)
        {
            _agentServiceHubPusher.Restart(locationId);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult RemoveAgentKey(string locationId)
        {
            _agentHubPusher.Stop(locationId);
            _agentServiceHubPusher.Stop(locationId);

            Thread.Sleep(2000);

            var result = _locationService.RemoveAgentFingerprint(locationId);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult UpdateAgent(string locationId)
        {
            var updateUrl = CurrentDomain + "agent/download?locationId=" + locationId;
            _agentServiceHubPusher.SendUpdateRequest(locationId, "last version", updateUrl);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}