﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Autofac;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Api.Attributes;
using Onlinico.MiPoint.Web.Api.Hubs.Wrappers;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;

namespace Onlinico.MiPoint.Web.Api.Hubs
{
    [HubName("vicarHub")]
    [AuthorizeAgentHub]
    public class VicarHub : Hub
    {
        private readonly IAgentManager _agentManager;
        private readonly IAgentService _agentService;

        public VicarHub(ILifetimeScope lifetimeScope)
        {
            _agentManager = lifetimeScope.Resolve<IAgentManager>();
            _agentService = lifetimeScope.Resolve<IAgentService>();
        }

        public override Task OnConnected()
        {
            _agentManager.VicarConnected(ApiKey, Fingerprint);

            new UserHubPusher().AgentStateChanged(ApiKey, "Vicar-Conected");

            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            _agentManager.VicarReconnected(ApiKey, Fingerprint);

            new UserHubPusher().AgentStateChanged(ApiKey, "Vicar-Reconnected");

            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            //You should not manually remove the user from the group when the user disconnects. This action is automatically performed by the SignalR framework.

            _agentManager.VicarDisconnected(ApiKey, Fingerprint);

            new UserHubPusher().AgentStateChanged(ApiKey, "Vicar-Disconected");

            return base.OnDisconnected(stopCalled);
        }

        [HubMethodName("addSessionLog")]
        public async Task<bool> AddSessionLog(List<DeviceLogDto> model)
        {
            var result = await _agentService.AddSessionLogsAsync(ApiKey, model);
            return result.Succeed;
        }


        private string ApiKey => Context.Request.Headers.GetValues(Core.Constants.Constants.ApiKey).FirstOrDefault();

        private string Fingerprint => Context.Request.Headers.GetValues(Core.Constants.Constants.Fingerprint).FirstOrDefault();
    }
}