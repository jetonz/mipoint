﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v3;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Validation;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v3;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v3
{
    /// <summary>
    /// SessionLog controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v3/log")]
    public class SessionLogController : BaseController
    {
        private readonly ISessionLogService _logService;

        private readonly IConfiguration _configuration;

        /// <summary>
        /// Session log constructor
        /// </summary>
        /// <param name="logService">Inject log service</param>
        /// <param name="configuration">Inject configuration</param>
        public SessionLogController(ISessionLogService logService, IConfiguration configuration)
        {
            _logService = logService;
            _configuration = configuration;
        }

        /// <summary>
        /// Add MiPoint App Session Log
        /// </summary>
        /// <param name="model">Session log model</param>
        /// <returns></returns>
        [HttpPost, Route]
        public async Task<IHttpActionResult> Post([FromBody]LogModel model)
        {
            if (model.IsValid())
            {
                var result = await _logService.AddAsync(model.ToDomainLogModel(Request.Properties[_configuration.Secret].ToString()));
                return result.Succeed ? Ok() : (IHttpActionResult)BadRequest();
            }
            else
            {
                return BadRequest("Invalid model.");
            }
        }

        protected override void Dispose(bool disposing)
        {
            _logService.Dispose();
            base.Dispose(disposing);
        }
    }
}
