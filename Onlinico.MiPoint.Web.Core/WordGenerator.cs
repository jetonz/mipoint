﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Core
{
    public class StringGenerator
    {
        public static string RandomString(int firstLength)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, firstLength).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomString(int firstLength, int secondLength)
        {
            

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var firstString = new string(Enumerable.Repeat(chars, firstLength).Select(s => s[random.Next(s.Length)]).ToArray());
            var secondString = new string(Enumerable.Repeat(chars, secondLength).Select(s => s[random.Next(s.Length)]).ToArray());
            return firstString + "-" + secondString;
        }

        public static string RandomStringKey()
        {
            const string letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string number = "0123456789";
            var random = new Random();
            var firstString = new string(Enumerable.Repeat(letter, 2).Select(s => s[random.Next(s.Length)]).ToArray());
            var secondString = new string(Enumerable.Repeat(number, 1).Select(s => s[random.Next(s.Length)]).ToArray());
            var thirdString = new string(Enumerable.Repeat(letter, 2).Select(s => s[random.Next(s.Length)]).ToArray());
            var fourthString = new string(Enumerable.Repeat(number, 1).Select(s => s[random.Next(s.Length)]).ToArray());
            var fifthString = new string(Enumerable.Repeat(letter, 2).Select(s => s[random.Next(s.Length)]).ToArray());
            return firstString + secondString + thirdString + fourthString + fifthString;
        }
    }
}
