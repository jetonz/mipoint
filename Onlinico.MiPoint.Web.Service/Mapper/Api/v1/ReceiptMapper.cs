﻿using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v1
{
    public static class ReceiptMapper
    {
        public static ReceiptModel ToDomainReceiptModel(this Data.DataModel.Receipt receipt)
        {
            return new ReceiptModel
            {
                Id = receipt.Id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,

                Payment = receipt.Payment.ToDomainPaymentModel(),
                Items = receipt.Items.ToDomainReceiptItemModels(),
            };
        }

        public static List<ReceiptItemModel> ToDomainReceiptItemModels(this IEnumerable<Data.DataModel.ReceiptItem> items)
        {
            return items.Select(i => new ReceiptItemModel
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static PaymentModel ToDomainPaymentModel(this Data.DataModel.ReceiptPayment payment)
        {
            return new PaymentModel
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                Cvm = payment.Cvm,
                TransactionType = payment.TransactionType
            };
        }

        public static Data.DataModel.Receipt ToEntityReceiptModel(this ReceiptModel receipt)
        {
            return new Data.DataModel.Receipt
            {
                Id = receipt.Id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,

                Payment = receipt.Payment.ToEntityPaymentModel(),
                Items = receipt.Items.ToEntityReceiptItems(),
            };
        }

        public static Data.DataModel.Receipt ToEntityReceiptModel(this ReceiptModel receipt, string id)
        {
            return new Data.DataModel.Receipt
            {
                Id = id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,

                Payment = receipt.Payment.ToEntityPaymentModel(),
                Items = receipt.Items.ToEntityReceiptItems(),
            };
        }

        public static List<Data.DataModel.ReceiptItem> ToEntityReceiptItems(this List<ReceiptItemModel> items)
        {
            return items.Select(i => new Data.DataModel.ReceiptItem
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Data.DataModel.ReceiptPayment ToEntityPaymentModel(this PaymentModel payment)
        {
            return new Data.DataModel.ReceiptPayment
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }
    }
}
