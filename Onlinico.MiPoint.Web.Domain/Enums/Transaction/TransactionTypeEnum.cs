﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Enums.Transaction
{
    public enum TransactionTypeEnum
    {
        AUTH = 0,
        PREAUTH = 1,
        PREAUTHCAPTURE = 2,
        ADJUST = 3,
        VOID = 4,
        VOIDRETURN = 5,
        RETURN = 6,
        REFUND = 7,
        NAKEDREFUND = 8,
        GETBALANCE = 9,
        BATCHCLOSE = 10,
        ACTIVATE = 11,
        BALANCE_LOCK = 12,
        LOAD = 13,
        CASHOUT = 14,
        CASHOUT_ACTIVE_STATUS = 15,
        REDEMPTION = 16,
        REDEMPTION_UNLOCK = 17,
        RELOAD = 18,
    }
}
