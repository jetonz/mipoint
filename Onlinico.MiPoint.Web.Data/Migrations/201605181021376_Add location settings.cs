namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addlocationsettings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationSettings", "SyncPeriod", c => c.Time(nullable: false, precision: 7));
            AddColumn("dbo.LocationSettings", "AfterPaymentLogOut", c => c.Boolean(nullable: false, defaultValueSql: "0"));
            AddColumn("dbo.LocationSettings", "SessionTimeout", c => c.Int(nullable: false, defaultValueSql:"60"));
            AddColumn("dbo.LocationSettings", "OrderLimit", c => c.Int(nullable: false, defaultValueSql: "50"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocationSettings", "OrderLimit");
            DropColumn("dbo.LocationSettings", "SessionTimeout");
            DropColumn("dbo.LocationSettings", "AfterPaymentLogOut");
            DropColumn("dbo.LocationSettings", "SyncPeriod");
        }
    }
}
