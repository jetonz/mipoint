﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class LogMapper
    {
        public static Log ToEntityModel(this LogModel model)
        {
            return new Log
            {
                Id = model.Id,
                Model = model.Model,
                Content = model.Content,
                Serial = model.Serial,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Fingerprint = model.Fingerprint,
                Created = DateTime.UtcNow,
                AppVersion = model.AppVersion,
            };
        }

        public static List<LogModel> ToDomainLogModels(this IEnumerable<Log> models)
        {
            return models.Select(l => new LogModel
            {
                Id = l.Id,
                Model = l.Model,
                Fingerprint = l.Fingerprint,
                LocationId = l.LocationId,
                Manufacturer = l.Manufacturer,
                Serial = l.Serial,
                Created = l.Created,
                AppVersion = l.AppVersion,
            }).ToList();
        }

        public static LogModel ToDomainLogModel(this Log model)
        {
            return new LogModel
            {
                Id = model.Id,
                Model = model.Model,
                Fingerprint = model.Fingerprint,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Serial = model.Serial,
                Created = model.Created,
                Content = model.Content,
                AppVersion = model.AppVersion,
            };
        }
    }
}
