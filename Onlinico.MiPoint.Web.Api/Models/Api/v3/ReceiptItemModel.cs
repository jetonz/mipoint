﻿using System;
using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3
{
    [DataContract]
    public class ReceiptItemModel
    {
        public Guid Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "quantity")]
        public long Quantity { get; set; }

        [DataMember(Name = "price")]
        public long Price { get; set; }
    }
}