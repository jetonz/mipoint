﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class LocationMapper
    {
        public static LocationModel ToDomainLocation(this Location location)
        {
            return new LocationModel
            {
                Id = location.Id,
                Identifier = location.Identifier,
                Name = location.Name,
                Phone = location.Phone,
                Address = location.Address,
                AddressFull = location.AddressFull,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType
            };
        }
    }
}
