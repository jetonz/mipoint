﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Onlinico.MiPoint.Web.Api.Helpers;
using Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Logger.Interfaces;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.Agent.v1
{
    public abstract class BaseApiController : ApiController
    {
        protected ILogger Logger;
        protected IConfiguration Configuration;

        protected BaseApiController(ILogger logger, IConfiguration configuration)
        {
            Logger = logger;
            Configuration = configuration;
        }

        protected HttpResponseException CreateApiException(string errorMessage, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            var responce = new HttpResponseMessage(statusCode)
            {
                Content = new ObjectContent<ApiExceptionModel>(new ApiExceptionModel()
                {
                    ErrorMessage = errorMessage,
                }, new JsonMediaTypeFormatter())
            };
            throw new HttpResponseException(responce);
        }

        protected HttpResponseException CreateApiException(string errorMessage, string internalErrorMesage, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            var responce = new HttpResponseMessage(statusCode)
            {
                Content = new ObjectContent<ApiExceptionModel>(new ApiExceptionModel()
                {
                    ErrorMessage = errorMessage,
                    InternalErrorMessage = internalErrorMesage,
                }, new JsonMediaTypeFormatter())
            };
            throw new HttpResponseException(responce);
        }

        protected HttpResponseException CreateApiException(ModelStateDictionary modelStateDictionary, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            var responce = new HttpResponseMessage(statusCode)
            {
                Content = new ObjectContent<ApiExceptionModel>(new ApiExceptionModel()
                {
                    ErrorMessage = modelStateDictionary.Values.ToErrorString()
                }, new JsonMediaTypeFormatter())
            };
            throw new HttpResponseException(responce);
        }
    }
}
