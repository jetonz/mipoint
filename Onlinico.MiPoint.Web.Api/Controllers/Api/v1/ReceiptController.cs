﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v1;
using Onlinico.MiPoint.Web.Api.Models.Api.v1.Mapper;
using Onlinico.MiPoint.Web.Api.Models.Api.v1.Validation;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Onlinico.MiPoint.Web.Service.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v1
{
    /// <summary>
    /// Receipt controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/receipt")]
    public class ReceiptController : BaseController
    {
        private readonly IReceiptService _receiptService;
        private readonly IEmailService _emailService;
        private readonly ISmsService _smsService;

        /// <summary>
        /// Receipt controller constructor
        /// </summary>
        /// <param name="receiptService">Inject receipt service</param>
        /// <param name="emailService">Inject email service</param>
        /// <param name="smsService">Inject sms service</param>
        public ReceiptController(IReceiptService receiptService, IEmailService emailService, ISmsService smsService)
        {
            _receiptService = receiptService;
            _emailService = emailService;
            _smsService = smsService;
        }

        /// <summary>
        /// Send receipt to email or sms
        /// </summary>
        /// <param name="model">Bill model</param>
        /// <returns>Http status code</returns>
        [HttpPost, Route("send")]
        public async Task<IHttpActionResult> Send([FromBody]Models.Api.v1.ReceiptModel model)
        {
            if (!model.IsValid())
            {
                return BadRequest("Invalid receipt model.");
            }
            var objectId = string.Empty;
            var result = await _receiptService.AddAsync(model.ToDomainReceiptModel());

            if (result.Succeed)
            {
                objectId = result.ObjectId;
                var receiptUrl = CurrentDomain + "bill/" + objectId;

                switch (model.ReceiptType.ToLower())
                {
                    case "email":

                        result = await GetReceiptHtmlAsync(receiptUrl);
                        if (result.Succeed)
                        {
                            result = await _emailService.SendAsync(model.Destination, $"Receipt from {model.LocationName}" , result.ObjectId);
                        }
                        break;
                    case "sms":

                        var message = $"Receipt : {receiptUrl}";
                        result = await _smsService.SendAsync(model.Destination, message);
                        break;
                }
            }

            return result.Succeed ? Ok(new CreateResultModel(objectId)) : (IHttpActionResult)BadRequest(result.Error);
        }

        private async Task<ResultModel> GetReceiptHtmlAsync(string receiptUrl)
        {
            var result = new ResultModel(true);
            try
            {
                using (var client = new HttpClient())
                {
                    var requestResult = await client.GetAsync(receiptUrl);
                    switch (requestResult.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            result.ObjectId = await requestResult.Content.ReadAsStringAsync();
                            break;
                        default:
                            result = new ResultModel(false, "Cannot get receipt html page.");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }
    }
}
