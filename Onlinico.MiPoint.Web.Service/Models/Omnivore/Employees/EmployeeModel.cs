﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Service.Models.Omnivore.Employees
{
    public class Self
    {
        public string href { get; set; }
    }

    public class Links
    {
        public Self self { get; set; }
    }

    public class Employee : ICloneable
    {
        public Links _links { get; set; }
        public string check_name { get; set; }
        public string first_name { get; set; }
        public string id { get; set; }
        public string last_name { get; set; }
        public string login { get; set; }

        public object Clone()
        {
            return new Employee
            {
                check_name = this.check_name,
                first_name = this.first_name,
                id = this.id,
                last_name = this.last_name,
                login = this.login
            };
        }
    }

    public class Embedded
    {
        public List<Employee> employees { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
    }

    public class RootObject
    {
        public Embedded _embedded { get; set; }
        public Links2 _links { get; set; }
        public int count { get; set; }
    }
}
