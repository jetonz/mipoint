﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class InviteMapper
    {
        public static Domain.Models.Web.InviteModel ToDomainInviteModel(this Data.DataModel.Invite model)
        {
            return new Domain.Models.Web.InviteModel
            {
                Id = model.Id,
                Email = model.Email,
                MerchantId = model.Merchant.Id
            };
        }
    }
}
