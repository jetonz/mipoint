﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class MerchantModel
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Email { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public string Country { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }
        
        public string Zip { get; set; }
        
        public string Address { get; set; }

        public string WebSite { get; set; }

        public string LogoUrl { get; set; }


        public DateTime Created { get; set; }
        
        public DateTime Updated { get; set; }
    }
}
