﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Microsoft.AspNet.SignalR;
using Onlinico.MiPoint.Web.Api.Hubs;
using Onlinico.MiPoint.Web.Api.Models.Api.v4;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v4;
using Swashbuckle.Swagger.Annotations;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v4
{
    ///// <summary>
    ///// Application settings controller
    ///// </summary>
    //[Attributes.Authorize, RoutePrefix("api/v4/locations")]
    //public class LocationController : BaseController
    //{
    //    private readonly ILocationService _locationService;

    //    /// <summary>
    //    /// Location controller constructor
    //    /// </summary>
    //    public LocationController(Logger.Interfaces.ILogger logger, ILocationService locationService, IConfiguration configuration) : base(logger, configuration)
    //    {
    //        _locationService = locationService;
    //    }


    //    /// <summary>
    //    /// Get location settings
    //    /// </summary>
    //    /// <returns></returns>
    //    [HttpGet]
    //    [Route("current/settings")]
    //    [SwaggerResponse(HttpStatusCode.OK, Type = typeof(LocationSettingsModel))]
    //    [SwaggerResponse(HttpStatusCode.NoContent)]
    //    [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
    //    public async Task<IHttpActionResult> GetSettings()
    //    {
    //        //var connectorHubContext = GlobalHost.ConnectionManager.GetHubContext<ConnectorHub>();
    //        //connectorHubContext.Clients.All.getLogByDate(DateTime.UtcNow.AddDays(-1).Date);
            
    //        var locationSettingsResult = await _locationService.GetSettingsAsync(LocationId);

    //        if (locationSettingsResult.Succeed && locationSettingsResult.Object != null)
    //        {
    //            return Ok(locationSettingsResult.Object.ToApiLocationSettingsModel());
    //        }
    //        else
    //        {
    //            throw CreateApiException(locationSettingsResult.Error);
    //        }
    //    }
        
    //    protected override void Dispose(bool disposing)
    //    {
    //        _locationService.Dispose();
    //        base.Dispose(disposing);
    //    }
    //}
}
