﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.Results;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Api.Models;
using Onlinico.MiPoint.Web.Api.Models.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces;
using PagedList;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeController : BaseController
    {
        /// <summary>
        /// Home Controller constructor
        /// </summary>
        public HomeController(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<ActionResult> Index()
        {
            return View();
        }
    }
}
