namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmployeestoLocation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EmployeeId = c.String(maxLength: 50),
                        CheckName = c.String(maxLength: 50),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 50),
                        Login = c.String(maxLength: 50),
                        Location_Id = c.String(nullable: false, maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.Login)
                .Index(t => t.Location_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "Location_Id", "dbo.Locations");
            DropIndex("dbo.Employees", new[] { "Location_Id" });
            DropIndex("dbo.Employees", new[] { "Login" });
            DropTable("dbo.Employees");
        }
    }
}
