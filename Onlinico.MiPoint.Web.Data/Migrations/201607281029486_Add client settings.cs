namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addclientsettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientCounterModeSettings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DisplayLineItemsOnReceipt = c.Boolean(nullable: false),
                        ShowReceiptScreen = c.Boolean(nullable: false),
                        PrintReceiptEnabled = c.Boolean(nullable: false),
                        SendReceiptMessageEnabled = c.Boolean(nullable: false),
                        SendReceiptEmailEnabled = c.Boolean(nullable: false),
                        ShowThanksSreen = c.Boolean(nullable: false),
                        ThanksMessage = c.String(maxLength: 250),
                        WelcomeMessage = c.String(maxLength: 250),
                        TipSuggestions = c.String(maxLength: 250),
                        ShowTipsScreen = c.Boolean(nullable: false),
                        RequireSignature = c.Boolean(nullable: false),
                        AutoPrintReceipt = c.Boolean(nullable: false),
                        ShowTipLineOnReceipt = c.Boolean(nullable: false),
                        ShowSignatureLineOnReceipt = c.Boolean(nullable: false),
                        Location_Id = c.String(nullable: false, maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.ClientTableModeSettings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DisplayLineItemsOnReceipt = c.Boolean(nullable: false),
                        ShowReceiptScreen = c.Boolean(nullable: false),
                        PrintReceiptEnabled = c.Boolean(nullable: false),
                        SendReceiptMessageEnabled = c.Boolean(nullable: false),
                        SendReceiptEmailEnabled = c.Boolean(nullable: false),
                        TipSuggestions = c.String(maxLength: 250),
                        ShowTipsScreen = c.Boolean(nullable: false),
                        RequireSignature = c.Boolean(nullable: false),
                        AutoPrintReceipt = c.Boolean(nullable: false),
                        OnlyMyOrdersFilterEnabled = c.Boolean(nullable: false),
                        AfterPaymentLogOut = c.Boolean(nullable: false),
                        SessionTimeOut = c.Int(nullable: false),
                        SessionTimeOutEnabled = c.Boolean(nullable: false),
                        ShowTipLineOnReceipt = c.Boolean(nullable: false),
                        ShowSignatureLineOnReceipt = c.Boolean(nullable: false),
                        Location_Id = c.String(nullable: false, maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.Location_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClientTableModeSettings", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.ClientCounterModeSettings", "Location_Id", "dbo.Locations");
            DropIndex("dbo.ClientTableModeSettings", new[] { "Location_Id" });
            DropIndex("dbo.ClientCounterModeSettings", new[] { "Location_Id" });
            DropTable("dbo.ClientTableModeSettings");
            DropTable("dbo.ClientCounterModeSettings");
        }
    }
}
