﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3
{
    public class LubModel
    {
        [JsonProperty("agentVersion")]
        public string  AgentVersion { get; set; }

        [JsonProperty("fingerprint")]
        public string Fingerprint { get; set; }

        /// <summary>
        /// Unknown or NotInstalled or Stopped or StopPending or Running or StartPending
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public AgentStatusEnum Status { get; set; }
    }

    public class DubModel
    {
        [JsonProperty("availableVersion")]
        public string AvailableVersion { get; set; }

        [JsonProperty("updateUrl")]
        public string UpdateUrl { get; set; }
    }
}