﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class PaymentMapper
    {
        public static Domain.Models.Global.Omnivore.ThirdPartyPaymentModel ToThirdPartyPaymentModel(
            this Domain.Models.Web.PaymentModel model)
        {
            var ticketId = model.OrderId.Split('-').FirstOrDefault();

            return new Domain.Models.Global.Omnivore.ThirdPartyPaymentModel
            {
                TicketId = ticketId,
                Amount = model.Amount,
                Tip = model.Tip,
                TenderType = model.TenderType,
            };
        }


        public static List<PaymentViewModel> ToPaymentsViewModel(this IEnumerable<PaymentModel> models)
        {
            return models.Select(p => p.ToPaymentViewModel()).ToList();
        }

        public static PaymentViewModel ToPaymentViewModel(this PaymentModel model)
        {
            return new PaymentViewModel
            {
                CardType = model.CardType,
                TenderType = model.TenderType,
                LocationId = model.LocationId,
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = Math.Round((double)model.Amount / 100, 2, MidpointRounding.AwayFromZero).ToString("f2"),
                Tip = model.Tip,
                Tax = model.Tax,
                Status = model.Status.ToString(),
                Type = model.Type.ToString(),
                Message = model.Message,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,
                Created = model.Created,

                ReceptInfo = model.ReceptInfo?.ToPaymentReceptInfoViewModel(),
                Transaction = model.Transaction?.ToPaymentTransactionViewModel(),
            };
        }

        public static PaymentTransactionViewModel ToPaymentTransactionViewModel(this PaymentTransactionModel model)
        {
            return new PaymentTransactionViewModel
            {
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = model.CardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType,
                Issued = model.Issued,
            };
        }

        public static PaymentReceptInfoViewModel ToPaymentReceptInfoViewModel(this PaymentReceptInfoModel model)
        {
            return new PaymentReceptInfoViewModel
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }


    }
}