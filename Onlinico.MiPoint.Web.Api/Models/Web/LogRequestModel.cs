﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class LogRequestModel
    {
        [Required]
        public string LocationId { get; set; }

        [Required]
        public bool IsAgentLog { get; set; }

        [Required]
        public DateTime LogDate { get; set; }
    }
}