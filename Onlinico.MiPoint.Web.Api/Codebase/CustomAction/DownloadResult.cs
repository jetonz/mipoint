﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Onlinico.MiPoint.Web.Api.Codebase.CustomAction
{
    public class DownloadResult : ActionResult
    {

        public DownloadResult() { }

        public DownloadResult(string virtualPath, bool isServerMapPath)
        {
            this.VirtualPath = virtualPath;
            IsServerMapPath = isServerMapPath;
        }

        public string VirtualPath { get; set; }

        public string FileDownloadName { get; set; }

        public string ContentType { get; set; }

        public bool IsServerMapPath { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (!String.IsNullOrEmpty(FileDownloadName))
            {
                context.HttpContext.Response.AddHeader("content-disposition",
                    "attachment; filename=" + this.FileDownloadName);
            }

            string filePath = IsServerMapPath ? context.HttpContext.Server.MapPath(this.VirtualPath) : this.VirtualPath;

            context.HttpContext.Response.ContentType = ContentType;

            context.HttpContext.Response.TransmitFile(filePath);
        }
    }
}