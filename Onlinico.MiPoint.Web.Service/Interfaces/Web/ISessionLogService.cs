﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface ISessionLogService
    {
        ResultModel<List<SessionLogModel>> Get(DateTime start, DateTime end);

        ResultModel<SessionLogModel> Get(Guid logId);

        Task<ResultModel> RemoveOldRecordsAsync(DateTime star);

        Task<ResultModel> RemoveOldContentAsync(DateTime star, DateTime end);
    }
}
