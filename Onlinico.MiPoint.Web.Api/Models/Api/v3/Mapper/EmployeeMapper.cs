﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper
{
    public static class EmployeeMapper
    {
        public static Models.Api.v3.EmployeeModel ToEmployeeModel(this Domain.Models.Api.v3.EmployeeModel employee)
        {
            return new Models.Api.v3.EmployeeModel
            {
                Id = employee.Id,
                EmployeeId = employee.EmployeeId,
                Login = employee.Login,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                CheckName = employee.CheckName,
            };
        }
    }
}