﻿using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v1;
using Onlinico.MiPoint.Web.Api.Models.Api.v1.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v1
{
    /// <summary>
    /// Application settings controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/locations")]
    public class LocationController : BaseController
    {
        private readonly ILocationService _locationService;

        private readonly IConfiguration _configuration;

        /// <summary>
        /// Location controller constructor
        /// </summary>
        public LocationController(Logger.Interfaces.ILogger logger, ILocationService locationService, IConfiguration configuration) : base(logger)
        {
            _locationService = locationService;
            _configuration = configuration;
        }

        /// <summary>
        /// Get current application settings
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/settings")]
        public LocationSettingsModel Get()
        {
            var locationSettings = _locationService.GetSettings(Request.Properties[_configuration.Secret].ToString());
            var result = locationSettings?.ToApiLocationSettingsModel();
            return result;
        }
    }
}
