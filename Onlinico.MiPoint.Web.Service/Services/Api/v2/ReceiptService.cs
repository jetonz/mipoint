﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v2;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v2
{
    public class ReceiptService : BaseService, IReceiptService
    {
        public ReceiptService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel<List<Domain.Models.Api.v2.ReceiptModel>>> GetByTicketIdAsync(string locationId, string ticketId)
        {
            var result = new ResultModel<List<Domain.Models.Api.v2.ReceiptModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var receipts = await context.Receipts.Where(r => r.TicketId == ticketId && r.LocationId == locationId).ToListAsync();
                    result.Object = receipts?.ToDomainReceipts();
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel<List<Domain.Models.Api.v2.ReceiptModel>>(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public async Task<ResultModel<ReceiptModel>> GetAsync(string receiptId)
        {
            var result = new ResultModel<ReceiptModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var receipt = await context.Receipts
                                .Include(r => r.Items)
                                .Include(r => r.Payment)
                                .FirstOrDefaultAsync(r => r.Id == receiptId);
                    if (receipt != null)
                    {
                        result.Object = receipt.ToDomainReceiptModel();
                    }
                    else
                    {
                        result = new ResultModel<ReceiptModel>(false, "Invalid receiptId.");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel<ReceiptModel>(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public async Task<ResultModel> AddAsync(ReceiptModel model)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    Data.DataModel.Receipt receipt = null;

                    if (!string.IsNullOrEmpty(model.Payment?.TransactionId))
                    {
                        receipt = await context.Receipts.Include(r=>r.Payment).FirstOrDefaultAsync(r => r.Payment.TransactionId == model.Payment.TransactionId);
                    }
                    if (receipt == null)
                    {
                        var receiptId = string.Empty;
                        var exit = false;
                        while (!exit)
                        {
                            receiptId = Core.StringGenerator.RandomString(10);
                            if (!context.Receipts.Any(a => a.Id == receiptId))
                            {
                                exit = true;
                            }
                        }

                        receipt = model.ToEntityReceiptModel(receiptId);

                        context.Receipts.Add(receipt);

                        await context.SaveChangesAsync();
                    }

                    result.ObjectId = receipt?.Id;
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }
    }
}
