﻿using System;
using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2
{
    [DataContract]
    public class ReceiptPaymentModel
    {
        public Guid Id { get; set; }

        [DataMember(Name = "transactionId")]
        public string TransactionId { get; set; }

        [DataMember(Name = "amount")]
        public long Amount { get; set; }

        [DataMember(Name = "createdTime")]
        public string CreatedTime { get; set; }

        [DataMember(Name = "cardEntryType")]
        public string CardEntryType { get; set; }

        [DataMember(Name = "cardType")]
        public string CardType { get; set; }

        [DataMember(Name = "last4")]
        public string Last4 { get; set; }

        [DataMember(Name = "referenceId")]
        public string ReferenceId { get; set; }

        [DataMember(Name = "authCode")]
        public string AuthCode { get; set; }

        [DataMember(Name = "applicationId")]
        public string ApplicationId { get; set; }

        [DataMember(Name = "transactionType")]
        public string TransactionType { get; set; }

        [DataMember(Name = "cvm")]
        public string Cvm { get; set; }
    }
}