﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Helpers
{
    public static class ModelStateHelper
    {
        public static string ToErrorString(this ICollection<System.Web.Http.ModelBinding.ModelState> modelStates)
        {
            return string.Join("| ", modelStates.Select(
                v =>
                    !string.IsNullOrEmpty(v.Errors.FirstOrDefault()?.ErrorMessage)
                        ? v.Errors.FirstOrDefault()?.ErrorMessage
                        : v.Errors.FirstOrDefault()?.Exception?.Message));
        }
    }
}