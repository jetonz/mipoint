﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface IReceiptService
    {
        Task<ResultModel<ReceiptModel>> GetAsync(string receiptId);

        Task<ResultModel<ReceiptModel>> GetFromPaymentAsync(string paymentId);
    }
}
