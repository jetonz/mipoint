﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Global.MappingScheme
{
    public static class MappingSchemeMapper
    {
        public static Dictionary<string, string> ToMappingScheme(this string data)
        {
            return string.IsNullOrEmpty(data) ? null : data.Split(';').Select(part => part.Split('=')).Where(part => part.Length == 2).ToDictionary(sp => sp[0], sp => sp[1]);
        }

        public static string ToMappingScheme(this Dictionary<string, string> data)
        {
            return string.Join(";", data.Select(x => x.Key + "=" + x.Value).ToArray());
        }
    }
}
