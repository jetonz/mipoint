﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class ReceiptMapper
    {
        public static Domain.Models.Web.ReceiptModel ToDomainReceiptModel(this Data.DataModel.Payment payment)
        {
            return new Domain.Models.Web.ReceiptModel
            {
                Id = payment.Id,
                Created = payment.Created,
                Address1 = payment.ReceiptInfo.ReceiptHeader1,
                Address2 = payment.ReceiptInfo.ReceiptHeader2,
                Cashier = payment.ReceiptInfo.Cashier,
                AmountToPay = payment.Amount, // .AmountToPay,
                LocationName = payment.ReceiptInfo.LocationName,//  .LocationName,
                Phone = payment.ReceiptInfo.PhoneNumber,
                ReceiptDate = payment.ReceiptInfo.Issued.DateTime.ToString(),  //.ReceiptDate,
                TicketNumber = payment. ReceiptInfo.OrderNumber, //.TicketNumber,
                TipSum = payment.Tip,// .TipSum,
                TaxSum = payment.Tax, //  .TaxSum,
                TotalSum = payment.Amount,// TotalSum,
                ServiceChargesSum = payment.ReceiptInfo.OrderServiceCharge,//  .ServiceChargesSum,
                TableNamber = payment.TableNumber,

                Payment = payment.Transaction?.ToDomainPaymentModel(payment.Amount + payment.Tip),
                Items = payment.Items?.ToDomainReceiptItemModels(),
            };
        }

        public static List<Domain.Models.Web.ReceiptItemModel> ToDomainReceiptItemModels(this IEnumerable<Data.DataModel.PaymentItem> items)
        {
            return items.Select(i => new Domain.Models.Web.ReceiptItemModel
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Domain.Models.Web.ReceiptPaymentModel ToDomainPaymentModel(this Data.DataModel.PaymentTransaction payment, long amount)
        {
            return new Domain.Models.Web.ReceiptPaymentModel
            {
                TransactionId = payment.TransactionId,
                Amount = amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.Issued.DateTime.ToString(), //CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }

        public static Domain.Models.Web.ReceiptModel ToDomainReceiptModel(this Data.DataModel.Receipt receipt)
        {
            return new Domain.Models.Web.ReceiptModel
            {
                Id = receipt.Id,
                Created = receipt.Created,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPay = receipt.AmountToPay,
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TipSum = receipt.TipSum,
                TaxSum = receipt.TaxSum,
                TotalSum = receipt.TotalSum,
                ServiceChargesSum = receipt.ServiceChargesSum,

                Payment = receipt.Payment.ToDomainPaymentModel(),
                Items = receipt.Items.ToDomainReceiptItemModels(),
            };
        }

        public static List<Domain.Models.Web.ReceiptItemModel> ToDomainReceiptItemModels(this IEnumerable<Data.DataModel.ReceiptItem> items)
        {
            return items.Select(i => new Domain.Models.Web.ReceiptItemModel
            {
                Id = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Domain.Models.Web.ReceiptPaymentModel ToDomainPaymentModel(this Data.DataModel.ReceiptPayment payment)
        {
            return new Domain.Models.Web.ReceiptPaymentModel
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = payment.Amount,
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }
    }
}
