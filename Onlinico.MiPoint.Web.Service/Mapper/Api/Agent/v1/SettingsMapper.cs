﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiPoint.Shared.Types.Enums;
using MiPoint.Shared.Types.Dto;
using Onlinico.MiPoint.Web.Data.DataModel;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.Agent.v1
{
    public static class SettingsMapper
    {
        public static SettingsDto ToSettingsModel(this LocationSettings settings)
        {
            return new SettingsDto
            {
                TenderMappings = settings.TenderTypes?.ToTenderTyps(),
                FieldMappings = settings.MappingSchemes?.ToFieldMappings(),
            };
        }

        public static List<TenderMappingDto> ToTenderTyps(this IEnumerable<TenderType> models)
        {
            return models.Select(m => new TenderMappingDto
            {
                TenderTypeId = m.Type,
                CardType = GetCardType(m.Card.Type),
            }).ToList();
        }

        public static CardTypeEnum GetCardType(string cardType)
        {
            cardType = cardType.ToUpper();

            switch (cardType)
            {
                case "VISA":
                    return CardTypeEnum.Visa;
                case "AMEX":
                    return CardTypeEnum.Amex;
                case "MASTERCARD":
                    return CardTypeEnum.Mastercard;
                case "DISCOVER":
                    return CardTypeEnum.Discover;
                default:
                    return CardTypeEnum.Other;
            }
        }

        public static List<FieldMappingDto> ToFieldMappings(this IEnumerable<MappingScheme> models)
        {
            return models.Select(m => new FieldMappingDto
            {
                SchemeType = (MappingSchemeEnum)(int)m.Type,
                Scheme = ToMappingScheme(m.Scheme)
            }).ToList();
        }

        private static Dictionary<string, string> ToMappingScheme(string data)
        {
            return string.IsNullOrEmpty(data) ? null : data.Split(';').Select(part => part.Split('=')).Where(part => part.Length == 2).ToDictionary(sp => GetAgentMapTarget(sp[0]), sp => GetAgentMapTarget(sp[1]));
        }

        private static string GetAgentMapTarget(string val)
        {
            switch (val)
            {
                //Global
                case "id":
                    return "Id";

                //Employee
                case "check_name":
                    return "CheckName";
                case "first_name":
                    return "FirstName";
                case "last_name":
                    return "LastName";
                case "login":
                    return "Login";

                //Table
                case "available":
                    return "IsAvailable";
                case "name":
                    return "Name";
                case "number":
                    return "Number";
                case "seats":
                    return "Seats";

                //Default
                default:
                    return val;
            }
        }
    }
}
