﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Global.Auth
{
    public class IdentityModel
    {
        public IdentityModel()
        {
            
        }

        public IdentityModel(string locationId, string omnivoreApiKey, string omnivoreLocationId)
        {
            LocationId = locationId;
            OmnivoreApiKey = omnivoreApiKey;
            OmnivoreLocationId = omnivoreLocationId;
        }

        public string LocationId { get; set; }

        public string OmnivoreApiKey { get; set; }

        public string OmnivoreLocationId { get; set; }
    }
}
