﻿using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1.Enum;

namespace Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1.Mapper
{
    public static class PollMapper
    {
        public static Domain.Models.Api.Agent.v1.PingModel ToDomainPingModel(this PingModel model)
        {
            return new Domain.Models.Api.Agent.v1.PingModel
            {
                Fingerprint = model.Fingerprint,
                AgentVersion = model.AgentVersion,
                Status = (AgentStatusEnum) model.Status
            };
        }
    }
}