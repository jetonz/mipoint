﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Attributes
{
    public class AuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        private readonly IAuthService _authService;
        private readonly Core.Interfaces.IConfiguration _configuration;
        
        public AuthorizeAttribute()
        {
            _authService = DependencyResolver.Current.GetService<IAuthService>();
            _configuration = DependencyResolver.Current.GetService<Core.Interfaces.IConfiguration>();
        }
        

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Contains(_configuration.HeaderApiKey))
            {
                var locationId = actionContext.Request.Headers.GetValues(_configuration.HeaderApiKey).First();

                var identityResult = _authService.GetIdentity(locationId);
                if (identityResult.Succeed && identityResult.Object != null)
                {
                    actionContext.Request.Properties.Add(_configuration.Secret, locationId);
                    actionContext.Request.Headers.Remove(_configuration.HeaderApiKey);
                    actionContext.Request.Headers.Add(_configuration.HeaderApiKey, identityResult.Object.OmnivoreApiKey);
                }
                else
                {
                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
            }
            else
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
        }
    }
}