﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class LocationModel
    {
        public string Id { get; set; }

        public string AgentFingerprint { get; set; }
    }
}
