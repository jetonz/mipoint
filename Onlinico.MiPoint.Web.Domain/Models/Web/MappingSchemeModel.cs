﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class MappingSchemeModel
    {
        public string LocationId { get; set; }

        public Dictionary<string, string> EmployeeScheme { get; set; }

        public Dictionary<string, string> TableScheme { get; set; }
    }
}
