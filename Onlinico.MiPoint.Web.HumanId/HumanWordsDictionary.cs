﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Onlinico.MiPoint.Web.HumanId
{
    [Serializable]
    [XmlRoot("WordsDictionary")]
    public class HumanWordsDictionary
    {
        private static Random rnd = new Random();

        [XmlArrayItem(ElementName = "Noun")]
        public List<string> Nouns { get; set; }

        [XmlArrayItem(ElementName = "Adjective")]
        public List<string> Adjectives { get; set; }

        public string GetRandomAdjective()
        {
            return Adjectives.PickRandom().Trim();
        }

        public string GetRandomNoon()
        {
            return Nouns.PickRandom().Trim();
        }

        public static HumanWordsDictionary Load(string path)
        {
            var retval = new HumanWordsDictionary();
            if (File.Exists(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(HumanWordsDictionary));
                
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    retval = serializer.Deserialize(stream) as HumanWordsDictionary;
                }
            }
            else
            {
                throw new FileNotFoundException("Dictionary not found", path);
            }
            return retval;
        }
    }
}
