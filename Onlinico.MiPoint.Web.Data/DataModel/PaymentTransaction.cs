﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class PaymentTransaction
    {
        [Key]
        public Guid Id { get; set; }

        [Index]
        [MaxLength(36)]
        public string TransactionId { get; set; }
        
        public DateTimeOffset Issued { get; set; }

        [MaxLength(30)]
        public string CardEntryType { get; set; }

        [MaxLength(30)]
        public string CardType { get; set; }

        [MaxLength(4)]
        public string Last4 { get; set; }

        [MaxLength(36)]
        public string ReferenceId { get; set; }

        [MaxLength(36)]
        public string AuthCode { get; set; }

        [MaxLength(36)]
        public string ApplicationId { get; set; }

        [MaxLength(30)]
        public string TransactionType { get; set; }

        [MaxLength(50)]
        public string Cvm { get; set; }


        public virtual Payment Payment { get; set; }
    }
}
