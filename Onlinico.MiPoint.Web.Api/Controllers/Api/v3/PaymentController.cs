﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData.Query;
using Newtonsoft.Json;
using Onlinico.MiPoint.Web.Api.Models.Api.v3;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums;
using Onlinico.MiPoint.Web.Api.Models.Api.v3.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v3;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Interfaces.Global.External;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v3
{
    /// <summary>
    /// Payment controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v3/payments")]
    public class PaymentController : BaseController
    {
        private readonly IPaymentService _paymentService;
        private readonly IOmnivoreService _omnivoreService;
        private readonly IAuthService _authService;

        /// <summary>
        /// Payment controller constructor
        /// </summary>
        /// <param name="paymentService">Inject payment service</param>
        /// <param name="omnivoreService">Inject omnivore service</param>
        /// <param name="authService">Inject auth service</param>
        /// <param name="logger">Inject logger service</param>
        /// <param name="configuration">Inject config</param>
        public PaymentController(IPaymentService paymentService, IOmnivoreService omnivoreService, IAuthService authService, ILogger logger, IConfiguration configuration) : base(logger, configuration)
        {
            _paymentService = paymentService;
            _omnivoreService = omnivoreService;
            _authService = authService;
        }

        /// <summary>
        /// Get payments(OData query v3 available)
        /// </summary>
        /// <param name="queryOptions">ODAta PaymentModel query options</param>
        /// <returns></returns>
        [HttpGet, Route("")]
        [ResponseType(typeof(List<PaymentModel>))]
        public async Task<IHttpActionResult> Get(ODataQueryOptions<PaymentModel> queryOptions)
        {
            var result = await _paymentService.GetByLocationAsync(LocationId);

            if (result.Succeed && result.Object != null)
            {
                try
                {
                    var payment = result.Object.ToApiPayments();

                    return Ok(queryOptions.ApplyTo(payment));
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return BadRequest(result.Error);
            }
        }

        /// <summary>
        /// Add payment
        /// </summary>
        /// <param name="model">Payment model</param>
        /// <returns></returns>
        [HttpPost, Route("")]
        [ResponseType(typeof(CreateResultModel))]
        public async Task<IHttpActionResult> Post(PaymentModel model)
        {
            var result = new Domain.Models.Api.v3.ResultModel(false);

            if (ModelState.IsValid)
            {
                if (model.Type == PaymentTypeEnum.Normal)
                {
                    var identityResult = _authService.GetIdentity(LocationId);
                    if (identityResult.Succeed && identityResult.Object != null)
                    {
                        var ticketId = model?.OrderId?.Split('-').FirstOrDefault();
                        var omnivoreTicketResult = await _omnivoreService.GetTicketAsync(identityResult.Object.OmnivoreApiKey,
                                identityResult.Object.OmnivoreLocationId, ticketId);
                        if (omnivoreTicketResult.Succeed && omnivoreTicketResult.Object != null)
                        {
                            if (omnivoreTicketResult.Object.closed_at == null)
                            {
                                if (omnivoreTicketResult.Object.totals.due >= model.Amount)
                                {
                                    result = await _paymentService.AddAsync(LocationId, model.ToDomainPaymentModel());
                                }
                                else
                                {
                                    result.Error = "Omnivore ticket due lower than payment amount.";
                                }
                            }
                            else
                            {
                                result.Error = "Omnivore ticket closed.";
                            }
                        }
                        else
                        {
                            result.Error = omnivoreTicketResult.Error;
                        }
                    }
                    else
                    {
                        result.Error = identityResult.Error;
                    }
                }
                else
                {
                    result = await _paymentService.AddAsync(LocationId, model.ToDomainPaymentModel());
                }
            }
            else
            {
                result.Error = string.Join("| ", ModelState.Values.Select(v => !string.IsNullOrEmpty(v.Errors.FirstOrDefault()?.ErrorMessage) ? v.Errors.FirstOrDefault()?.ErrorMessage : v.Errors.FirstOrDefault()?.Exception?.Message));
            }

            return result.Succeed ? Ok(new CreateResultModel(result.ObjectId)) : (IHttpActionResult)BadRequest(result.Error);
        }

        /// <summary>
        /// Payment confirm
        /// </summary>
        /// <param name="paymentId">Payment id</param>
        /// <param name="transaction">Payment transaction model</param>
        /// <returns></returns>
        [HttpPost, Route("{paymentId}/confirm")]
        public async Task<IHttpActionResult> Confirm([FromUri]string paymentId, [FromBody]PaymentTransaction transaction)
        {
            var result = new Domain.Models.Api.v3.ResultModel(true);

            if (ModelState.IsValid)
            {
                var identityResult = _authService.GetIdentity(LocationId);
                if (identityResult.Succeed && identityResult.Object != null)
                {
                    result = await _paymentService.ConfirmAsync(paymentId, transaction?.ToDomainPaymentTransaction());
                    if (result.Succeed)
                    {
                        var paymentResult = await _paymentService.GetPaymentToThirdPartyPayment(paymentId);
                        if (paymentResult.Succeed && paymentResult.Object != null)
                        {
                            var omnivoreTicketResult = await _omnivoreService.GetTicketAsync(identityResult.Object.OmnivoreApiKey,
                                identityResult.Object.OmnivoreLocationId, paymentResult.Object.TicketId);
                            if (omnivoreTicketResult.Succeed && omnivoreTicketResult.Object != null)
                            {
                                if (omnivoreTicketResult.Object.closed_at == null)
                                {
                                    if (omnivoreTicketResult.Object.totals.due >= paymentResult.Object.Amount)
                                    {
                                        var sendPaymentResult = await _omnivoreService.SendThirdPartyPayment(identityResult.Object.OmnivoreApiKey, identityResult.Object.OmnivoreLocationId, paymentResult.Object);

                                        if (sendPaymentResult.Succeed)
                                        {
                                            var paymentResultContent = JsonConvert.SerializeObject(sendPaymentResult.Object);
                                            await _paymentService.UpdateStatusAsync(paymentId, Domain.Enums.PaymentStatusEnum.Paid, "Paid : " + paymentResultContent);
                                        }
                                        else
                                        {
                                            await _paymentService.UpdateStatusAsync(paymentId, Domain.Enums.PaymentStatusEnum.Confirmed, sendPaymentResult.Error);
                                        }
                                    }
                                    else
                                    {
                                        await _paymentService.UpdateStatusAsync(paymentId, Domain.Enums.PaymentStatusEnum.Cancelled, "Omnivore ticket due lower than payment amount.");
                                    }
                                }
                                else
                                {
                                    await _paymentService.UpdateStatusAsync(paymentId, Domain.Enums.PaymentStatusEnum.Cancelled, "Omnivore order closed.");
                                }
                            }
                            else
                            {
                                await _paymentService.UpdateMessageAsync(paymentId, omnivoreTicketResult.Error);
                            }
                        }
                        else
                        {
                            await _paymentService.UpdateMessageAsync(paymentId, paymentResult.Error);
                        }
                    }
                }
                else
                {
                    result = new Domain.Models.Api.v3.ResultModel(false, identityResult.Error);
                }
            }
            else
            {
                result.Succeed = false;
                result.Error = string.Join("| ", ModelState.Values.Select(v => !string.IsNullOrEmpty(v.Errors.FirstOrDefault()?.ErrorMessage) ? v.Errors.FirstOrDefault()?.ErrorMessage : v.Errors.FirstOrDefault()?.Exception?.Message));
            }

            return result.Succeed ? Ok() : (IHttpActionResult)BadRequest(result.Error);
        }

        /// <summary>
        /// Cancel payment
        /// </summary>
        /// <param name="paymentId">Payment id</param>
        /// <returns></returns>
        [HttpPost, Route("{paymentId}/cancel")]
        public async Task<IHttpActionResult> Cancel([FromUri]string paymentId)
        {
            var result = await _paymentService.CancelAsync(paymentId, $"Payment canceled by client.");

            return result.Succeed ? Ok() : (IHttpActionResult)BadRequest(result.Error);
        }

        protected override void Dispose(bool disposing)
        {
            _paymentService.Dispose();
            base.Dispose(disposing);
        }
    }
}
