﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class ReceiptModel
    {
        public string Id { get; set; }

        public DateTime Created { get; set; }

        public string ReceiptType { get; set; }

        public string Destination { get; set; }

        public string LocationName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Phone { get; set; }

        public string Cashier { get; set; }

        public string ReceiptDate { get; set; }

        public string TipSum { get; set; }

        public string TotalSum { get; set; }

        public string TaxSum { get; set; }

        public string ServiceChargesSum { get; set; }

        public string AmountToPaye { get; set; }

        public string TicketNumber { get; set; }

        public string TableNumber { get; set; }


        public ReceiptPaymentModel Payment { get; set; }

        public List<ReceiptItemModel> Items { get; set; }
    }
}