﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class PaymentReceiptInfo
    {
        [Key]
        public Guid Id { get; set; }

        [Index]
        [Required]
        [MaxLength(36)]
        public string OrderNumber { get; set; }
        
        public DateTimeOffset Issued { get; set; }

        public long OrderSubTotal { get; set; }
        
        public long OrderTax { get; set; }

        public long OrderTotal { get; set; }

        public long OrderServiceCharge { get; set; }
        
        [MaxLength(50)]
        public string Cashier { get; set; }

        [MaxLength(100)]
        public string LocationName { get; set; }

        [MaxLength(100)]
        public string ReceiptHeader1 { get; set; }

        [MaxLength(100)]
        public string ReceiptHeader2 { get; set; }

        [MaxLength(20)]
        public string PhoneNumber { get; set; }

        [MaxLength(50)]
        public string Website { get; set; }


        public virtual Payment Payment { get; set; }
    }
}
