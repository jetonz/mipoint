// <auto-generated />
namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddAvailableTenderTypes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddAvailableTenderTypes));
        
        string IMigrationMetadata.Id
        {
            get { return "201608041224070_Add AvailableTenderTypes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
