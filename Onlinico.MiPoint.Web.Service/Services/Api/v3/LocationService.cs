﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v3;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v3;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v3;
using Onlinico.MiPoint.Web.Service.Mapper.Global.MappingScheme;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v3
{
    public class LocationService : BaseService, ILocationService
    {
        private readonly IMemoryCacher _memoryCacher;

        public LocationService(ILogger logger, IMemoryCacher memoryCacher) : base(logger)
        {
            _memoryCacher = memoryCacher;
        }

        public async Task<ResultModel<EmployeeModel>> GetEmployeeAsync(string locationId, string login)
        {
            var result = new ResultModel<EmployeeModel>(true);
            try
            {
                var employee = await Context.Employees.Include(e => e.Location).FirstOrDefaultAsync(e => e.Location.Id == locationId && e.Login == login && !e.Location.IsRemoved);

                result.Object = employee?.ToDomainEmployeeModel();

            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<EmployeeModel>(false, ex.Message);
            }

            return result;
        }

        public ResultModel<LocationModel> Get(string locationId)
        {
            var result = new ResultModel<LocationModel>(true);
            try
            {
                var location = Context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                result.Object = location?.ToDomainLocation();
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<LocationModel>(false, ex.Message);
            }
            return result;
        }

        public async Task<ResultModel<LocationModel>> GetAsync(string locationId)
        {
            var result = new ResultModel<LocationModel>(true);
            try
            {
                var location = await Context.Locations.FirstOrDefaultAsync(l => l.Id == locationId && !l.IsRemoved);
                result.Object = location?.ToDomainLocation();
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<LocationModel>(false, ex.Message);
            }
            return result;
        }

        public ResultModel<LocationSettingsModel> GetSettings(string locationId)
        {
            var result = new ResultModel<LocationSettingsModel>(true);
            try
            {
                var offlineIdentifiers = Enum.GetNames(typeof(AgentIdentifiersEnum)).ToList();

                var location = Context.Locations
                    .Include(l => l.Settings.TenderTypes)
                    .FirstOrDefault(l => l.Id == locationId && !l.IsRemoved && !offlineIdentifiers.Contains(l.Identifier));
                var settings = location?.Settings?.ToDomainSettings(location.Id, location.Identifier);

                result.Object = settings;
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public ResultModel<Dictionary<string, string>> GetTableMappingScheme(string locationId)
        {
            var result = new ResultModel<Dictionary<string, string>>(true);

            try
            {
                var location = Context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                if (location != null)
                {
                    var tableMappingScheme = location.Settings.MappingSchemes.FirstOrDefault(m => m.Type == MappingSchemeTypes.Table);
                    if (tableMappingScheme != null)
                    {
                        result.Object = tableMappingScheme.Scheme.ToMappingScheme();
                    }
                }
                else
                {
                    result = new ResultModel<Dictionary<string, string>>(false, "Invalid location.");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<Dictionary<string, string>>(false, ex.Message);
            }

            return result;
        }
    }
}
