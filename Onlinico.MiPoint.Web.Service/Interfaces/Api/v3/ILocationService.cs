﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v3;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v3
{
    public interface ILocationService : IBaseService
    {
        ResultModel<LocationSettingsModel> GetSettings(string locationId);

        ResultModel<Domain.Models.Api.v3.LocationModel> Get(string locationId);

        Task<ResultModel<Domain.Models.Api.v3.LocationModel>> GetAsync(string locationId);

        Task<ResultModel<EmployeeModel>> GetEmployeeAsync(string locationId, string login);

        ResultModel<Dictionary<string, string>> GetTableMappingScheme(string locationId);
    }
}
