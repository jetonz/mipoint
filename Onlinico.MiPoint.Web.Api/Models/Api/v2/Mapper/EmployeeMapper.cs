﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2.Mapper
{
    public static class EmployeeMapper
    {
        public static Models.Api.v2.EmployeeModel ToEmployeeModel(this Domain.Models.Api.v2.EmployeeModel employee)
        {
            return new Models.Api.v2.EmployeeModel
            {
                Id = employee.Id,
                EmployeeId = employee.EmployeeId,
                Login = employee.Login,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                CheckName = employee.CheckName,
            };
        }
    }
}