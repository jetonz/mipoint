﻿using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Exceptions
{
    public static class DbException
    {
        public static string ToFullErrorMessage(this System.Data.Entity.Validation.DbEntityValidationException ex)
        {
            var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

            var fullErrorMessage = string.Join("; ", errorMessages);
            return fullErrorMessage;
        }
    }
}
