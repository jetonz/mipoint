﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class HistoryMapper
    {
        public static List<HistoryModel> ToHistoryViewModels(this List<Domain.Models.Web.HistoryModel> models, int timezoneOffset)
        {
            return models.Select(h => h.ToHistoryViewModel(timezoneOffset)).ToList();
        }

        public static HistoryModel ToHistoryViewModel(this Domain.Models.Web.HistoryModel model, int timezoneOffset)
        {
            return new HistoryModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                Agent = model.Agent,
                LocationSecret = model.LocationSecret,
                Method = model.Method,
                Url = model.Url,
                Start = model.Start.AddMinutes(timezoneOffset),
                Duration = model.End - model.Start,
                StatusCode = model.StatusCode,
                RequestHeaders = model.RequestHeaders,
                RequestContent = model.RequestContent,
                ResponseHeaders = model.ResponseHeaders,
                ResponseContent = model.ResponseContent,
            };
        }
    }
}