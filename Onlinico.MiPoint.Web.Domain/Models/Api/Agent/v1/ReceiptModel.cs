﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1.Enum;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class SendReceiptModel
    {
        public string PaymentId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public CarierTypeEnum CarierType { get; set; }

        public string Address { get; set; }
    }
}
