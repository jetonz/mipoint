﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface IPaymentService
    {
        Task<ResultModel<List<PaymentModel>>> GetAsync(string locationId, DateTime from, DateTime to);

        Task<ResultModel<List<PaymentModel>>> GetFailedPaymentsAsync();

        Task<ResultModel> UpdateStatusAsync(string paymentId, PaymentStatusEnum status, string message);

        Task<ResultModel> UpdateMessageAsync(string paymentId, string message);
    }
}
