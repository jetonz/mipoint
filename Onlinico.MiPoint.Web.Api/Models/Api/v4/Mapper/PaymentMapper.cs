﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums.Transaction;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper
{
    public static class PaymentMapper
    {
        public static IQueryable<PaymentModel> ToApiPayments(this IEnumerable<Domain.Models.Api.v4.PaymentModel> models)
        {
            return models.Select(p => p.ToApiPaymentModel()).AsQueryable();
        } 

        public static PaymentModel ToApiPaymentModel(this Domain.Models.Api.v4.PaymentModel model)
        {
            return new PaymentModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tips = model.Tip,
                Tax = model.Tax,
                Status = model.Status,
                PaymentType = model.Type,
                EmployeeId = model.EmployeeCode,

                Items = model.Items?.ToApiPaymentItems(),
                Receipt = model.ReceiptInfo?.ToApiPaymentReceptInfo(model.TableNumber),
                Transaction = model.Transaction?.ToApiPaymentTransaction(),
                
            };
        }

        public static IEnumerable<PaymentItemModel> ToApiPaymentItems(this IEnumerable<Domain.Models.Api.v4.PaymentItemModel> models)
        {
            return models.Select(i => new PaymentItemModel
            {
                Id = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            });
        }

        public static PaymentReceptInfo ToApiPaymentReceptInfo(this Domain.Models.Api.v4.PaymentReceptInfoModel model, string tableName)
        {
            return new PaymentReceptInfo
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                TableName = tableName,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                ServiceCharge = model.OrderServiceCharge,
                SubTotal = model.OrderSubTotal,
                Tax = model.OrderTax,
                Total = model.OrderTotal,
                Headers = new List<string>() { model.ReceiptHeader1 , model.ReceiptHeader2 },
                Website = model.Website,
            };
        }

        public static PaymentTransaction ToApiPaymentTransaction(this Domain.Models.Api.v4.PaymentTransactionModel model)
        {
            return new PaymentTransaction
            {
                TransactionId = model.TransactionId,
                TransactionType = (TransactionTypeEnum) (int)model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = (CardTypeEnum) (int)model.CardType,
                ReferenceId = model.ReferenceId,
                lastFourDigits = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = (CardEntryTypeEnum) (int)model.CardEntryType,
                Issued = model.Issued,//DateTime.SpecifyKind(model.Issued, DateTimeKind.Utc),
            };
        }





        public static Domain.Models.Api.v4.PaymentModel ToDomainPaymentModel(this PaymentModel model)
        {
            return new Domain.Models.Api.v4.PaymentModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tips,
                Tax = model.Tax,
                Status = model.Status,
                Type = model.PaymentType,
                EmployeeCode = model.EmployeeId,
                TableNumber = model.Receipt?.TableName,

                Items = model.Items?.ToDomainPaymentItems(),
                ReceiptInfo = model.Receipt?.ToDomainPaymentReceptInfo(),
                Transaction = model.Transaction?.ToDomainPaymentTransaction(),

            };
        }

        public static Domain.Models.Api.v4.PaymentModel ToDomainPaymentModel(this ShortPaymentModel model)
        {
            return new Domain.Models.Api.v4.PaymentModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tips,
                Tax = model.Tax,
                Status = model.Status,
                Type = model.PaymentType,
                EmployeeCode = model.EmployeeId,
                TableNumber = model.Receipt?.TableName,

                Items = model.Items?.ToDomainPaymentItems(),
                ReceiptInfo = model.Receipt?.ToDomainPaymentReceptInfo(),

            };
        }

        public static List<Domain.Models.Api.v4.PaymentItemModel> ToDomainPaymentItems(this IEnumerable<PaymentItemModel> models)
        {
            return models.Select(i => new Domain.Models.Api.v4.PaymentItemModel
            {
                ItemId = i.Id,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }

        public static Domain.Models.Api.v4.PaymentReceptInfoModel ToDomainPaymentReceptInfo(this PaymentReceptInfo model)
        {
            return new Domain.Models.Api.v4.PaymentReceptInfoModel
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.ServiceCharge,
                OrderSubTotal = model.SubTotal,
                OrderTax = model.Tax,
                OrderTotal = model.Total,
                ReceiptHeader1 = model.Headers.Count >= 1 ? model.Headers[0] : string.Empty,
                ReceiptHeader2 = model.Headers.Count >= 2 ? model.Headers[1] : string.Empty,
                Website = model.Website,
            };
        }

        public static Domain.Models.Api.v4.PaymentTransactionModel ToDomainPaymentTransaction(this PaymentTransaction model)
        {
            return new Domain.Models.Api.v4.PaymentTransactionModel
            {
                TransactionId = model.TransactionId,
                TransactionType = (Domain.Enums.Transaction.TransactionTypeEnum) (int)model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = (Domain.Enums.Transaction.CardTypeEnum) (int)model.CardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.lastFourDigits,
                AuthCode = model.AuthCode,
                CardEntryType = (Domain.Enums.Transaction.CardEntryTypeEnum) (int)model.CardEntryType,
                Issued = model.Issued,
            };
        }
    }
}