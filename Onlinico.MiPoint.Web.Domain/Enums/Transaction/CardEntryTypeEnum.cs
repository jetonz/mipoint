﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Enums.Transaction
{
    public enum CardEntryTypeEnum
    {
        SWIPED = 0,
        KEYED = 1,
        VOICE = 2,
        VAULTED = 3,
        OFFLINE_SWIPED = 4,
        OFFLINE_KEYED = 5,
        EMV_CONTACT = 6,
        EMV_CONTACTLESS = 7,
        MSD_CONTACTLESS = 8,
        PINPAD_MANUAL_ENTRY = 9
    }
}
