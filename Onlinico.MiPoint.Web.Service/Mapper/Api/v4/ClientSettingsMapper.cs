﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v4
{
    public static class ClientSettingsMapper
    {
        public static ClientSettingsModel ToClientSettingsModel(this Location location)
        {
            Guid serverId;
            Guid.TryParse(location.AgentFingerprint, out serverId);

            return new ClientSettingsModel
            {
                ServerId = serverId,
                MerchantInfo = location.Merchant?.ToMerchantInfoModel(),
                TableModeSettings = location.ClientTableModeSettings?.ToTableModeSettingsModel(location.Settings.OpenOrderLimit, location.Settings.ClosedOrderLimit),
                CounterModeSettings = location.ClientCounterModeSettings?.ToTableModeSettingsModel(),
            };
        }

        public static MerchantInfoModel ToMerchantInfoModel(this Merchant merchant)
        {
            return new MerchantInfoModel
            {
                Address = merchant.Address,
                PhoneNumber = merchant.PhoneNumber,
                Website = merchant.WebSite,
                LogoUrl = merchant.LogoUrl,
                MerchantName = merchant.Name,
            };
        }

        public static TableModeSettingsModel ToTableModeSettingsModel(this ClientTableModeSettings settings, int openOrderLimit, int closeOrderLimit)
        {
            return new TableModeSettingsModel
            {
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                AutoPrintReceipt = settings.AutoPrintReceipt,
                DisplayLineItemsOnReceipt = settings.DisplayLineItemsOnReceipt,
                OnlyMyOrdersFilterEnabled = settings.OnlyMyOrdersFilterEnabled,
                PrintReceiptEnabled = settings.PrintReceiptEnabled,
                RequireSignature = settings.RequireSignature,
                SendReceiptEmailEnabled = settings.SendReceiptEmailEnabled,
                SendReceiptMessageEnabled = settings.SendReceiptMessageEnabled,
                SessionTimeOut = settings.SessionTimeOut,
                ShowReceiptScreen = settings.ShowReceiptScreen,
                ShowTipsScreen = settings.ShowTipsScreen,
                SessionTimeOutEnabled = settings.SessionTimeOutEnabled,
                ShowSignatureLineOnReceipt = settings.ShowSignatureLineOnReceipt,
                ShowTipLineOnReceipt = settings.ShowTipLineOnReceipt,
                OpenOrderLimit = openOrderLimit,
                ClosedOrderLimit = closeOrderLimit,

                TipSuggestions = settings.TipSuggestions?.Split(',').Select(Int16.Parse)
            };
        }

        public static CounterModeSettingsModel ToTableModeSettingsModel(this ClientCounterModeSettings settings)
        {
            return new CounterModeSettingsModel
            {
                AutoPrintReceipt = settings.AutoPrintReceipt,
                DisplayLineItemsOnReceipt = settings.DisplayLineItemsOnReceipt,
                PrintReceiptEnabled = settings.PrintReceiptEnabled,
                RequireSignature = settings.RequireSignature,
                SendReceiptEmailEnabled = settings.SendReceiptEmailEnabled,
                SendReceiptMessageEnabled = settings.SendReceiptMessageEnabled,
                ShowReceiptScreen = settings.ShowReceiptScreen,
                ShowTipsScreen = settings.ShowTipsScreen,
                ShowThanksSreen = settings.ShowThanksSreen,
                ThanksMessage = settings.ThanksMessage,
                WelcomeMessage = settings.WelcomeMessage,
                ShowTipLineOnReceipt = settings.ShowTipLineOnReceipt,
                ShowSignatureLineOnReceipt = settings.ShowSignatureLineOnReceipt,
                TipSuggestions = settings.TipSuggestions?.Split(',').Select(Int16.Parse),
            };
        }
    }
}
