﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Global;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;

namespace Onlinico.MiPoint.Web.Service.Services.Global
{
    public class RequestService : BaseService, IRequestService
    {
        public RequestService(ILogger logger) : base(logger)
        {
        }

        public async Task<ResultModel> AddAsync(RequestModel model)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == model.LocationSecret && !l.IsRemoved);
                    if (location != null)
                    {
                        var history = new History
                        {
                            Id = Guid.NewGuid(),
                            DeviceId = model.DeviceId,
                            Agent = model.Agent,
                            Method = model.Method,
                            Uri = model.Url,
                            Start = model.Start,
                            End = model.End,
                            StatusCode = model.StatusCode,
                            RequestHeaders = model.RequestHeaders,
                            RequestContent = model.RequestContent,
                            ResponseHeaders = model.ResponseHeaders,
                            ResponseContent = model.ResponseContent,
                            Location = location
                        };

                        context.Histories.Add(history);

                        await context.SaveChangesAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        result = new ResultModel(false, "Can't find location by 'location secret'");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
            }

            return result;
        }
    }
}
