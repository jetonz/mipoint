﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v4;
using Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v4
{
    public interface IPaymentService : IBaseService
    {
        Task<ResultModel<List<PaymentModel>>> GetPaymentsForOrdersAsync(string locationId,
            List<string> ordersId, PaymentStatusEnum status);

        Task<ResultModel<ThirdPartyPaymentModel>> GetPaymentToThirdPartyPayment(string paymentId);

        Task<ResultModel<PaymentModel>> GetAsync(string paymentId);

        Task<ResultModel<IEnumerable<PaymentModel>>> GetByLocationAsync(string locationId);

        Task<ResultModel> AddAsync(string locationId, PaymentModel model);

        Task<ResultModel> ConfirmAsync(string paymentId, PaymentTransactionModel transaction);

        Task<ResultModel> UpdateStatusAsync(string paymentId, PaymentStatusEnum paymentStatus, string message);

        Task<ResultModel> UpdateMessageAsync(string paymentId, string message);

        Task<ResultModel> CancelAsync(string paymentId, string message);
    }
}
