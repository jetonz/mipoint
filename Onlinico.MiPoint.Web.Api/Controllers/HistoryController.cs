﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// History Controller
    /// </summary>
    public class HistoryController : BaseController
    {
        private readonly IHistoryService _historyService;

        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="historyService">Inject history service</param>
        /// <param name="configuration">Inject configuration</param>
        public HistoryController(IHistoryService historyService, IConfiguration configuration) : base(configuration)
        {
            _historyService = historyService;
        }
        
        /// <summary>
        /// History root page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get history rows
        /// </summary>
        /// <param name="start">Start datetime</param>
        /// <param name="end">End datetime</param>
        /// <param name="timezoneOffset">Client offset</param>
        /// <returns></returns>
        public async Task<ActionResult> HistoryRows(DateTime start, DateTime end, int timezoneOffset = 0)
        {
            var inverOffset = timezoneOffset * -1;
            
            var histories = _historyService.Get(start.AddMinutes(timezoneOffset), end.AddMinutes(timezoneOffset));

            return View(histories.Object?.ToHistoryViewModels(inverOffset));
        }

        /// <summary>
        /// Get history content page
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult HistoryDetails(Guid id, int timezoneOffset = 0)
        {
            ViewBag.HistoryId = id;

            var inverOffset = timezoneOffset * -1;

            var content = _historyService.Get(id);

            return View(content.Object?.ToHistoryViewModel(inverOffset));
        }

        public ActionResult GetHistoryContent(Guid id)
        {
            var contentResuult = _historyService.GetContent(id);
            return Content(contentResuult.Object);
        }
    }
}