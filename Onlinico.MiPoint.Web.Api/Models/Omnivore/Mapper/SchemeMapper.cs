﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Onlinico.MiPoint.Web.Api.Helpers;
using Onlinico.MiPoint.Web.Core.Mapper;

namespace Onlinico.MiPoint.Web.Api.Models.Omnivore.Mapper
{
    public static class SchemeMapper
    {
        public static void ToScheme(this Models.Omnivore.Tickets.Employee model, Dictionary<string, string> scheme)
        {
            var clon = (Models.Omnivore.Tickets.Employee)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }

        public static void ToScheme(this Models.Omnivore.Tickets.Table model, Dictionary<string, string> scheme)
        {
            var clon = (Models.Omnivore.Tickets.Table)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }

        public static void ToScheme(this Service.Models.Omnivore.Ticket.Tickets.Employee model, Dictionary<string, string> scheme)
        {
            var clon = (Service.Models.Omnivore.Ticket.Tickets.Employee)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }

        public static void ToScheme(this Service.Models.Omnivore.Ticket.Tickets.Table model, Dictionary<string, string> scheme)
        {
            var clon = (Service.Models.Omnivore.Ticket.Tickets.Table)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }

        public static void ToScheme(this Service.Models.Omnivore.Ticket.Ticket.Employee model, Dictionary<string, string> scheme)
        {
            var clon = (Service.Models.Omnivore.Ticket.Ticket.Employee)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }

        public static void ToScheme(this Service.Models.Omnivore.Ticket.Ticket.Table model, Dictionary<string, string> scheme)
        {
            var clon = (Service.Models.Omnivore.Ticket.Ticket.Table)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }

        public static void ToScheme(this Models.Omnivore.Ticket.Table model, Dictionary<string, string> scheme)
        {
            var clon = (Models.Omnivore.Ticket.Table)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }

        public static void ToScheme(this Models.Omnivore.Ticket.Employee model, Dictionary<string, string> scheme)
        {
            var clon = (Models.Omnivore.Ticket.Employee)model.Clone();

            Core.Mapper.SchemeMapper.To(clon, model, scheme);
        }
    }
}