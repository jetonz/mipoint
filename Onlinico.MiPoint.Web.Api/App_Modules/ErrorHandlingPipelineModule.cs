﻿using System;
using Autofac;
using Microsoft.AspNet.SignalR.Hubs;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;

namespace Onlinico.MiPoint.Web.Api.App_Modules
{
    public class ErrorHandlingPipelineModule : HubPipelineModule
    {
        private ILogger _logger;

        public ErrorHandlingPipelineModule(Autofac.IContainer container)
        {
            _logger = container.Resolve<ILogger>();
        }

        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext)
        {
            _logger.WriteMessageToLog(StringHelper.ErrorBuilder($"ErrorHandlingPipelineModule: {exceptionContext.Error?.Message}. InnerException: {exceptionContext.Error?.InnerException?.Message}"));

            try
            {
                invokerContext.Hub.Clients.Caller.notifyOfException(exceptionContext.Error);
            }
            catch (Exception ex)
            {
                _logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. InnerException: {ex.InnerException?.Message}"));
            }

            base.OnIncomingError(exceptionContext, invokerContext);

        }
    }
}