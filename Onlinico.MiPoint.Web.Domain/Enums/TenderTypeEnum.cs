﻿using System.ComponentModel;

namespace Onlinico.MiPoint.Web.Domain.Enums
{
    public enum TenderTypeEnum
    {
        //NotSet = 0,

        Cash = 101,
        Checks = 102,
        [Description("GC Redeem")]
        GcRedeem = 103,
        Coupon = 104,
        [Description("Online Order")]
        OnlineOrder = 105,
        WingCash = 106,

        Visa = 201,
        Mastercard = 202,
        Discover = 203,
        [Description("AMEX")]
        Amex = 204,

        [Description("Issue Points")]
        IssuePoints = 302,
        [Description("GCard Redeem")]
        GcardRedeem = 303,
        [Description("PP Gift Rdm")]
        PpGiftRdm = 305,
    }
}
