﻿namespace Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1.Enums
{
    public enum AgentStatusEnum
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown,
        /// <summary>
        /// NotInstalled
        /// </summary>
        NotInstalled,
        /// <summary>
        /// Stopped
        /// </summary>
        Stopped,
        /// <summary>
        /// StopPending
        /// </summary>
        StopPending,
        /// <summary>
        /// Running
        /// </summary>
        Running,
        /// <summary>
        /// StartPending
        /// </summary>
        StartPending
    }
}