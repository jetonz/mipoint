﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class OfflineLocationModel
    {
        [Required]
        public Guid MerchantId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Owner { get; set; }

        [Required]
        public string PosType { get; set; }
        
        public string Phone { get; set; }
        
        public string Address { get; set; }
        
        public string AddressFull { get; set; }
    }
}