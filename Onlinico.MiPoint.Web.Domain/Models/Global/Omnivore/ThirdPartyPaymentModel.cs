﻿using Newtonsoft.Json;

namespace Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore
{
    public class ThirdPartyPaymentModel
    {
        [JsonIgnore]
        public string TicketId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; } = "3rd_party";

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("tip")]
        public long Tip { get; set; }

        [JsonProperty("tender_type")]
        public string TenderType { get; set; }

        [JsonProperty("payment_source")]
        public string PaymentSource { get; set; } = "External transaction";
    }
}
