﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class ReceiptItem
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public long Quantity { get; set; }

        public long Price { get; set; }


        public virtual Receipt Receipt { get; set; }
    }
}
