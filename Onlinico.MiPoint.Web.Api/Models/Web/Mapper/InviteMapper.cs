﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class InviteMapper
    {
        public static Domain.Models.Web.InviteModel ToDomainInviteModel(this Models.Web.InviteModel model)
        {
            return new Domain.Models.Web.InviteModel
            {
                Id = model.Id,
                Email = model.Email,
                MerchantId = model.MerchantId
            };
        }
    }
}