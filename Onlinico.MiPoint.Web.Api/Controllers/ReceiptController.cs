﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Receipt controller
    /// </summary>
    [System.Web.Mvc.AllowAnonymous]
    public class ReceiptController : BaseController
    {
        private readonly IReceiptService _receiptService;

        /// <summary>
        /// Receipt controller constructor
        /// </summary>
        /// <param name="receiptService">Inject receipt service</param>
        /// <param name="configuration">Inject configuration</param>
        public ReceiptController(IReceiptService receiptService, IConfiguration configuration) : base(configuration)
        {
            _receiptService = receiptService;
        }

        /// <summary>
        /// Receipt details info
        /// </summary>
        /// <param name="id">Receipt id</param>
        /// <returns>View</returns>
        [System.Web.Mvc.HttpGet, System.Web.Mvc.Route("{id}")]
        public async Task<ActionResult> Details([FromUri]string id)
        {
            var receipt = await _receiptService.GetAsync(id);

            return View(receipt?.Object?.ToReceiptModel());
        }

        /// <summary>
        /// Receipt view
        /// </summary>
        /// <param name="paymentId">Paument id</param>
        /// <returns>View</returns>
        [System.Web.Mvc.HttpGet, System.Web.Mvc.Route("{paymentId}")]
        public async Task<ActionResult> Info([FromUri]string paymentId)
        {
            var receipt = await _receiptService.GetFromPaymentAsync(paymentId);

            return View("Details", receipt?.Object?.ToReceiptModel());
            
        }
    }
}