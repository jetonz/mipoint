﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Location = Onlinico.MiPoint.Web.Service.Models.Omnivore.Locations.Location;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class LocationMapper
    {
        public static List<Domain.Models.Web.LocationModel> ToDomainLocations(this List<Location> omnivoreLocations)
        {
            return omnivoreLocations.Select(l => new Domain.Models.Web.LocationModel
            {
                Identifier = l.id,
                Name = l.name,
                Phone = l.phone,
                Address = l.address?.street1,
                AddressFull = $"{l.address?.city} {l.address?.state} {l.address?.zip}",
                Owner = l.owner,
                IsDev = l.development ?? false,
                PosType = l.pos_type,
                Employees = new List<Domain.Models.Web.EmployeeModel>(),
            }).ToList();
        }

        public static List<Domain.Models.Web.LocationModel> ToDomainLocations(this ICollection<Data.DataModel.Location> locations)
        {
            return locations.Select(l => l.ToDomainLocation()).ToList();
        }

        public static Domain.Models.Web.LocationModel ToDomainLocation(this Data.DataModel.Location location)
        {
            Domain.Enums.AgentIdentifiersEnum agentEnum;

            var result = Enum.TryParse(location.Identifier, true, out agentEnum);

            return new Domain.Models.Web.LocationModel
            {
                Id = location.Id,
                Identifier = location.Identifier,
                Name = location.Name,
                Phone = location.Phone,
                Address = location.Address,
                AddressFull = location.AddressFull,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType,
                AgentFingerprint = location.AgentFingerprint,
                IsConfigured = location.Settings.TenderTypes.All(t => t.Type != null),
                IsOffline = result
            };
        }

        public static Data.DataModel.Location ToNewLocationEntity(this Domain.Models.Web.LocationModel location)
        {
            return new Data.DataModel.Location
            {
                Identifier = location.Identifier,
                Name = location.Name,
                Phone = location.Phone,
                Address = location.Address,
                AddressFull = location.AddressFull,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType,
            };
        }

        public static Data.DataModel.Location ToNewLocationEntity(this Domain.Models.Web.LocationModel location, string id, List<Data.DataModel.Card> cards, int timeOffset = 0)
        {
            var syncTime = DateTime.UtcNow.Date.AddMinutes(timeOffset).TimeOfDay;
            var tenderTypes = cards.Select(c => new TenderType
            {
                Id = Guid.NewGuid(),
                Card = c
            }).ToList();

            return new Data.DataModel.Location
            {
                Id = id,
                Identifier = location.Identifier,
                Name = location.Name,
                Phone = location.Phone,
                Address = location.Address,
                AddressFull = location.AddressFull,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType,
                LastSync = DateTime.UtcNow,
                IsRemoved = false,
                Settings = Data.DataModel.Constructor.EntityConstructor.CreateLocationSettings(tenderTypes, syncTime),
                ClientCounterModeSettings = Data.DataModel.Constructor.EntityConstructor.CreateClientCounterModeSettings(),
                ClientTableModeSettings = Data.DataModel.Constructor.EntityConstructor.CreateClientTableModeSettings(),
                Employees = location.Employees?.Select(e => new Employee
                {
                    Id = e.Id,
                    EmployeeId = e.EmployeeId,
                    Login = e.Login,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    CheckName = e.CheckName,
                }).ToList()
            };
        }

        public static void UpdateLocationEntity(this Data.DataModel.Location location, Domain.Models.Web.LocationModel domainLocation)
        {
            location.Owner = domainLocation.Owner;
            location.Name = domainLocation.Name;
            location.Phone = domainLocation.Phone;
            location.Address = domainLocation.Address;
            location.AddressFull = domainLocation.AddressFull;
            location.IsDev = domainLocation.IsDev;
            location.PosType = domainLocation.PosType;
            //location.LastSync = DateTime.UtcNow;
        }
    }
}
