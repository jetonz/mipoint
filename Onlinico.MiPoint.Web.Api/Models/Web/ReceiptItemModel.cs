﻿using System;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class ReceiptItemModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public long Quantity { get; set; }

        public string Price { get; set; }
    }
}