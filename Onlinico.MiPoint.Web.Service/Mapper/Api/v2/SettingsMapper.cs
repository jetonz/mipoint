﻿using System;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v2
{
    public static class SettingsMapper
    {
        public static Domain.Models.Api.v2.LocationSettingsModel ToDomainSettings(this Data.DataModel.LocationSettings settings, string locationId, string omnivoreId)
        {
            return new Domain.Models.Api.v2.LocationSettingsModel
            {
                Id = settings.Id,
                OmnivoreId = omnivoreId,
                TaxRate = settings.TaxRate,
                Tips = settings.Tips.Split(',').Select(Int32.Parse).ToArray(),
                TenderTypes = settings.TenderTypes.ToDomainTenderTypes(),
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                SessionTimeout = settings.SessionTimeout,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }
        
        //public static Data.DataModel.LocationSettings ToEntitySettings(this Domain.Models.Api.v2.LocationSettingsModel settings)
        //{
        //    return new Data.DataModel.LocationSettings
        //    {
        //        Id = settings.Id,
        //        TaxRate = settings.TaxRate,
        //        Tips = string.Join(",", settings.Tips),
        //        TenderTypes = settings.TenderTypes.ToEntityTenderTypes()
        //    };
        //}

        //public static void UpdateSettings(this Data.DataModel.LocationSettings entity, Domain.Models.Api.v2.LocationSettingsModel settings)
        //{
        //    entity.TaxRate = settings.TaxRate;
        //    entity.Tips = string.Join(",", settings.Tips);
        //    entity.TenderTypes.ToList().ForEach(e=>e.UpdateTenderTypeEntity(settings.TenderTypes.First(t=>t.Id == e.Id)));
        //}
    }
}
