﻿using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v1
{
    [DataContract]
    public class LogModel
    {
        [DataMember(Name = "manufacturer")]
        public string Manufacturer { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "fingerprint")]
        public string Fingerprint { get; set; }

        [DataMember(Name = "serial")]
        public string Serial { get; set; }

        [DataMember(Name = "content")]
        public string Content { get; set; }
    }
}