﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Onlinico.MiPoint.Web.Api.Extensions;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v2;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2.Mapper
{
    public static class PaymentMapper
    {
        public static Domain.Models.Api.v2.PaymentModel ToDomainPaymentModel(this Models.Api.v2.PaymentModel model)
        {
            return new Domain.Models.Api.v2.PaymentModel
            {
                Id = model.Id,
                TicketId = model.TicketId,
                Amount = model.Amount,
                Tip = model.Tip,
                CardType = model.CardType,
                Items = model.Items?.ToPaymentItems()
            };
        }

        public static List<Domain.Models.Api.v2.PaymentItemModel> ToPaymentItems(
            this List<Models.Api.v2.PaymentItem> models)
        {
            return models.Select(i => new Domain.Models.Api.v2.PaymentItemModel
            {
                ItemId = i.Id,
                Name = i.Name,
                Quantity = i.Quantity,
                Price = i.Price,
            }).ToList();
        }

        public static List<Models.Api.v2.PaymentModel> ToPayments(this List<Domain.Models.Api.v2.PaymentModel> models)
        {
            return models.Select(p => new Models.Api.v2.PaymentModel
            {
                Id = p.Id,
                TicketId = p.TicketId,
                //Amount = p.Amount,
                //Tip = p.Tip,
                CardType = p.CardType,
                Items = p.Items?.ToPaymentItems(),
            }).ToList();
        }

        public static List<PaymentItem> ToPaymentItems(this List<Domain.Models.Api.v2.PaymentItemModel> items)
        {
            return items.Select(i => new PaymentItem
            {
                Id = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity,
            }).ToList();
        } 

        public static Models.Api.v2.PaymentModel ToPayment(this Domain.Models.Api.v2.PaymentModel m)
        {
            return new Models.Api.v2.PaymentModel
            {
                //Id = m.Id,
                //OrderId = m.OrderId,
                //Amount = m.Amount,
                //Tip = m.Tip,
                //Payload = m.Payload,
                //ErrorMessage = m.ErrorMessage,
                //Issued = m.Issued.ToUtc(),
            };
        }
    }
}