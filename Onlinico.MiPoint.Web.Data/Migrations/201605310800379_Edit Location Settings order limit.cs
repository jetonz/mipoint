namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditLocationSettingsorderlimit : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.LocationSettings", "OrderLimit", "OpenOrderLimit");
            AddColumn("dbo.LocationSettings", "ClosedOrderLimit", c => c.Int(nullable: false, defaultValueSql: "25"));
        }
        
        public override void Down()
        {
            RenameColumn("dbo.LocationSettings", "OpenOrderLimit", "OrderLimit");
            DropColumn("dbo.LocationSettings", "ClosedOrderLimit");
        }
    }
}
