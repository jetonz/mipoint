﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Api.Codebase.CustomAction;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using RestSharp.Extensions;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    public class AgentController : BaseController
    {
        private readonly IAuthService _authService;

        public AgentController(IConfiguration configuration, IAuthService authService) : base(configuration)
        {
            _authService = authService;
        }

        [AllowAnonymous, AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Download(string locationId)
        {
            var identity = _authService.GetIdentity(locationId);
            if (identity.Succeed && identity.Object != null)
            {
                return new DownloadResult(GetSetupFilePath(), false)
                {
                    FileDownloadName = $"MiPointSetup.{locationId}.Agent.exe",
                    ContentType = "application/exe"
                };
            }

            return new HttpUnauthorizedResult();
        }

        private string GetSetupFilePath()
        {
            return "c:\\data\\agent\\setup.exe";
        }
    }
}