﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Core.Interfaces
{
    public interface IAgentManager
    {
        void Connected(string apiKey, string fingerptint);

        void Reconnected(string apiKey, string fingerptint);

        void Disconnected(string apiKey, string fingerptint);

        bool IsConnected(string apikey, string fingerprint);

        void VicarConnected(string apiKey, string fingerptint);

        void VicarReconnected(string apiKey, string fingerptint);

        void VicarDisconnected(string apiKey, string fingerptint);

        bool IsVicarConnected(string apikey, string fingerprint);
    }
}
