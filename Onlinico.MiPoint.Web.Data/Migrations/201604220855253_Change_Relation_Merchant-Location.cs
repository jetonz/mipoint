namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Relation_MerchantLocation : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Locations", new[] { "Merchant_Id" });
            AlterColumn("dbo.Locations", "Merchant_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.Locations", "Merchant_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Locations", new[] { "Merchant_Id" });
            AlterColumn("dbo.Locations", "Merchant_Id", c => c.Guid());
            CreateIndex("dbo.Locations", "Merchant_Id");
        }
    }
}
