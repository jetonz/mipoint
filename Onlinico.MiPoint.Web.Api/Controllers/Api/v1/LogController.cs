﻿using System.Threading.Tasks;
using System.Web.Http;
using Onlinico.MiPoint.Web.Api.Models.Api.v1;
using Onlinico.MiPoint.Web.Api.Models.Api.v1.Mapper;
using Onlinico.MiPoint.Web.Api.Models.Api.v1.Validation;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v1
{
    [Attributes.Authorize, RoutePrefix("api/log")]
    public class LogController : BaseController
    {
        private readonly ILogService _logService;

        private readonly IConfiguration _configuration;

        public LogController(ILogService logService, IConfiguration configuration)
        {
            _logService = logService;
            _configuration = configuration;
        }

        /// <summary>
        /// Add log
        /// </summary>
        /// <returns>Http status code</returns>
        [HttpPost, Route]
        public async Task<IHttpActionResult> Post([FromBody]LogModel model)
        {
            if (model.IsValid())
            {
                var result = await _logService.AddAsync(model.ToDomainLogModel(Request.Properties[_configuration.Secret].ToString()));
                return result.Succeed ? Ok() : (IHttpActionResult)BadRequest();
            }
            else
            {
                return BadRequest("Invalid model.");
            }
        }
    }
}
