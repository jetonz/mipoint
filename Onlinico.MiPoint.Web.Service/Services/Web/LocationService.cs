﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;
using Onlinico.MiPoint.Web.Service.Mapper.Exceptions;
using Onlinico.MiPoint.Web.Service.Mapper.Web;

namespace Onlinico.MiPoint.Web.Service.Services.Web
{
    public class LocationService : BaseService, ILocationService
    {
        private readonly IMemoryCacher _memoryCacher;

        public LocationService(ILogger logger, IMemoryCacher memoryCacher) : base(logger)
        {
            _memoryCacher = memoryCacher;
        }

        public async Task<ResultModel> AddAsync(Guid merchantId, List<LocationModel> locations, int timeOffset = 0)
        {
            ResultModel result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = await context.Merchants.FirstOrDefaultAsync(m => m.Id == merchantId);
                    if (merchant != null)
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;
                        var generatedKeys = new List<string>();
                        locations.ForEach(l =>
                        {
                            var freeId = string.Empty;
                            var exit = false;
                            while (!exit)
                            {
                                freeId = Core.StringGenerator.RandomStringKey();
                                if (generatedKeys.All(k => k != freeId) && context.Locations.All(a => a.Id != freeId))
                                {
                                    generatedKeys.Add(freeId);
                                    exit = true;
                                }
                            }

                            var cards = context.Cards.ToList();

                            merchant.Locations.Add(l.ToNewLocationEntity(freeId, cards, timeOffset));
                        });

                        context.Configuration.AutoDetectChangesEnabled = true;

                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid merchant id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            return result;
        }

        public async Task<ResultModel> RemoveAsync(string locationId)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == locationId);
                    if (location != null)
                    {
                        location.IsRemoved = true;

                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid location id.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel> UpdateAsync(LocationModel model)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == model.Id);
                    if (location != null)
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;

                        location.UpdateLocationEntity(model);

                        context.Employees.RemoveRange(location.Employees);

                        context.Employees.AddRange(model.Employees.ToNewEmployees(location));

                        context.Configuration.AutoDetectChangesEnabled = true;

                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        result.Succeed = false;
                        result.Error = "Invalid location id.";
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var error = ex.ToFullErrorMessage();
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(error));
                result.Succeed = false;
                result.Error = ex.Message;
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<List<LocationModel>>> GetAsync()
        {
            var result = new ResultModel<List<LocationModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var locations = await context.Locations.Where(l => !l.IsRemoved).ToListAsync();
                    result.Object = locations?.ToDomainLocations();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result = new ResultModel<List<LocationModel>>(false, ex.Message);
            }

            return result;
        }

        public async Task<ResultModel<LocationModel>> GetAsync(string locationId)
        {
            var result = new ResultModel<LocationModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = await context.Locations.FirstOrDefaultAsync(l => l.Id == locationId && !l.IsRemoved);
                    result.Object = location?.ToDomainLocation();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public ResultModel<List<LocationModel>> Get(TimeSpan startSync, TimeSpan endSync)
        {
            var result = new ResultModel<List<LocationModel>>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var locations = context.Locations
                        .Include(l => l.Settings).Include(l => l.Merchant).ToList()
                            .Where(l =>
                                    !l.IsRemoved &&
                                    (l.Settings.SyncTime > startSync && l.Settings.SyncTime <= endSync) ||
                                    (l.Settings.SyncPeriod != TimeSpan.Zero &&
                                     l.LastSync.Add(l.Settings.SyncPeriod).TimeOfDay > startSync &&
                                     l.LastSync.Add(l.Settings.SyncPeriod).TimeOfDay <= endSync))
                            .ToList();
                    //.Include(l=>l.Merchant).ToList();
                    result.Object = locations.ToDomainLocations();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result = new ResultModel<List<LocationModel>>(false, ex.Message);
            }

            return result;
        }

        public List<LocationModel> GetByMerchant(Guid merchantId)
        {
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var locations = context.Locations.Where(l => l.Merchant.Id == merchantId && !l.IsRemoved).ToList();

                    return locations.ToDomainLocations();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
            }

            return null;
        }

        public LocationSettingsModel GetSettings(string locationId)
        {
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.Include(l => l.Settings.TenderTypes).FirstOrDefault(l => l.Id == locationId);
                    return location?.Settings?.ToDomainSettings();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
            }

            return null;
        }

        public ResultModel<LocationSettingsModel> UpdatteSettings(LocationSettingsModel model)
        {
            var result = new ResultModel<LocationSettingsModel>(true);
            try
            {

                using (var context = new MiPointDbContext())
                {
                    var settings = context.LocationSettings.Include(s => s.Location).FirstOrDefault(s => s.Id == model.Id);
                    if (settings != null)
                    {
                        settings.UpdateSettings(model);

                        context.SaveChanges();

                        result.Object = settings.ToDomainSettings();
                    }
                    else
                    {
                        result = new ResultModel<LocationSettingsModel>(false, "Invalid data.");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel<LocationSettingsModel>(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }

            return result;
        }

        public ResultModel SyncEmployees(string locationId, List<EmployeeModel> employees)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    if (location != null)
                    {
                        var employeeEntities = location.Employees.ToList();
                        context.Configuration.AutoDetectChangesEnabled = false;

                        employees.ForEach(e =>
                        {
                            var employeeEntity = employeeEntities.FirstOrDefault(r => r.EmployeeId == e.EmployeeId);
                            if (employeeEntity != null)
                            {
                                employeeEntity.Update(e);
                            }
                            else
                            {
                                context.Employees.Add(e.ToNewEmployee(location));
                            }
                        });

                        context.Configuration.AutoDetectChangesEnabled = true;

                        context.SaveChanges();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid location id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public ResultModel UpdateLocationLastSyncDate(string locationId)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    if (location != null)
                    {
                        location.LastSync = DateTime.UtcNow;

                        context.SaveChanges();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid location id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public ResultModel<MappingSchemeModel> GetMappingScheme(string locationId)
        {
            var result = new ResultModel<MappingSchemeModel>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    if (location != null)
                    {
                        result.Object = location.Settings.MappingSchemes?.ToList().ToDomainMappingSchemeModel(locationId);
                    }
                    else
                    {
                        result = new ResultModel<MappingSchemeModel>(false, "Invalid location.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<MappingSchemeModel>(false, ex.Message);
            }

            return result;
        }

        public ResultModel<MappingSchemeModel> GetMappingSchemeByIdentifier(string locationSecret)
        {
            var result = new ResultModel<MappingSchemeModel>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Identifier == locationSecret && !l.IsRemoved);
                    if (location != null)
                    {
                        result.Object = location.Settings.MappingSchemes?.ToList().ToDomainMappingSchemeModel(location.Id);
                    }
                    else
                    {
                        result = new ResultModel<MappingSchemeModel>(false, "Invalid location.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel<MappingSchemeModel>(false, ex.Message);
            }

            return result;
        }

        public ResultModel UpdateMappingScheme(string locationId, MappingSchemeModel model)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    if (location != null)
                    {
                        context.MappingSchemes.RemoveRange(location.Settings.MappingSchemes);

                        model.ToNewMappingSchemes().ForEach(m =>
                        {
                            location.Settings.MappingSchemes.Add(m);
                        });

                        context.SaveChanges();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid location.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public ResultModel RemoveAgentFingerprint(string locationId)
        {
            var result = new ResultModel(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                    if (location != null)
                    {
                        location.AgentFingerprint = null;
                        context.SaveChanges();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid location.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }

            return result;
        }

        public ResultModel<List<AvailableTenderTypeModel>> GetAvailableTenderTypes(string locationId)
        {
            var result = new ResultModel<List<AvailableTenderTypeModel>>(true);

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);

                    result.Object = location?.Settings?.AvailableTenderTypes?.ToDomainAvailableTenderTypes();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        
    }
}
