﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3
{
    public class OrderModel
    {
        // "{order_number}-{order_open_time}"
        [JsonProperty("id")]
        public string OrderId { get; set; }

        [JsonProperty("employeeId")]
        public string EmployeeId { get; set; }

        [JsonProperty("orderNumber")]
        public int OrderNumber { get; set; }

        [JsonProperty("guestCount")]
        public int GuestCount { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("paymentStatus")]
        public Enums.OrderPaymentStatusEnum PaymentStatus { get; set; }

        [JsonProperty("openedAt")]
        public DateTime OpenedAt { get; set; }

        [JsonProperty("closedAt")]
        public DateTime? ClosedAt { get; set; }

        [JsonProperty("hasDiscounts")]
        public bool HasDiscounts { get; set; }

        [JsonProperty("totals")]
        public OrderTotalModel Total { get; set; }

        [JsonProperty("table")]
        public OrderTableModel Table { get; set; }

        [JsonProperty("items")]
        public List<OrderItemModel> Items { get; set; }

        [JsonProperty("payments")]
        public List<PaymentModel> Payments { get; set; }

        [JsonProperty("discounts")]
        public List<DiscountModel> Discounts { get; set; } 
    }

    public class ShortOrderModel
    {
        // "{order_number}-{order_open_time}"
        [JsonProperty("id")]
        public string OrderId { get; set; }

        [JsonProperty("employeeId")]
        public string EmployeeId { get; set; }

        [JsonProperty("orderNumber")]
        public int OrderNumber { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("paymentStatus")]
        public Enums.OrderPaymentStatusEnum PaymentStatus { get; set; }

        [JsonProperty("openedAt")]
        public DateTime OpenedAt { get; set; }

        [JsonProperty("totals")]
        public OrderTotalModel Total { get; set; }

        [JsonProperty("table")]
        public OrderTableModel Table { get; set; }
    }

    public class OrderTotalModel
    {
        [JsonProperty("due")]
        public long Due { get; set; }

        [JsonProperty("otherCharges")]
        public long OtherCharges { get; set; }

        [JsonProperty("serviceCharges")]
        public long ServiceCharges { get; set; }

        [JsonProperty("subTotal")]
        public long SubTotal { get; set; }

        [JsonProperty("tax")]
        public long Tax { get; set; }

        [JsonProperty("total")]
        public long Total { get; set; }
    }

    public class OrderItemModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("hasDiscounts")]
        public bool HasDiscounts { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("discounts")]
        public List<DiscountModel> Discounts { get; set; } 
    }

    public class DiscountModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }
    }

    public class OrderTableModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("available")]
        public bool Available { get; set; }

        [JsonProperty("number")]
        public long Number { get; set; }

        [JsonProperty("seats")]
        public long Seats { get; set; }
    }
}