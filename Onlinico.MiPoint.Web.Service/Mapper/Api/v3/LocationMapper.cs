﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Location = Onlinico.MiPoint.Web.Service.Models.Omnivore.Locations.Location;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v3
{
    public static class LocationMapper
    {

        public static Domain.Models.Api.v3.LocationModel ToDomainLocation(this Data.DataModel.Location location)
        {
            return new Domain.Models.Api.v3.LocationModel
            {
                Id = location.Id,
                Identifier = location.Identifier,
                Name = location.Name,
                Phone = location.Phone,
                Address = location.Address,
                AddressFull = location.AddressFull,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType
            };
        }
    }
}
