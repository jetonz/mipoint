﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Web.Http;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Core.Interfaces;
using RestSharp.Extensions;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v3
{
    /// <summary>
    /// Base web api controller
    /// </summary>
    public class BaseController : ApiController
    {
        /// <summary>
        /// Base Omnivore url
        /// </summary>
        protected readonly string BaseUri;

        /// <summary>
        /// Logger
        /// </summary>
        protected readonly Logger.Interfaces.ILogger Logger;

        /// <summary>
        /// App configuration
        /// </summary>
        protected readonly IConfiguration Configuration;

        /// <summary>
        /// Base controller default constructor
        /// </summary>
        public BaseController()
        {
            BaseUri = "https://api.omnivore.io/";
            Logger = DependencyResolver.Current.GetService<Logger.Interfaces.ILogger>();
            Configuration = DependencyResolver.Current.GetService<IConfiguration>();

            
        }

        /// <summary>
        /// Base controller constructor
        /// </summary>
        /// <param name="logger">Inject logger</param>
        public BaseController(Logger.Interfaces.ILogger logger)
        {
            BaseUri = "https://api.omnivore.io/";
            Logger = logger;
            Configuration = DependencyResolver.Current.GetService<IConfiguration>();
        }

        /// <summary>
        /// Base controller constructor
        /// </summary>
        /// <param name="logger">Inject logger</param>
        /// <param name="configuration">Inject app configuration</param>
        public BaseController(Logger.Interfaces.ILogger logger, IConfiguration configuration)
        {
            BaseUri = "https://api.omnivore.io/";
            Logger = logger;
            Configuration = configuration;
        }

        /// <summary>
        /// Get current server domain
        /// </summary>
        protected Uri CurrentDomain => new Uri(Request.RequestUri.Scheme + System.Uri.SchemeDelimiter + Request.RequestUri.Host + (Request.RequestUri.IsDefaultPort ? "" : ":" + Request.RequestUri.Port));

        /// <summary>
        /// 
        /// </summary>
        protected string LocationId => Request.Properties[Configuration.Secret].ToString();
    }
}
