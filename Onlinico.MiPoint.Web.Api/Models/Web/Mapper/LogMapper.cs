﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class LogMapper
    {
        public static List<LogModel> ToLogModels(this List<SessionLogModel> models, int timezoneOffset)
        {
            return models.Select(r => new LogModel
            {
                Id = r.Id,
                Model = r.Model,
                LocationId = r.LocationId,
                Manufacturer = r.Manufacturer,
                Serial = r.Serial,
                Fingerprint = r.Fingerprint,
                Created = r.Created.AddMinutes(timezoneOffset),
                AppVersion = r.AppVersion
            }).ToList();
        }

        public static LogModel ToLogModel(this SessionLogModel model, int timezoneOffset)
        {
            return new LogModel
            {
                Id = model.Id,
                Model = model.Model,
                Content = IsBase64String(model.Content)? DecodeBase64(model.Content): model.Content,
                LocationId = model.LocationId,
                Manufacturer = model.Manufacturer,
                Serial = model.Serial,
                Fingerprint = model.Fingerprint,
                Created = model.Created.AddMinutes(timezoneOffset),
                AppVersion = model.AppVersion
            };
        }

        private static bool IsBase64String(this string s)
        {
            s = s.Trim();
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);

        }

        private static string DecodeBase64(string str)
        {
            try
            {
                byte[] data = Convert.FromBase64String(str);
                string decodedString = Encoding.UTF8.GetString(data);
                return decodedString;
            }
            catch
            {
                return str;
            }
        }
    }
}