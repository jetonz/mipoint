﻿using System;

namespace Onlinico.MiPoint.Web.Api.Hubs.Interfaces
{
    public interface IAgentHubPusher
    {
        void Restart(string locationId);

        void Stop(string locationId);

        void SendAgentSettings(string locationId);

        void SendClientSettings(string locationId);

        void SendClientSettingsByMerchant(Guid merchantId);

        void SendUpdateRequest(string locationId, string verion, string updateUrl);

        void SendAgentLogRequest(string locationId, DateTime dateTime);

        void SenClientLogRequest(string locationId, DateTime dateTime);
    }
}
