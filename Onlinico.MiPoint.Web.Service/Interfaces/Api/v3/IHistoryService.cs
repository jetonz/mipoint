﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v3;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v3
{
    public interface IHistoryService : IBaseService
    {
        Task<ResultModel> AddAsync(HistoryModel model);
    }
}
