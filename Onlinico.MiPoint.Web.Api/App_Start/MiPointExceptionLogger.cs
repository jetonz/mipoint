﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Mvc;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using WebGrease;

namespace Onlinico.MiPoint.Web.Api.App_Start
{
    /// <summary>
    /// Exception Logger Services
    /// </summary>
    public class MiPointExceptionLogger : ExceptionLogger
    {
        /// <summary>
        /// Exception loging
        /// </summary>
        /// <param name="context">Exception context</param>
        public override void Log(ExceptionLoggerContext context)
        {
            var logger = DependencyResolver.Current.GetService<Logger.Interfaces.ILogger>();
            var message = StringHelper.ErrorBuilder(context.Request.RequestUri?.ToString() ?? "None");
            logger.WriteExceptionToLog(context.Exception, message);
        }
    }
}