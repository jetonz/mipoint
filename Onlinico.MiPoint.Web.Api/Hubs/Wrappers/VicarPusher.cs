﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Onlinico.MiPoint.Web.Api.Hubs.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;

namespace Onlinico.MiPoint.Web.Api.Hubs.Wrappers
{
    public class VicarPusher : IVicarPusher
    {
        private readonly IHubContext _agentServiceHubContext;
        private readonly ILocationService _locationService;

        public VicarPusher(ILocationService locationService)
        {
            _agentServiceHubContext = GlobalHost.ConnectionManager.GetHubContext<VicarHub>();
            _locationService = locationService;
        }

        public void Restart(string locationId)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentServiceHubContext.Clients.User(result.ObjectId).restart();
        }

        public void Start(string locationId)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentServiceHubContext.Clients.User(result.ObjectId).start();
        }

        public void Stop(string locationId)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentServiceHubContext.Clients.User(result.ObjectId).stop();
        }

        public void SendUpdateRequest(string locationId, string verion, string updateUrl)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentServiceHubContext.Clients.User(result.ObjectId).update(verion, updateUrl);
        }

        public void SendAgentLogRequest(string locationId, DateTime dateTime)
        {
            var result = _locationService.GetFingerprint(locationId);
            if (result.Succeed && !string.IsNullOrEmpty(result.ObjectId))
                _agentServiceHubContext.Clients.User(result.ObjectId).getAgentLog(dateTime);
        }
    }
}