﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v3
{
    public class ReceiptItemModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public long Quantity { get; set; }

        public long Price { get; set; }
    }
}
