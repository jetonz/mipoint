﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Logger.Interfaces
{
    public interface ILogger
    {
        void WriteTraceToLog(string message);

        void WriteMessageToLog(string message);

        void WriteExceptionToLog(Exception ex, string message);

        void WriteError(string error);
    }
}
