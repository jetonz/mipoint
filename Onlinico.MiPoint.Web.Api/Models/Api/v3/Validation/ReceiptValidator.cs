﻿namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Validation
{
    public static class ReceiptValidator
    {
        public static bool IsValid(this ReceiptModel receipt)
        {
            var type = receipt?.ReceiptType?.ToLower();

            return !string.IsNullOrEmpty(type) 
                && (type == "email" || type == "sms" || type == "storage")
                && !string.IsNullOrEmpty(receipt.Destination);
        }
    }
}