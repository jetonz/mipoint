﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Onlinico.MiPoint.Web.Api.Models.Api.v2;
using Onlinico.MiPoint.Web.Api.Models.Api.v2.Mapper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v2;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v2
{
    /// <summary>
    /// Application settings controller
    /// </summary>
    [Attributes.Authorize, RoutePrefix("api/v2/locations")]
    public class LocationController : BaseController
    {
        private readonly ILocationService _locationService;

        private readonly IConfiguration _configuration;

        /// <summary>
        /// Location controller constructor
        /// </summary>
        public LocationController(Logger.Interfaces.ILogger logger, ILocationService locationService, IConfiguration configuration) : base(logger)
        {
            _locationService = locationService;
            _configuration = configuration;
        }

        /// <summary>
        /// Get location info
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current")]
        public async Task<LocationModel> Get()
        {
            var result = await _locationService.GetAsync(Request.Properties[_configuration.Secret].ToString());

            return result.Object?.ToApiLocationModel();
        }

        /// <summary>
        /// Get current application settings
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("current/settings")]
        public IHttpActionResult /*LocationSettingsModel*/ GetSettings()
        {
            var locationSettings = _locationService.GetSettings(Request.Properties[_configuration.Secret].ToString());

            if (locationSettings != null)
            {
                return Ok(locationSettings.ToApiLocationSettingsModel());
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get employee by employee login
        /// </summary>
        /// <param name="login">employee login</param>
        /// <returns></returns>
        [HttpGet, Route("current/employees/{login}")]
        public async Task<IHttpActionResult> GetEmployee(string login)
        {
            var locationId = Request.Properties[_configuration.Secret].ToString();

            var result = await _locationService.GetEmployeeAsync(locationId, login);

            return result.Succeed ? Ok(result.Object?.ToEmployeeModel()) : (IHttpActionResult)BadRequest(result.Error);
        }
    }
}
