﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class LogModel
    {
        public Guid Id { get; set; }

        public string LocationId { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string Fingerprint { get; set; }

        public string Serial { get; set; }

        public string Content { get; set; }

        public DateTime Created { get; set; }

        public string AppVersion { get; set; }
    }
}