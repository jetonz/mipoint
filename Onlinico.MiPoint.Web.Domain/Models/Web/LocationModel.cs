﻿using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class LocationModel
    {
        public string Id { get; set; }
        
        public string Identifier { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string AddressFull { get; set; }

        public string Owner { get; set; }

        public string PosType { get; set; }

        public bool IsDev { get; set; }

        public bool IsConfigured { get; set; }

        public string AgentFingerprint { get; set; }

        public bool IsAgentActive { get; set; }

        public bool IsVicarActive { get; set; }

        public bool IsOffline { get; set; }

        public List<EmployeeModel> Employees { get; set; } 
    }
}
