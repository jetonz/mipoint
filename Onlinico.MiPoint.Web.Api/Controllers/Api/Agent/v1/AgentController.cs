﻿//using System.Net;
//using System.Threading.Tasks;
//using System.Web.Http;
//using Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1;
//using Onlinico.MiPoint.Web.Api.Models.Api.Agent.v1.Mapper;
//using Onlinico.MiPoint.Web.Logger.Interfaces;
//using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;
//using Swashbuckle.Swagger.Annotations;

//namespace Onlinico.MiPoint.Web.Api.Controllers.Api.Agent.v1
//{
//    [RoutePrefix("api/agent/v1")]
//    public class AgentController : BaseApiController
//    {
//        private readonly IAgentService _connectorService;
//        private readonly ILocationService _locationService;
//        private new const string Unauthorized = "Unauthorized";

//        /// <summary>
//        /// Connector control constructor
//        /// </summary>
//        /// <param name="logger">Inject logger</param>
//        /// <param name="configuration">Inject configuration</param>
//        /// <param name="locationService">Inject location service</param>
//        /// <param name="connectorService">Inject agent service</param>
//        public AgentController(ILogger logger, Core.Interfaces.IConfiguration configuration,
//            ILocationService locationService, IAgentService connectorService) : base(logger, configuration)
//        {
//            _connectorService = connectorService;
//            _locationService = locationService;
//        }

//        /// <summary>
//        /// Verify Credentials
//        /// </summary>
//        [HttpPost]
//        [Route("verifycredentials")]
//        [SwaggerResponse(HttpStatusCode.OK, "Credentials have been verified")]
//        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
//        [SwaggerResponse(HttpStatusCode.Unauthorized)]
//        [SwaggerResponse(HttpStatusCode.Conflict, "Fingerprint conflict")]
//        public async Task<IHttpActionResult> VerifyCredentials(VerifyModel model)
//        {
//            if (ModelState.IsValid)
//            {
//                var result = await _locationService.VerifyAsync(model.ToDomainVerifyModel());
//                if (result.Succeed)
//                {
//                    if (string.IsNullOrEmpty(result.Error))
//                    {
//                        return Ok();
//                    }
//                    else
//                    {
//                        throw CreateApiException(result.Error, HttpStatusCode.Conflict);
//                    }
//                }
//                else
//                {
//                    throw CreateApiException(result.Error,
//                        result.Error == Unauthorized ? HttpStatusCode.Unauthorized : HttpStatusCode.BadRequest);
//                }
//            }
//            else
//            {
//                throw CreateApiException(ModelState);
//            }
//        }

//        /// <summary>
//        /// Register connector
//        /// </summary>
//        /// <response code="200">Connector was successfully registered</response>
//        /// <response code="400">Bad Request </response>
//        /// <response code="401">Unauthorized</response>
//        /// <response code="409">Conflict, fingerprint in already use</response>
//        [HttpPost]
//        [Route("register")]
//        [SwaggerResponse(HttpStatusCode.OK, "Agent was successfully registered")]
//        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ApiExceptionModel))]
//        [SwaggerResponse(HttpStatusCode.Unauthorized)]
//        [SwaggerResponse(HttpStatusCode.Conflict, "Fingerprint conflict")]
//        public async Task<IHttpActionResult> RegisterAsync(RegisterModel model)
//        {
//            if (ModelState.IsValid)
//            {
//                var result = await _locationService.RegisterAsync(model.ToDomainResultModel());
//                if (result.Succeed)
//                {
//                    if (string.IsNullOrEmpty(result.Error))
//                    {
//                        return Ok();
//                    }
//                    else
//                    {
//                        throw CreateApiException(result.Error, HttpStatusCode.Conflict);
//                    }
//                }
//                else
//                {
//                    throw CreateApiException(result.Error,
//                        result.Error == Unauthorized ? HttpStatusCode.Unauthorized : HttpStatusCode.BadRequest);
//                }
//            }
//            else
//            {
//                throw CreateApiException(ModelState);
//            }

//        }
//    }
//}
