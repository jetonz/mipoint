﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Location = Onlinico.MiPoint.Web.Service.Models.Omnivore.Locations.Location;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v1
{
    public static class LocationMapper
    {
        public static List<LocationModel> ToDomainLocations(this List<Location> omnivoreLocations)
        {
            return omnivoreLocations.Select(l => new LocationModel
            {
                Identifier = l.id,
                Name = l.name,
                Owner = l.owner,
                IsDev = l.development ?? false,
                PosType = l.pos_type
            }).ToList();
        }

        public static List<LocationModel> ToDomainLocations(this ICollection<Data.DataModel.Location> locations)
        {
            return locations.Select(l => new LocationModel
            {
                Id = l.Id,
                Identifier = l.Identifier,
                Name = l.Name,
                Owner = l.Owner,
                IsDev = l.IsDev,
                PosType = l.PosType
            }).ToList();
        }

        public static LocationModel ToDomainLocation(this Data.DataModel.Location location)
        {
            return new LocationModel
            {
                Id = location.Id,
                Identifier = location.Identifier,
                Name = location.Name,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType
            };
        }

        public static Data.DataModel.Location ToNewLocationEntity(this LocationModel location)
        {
            return new Data.DataModel.Location
            {
                Identifier = location.Identifier,
                Name = location.Name,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType,
            };
        }

        public static Data.DataModel.Location ToNewLocationEntity(this LocationModel location, string id)
        {
            return new Data.DataModel.Location
            {
                Id = id,
                Identifier = location.Identifier,
                Name = location.Name,
                Owner = location.Owner,
                IsDev = location.IsDev,
                PosType = location.PosType,
                Settings = new LocationSettings
                {
                    Id = Guid.NewGuid(),
                    //TenderType = 
                    //TenderType = TenderTypeEnum.Cash,
                    TaxRate = 0,
                    Tips = "15, 18, 20, 30"
                }
            };
        }

        public static void UpdateLocationEntity(this Data.DataModel.Location location, LocationModel domainLocation)
        {
            location.Owner = domainLocation.Owner;
            location.Name = domainLocation.Name;
            location.IsDev = domainLocation.IsDev;
            location.PosType = domainLocation.PosType;
        }
    }
}
