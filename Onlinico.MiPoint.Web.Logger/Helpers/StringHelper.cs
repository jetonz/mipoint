﻿namespace Onlinico.MiPoint.Web.Logger.Helpers
{
    public class StringHelper
    {

        public static string LogMessageBuilder(string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "")
        {

            return $"Source file path: {sourceFilePath}. Member name: {memberName}. Message: {message}.";
        }

        public static string ErrorBuilder(string error, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "")
        {

            return $"Source file path: {sourceFilePath}. Member name: {memberName}. Error: {error}.";
        }

        public static string ErrorBuilder(string nameSpace, string className, string method, string error)
        {
            return $"Namespace: {nameSpace}. Class: {className}. Method: {method}. Error: {error}.";
        }

        public static string RequestErrorBuilder(string url, string method, string error)
        {
            return $"Url: {url}. Method: {method}. Error: {error}.";
        }
    }
}
