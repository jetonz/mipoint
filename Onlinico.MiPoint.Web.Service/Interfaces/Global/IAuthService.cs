﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Global;
using Onlinico.MiPoint.Web.Domain.Models.Global.Auth;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Global
{
    public interface IAuthService
    {
        ResultModel<IdentityModel> GetIdentity(string locationId);

        ResultModel<bool> VerifyFingerprint(string locationId, string fingerprint);
    }
}
