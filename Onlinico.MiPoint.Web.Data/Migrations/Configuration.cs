using System.Collections.Generic;
using System.Data.Entity.Infrastructure.DependencyResolution;
using System.Runtime.CompilerServices;
using Microsoft.AspNet.Identity.EntityFramework;
using Onlinico.MiPoint.Web.Data.DataModel;

namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Onlinico.MiPoint.Web.Data.DataModel.MiPointDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Onlinico.MiPoint.Web.Data.DataModel.MiPointDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //Add roles and user admin
            if (!context.Users.Any())
            {
                var roles = new List<IdentityRole>
                {
                    new IdentityRole {Id = Guid.NewGuid().ToString(), Name = "Admin"},
                    new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Merchant Manager" },
                    new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Location Manager" }
                };

                roles.ForEach(r => context.Roles.Add(r));

                var superAdmin = new User
                {
                    Id = Guid.NewGuid().ToString(),
                    Email = "admin",
                    FullName = "Super Admin",
                    EmailConfirmed = false,
                    PasswordHash = "AKGpUg094PrQJChru0AZIb3bb0bIu4VpVggR3WNiXa8fPQ+3mZzrd8/R1phdAy/0bA==",
                    SecurityStamp = "0945223c-1526-4c5a-b8b0-1cc2acb9a45e",
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEndDateUtc = null,
                    LockoutEnabled = false,
                    AccessFailedCount = 0,
                    UserName = "admin"
                };

                context.Users.Add(superAdmin);

                roles.ForEach(r => superAdmin.Roles.Add(new IdentityUserRole { UserId = superAdmin.Id, RoleId = r.Id }));

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    //TODO
                    var test = ex.Message;
                }
            }

            //Add cards
            if (!context.Cards.Any())
            {
                var cards = new List<Card>
                {
                    new Card {Id = Guid.NewGuid(), Name = "VISA", Type = "VISA"},
                    new Card {Id = Guid.NewGuid(), Name = "Master Card", Type = "MasterCard"},
                    new Card {Id = Guid.NewGuid(), Name = "AMEX", Type = "AMEX"},
                    new Card {Id = Guid.NewGuid(), Name = "Discover", Type = "Discover"},
                    new Card {Id = Guid.NewGuid(), Name = "Other", Type = "Other"},
                };

                context.Cards.AddRange(cards);

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    //TODO
                    var test = ex.Message;
                }
            }

            var locations = context.Locations.Where(l => l.ClientCounterModeSettings == null && l.ClientTableModeSettings == null).ToList();
            if (locations.Any())
            {
                locations.ForEach(l =>
                {
                    l.ClientCounterModeSettings = DataModel.Constructor.EntityConstructor.CreateClientCounterModeSettings();

                    l.ClientTableModeSettings = DataModel.Constructor.EntityConstructor.CreateClientTableModeSettings();
                });

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    //TODO
                    var test = ex.Message;
                }
            }

            ////Update location settings
            //var settings = context.LocationSettings.Where(s => !s.TenderTypes.Any()).ToList();
            //if (settings.Any())
            //{
            //    var allCards = context.Cards.ToList();
            //    settings.ForEach(s =>
            //    {
            //        allCards.ForEach(c => s.TenderTypes.Add(new TenderType { Id = Guid.NewGuid(), Card = c }));
            //    });
            //    try
            //    {
            //        context.SaveChanges();
            //    }
            //    catch (Exception ex)
            //    {
            //        //TODO
            //        var test = ex.Message;
            //    }
            //}

            //context.Dispose();
        }
    }
}
