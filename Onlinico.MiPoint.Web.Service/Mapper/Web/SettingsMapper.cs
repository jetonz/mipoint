﻿using System;
using System.Linq;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class SettingsMapper
    {
        public static Domain.Models.Web.LocationSettingsModel ToDomainSettings(this Data.DataModel.LocationSettings settings)
        {
            return new Domain.Models.Web.LocationSettingsModel
            {
                Id = settings.Id,
                //TaxRate = settings.TaxRate,
                //Tips = settings.Tips.Split(',').Select(Int32.Parse).ToArray(),
                TenderTypes = settings.TenderTypes.ToDomainTenderTypes(),
                SyncTime = settings.SyncTime,
                SyncPeriod = settings.SyncPeriod,
                SessionTimeout = settings.SessionTimeout,
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }

        public static Data.DataModel.LocationSettings ToEntitySettings(this Domain.Models.Web.LocationSettingsModel settings)
        {
            return new Data.DataModel.LocationSettings
            {
                Id = settings.Id,
                //TaxRate = settings.TaxRate,
                //Tips = string.Join(",", settings.Tips),
                TenderTypes = settings.TenderTypes.ToEntityTenderTypes(),
                SyncTime = settings.SyncTime,
                SyncPeriod = settings.SyncPeriod,
                AfterPaymentLogOut = settings.AfterPaymentLogOut,
                SessionTimeout = settings.SessionTimeout,
                OpenOrderLimit = settings.OpenOrderLimit,
                ClosedOrderLimit = settings.ClosedOrderLimit,
            };
        }

        public static void UpdateSettings(this Data.DataModel.LocationSettings entity, Domain.Models.Web.LocationSettingsModel settings)
        {
            //entity.TaxRate = settings.TaxRate;
            //entity.Tips = string.Join(",", settings.Tips);
            entity.TenderTypes.ToList().ForEach(e=>e.UpdateTenderTypeEntity(settings.TenderTypes.First(t=>t.Id == e.Id)));
            entity.SyncTime = settings.SyncTime;
            entity.SyncPeriod = settings.SyncPeriod;
            entity.AfterPaymentLogOut = settings.AfterPaymentLogOut;
            entity.SessionTimeout = settings.SessionTimeout;
            entity.OpenOrderLimit = settings.OpenOrderLimit;
            entity.ClosedOrderLimit = settings.ClosedOrderLimit;
        }
    }
}
