﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Data.DataModel.Enums;
using Onlinico.MiPoint.Web.Service.Mapper.Global.MappingScheme;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class ModelSchemeMapper
    {
        public static Domain.Models.Web.MappingSchemeModel ToDomainMappingSchemeModel(
            this List<Data.DataModel.MappingScheme> models, string locationId)
        {
            var employee = models.FirstOrDefault(m => m.Type == MappingSchemeTypes.Employee);

            var employeeScheme = employee?.Scheme.ToMappingScheme();

            var table = models.FirstOrDefault(m => m.Type == MappingSchemeTypes.Table);

            var tableScheme = table?.Scheme.ToMappingScheme();

            return new Domain.Models.Web.MappingSchemeModel
            {
                LocationId = locationId,
                EmployeeScheme = employeeScheme,
                TableScheme = tableScheme,
            };
        }

        public static List<Data.DataModel.MappingScheme> ToNewMappingSchemes(
            this Domain.Models.Web.MappingSchemeModel model)
        {
            return new List<MappingScheme>
            {
                new MappingScheme {Id = Guid.NewGuid(), Scheme = model.EmployeeScheme.ToMappingScheme(), Type = MappingSchemeTypes.Employee},
                new MappingScheme {Id = Guid.NewGuid(), Scheme = model.TableScheme.ToMappingScheme(), Type = MappingSchemeTypes.Table}
            };
        }
    }
}
