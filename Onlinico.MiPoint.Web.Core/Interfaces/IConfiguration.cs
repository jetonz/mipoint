﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Core.Interfaces
{
    public interface IConfiguration
    {
        string OmnivoreProdApiKey { get; }

        string OmnivoreDevApiKey { get; }

        string BaseOmnivoreApiUrl { get; }

        string HeaderApiKey { get; }

        string Secret { get; }


        string AdminRole { get; }

        string MerchantManagerRole { get; }

        string LocationManagerRole { get; }


        //Twilio

        string TwilioAccountSid { get; }

        string TwilioAuthToken { get; }

        string TwilioPhoneNumber { get; }

        //Sync config

        int SyncTriggerIntervalInMinutes { get; }

        int PaymentResolverTriggerIntervalInMinutes { get; }

        //Data retention period
        int DataRotationJobStartTimeInUtcHour { get; }

        int ActivityRetentionPeriodInDays { get; }

        int ActivityContentRetentionPeriodInDays { get; }

        int SessionLogRetentionPeriodInDays { get; }

        int SessionLogContentRetentionPeriodInDays { get; }
    }
}
