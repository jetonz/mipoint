﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using Autofac;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace Onlinico.MiPoint.Web.Api.Providers
{
    public class SignalrUserIdProvider : IUserIdProvider
    {
        public SignalrUserIdProvider(Autofac.IContainer container)
        {
        }

        public string GetUserId(IRequest request)
        {
            var clientId = Guid.NewGuid().ToString();

            if (request.User.Identity.IsAuthenticated)
            {
                clientId = request.User.Identity.GetUserId();
            }
            else
            {
                if (!string.IsNullOrEmpty(request.Headers.Get(Core.Constants.Constants.Fingerprint)))
                {
                    var fingerprint = request.Headers.GetValues(Core.Constants.Constants.Fingerprint).FirstOrDefault();
                    if (!string.IsNullOrEmpty(fingerprint))
                    {
                        clientId = fingerprint;
                    }
                }
            }

            return clientId;
        }
    }
}