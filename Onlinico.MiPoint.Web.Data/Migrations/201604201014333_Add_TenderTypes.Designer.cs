// <auto-generated />
namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_TenderTypes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_TenderTypes));
        
        string IMigrationMetadata.Id
        {
            get { return "201604201014333_Add_TenderTypes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
