﻿using System;

namespace Onlinico.MiPoint.Web.Domain.Models.Global.Omnivore
{
    public class TenderTypeModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public CardModel Card { get; set; }
    }
}
