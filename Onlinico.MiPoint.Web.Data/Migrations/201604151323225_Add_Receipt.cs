namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Receipt : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TransactionId = c.String(),
                        Amount = c.Long(nullable: false),
                        CreatedTime = c.String(),
                        CardEntryType = c.String(),
                        CardType = c.String(),
                        Last4 = c.String(),
                        ReferenceId = c.String(),
                        AuthCode = c.String(),
                        ApplicationId = c.String(),
                        Receipt_Id = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Receipts", t => t.Receipt_Id, cascadeDelete: true)
                .Index(t => t.Receipt_Id);
            
            CreateTable(
                "dbo.Receipts",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 10),
                        Created = c.DateTime(nullable: false),
                        ReceiptType = c.String(),
                        Destination = c.String(),
                        LocationName = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Phone = c.String(),
                        Cashier = c.String(),
                        ReceiptDate = c.String(),
                        TipSum = c.Long(nullable: false),
                        TotalSum = c.Long(nullable: false),
                        AmountToPay = c.Long(nullable: false),
                        TaxSum = c.Single(nullable: false),
                        TicketNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReceiptItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Quantity = c.Long(nullable: false),
                        Price = c.Long(nullable: false),
                        Receipt_Id = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Receipts", t => t.Receipt_Id, cascadeDelete: true)
                .Index(t => t.Receipt_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payments", "Receipt_Id", "dbo.Receipts");
            DropForeignKey("dbo.ReceiptItems", "Receipt_Id", "dbo.Receipts");
            DropIndex("dbo.ReceiptItems", new[] { "Receipt_Id" });
            DropIndex("dbo.Payments", new[] { "Receipt_Id" });
            DropTable("dbo.ReceiptItems");
            DropTable("dbo.Receipts");
            DropTable("dbo.Payments");
        }
    }
}
