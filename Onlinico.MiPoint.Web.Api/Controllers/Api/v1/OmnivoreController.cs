﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;
using WebGrease.Css.Extensions;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v1
{
    /// <summary>
    /// Omnivore proxy controller
    /// </summary>
    [Attributes.Authorize, System.Web.Http.RoutePrefix("api/proxy/omnivore")]
    public class OmnivoreController : BaseController
    {
        //private readonly IHistoryService _historyService;
        //private readonly Core.Interfaces.IConfiguration _configuration;
        //private HistoryModel _history;

        /// <summary>
        /// Omnivore controller constructor
        /// </summary>
        /// <param name="logger">Inject logger</param>
        /// <param name="historyService">Inject history service</param>
        /// <param name="configuration">Inject configuration</param>
        public OmnivoreController(Logger.Interfaces.ILogger logger/*, IHistoryService historyService, Core.Interfaces.IConfiguration configuration*/) : base(logger)
        {
            //_historyService = historyService;
            //_configuration = configuration;
        }

        /// <summary>
        /// Initialize Omnivore controller
        /// </summary>
        /// <param name="controllerContext">Current controller context</param>
        //protected override void Initialize(HttpControllerContext controllerContext)
        //{
        //    base.Initialize(controllerContext);
        //    _history = new HistoryModel { Id = Guid.NewGuid(), Method = Request.Method.Method, Start = DateTime.UtcNow };
        //}

        /// <summary>
        /// Proxy GET
        /// </summary>
        /// <param name="url">Omnivore web api url</param>
        /// <returns>Http response message</returns>
        [System.Web.Http.Route("{*url}")]
        public async Task<HttpResponseMessage> Get([FromUri]string url)
        {
            HttpResponseMessage result = null;
            using (var httpClient = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                if (string.IsNullOrEmpty(Request.RequestUri.Query))
                {
                    var parts = url.Split('/');
                    if (parts.Length == 4 && parts[0] == "0.1" && parts[1].ToLower() == "locations" && parts[3].ToLower() == "tickets")
                    {
                        url += "?where=eq(open,true)";
                    }
                }
                url += Request.RequestUri.Query;
                var request = new HttpRequestMessage(HttpMethod.Get, BaseUri + url);
                Request.Headers.ForEach(h => request.Headers.TryAddWithoutValidation(h.Key, h.Value));
                try
                {
                    result = await httpClient.SendAsync(request);

                    //_history.Content = await result.Content.ReadAsStringAsync();
                }
                catch (Exception ex)
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    Logger.WriteMessageToLog(StringHelper.ErrorBuilder("Onlinico.MiPoint.Web.Api.Controllers.Api", "OmnivoreController", "Get", ex.Message));
                }
                //finally
                //{
                //    await AddHistoryAsync(url, result?.StatusCode).ConfigureAwait(false);
                //}
            }
            return result;
        }

        /// <summary>
        /// Proxy POST
        /// </summary>
        /// <param name="url">Omnivore web api url</param>
        /// <returns>Http response message</returns>
        [System.Web.Http.Route("{*url}")]
        public async Task<HttpResponseMessage> Post([FromUri]string url)
        {
            HttpResponseMessage result = null;
            using (var httpClient = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                var request = new HttpRequestMessage(HttpMethod.Post, BaseUri + url + Request.RequestUri.Query);
                Request.Headers.ForEach(h => request.Headers.TryAddWithoutValidation(h.Key, h.Value));
                request.Content = Request.Content;

                try
                {
                    //_history.Content = await request.Content.ReadAsStringAsync();

                    result = await httpClient.SendAsync(request);
                }
                catch (Exception ex)
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    Logger.WriteMessageToLog(StringHelper.ErrorBuilder("Onlinico.MiPoint.Web.Api.Controllers.Api", "OmnivoreController", "Post", ex.Message));
                }
                //finally
                //{
                //    await AddHistoryAsync(url, result?.StatusCode).ConfigureAwait(false);
                //}
            }
            return result;
        }

        /// <summary>
        /// Proxy DELETE
        /// </summary>
        /// <param name="url">Omnivore web api url</param>
        /// <returns>Http response message</returns>
        [System.Web.Http.Route("{*url}")]
        public async Task<HttpResponseMessage> Delete([FromUri]string url)
        {
            HttpResponseMessage result = null;
            using (var httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Delete, BaseUri + url + Request.RequestUri.Query);
                Request.Headers.ForEach(h => request.Headers.TryAddWithoutValidation(h.Key, h.Value));
                try
                {
                    result = await httpClient.SendAsync(request);
                }
                catch (Exception ex)
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    Logger.WriteMessageToLog(StringHelper.ErrorBuilder("Onlinico.MiPoint.Web.Api.Controllers.Api", "OmnivoreController", "Delete", ex.Message));
                }
                //finally
                //{
                //    await AddHistoryAsync(url, result?.StatusCode).ConfigureAwait(false);
                //}
            }
            return result;
        }

        //private async Task AddHistoryAsync(string url, HttpStatusCode? statusCode)
        //{
        //    var locationSecret = Request.Properties[_configuration.Secret].ToString();

        //    _history.Url = url;
        //    _history.LocationSecret = locationSecret;
        //    _history.StatusCode = statusCode != null ? (int)statusCode : 400;
        //    _history.End = DateTime.UtcNow;
        //    await _historyService.AddAsync(_history).ConfigureAwait(false);
        //}

    }
}
