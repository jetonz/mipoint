﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Extensions
{
    public static class DateTimeExtension
    {
        public static long ToUtc(this System.DateTime dateTime)
        {
            return (long)dateTime.Subtract(new System.DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        public static DateTime ToDateTime(this long dateTimeInSeconds)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(dateTimeInSeconds);
        }
    }
}