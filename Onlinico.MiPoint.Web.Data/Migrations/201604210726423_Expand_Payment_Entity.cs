namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Expand_Payment_Entity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payments", "TransactionType", c => c.String(nullable: false, defaultValue: "SALE"));
            AddColumn("dbo.Payments", "Cvm", c => c.String(nullable: false, defaultValue: "SIGNATURE VERIFIED"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payments", "Cvm");
            DropColumn("dbo.Payments", "TransactionType");
        }
    }
}
