﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.v1;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;
using Onlinico.MiPoint.Web.Service.Mapper.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Services.Api.v1
{
    public class MerchantService : BaseService, IMerchantService
    {
        private readonly IMemoryCacher _memoryCacher;

        public MerchantService(ILogger logger, IMemoryCacher memoryCacher) : base(logger)
        {
            _memoryCacher = memoryCacher;
        }

        public List<MerchantModel> Get()
        {
            List<MerchantModel> result = null;
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var apps = context.Merchants.OrderByDescending(a => a.Created).ToList();
                    //result = apps?.ToDomainMerchantModels();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public MerchantModel Get(Guid merchantId)
        {
            MerchantModel result = null;
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = context.Merchants.FirstOrDefault(m => m.Id == merchantId);
                    //result = merchant?.ToDomainMerchantModel();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public List<MerchantItemModel> GetMerchantItemsForUser(string userId)
        {
            List<MerchantItemModel> result = null;
            try
            {
                using (var context = new MiPointDbContext())
                {
                    result = context.Merchants.Select(m => new MerchantItemModel
                    {
                        Id = m.Id,
                        Name = m.Name,
                        Created = m.Created,
                        Selected = m.Users.Any(u => u.Id == userId)
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public ResultModel RelateMerchantsWithUser(List<Guid> merchantIds, string userId)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.FirstOrDefault(u => u.Id == userId);
                    if (user != null)
                    {
                        var merchants = context.Merchants.Where(m => merchantIds.Contains(m.Id) && m.Users.All(u => u.Id != userId)).ToList();
                        if (merchants.Any())
                        {

                            merchants.ForEach(m => user.Merchants.Add(m));

                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid user.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
                result = new ResultModel(false, ex.Message);
            }
            return result;
        }

        public List<MerchantModel> GetForUser(string userId)
        {
            List<MerchantModel> result = null;
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.Find(userId);
                    //result = user?.Merchants.ToDomainMerchantModels();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public ResultModel<MerchantModel> Update(MerchantModel model)
        {
            var result = new ResultModel<MerchantModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = context.Merchants.FirstOrDefault(m => m.Id == model.Id);
                    if (merchant != null)
                    {
                        merchant.Name = model.Name;
                        merchant.Created = DateTime.UtcNow;
                        context.SaveChanges();
                    }
                    //result.Object = merchant.ToDomainMerchantModel();
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel<MerchantModel>(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public async Task<ResultModel> UpdateMerchantLocationsAsync(Guid merchantId, List<LocationModel> locations, string dictionaryPath)
        {
            ResultModel result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = await context.Merchants.FirstOrDefaultAsync(m => m.Id == merchantId);
                    if (merchant != null)
                    {
                        var humainId = new HumanId.HumanId(dictionaryPath);
                        locations.ForEach(l =>
                        {
                            var merchantLocation = merchant.Locations.FirstOrDefault(v => v.Identifier == l.Identifier);
                            if (merchantLocation != null)
                            {
                                merchantLocation.UpdateLocationEntity(l);
                            }
                            else
                            {
                                var freeId = string.Empty;
                                var exit = false;
                                while (!exit)
                                {
                                    freeId = humainId.GetId().ToString();
                                    if (!context.Locations.Any(a => a.Id == freeId))
                                    {
                                        exit = true;
                                    }
                                }
                                merchant.Locations.Add(l.ToNewLocationEntity(freeId));
                            }
                        });

                        //Remove not avalible locations
                        var locationIdentifiers = locations.Select(l => l.Identifier);
                        var notAvailableLocations = merchant.Locations.Where(l => !locationIdentifiers.Contains(l.Identifier)).ToList();
                        notAvailableLocations.ForEach(l => merchant.Locations.Remove(l));

                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        result = new ResultModel(false, "Invalid merchant id.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(ex.Message);
                result = null;
            }
            return result;
        }

        public async Task<ResultModel<MerchantModel>> AddAsync(MerchantModel model, string userId, string dictionaryPath)
        {
            var result = new ResultModel<MerchantModel>(true);
            try
            {
                using (var context = new MiPointDbContext())
                {

                    //if (!await context.Merchants.AnyAsync(m => m.OmnivoreApiKey == model.OmnivoreApiKey))
                    //{
                    //    var merchant = model.ToNewMerchantEntity();
                    //    context.Merchants.Add(merchant);

                    //    if (!string.IsNullOrEmpty(userId))
                    //    {
                    //        var user = await context.Users.FirstOrDefaultAsync(u => u.Id == userId);
                    //        user?.Merchants.Add(merchant);
                    //    }

                    //    var humainId = new HumanId.HumanId(dictionaryPath);
                    //    model.Locations.ForEach(l =>
                    //    {
                    //        var freeId = string.Empty;
                    //        var exit = false;
                    //        while (!exit)
                    //        {
                    //            freeId = humainId.GetId().ToString();
                    //            if (!context.Locations.Any(a => a.Id == freeId))
                    //            {
                    //                exit = true;
                    //            }
                    //        }

                    //        merchant.Locations.Add(l.ToNewLocationEntity(freeId));
                    //    });
                    //    await context.SaveChangesAsync();
                    //    result.Object = merchant.ToDomainMerchantModel();
                    //}
                    //else
                    //{
                    //    result = new ResultModel<MerchantModel>(false, "Merchant with this 'omnivore api key' already exist.");
                    //}
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel<MerchantModel>(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public ResultModel RemoveMerchantsFromUser(string userId)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var user = context.Users.FirstOrDefault(a => a.Id == userId);
                    if (user != null)
                    {
                        var userMerchants = user.Merchants.ToList();
                        userMerchants.ForEach(m => user.Merchants.Remove(m));
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }

        public async Task<ResultModel> RemoveAsync(Guid id)
        {
            var result = new ResultModel(true);
            try
            {
                using (var context = new MiPointDbContext())
                {
                    var merchant = await context.Merchants.FirstOrDefaultAsync(a => a.Id == id);
                    if (merchant != null)
                    {
                        merchant.Locations.ToList().ForEach(l =>
                        {
                            _memoryCacher.Delete(l.Id);
                        });
                        var merchantUsers = merchant.Users.ToList();
                        merchantUsers.ForEach(u => merchant.Users.Remove(u));
                        context.Merchants.Remove(merchant);
                    }
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                result = new ResultModel(false, ex.Message);
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder(ex.Message));
            }
            return result;
        }
    }
}
