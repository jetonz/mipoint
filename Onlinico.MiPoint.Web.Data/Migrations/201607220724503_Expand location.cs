namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Expandlocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "AgentFingerprint", c => c.String(maxLength: 128));
            AddColumn("dbo.Locations", "LastAgentPoll", c => c.DateTime());
            AddColumn("dbo.Locations", "AgentVersion", c => c.String(maxLength: 128));
            AddColumn("dbo.Locations", "AgentAddress", c => c.String(maxLength: 128));
            AlterColumn("dbo.Locations", "Name", c => c.String(maxLength: 250));
            AlterColumn("dbo.Locations", "Phone", c => c.String(maxLength: 250));
            AlterColumn("dbo.Locations", "Address", c => c.String(maxLength: 250));
            AlterColumn("dbo.Locations", "AddressFull", c => c.String(maxLength: 250));
            AlterColumn("dbo.Locations", "Owner", c => c.String(maxLength: 250));
            AlterColumn("dbo.Locations", "PosType", c => c.String(maxLength: 250));
            CreateIndex("dbo.Locations", "AgentFingerprint");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Locations", new[] { "AgentFingerprint" });
            AlterColumn("dbo.Locations", "PosType", c => c.String());
            AlterColumn("dbo.Locations", "Owner", c => c.String());
            AlterColumn("dbo.Locations", "AddressFull", c => c.String());
            AlterColumn("dbo.Locations", "Address", c => c.String());
            AlterColumn("dbo.Locations", "Phone", c => c.String());
            AlterColumn("dbo.Locations", "Name", c => c.String());
            DropColumn("dbo.Locations", "AgentAddress");
            DropColumn("dbo.Locations", "AgentVersion");
            DropColumn("dbo.Locations", "LastAgentPoll");
            DropColumn("dbo.Locations", "AgentFingerprint");
        }
    }
}
