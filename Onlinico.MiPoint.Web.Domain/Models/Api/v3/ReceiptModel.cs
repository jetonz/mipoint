﻿using System;
using System.Collections.Generic;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v3
{
    public class ReceiptModel
    {
        public string Id { get; set; }

        public string TicketId { get; set; }

        public string LocationId { get; set; }

        public DateTime Created { get; set; }

        public string ReceiptType { get; set; }

        public string Destination { get; set; }

        public string LocationName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Phone { get; set; }

        public string Cashier { get; set; }

        public string ReceiptDate { get; set; }

        public long TipSum { get; set; }

        public long TotalSum { get; set; }

        public float TaxSum { get; set; }

        public long ServiceChargesSum { get; set; }

        public long AmountToPay { get; set; }

        public string TicketNumber { get; set; }


        public ReceiptPaymentModel Payment { get; set; }

        public List<ReceiptItemModel> Items { get; set; }
    }
}
