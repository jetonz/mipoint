﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1.Enum;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1
{
    public class ActivityModel
    {
        public string UserAgent { get; set; }

        public string DeviceId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public RequestMethodEnum Method { get; set; }

        public string Url { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int StatusCode { get; set; }

        public string RequestContent { get; set; }

        public string ResponseContent { get; set; }
    }
}
