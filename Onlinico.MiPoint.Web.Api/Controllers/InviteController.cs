﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Api.Models.Web.Mapper;
using Onlinico.MiPoint.Web.Core.Helper;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Web;

namespace Onlinico.MiPoint.Web.Api.Controllers
{
    /// <summary>
    /// Invite controller
    /// </summary>
    [Authorize]
    public class InviteController : BaseController
    {
        private readonly IInviteService _inviteService;
        private readonly IEmailService _emailService;
        private readonly IMerchantService _merchantService;

        /// <summary>
        /// Invite controller constructor
        /// </summary>
        public InviteController(IInviteService inviteService, IEmailService emailService, IMerchantService merchantService, IConfiguration configuration) : base(configuration)
        {
            _inviteService = inviteService;
            _emailService = emailService;
            _merchantService = merchantService;
        }

        /// <summary>
        /// Accept invite
        /// </summary>
        /// <param name="inviteId">Invite id</param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Accept(Guid inviteId)
        {
            var invite = _inviteService.Get(inviteId);
            if (invite.Object != null)
            {
                if (User.Identity.IsAuthenticated)
                {
                    var result = _inviteService.AcceptInvite(inviteId, User.Identity.GetUserId());
                    if (result.Succeed)
                    {
                        return RedirectToAction("Index", "Merchant");
                    }
                    else
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return RedirectToAction("Register", "Account", new { InviteId = inviteId, ReturnUrl = Url.Action("Accept", "Invite", new { inviteId }) });
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// Synchronize merchant locations
        /// </summary>
        /// <param name="model">Invite model</param>
        /// <returns></returns>
        public async Task<ActionResult> InviteUser(InviteModel model)
        {
            if (ModelState.IsValid)
            {
                var userResult = await _inviteService.GetInvitedUser(model.MerchantId, model.Email);
                if (userResult.Succeed && userResult.Object == null)
                {
                    var merchant = _merchantService.Get(model.MerchantId);
                    if (merchant != null)
                    {
                        model.Id = Guid.NewGuid();
                        var result = await _emailService.SendAsync(model.Email, EmailHelper.GenerateInviteSubject(merchant.Object?.Name), EmailHelper.GenerateInviteBody(merchant.Object?.Name, GenerateInviteLink(model.Id)));
                        if (result.Succeed)
                        {
                            //var invite = _inviteService.Find(model.MerchantId, model.Email);
                            //if (invite.Object == null)
                            //{
                                result = _inviteService.Add(model.ToDomainInviteModel());
                            //}
                            //else
                            //{
                            //    // User with this email already invited.
                            //}
                        }
                        return new HttpStatusCodeResult(result.Succeed ? HttpStatusCode.OK : HttpStatusCode.BadRequest, result.Error ?? "");
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid merchant.");
                    }
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, userResult.Error ?? "User with this email already related to merchant.");
                }

            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid email format.");
        }

        private string GenerateInviteLink(Guid inviteId)
        {
            var url = HttpContext.Request.Url;
            return $"{url?.Scheme}://{url?.Authority}/Invite/Accept?inviteId={inviteId}";
        }
    }
}