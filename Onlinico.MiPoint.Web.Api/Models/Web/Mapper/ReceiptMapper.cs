﻿using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class ReceiptMapper
    {

        public static ReceiptModel ToReceiptModel(this Domain.Models.Web.ReceiptModel receipt)
        {
            return new ReceiptModel
            {
                Id = receipt.Id,
                ReceiptType = receipt.ReceiptType,
                Destination = receipt.Destination,
                Address1 = receipt.Address1,
                Address2 = receipt.Address2,
                Cashier = receipt.Cashier,
                AmountToPaye = ConvertToMoney(receipt.AmountToPay),
                LocationName = receipt.LocationName,
                Phone = receipt.Phone,
                ReceiptDate = receipt.ReceiptDate,
                TicketNumber = receipt.TicketNumber,
                TableNumber = receipt.TableNamber,
                TipSum = ConvertToMoney(receipt.TipSum),
                TaxSum = ConvertToMoney(receipt.TaxSum),
                TotalSum = ConvertToMoney(receipt.TotalSum),
                ServiceChargesSum = ConvertToMoney(receipt.ServiceChargesSum),

                Payment = receipt.Payment.ToPaymentModel(),
                Items = receipt.Items.ToReceiptItems(),
            };
        }

        public static List<ReceiptItemModel> ToReceiptItems(this List<Domain.Models.Web.ReceiptItemModel> items)
        {
            return items.Select(i => new ReceiptItemModel
            {
                Id = i.Id,
                Name = i.Name,
                Price = ConvertToMoney(i.Price),
                Quantity = i.Quantity
            }).ToList();
        }

        private static string ConvertToMoney(long cents)
        {
            double dolars = cents/100.00;
            return $"{dolars:C}";
        }

        private static string ConvertToMoney(float cents)
        {
            double dolars = cents / 100.00;
            return $"{dolars:C}";
        }

        private static string ConvertToMoney(string cents)
        {
            double dolars;
            double.TryParse(cents, out dolars);
            dolars = dolars / 100.00;
            return $"{dolars:C}";
        }

        public static ReceiptPaymentModel ToPaymentModel(this Domain.Models.Web.ReceiptPaymentModel payment)
        {
            return new ReceiptPaymentModel
            {
                Id = payment.Id,
                TransactionId = payment.TransactionId,
                Amount = ConvertToMoney(payment.Amount),
                ApplicationId = payment.ApplicationId,
                AuthCode = payment.AuthCode,
                CardEntryType = payment.CardEntryType,
                CardType = payment.CardType,
                CreatedTime = payment.CreatedTime,
                Last4 = payment.Last4,
                ReferenceId = payment.ReferenceId,
                TransactionType = payment.TransactionType,
                Cvm = payment.Cvm,
            };
        }
    }
}