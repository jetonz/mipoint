﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Omnivore.Ticket
{
    public class Totals
    {
        public int? due { get; set; }
        public int? other_charges { get; set; }
        public int? service_charges { get; set; }
        public int? sub_total { get; set; }
        public int? tax { get; set; }
        public int? total { get; set; }
    }

    public class Discounts
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Items
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Payments
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class VoidedItems
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links
    {
        public Discounts discounts { get; set; }
        public Items items { get; set; }
        public Payments payments { get; set; }
        public Self self { get; set; }
        public VoidedItems voided_items { get; set; }
    }

    public class ClockEntries
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self2
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links2
    {
        public ClockEntries clock_entries { get; set; }
        public Self2 self { get; set; }
    }

    public class Employee : ICloneable
    {
        public string check_name { get; set; }
        public string first_name { get; set; }
        public string id { get; set; }
        public string last_name { get; set; }
        public string login { get; set; }
        public Links2 _links { get; set; }

        public object Clone()
        {
            return new Employee { id = this.id, last_name = this.last_name, login = this.login, first_name = this.first_name, check_name = this.check_name };
        }
    }

    public class Discounts2
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class MenuItem
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Modifiers
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self3
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links3
    {
        public Discounts2 discounts { get; set; }
        public MenuItem menu_item { get; set; }
        public Modifiers modifiers { get; set; }
        public Self3 self { get; set; }
    }

    public class PriceLevel
    {
        public string id { get; set; }
        public int? price { get; set; }
    }

    public class ModifierGroups
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self4
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links4
    {
        public ModifierGroups modifier_groups { get; set; }
        public Self4 self { get; set; }
    }

    public class Items2
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self5
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links5
    {
        public Items2 items { get; set; }
        public Self5 self { get; set; }
    }

    public class Embedded4
    {
    }

    public class MenuCategory
    {
        public string id { get; set; }
        public string name { get; set; }
        public Links5 _links { get; set; }
        public Embedded4 _embedded { get; set; }
    }

    public class Embedded3
    {
        public List<MenuCategory> menu_categories { get; set; }
    }

    public class MenuItem2
    {
        public string id { get; set; }
        public bool? in_stock { get; set; }
        public int? modifier_groups_count { get; set; }
        public string name { get; set; }
        public bool? open { get; set; }
        public string pos_id { get; set; }
        public int? price { get; set; }
        public List<PriceLevel> price_levels { get; set; }
        public Links4 _links { get; set; }
        public Embedded3 _embedded { get; set; }
    }

    public class MenuModifier
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self6
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links6
    {
        public MenuModifier menu_modifier { get; set; }
        public Self6 self { get; set; }
    }

    public class PriceLevel2
    {
        public string id { get; set; }
        public int? price { get; set; }
    }

    public class Self7
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links7
    {
        public Self7 self { get; set; }
    }

    public class MenuModifier2
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool? open { get; set; }
        public string pos_id { get; set; }
        public List<PriceLevel2> price_levels { get; set; }
        public int? price_per_unit { get; set; }
        public Links7 _links { get; set; }
    }

    public class Embedded5
    {
        public MenuModifier2 menu_modifier { get; set; }
    }

    public class Modifier
    {
        public object comment { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string price_level { get; set; }
        public int? price_per_unit { get; set; }
        public int? quantity { get; set; }
        public Links6 _links { get; set; }
        public Embedded5 _embedded { get; set; }
    }

    public class Embedded2
    {
        public List<object> discounts { get; set; }
        public MenuItem2 menu_item { get; set; }
        public List<Modifier> modifiers { get; set; }
    }

    public class Item
    {
        public object comment { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string price_level { get; set; }
        public int? price_per_unit { get; set; }
        public int? quantity { get; set; }
        public bool? sent { get; set; }
        public Links3 _links { get; set; }
        public Embedded2 _embedded { get; set; }
    }

    public class Self8
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links8
    {
        public Self8 self { get; set; }
    }

    public class OrderType
    {
        public bool? available { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public Links8 _links { get; set; }
    }

    public class Self9
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links9
    {
        public Self9 self { get; set; }
    }

    public class RevenueCenter
    {
        public bool? @default { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public Links9 _links { get; set; }
    }

    public class Embedded
    {
        public List<object> discounts { get; set; }
        public Employee employee { get; set; }
        public List<Item> items { get; set; }
        public OrderType order_type { get; set; }
        public List<object> payments { get; set; }
        public RevenueCenter revenue_center { get; set; }
        public Table table { get; set; }
        public List<object> voided_items { get; set; }
    }

    public class Table : ICloneable
    {
        public Links4 _links { get; set; }
        public bool? available { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public int? number { get; set; }
        public int? seats { get; set; }

        public object Clone()
        {
            return new Table { id = this.id, name = this.name, number = this.number ?? 0, available = this.available ?? false, seats = this.seats ?? 0 };
        }
    }

    public class RootObject
    {
        public bool? auto_send { get; set; }
        public object closed_at { get; set; }
        public int? guest_count { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public bool? open { get; set; }
        public int? opened_at { get; set; }
        public int? ticket_number { get; set; }
        public Totals totals { get; set; }
        public bool? @void { get; set; }
        public Links _links { get; set; }
        public Embedded _embedded { get; set; }
    }
}