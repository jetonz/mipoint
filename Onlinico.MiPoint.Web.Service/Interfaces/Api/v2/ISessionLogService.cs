﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v2
{
    public interface ISessionLogService
    {
        Task<ResultModel> AddAsync(LogModel model);

        ResultModel<List<LogModel>> Get(DateTime start, DateTime end);

        ResultModel<LogModel> Get(Guid logId);
    }
}
