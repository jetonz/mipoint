﻿using System;

namespace Onlinico.MiPoint.Web.Api.Hubs.Interfaces
{
    public interface IVicarPusher
    {
        void Restart(string locationId);

        void Start(string locationId);

        void Stop(string locationId);

        void SendUpdateRequest(string locationId, string verion, string updateUrl);

        void SendAgentLogRequest(string locationId, DateTime dateTime);
    }
}
