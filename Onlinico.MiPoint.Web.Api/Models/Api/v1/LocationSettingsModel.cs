﻿using System.Runtime.Serialization;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v1
{
    /// <summary>
    /// Location settings
    /// </summary>
    [DataContract]
    public class LocationSettingsModel
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public LocationSettingsModel()
        {
            
        }

        /// <summary>
        /// Constructor with tender type
        /// </summary>
        /// <param name="tenderTypeId">Tender type id</param>
        public LocationSettingsModel(string tenderTypeId)
        {
            TenderTypeId = tenderTypeId;
        }

        /// <summary>
        /// Tender type
        /// </summary>
        [DataMember(Name = "tenderTypeId")]
        public string TenderTypeId { get; set; }

        /// <summary>
        /// Trend type name
        /// </summary>
        [DataMember(Name = "tenderType")]
        public string TenderType { get; set; }

        /// <summary>
        /// Tips array
        /// </summary>
        [DataMember(Name = "tips")]
        public int[] Tips { get; set; }

        /// <summary>
        /// Tax rate
        /// </summary>
        [DataMember(Name = "taxRate")]
        public float TaxRate { get; set; }
    }
}