﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Onlinico.MiPoint.Web.Api
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "BillShortUrl",
               "bill/{id}",
               new { controller = "Receipt", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ReceiptShortUrl",
               "receipt/{paymentId}",
               new { controller = "Receipt", action = "Info", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Eula",
               "eula",
               new { controller = "Account", action = "LicenceAgreement", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Privacypolicy",
               "privacypolicy",
               new { controller = "Account", action = "PrivacyPolicy", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Merchant", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}
