﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class ReceiptPayment
    {
        [Key]
        public Guid Id { get; set; }

        [Index]
        [MaxLength(36)]
        public string TransactionId { get; set; }

        public long Amount { get; set; }

        public string CreatedTime { get; set; }

        public string CardEntryType { get; set; }

        public string CardType { get; set; }

        public string Last4 { get; set; }

        public string ReferenceId { get; set; }

        public string AuthCode { get; set; }

        public string ApplicationId { get; set; }

        public string TransactionType { get; set; }

        public string Cvm { get; set; }


        public virtual Receipt Receipt { get; set; }
    }
}
