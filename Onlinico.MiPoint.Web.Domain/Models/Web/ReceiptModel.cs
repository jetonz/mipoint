﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class ReceiptModel
    {
        public string Id { get; set; }

        public string TicketId { get; set; }

        public string LocationId { get; set; }

        public DateTime Created { get; set; }

        public string ReceiptType { get; set; }

        public string Destination { get; set; }

        public string LocationName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Phone { get; set; }

        public string Cashier { get; set; }

        public string ReceiptDate { get; set; }

        public long TipSum { get; set; }

        public long TotalSum { get; set; }

        public float TaxSum { get; set; }

        public long ServiceChargesSum { get; set; }

        public long AmountToPay { get; set; }

        public string TicketNumber { get; set; }

        public string TableNamber { get; set; }

        public ReceiptPaymentModel Payment { get; set; }

        public List<ReceiptItemModel> Items { get; set; }
    }

    public class ReceiptPaymentModel
    {
        public Guid Id { get; set; }

        public string TransactionId { get; set; }

        public long Amount { get; set; }

        public string CreatedTime { get; set; }

        public string CardEntryType { get; set; }

        public string CardType { get; set; }

        public string Last4 { get; set; }

        public string ReferenceId { get; set; }

        public string AuthCode { get; set; }

        public string ApplicationId { get; set; }

        public string TransactionType { get; set; }

        public string Cvm { get; set; }
    }

    public class ReceiptItemModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public long Quantity { get; set; }

        public long Price { get; set; }
    }
}
