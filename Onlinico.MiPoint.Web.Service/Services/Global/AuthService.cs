﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Core.Interfaces;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Global;
using Onlinico.MiPoint.Web.Domain.Models.Global.Auth;
using Onlinico.MiPoint.Web.Logger.Helpers;
using Onlinico.MiPoint.Web.Logger.Interfaces;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;

namespace Onlinico.MiPoint.Web.Service.Services.Global
{
    public class AuthService : BaseService, IAuthService
    {
        private readonly IMemoryCacher _memoryCacher;

        private readonly IConfiguration _configuration;

        public AuthService(ILogger logger, IMemoryCacher memoryCacher, IConfiguration configuration) : base(logger)
        {
            _memoryCacher = memoryCacher;
            _configuration = configuration;
        }

        public ResultModel<IdentityModel> GetIdentity(string locationId)
        {
            var result = new ResultModel<IdentityModel>(true);
            try
            {
                var locationIdentity = _memoryCacher.GetValue<IdentityModel>(locationId);
                if (locationIdentity != null)
                {
                    result.Object = locationIdentity;
                }
                else
                {
                    using (var context = new MiPointDbContext())
                    {
                        var location = context.Locations.FirstOrDefault(l => l.Id == locationId && !l.IsRemoved);
                        if (location != null)
                        {
                            locationIdentity = new IdentityModel(locationId, location.IsDev ? _configuration.OmnivoreDevApiKey : _configuration.OmnivoreProdApiKey, location.Identifier);
                            _memoryCacher.Add(locationId, locationIdentity, DateTimeOffset.UtcNow.AddSeconds(10));
                            result.Object = locationIdentity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public ResultModel<bool> VerifyFingerprint(string locationId, string fingerprint)
        {
            var result = new ResultModel<bool>(true) { Object = false };

            try
            {
                using (var context = new MiPointDbContext())
                {
                    var location = context.Locations.FirstOrDefault(l => l.Id == locationId && l.AgentFingerprint == fingerprint && !l.IsRemoved);
                    if (location != null)
                    {
                        result.Object = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteMessageToLog(StringHelper.ErrorBuilder($"{ex.Message}. Inner Exception: {ex.InnerException?.InnerException?.Message}"));
                result.Succeed = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}
