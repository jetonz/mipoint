namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_TenderTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TenderTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Type = c.String(),
                        Settings_Id = c.Guid(nullable: false),
                        Card_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LocationSettings", t => t.Settings_Id, cascadeDelete: true)
                .ForeignKey("dbo.Cards", t => t.Card_Id, cascadeDelete: true)
                .Index(t => t.Settings_Id)
                .Index(t => t.Card_Id);
            
            DropColumn("dbo.LocationSettings", "TenderTypeId");
            DropColumn("dbo.LocationSettings", "TenderType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LocationSettings", "TenderType", c => c.String());
            AddColumn("dbo.LocationSettings", "TenderTypeId", c => c.String());
            DropForeignKey("dbo.TenderTypes", "Card_Id", "dbo.Cards");
            DropForeignKey("dbo.TenderTypes", "Settings_Id", "dbo.LocationSettings");
            DropIndex("dbo.TenderTypes", new[] { "Card_Id" });
            DropIndex("dbo.TenderTypes", new[] { "Settings_Id" });
            DropTable("dbo.TenderTypes");
            DropTable("dbo.Cards");
        }
    }
}
