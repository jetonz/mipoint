﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Onlinico.MiPoint.Web.Domain.Models.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Interfaces.Api.Agent.v1;
using Onlinico.MiPoint.Web.Service.Interfaces.Global;

namespace Onlinico.MiPoint.Web.Api.Attributes
{
    [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeAgentHubAttribute : Microsoft.AspNet.SignalR.AuthorizeAttribute, IAuthorizeHubConnection, IAuthorizeHubMethodInvocation
    {
        private readonly IAuthService _authService;
        private readonly ILocationService _locationService;

        public AuthorizeAgentHubAttribute()
        {
            _authService = DependencyResolver.Current.GetService<IAuthService>();
            _locationService = DependencyResolver.Current.GetService<ILocationService>();
        }


        public bool AuthorizeHubConnection(HubDescriptor hubDescriptor, IRequest request)
        {
            if (!string.IsNullOrEmpty(request.Headers.Get(Core.Constants.Constants.ApiKey)) && !string.IsNullOrEmpty(request.Headers.Get(Core.Constants.Constants.Fingerprint)))
            {
                var apiKey = request.Headers.GetValues(Core.Constants.Constants.ApiKey).FirstOrDefault();
                var fingerprint = request.Headers.GetValues(Core.Constants.Constants.Fingerprint).FirstOrDefault();
                if (!string.IsNullOrEmpty(apiKey) && !string.IsNullOrEmpty(fingerprint))
                {
                    var verifyResult = _authService.VerifyFingerprint(apiKey, fingerprint);
                    if (verifyResult.Succeed)
                    {
                        if (!verifyResult.Object)
                        {
                            var registerResult = _locationService.RegisterAsync(new RegisterModel { Fingerprint = fingerprint, ApiKey = apiKey }).Result;
                            return registerResult.Succeed;
                        }

                        return verifyResult.Object;
                    }
                }
            }
            return false;
        }

        public bool AuthorizeHubMethodInvocation(IHubIncomingInvokerContext hubIncomingInvokerContext, bool appliesToMethod)
        {
            return true;
        }
    }
}