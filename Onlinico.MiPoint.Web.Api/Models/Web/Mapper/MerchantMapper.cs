﻿using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Service.Mapper.Global.Omnivore;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class MerchantMapper
    {
        public static Domain.Models.Web.MerchantModel ToDomainMerchantModel(this MerchantModel model)
        {
            return new Domain.Models.Web.MerchantModel
            {
                Id = model.Id,
                Name = model.Name,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                Country = model.Country,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Address = model.Address,
                WebSite = model.WebSite,
                LogoUrl = model.LogoUrl,
            };
        }

        public static MerchantModel ToMerchantViewModel(this Domain.Models.Web.MerchantModel model)
        {
            return new MerchantModel
            {
                Id = model.Id,
                Name = model.Name,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                Country = model.Country,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Address = model.Address,
                Created = model.Created,
                WebSite = model.WebSite,
                LogoUrl = model.LogoUrl,
            };
        }

        public static List<MerchantModel> ToMerchantViewModels(this List<Domain.Models.Web.MerchantModel> models)
        {
            return models.Select(m => m.ToMerchantViewModel()).ToList();
        } 
    }
}