﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Data.DataModel
{
    public class ClientCounterModeSettings
    {
        [Key]
        public Guid Id { get; set; }

        public bool DisplayLineItemsOnReceipt { get; set; }

        public bool ShowReceiptScreen { get; set; }

        public bool PrintReceiptEnabled { get; set; }

        public bool SendReceiptMessageEnabled { get; set; }

        public bool SendReceiptEmailEnabled { get; set; }

        public bool ShowThanksSreen { get; set; }

        [MaxLength(250)]
        public string ThanksMessage { get; set; }

        [MaxLength(250)]
        public string WelcomeMessage { get; set; }

        [MaxLength(250)]
        public string TipSuggestions { get; set; }

        public bool ShowTipsScreen { get; set; }

        public bool RequireSignature { get; set; }

        public bool AutoPrintReceipt { get; set; }

        public bool ShowTipLineOnReceipt { get; set; }

        public bool ShowSignatureLineOnReceipt { get; set; }


        public virtual Location Location { get; set; }
    }
}
