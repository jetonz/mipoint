﻿using System;
using System.Collections.Generic;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Web
{
    public interface IUserService
    {
        List<UserModel> Get();

        ResultModel<List<MerchantModel>> GetMerchants(string userId);

        UserModel Get(string userId);

        bool IsUsernameUnique(string username);

        Domain.Models.Web.ResultModel UpdateUserRelations(string userId, List<Guid> merchantIds);
    }
}
