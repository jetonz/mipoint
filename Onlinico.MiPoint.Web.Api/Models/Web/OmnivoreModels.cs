﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Web
{
    public class Employee
    {
        public string check_name { get; set; }
        public string first_name { get; set; }
        public string id { get; set; }
        public string last_name { get; set; }
        public string login { get; set; }
    }
}