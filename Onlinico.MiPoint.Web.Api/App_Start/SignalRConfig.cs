﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.SignalR;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Onlinico.MiPoint.Web.Api.App_Modules;
using Onlinico.MiPoint.Web.Api.Providers;
using Owin;

namespace Onlinico.MiPoint.Web.Api.App_Start
{
    public static class SignalRConfig
    {
        public static void Configure(IAppBuilder app, Autofac.IContainer container)
        {
            GlobalHost.HubPipeline.AddModule(new ErrorHandlingPipelineModule(container));

            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => new SignalrUserIdProvider(container));

            var serializer = new JsonSerializer();
            serializer.Converters.Add(new StringEnumConverter());
            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);

            app.MapSignalR();

            app.MapSignalR("/signalr", GetSignalrHubConfiguration(container));

            //GlobalHost.HubPipeline.RequireAuthentication();
        }

        private static HubConfiguration GetSignalrHubConfiguration(Autofac.IContainer container)
        {
            var hubConfiguration = new HubConfiguration
            {
                EnableDetailedErrors = true,
                EnableJavaScriptProxies = false
            };

            return hubConfiguration;
        }
    }
}