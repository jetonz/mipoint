﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Routing;
using Onlinico.MiPoint.Web.Api.App_Start;
using Onlinico.MiPoint.Web.Api.Filters;

namespace Onlinico.MiPoint.Web.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MessageHandlers.Add(new RequestHandler());

            config.Services.Add(typeof(IExceptionLogger), new MiPointExceptionLogger());

            // Web API configuration and services
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.ContractResolver = new Core.Serialization.DataContract.CamelCaseExceptDictionaryKeysResolver();
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());

            //config.EnableCaseInsensitive(true);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{namespace}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.Add("AgentApi", new HttpRoute("api/agent/{namespace}/{controller}", new HttpRouteValueDictionary(new { controller = "Agent" })));

            config.Services.Replace(typeof(IHttpControllerSelector), new NamespaceHttpControllerSelector(config));

            config.Filters.Add(new UnhandledExceptionWebApiFilter());

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

        }
    }
}
