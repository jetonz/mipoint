﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v3
{
    public class PaymentModel
    {
        public string Id { get; set; }
        
        public string DeviceId { get; set; }
        
        public string OrderId { get; set; }
        
        public Enums.PaymentTypeEnum Type { get; set; }

        public Enums.PaymentStatusEnum Status { get; set; }

        public long Amount { get; set; }

        public long Tip { get; set; }

        public long Tax { get; set; }
        
        public string EmployeeCode { get; set; }
        
        public string TableNumber { get; set; }
        
        public string Message { get; set; }
        
        public PaymentTransactionModel Transaction { get; set; }

        public PaymentReceptInfoModel ReceiptInfo { get; set; }

        public IEnumerable<PaymentItemModel> Items { get; set; }
    }

    public class PaymentItemModel
    {
        public Guid Id { get; set; }

        public string ItemId { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public long Price { get; set; }
    }

    public class PaymentTransactionModel
    {
        public string TransactionId { get; set; }
        
        public DateTimeOffset Issued { get; set; }
        
        public string CardEntryType { get; set; }
        
        public string CardType { get; set; }
        
        public string Last4 { get; set; }
        
        public string ReferenceId { get; set; }
        
        public string AuthCode { get; set; }
        
        public string ApplicationId { get; set; }
        
        public string TransactionType { get; set; }
        
        public string Cvm { get; set; }
    }

    public class PaymentReceptInfoModel
    {
        public Guid Id { get; set; }
        
        public string OrderNumber { get; set; }
        
        public DateTimeOffset Issued { get; set; }

        public long OrderSubTotal { get; set; }

        public long OrderTax { get; set; }

        public long OrderTotal { get; set; }

        public long OrderServiceCharge { get; set; }
        
        public string Cashier { get; set; }
        
        public string LocationName { get; set; }
        
        public string ReceiptHeader1 { get; set; }
        
        public string ReceiptHeader2 { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public string Website { get; set; }
    }
}
