namespace Onlinico.MiPoint.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAvailableTenderTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AvailableTenderTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Type = c.String(),
                        Settings_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LocationSettings", t => t.Settings_Id, cascadeDelete: true)
                .Index(t => t.Settings_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AvailableTenderTypes", "Settings_Id", "dbo.LocationSettings");
            DropIndex("dbo.AvailableTenderTypes", new[] { "Settings_Id" });
            DropTable("dbo.AvailableTenderTypes");
        }
    }
}
