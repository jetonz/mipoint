﻿using System;
using System.Collections.Generic;
using Onlinico.MiPoint.Web.Domain.Models;
using Onlinico.MiPoint.Web.Domain.Models.Api.v1;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v1
{
    public interface ILocationService
    {
        LocationSettingsModel GetSettings(string locationId);

        ResultModel<LocationSettingsModel> UpdatteSettings(LocationSettingsModel model);

        string GetApiKey(string locationId);

        string GetLocationIdentifier(string locationId);

        List<LocationModel> GetByMerchant(Guid merchantId);
    }
}
