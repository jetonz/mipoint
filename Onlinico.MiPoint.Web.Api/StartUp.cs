﻿using System.Data.Entity;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Onlinico.MiPoint.Web.Api;
using Onlinico.MiPoint.Web.Api.App_Start;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace Onlinico.MiPoint.Web.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Data.DataModel.Initializer.SetDatabaseInitializer();

            ConfigureAuth(app);
            
            var container = AutofacConfig.RegisterContainer(app);

            //Configure SignalR
            SignalRConfig.Configure(app, container);

            //Configure Quartz
            QuartzConfig.Register(container);
        }
    }
}