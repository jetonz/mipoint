﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v3.Enums
{
    public enum AgentStatusEnum
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown,
        /// <summary>
        /// NotInstalled
        /// </summary>
        NotInstalled,
        /// <summary>
        /// Stopped
        /// </summary>
        Stopped,
        /// <summary>
        /// StopPending
        /// </summary>
        StopPending,
        /// <summary>
        /// Running
        /// </summary>
        Running,
        /// <summary>
        /// StartPending
        /// </summary>
        StartPending
    }
}