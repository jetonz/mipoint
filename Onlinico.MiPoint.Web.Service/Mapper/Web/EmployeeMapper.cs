﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class EmployeeMapper
    {
        public static List<Domain.Models.Web.EmployeeModel> ToDomainEmployees(this List<Service.Models.Omnivore.Employees.Employee> models)
        {
            return models.Select(e => new Domain.Models.Web.EmployeeModel
            {
                Id = Guid.NewGuid(),
                Login = e.login,
                CheckName = e.check_name,
                EmployeeId = e.id,
                FirstName = e.first_name,
                LastName = e.last_name,
            }).ToList();
        }

        public static void Update(this Data.DataModel.Employee entity, Domain.Models.Web.EmployeeModel model)
        {
            entity.Login = model.Login;
            entity.FirstName = model.FirstName;
            entity.LastName = model.LastName;
            entity.CheckName = model.CheckName;
        }

        public static List<Data.DataModel.Employee> ToNewEmployees(this List<Domain.Models.Web.EmployeeModel> models, Data.DataModel.Location location)
        {
            return models.Select(e => e.ToNewEmployee(location)).ToList();
        }

        public static Data.DataModel.Employee ToNewEmployee(this Domain.Models.Web.EmployeeModel model, Data.DataModel.Location location)
        {
            return new Data.DataModel.Employee
            {
                Id = Guid.NewGuid(),
                EmployeeId = model.EmployeeId,
                Login = model.Login,
                FirstName = model.FirstName,
                LastName = model.LastName,
                CheckName = model.CheckName,
                Location = location,
            };
        }
    }
}
