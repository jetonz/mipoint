﻿using System.Collections.Generic;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;

namespace Onlinico.MiPoint.Web.Domain.Models.Web
{
    public class UserModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string Login { get; set; }

        public List<RoleModel> Roles { get; set; } 

        public List<MerchantModel> Merchants { get; set; } 
    }

    public class RoleModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
