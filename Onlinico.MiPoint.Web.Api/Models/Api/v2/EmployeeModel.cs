﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v2
{
    [DataContract]
    public class EmployeeModel
    {
        public Guid Id { get; set; }
        
        [DataMember(Name = "id")]
        public string EmployeeId { get; set; }

        [DataMember(Name = "check_name")]
        public string CheckName { get; set; }

        [DataMember(Name = "first_name")]
        public string FirstName { get; set; }

        [DataMember(Name = "last_name")]
        public string LastName { get; set; }

        [DataMember(Name = "login")]
        public string Login { get; set; }
    }
}