﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Onlinico.MiPoint.Web.Api.Helpers;
using Onlinico.MiPoint.Web.Api.Models.Api.v4;
using Onlinico.MiPoint.Web.Api.Models.Web;
using Onlinico.MiPoint.Web.Core.Interfaces;
using RestSharp.Extensions;

namespace Onlinico.MiPoint.Web.Api.Controllers.Api.v4
{
    /// <summary>
    /// Base web api controller
    /// </summary>
    public abstract class BaseController : ApiController
    {
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly Logger.Interfaces.ILogger Logger;

        /// <summary>
        /// App configuration
        /// </summary>
        protected readonly IConfiguration Configuration;


        /// <summary>
        /// Base controller constructor
        /// </summary>
        /// <param name="logger">Inject logger</param>
        /// <param name="configuration">Inject app configuration</param>
        protected BaseController(Logger.Interfaces.ILogger logger, IConfiguration configuration)
        {
            Logger = logger;
            Configuration = configuration;
        }

        /// <summary>
        /// Get current server domain
        /// </summary>
        protected Uri CurrentDomain => new Uri(Request.RequestUri.Scheme + System.Uri.SchemeDelimiter + Request.RequestUri.Host + (Request.RequestUri.IsDefaultPort ? "" : ":" + Request.RequestUri.Port));

        /// <summary>
        /// 
        /// </summary>
        protected string LocationId => Request.Properties[Configuration.Secret].ToString();

        protected HttpResponseException CreateApiException(string errorMessage, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            var responce = new HttpResponseMessage(statusCode);

            if (statusCode != HttpStatusCode.NoContent)
            {
                responce.Content = new ObjectContent<ApiExceptionModel>(new ApiExceptionModel() { ErrorMessage = errorMessage }, new JsonMediaTypeFormatter());
            }
            
            throw new HttpResponseException(responce);
        }

        protected HttpResponseException CreateApiException(string errorMessage, string internalErrorMesage, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            var responce = new HttpResponseMessage(statusCode)
            {
                Content = new ObjectContent<ApiExceptionModel>(new ApiExceptionModel()
                {
                    ErrorMessage = errorMessage,
                    InternalErrorMessage = internalErrorMesage,
                }, new JsonMediaTypeFormatter())
            };
            throw new HttpResponseException(responce);
        }

        protected HttpResponseException CreateApiException(ModelStateDictionary modelStateDictionary, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            var responce = new HttpResponseMessage(statusCode)
            {
                Content = new ObjectContent<ApiExceptionModel>(new ApiExceptionModel()
                {
                    ErrorMessage = modelStateDictionary.Values.ToErrorString()
                }, new JsonMediaTypeFormatter())
            };
            throw new HttpResponseException(responce);
        }
    }
}
