﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Enums;
using Onlinico.MiPoint.Web.Domain.Models.Api.v3;

namespace Onlinico.MiPoint.Web.Service.Mapper.Api.v3
{
    public static class PaymentMapper
    {
        public static Domain.Models.Api.v3.PaymentModel ToDomainPayment(this Data.DataModel.Payment model)
        {
            return new Domain.Models.Api.v3.PaymentModel
            {
                Id = model.Id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tip,
                Tax = model.Tax,
                Status = (PaymentStatusEnum)((int)model.Status),
                Type = (PaymentTypeEnum)((int)model.Type),
                Message = model.Message,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,
                
                Items = model.Items?.ToDomainPaymentItems(),
                ReceiptInfo = model.ReceiptInfo?.ToDomainPaymentReceptInfo(),
                Transaction = model.Transaction?.ToDomainPaymentTransaction(),
            };
        }

        public static IEnumerable<Domain.Models.Api.v3.PaymentModel> ToDomainPayments(this IEnumerable<Data.DataModel.Payment> models)
        {
            var result = models.Select(p => p.ToDomainPayment());
            return result.AsQueryable();
        }

        public static PaymentTransactionModel ToDomainPaymentTransaction(this Data.DataModel.PaymentTransaction model)
        {
            return new PaymentTransactionModel
            {
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = model.CardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType,
                Issued = model.Issued,
            };
        }

        public static PaymentReceptInfoModel ToDomainPaymentReceptInfo(this Data.DataModel.PaymentReceiptInfo model)
        {
            return new PaymentReceptInfoModel
            {
                Id = model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }

        public static IEnumerable<PaymentItemModel> ToDomainPaymentItems(this IEnumerable<Data.DataModel.PaymentItem> items)
        {
            return items.Select(i => new PaymentItemModel
            {
                Id = i.Id,
                ItemId = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            });
        }

        public static Data.DataModel.Payment ToNewPayment(this PaymentModel model, Data.DataModel.Location location, string id)
        {
            return new Data.DataModel.Payment
            {
                Id = id,
                DeviceId = model.DeviceId,
                OrderId = model.OrderId,
                Amount = model.Amount,
                Tip = model.Tip,
                Tax = model.Tax,
                EmployeeCode = model.EmployeeCode,
                TableNumber = model.TableNumber,
                Status = Data.DataModel.Enums.PaymentStatusEnum.New,
                Type = (Data.DataModel.Enums.PaymentTypeEnum)((int)model.Type),
                Message = model.Message,
                
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,

                Items = model.Items?.ToPaymentItems(),
                ReceiptInfo = model.ReceiptInfo?.ToPaymentReceiptInfo(),
                Location = location,
            };
        }

        public static Data.DataModel.PaymentReceiptInfo ToPaymentReceiptInfo(this PaymentReceptInfoModel model)
        {
            return new PaymentReceiptInfo
            {
                Id = model.Id == Guid.Empty ? Guid.NewGuid() : model.Id,
                PhoneNumber = model.PhoneNumber,
                Issued = model.Issued,
                LocationName = model.LocationName,
                Cashier = model.Cashier,
                OrderNumber = model.OrderNumber,
                OrderServiceCharge = model.OrderServiceCharge,
                OrderSubTotal = model.OrderSubTotal,
                OrderTax = model.OrderTax,
                OrderTotal = model.OrderTotal,
                ReceiptHeader1 = model.ReceiptHeader1,
                ReceiptHeader2 = model.ReceiptHeader2,
                Website = model.Website,
            };
        }

        public static PaymentTransaction ToPaymentTransaction(this PaymentTransactionModel model)
        {
            return new PaymentTransaction
            {
                Id = Guid.NewGuid(),
                TransactionId = model.TransactionId,
                TransactionType = model.TransactionType,
                Cvm = model.Cvm,
                ApplicationId = model.ApplicationId,
                CardType = model.CardType,
                ReferenceId = model.ReferenceId,
                Last4 = model.Last4,
                AuthCode = model.AuthCode,
                CardEntryType = model.CardEntryType,
                Issued = model.Issued,
            };
        }

        public static List<Data.DataModel.PaymentItem> ToPaymentItems(this IEnumerable<PaymentItemModel> items)
        {
            return items.Select(i => new Data.DataModel.PaymentItem
            {
                Id = i.Id == Guid.Empty ? Guid.NewGuid() : i.Id,
                ItemId = i.ItemId,
                Name = i.Name,
                Price = i.Price,
                Quantity = i.Quantity
            }).ToList();
        }
    }
}
