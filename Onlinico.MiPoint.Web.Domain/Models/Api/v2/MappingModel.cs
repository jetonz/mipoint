﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Enums;

namespace Onlinico.MiPoint.Web.Domain.Models.Api.v2
{
    public class MappingModel
    {
        public MappingTypeEnum Type { get; set; }

        public Dictionary<string, string> Scheme { get; set; } 
    }
}
