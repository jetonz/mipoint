﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Onlinico.MiPoint.Web.Api.App_Start
{
    /// <summary>
    /// MiPoint configuration section
    /// </summary>
    public class Configuration : Core.Interfaces.IConfiguration
    {
        /// <summary>
        /// Get Omnivore Production api-key
        /// </summary>
        public string OmnivoreProdApiKey => ConfigurationManager.AppSettings["OmnivoreProdApiKey"];

        /// <summary>
        /// Get Omnivore Dev api-key
        /// </summary>
        public string OmnivoreDevApiKey => ConfigurationManager.AppSettings["OmnivoreDevApiKey"];

        /// <summary>
        /// Get BaseOmnivoreApiUrl from web.config
        /// </summary>
        public string BaseOmnivoreApiUrl => ConfigurationManager.AppSettings["BaseOmnivoreApiUrl"];

        /// <summary>
        /// Get ApiKey from web.config
        /// </summary>
        public string HeaderApiKey => ConfigurationManager.AppSettings["HeaderApiKey"];

        /// <summary>
        /// Get Secret from web.config
        /// </summary>
        public string Secret => ConfigurationManager.AppSettings["Secret"];

        /// <summary>
        /// Get Admin role from web.config
        /// </summary>
        public string AdminRole => ConfigurationManager.AppSettings["AdminRole"];

        /// <summary>
        /// Get Merchant manager role from web.config
        /// </summary>
        public string MerchantManagerRole => ConfigurationManager.AppSettings["MerchantManagerRole"];

        /// <summary>
        /// Get Location manager role from web.config
        /// </summary>
        public string LocationManagerRole => ConfigurationManager.AppSettings["LocationManagerRole"];

        /// <summary>
        /// Get Twilio account sid
        /// </summary>
        public string TwilioAccountSid => ConfigurationManager.AppSettings["TwilioAccountSid"];

        /// <summary>
        /// Get Twilio auth token
        /// </summary>
        public string TwilioAuthToken => ConfigurationManager.AppSettings["TwilioAuthToken"];

        /// <summary>
        /// Get Twilio phone number
        /// </summary>
        public string TwilioPhoneNumber => ConfigurationManager.AppSettings["TwilioPhoneNumber"];

        /// <summary>
        /// Get Sync trigger interval in minutes
        /// </summary>
        public int SyncTriggerIntervalInMinutes => int.Parse(ConfigurationManager.AppSettings["SyncTriggerIntervalInMinutes"]);

        /// <summary>
        /// Get payment resolver trigger interval in minutes
        /// </summary>
        public int PaymentResolverTriggerIntervalInMinutes => int.Parse(ConfigurationManager.AppSettings["PaymentResolverTriggerIntervalInMinutes"]);

        /// <summary>
        /// Get DataRotation job start time in UTC hour
        /// </summary>
        public int DataRotationJobStartTimeInUtcHour => int.Parse(ConfigurationManager.AppSettings["DataRotationJobStartTimeInUtcHour"]);

        /// <summary>
        /// Get activity retention period in days
        /// </summary>
        public int ActivityRetentionPeriodInDays => int.Parse(ConfigurationManager.AppSettings["ActivityRetentionPeriodInDays"]);

        /// <summary>
        /// Get activity content retention period in days
        /// </summary>
        public int ActivityContentRetentionPeriodInDays => int.Parse(ConfigurationManager.AppSettings["ActivityContentRetentionPeriodInDays"]);

        /// <summary>
        /// Get history retention period in days
        /// </summary>
        public int SessionLogRetentionPeriodInDays => int.Parse(ConfigurationManager.AppSettings["SessionLogRetentionPeriodInDays"]);

        /// <summary>
        /// Get history content retention period in days
        /// </summary>
        public int SessionLogContentRetentionPeriodInDays => int.Parse(ConfigurationManager.AppSettings["SessionLogContentRetentionPeriodInDays"]);
    }
}