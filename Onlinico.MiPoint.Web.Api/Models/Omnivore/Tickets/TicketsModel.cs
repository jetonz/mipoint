﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Policy;
using System.Web;
using Newtonsoft.Json;

namespace Onlinico.MiPoint.Web.Api.Models.Omnivore.Tickets
{
    public class ClockEntries
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links
    {
        public ClockEntries clock_entries { get; set; }
        public Self self { get; set; }
    }

    public class Employee : ICloneable
    {
        public Links _links { get; set; }
        public string check_name { get; set; }
        public string first_name { get; set; }
        public string id { get; set; }
        public string last_name { get; set; }
        public string login { get; set; }

        public object Clone()
        {
            return new Employee { id = this.id, last_name = this.last_name, login = this.login, first_name = this.first_name, check_name = this.check_name };
        }
    }

    public class Self2
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
    }

    public class OrderType
    {
        public Links2 _links { get; set; }
        public bool? available { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Self3
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links3
    {
        public Self3 self { get; set; }
    }

    public class RevenueCenter
    {
        public Links3 _links { get; set; }
        public bool? @default { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Self4
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links4
    {
        public Self4 self { get; set; }
    }

    public class Table : ICloneable
    {
        public Links4 _links { get; set; }
        public bool? available { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public int? number { get; set; }
        public int? seats { get; set; }

        public object Clone()
        {
            return new Table { id = this.id, name = this.name, number = this.number ?? 0, available = this.available ?? false, seats = this.seats ?? 0 };
        }
    }

    public class TicketEmbedded
    {
        public List<object> discounts { get; set; }
        public Employee employee { get; set; }
        public List<object> items { get; set; }
        public OrderType order_type { get; set; }
        public List<object> payments { get; set; }
        public RevenueCenter revenue_center { get; set; }
        public Table table { get; set; }
        public List<object> voided_items { get; set; }
    }

    public class Discounts
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Items
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Payments
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Self5
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class VoidedItems
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Table2
    {
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links5
    {
        public Discounts discounts { get; set; }
        public Items items { get; set; }
        public Payments payments { get; set; }
        public Self5 self { get; set; }
        public VoidedItems voided_items { get; set; }
        public Table2 table { get; set; }
    }

    public class Totals
    {
        public int? due { get; set; }
        public int? other_charges { get; set; }
        public int? service_charges { get; set; }
        public int? sub_total { get; set; }
        public int? tax { get; set; }
        public int? total { get; set; }
    }

    public class Ticket
    {
        public TicketEmbedded _embedded { get; set; }
        public Links5 _links { get; set; }
        public bool? auto_send { get; set; }
        public int? closed_at { get; set; }
        public int? guest_count { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public bool? open { get; set; }
        public int? opened_at { get; set; }
        public int? ticket_number { get; set; }
        public Totals totals { get; set; }
        public bool? @void { get; set; }
    }

    public class Embedded
    {
        public List<Ticket> tickets { get; set; }
    }

    public class Self6
    {
        public string etag { get; set; }
        public string href { get; set; }
        public string profile { get; set; }
    }

    public class Links6
    {
        public Self6 self { get; set; }
    }

    public class RootObject
    {
        public Embedded _embedded { get; set; }
        public Links6 _links { get; set; }
        public int? count { get; set; }
        public int? limit { get; set; }
    }
}