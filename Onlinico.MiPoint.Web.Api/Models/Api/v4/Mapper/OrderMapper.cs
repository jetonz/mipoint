﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Onlinico.MiPoint.Web.Api.Extensions;
using Onlinico.MiPoint.Web.Api.Models.Api.v4.Enums;
using Onlinico.MiPoint.Web.Domain.Enums;
using Quartz.Util;

namespace Onlinico.MiPoint.Web.Api.Models.Api.v4.Mapper
{
    public static class OrderMapper
    {
        public static OrderModel ToApiOrderModel(this Service.Models.Omnivore.Ticket.Ticket.RootObject model, List<Domain.Models.Api.v4.PaymentModel> payments)
        {
            var orderPayments = payments?.Where(p => p.OrderId == $"{model.id}-{model.opened_at}").ToList();

            return new OrderModel
            {
                Id = $"{model.id}-{model.opened_at}",
                Name = model.name ?? "",
                OpenedAt = (model.opened_at ?? 0).ToDateTime(),
                ClosedAt = model.closed_at == null ? (DateTime?)null : (model.closed_at ?? 0).ToDateTime(),
                EmployeeId = model._embedded?.employee?.id,
                GuestCount = model.guest_count ?? 0,
                Number = model.ticket_number ?? 0,
                HasPayments = payments?.Any() ?? false,

                Status = GetOrderStatus(model.open ?? false, model.totals.due ?? 0, model.totals.total ?? 0, orderPayments),
                PaymentsStatus = GetOrderPaymentStatus(orderPayments),
                
                Table = model._embedded?.table?.ToApiOrderTableModel(),
                Items = model._embedded?.items?.ToApiOrderItems(),
                Total = model.totals?.ToApiOrderTotalModel(),
                Discounts = model._embedded?.discounts?.ToDiscounts(),
            };
        }

        public static OrderTotalModel ToApiOrderTotalModel(this Service.Models.Omnivore.Ticket.Ticket.Totals model)
        {
            return new OrderTotalModel
            {
                Total = model.total ?? 0,
                Tax = model.tax ?? 0,
                Due = model.due ?? 0,
                OtherCharges = model.other_charges ?? 0,
                ServiceCharges = model.service_charges ?? 0,
                ItemsCost = model.sub_total ?? 0,
            };
        }

        public static OrderTableModel ToApiOrderTableModel(this Service.Models.Omnivore.Ticket.Ticket.Table model)
        {
            return new OrderTableModel
            {
                Id = model.id,
                Name = model.name,
                IsAvailable = model.available ?? false,
                Number = model.number ?? 0,
                Seats = model.seats ?? 0,
            };
        }

        public static List<OrderItemModel> ToApiOrderItems(this List<Service.Models.Omnivore.Ticket.Ticket.Item> model)
        {
            return model.Select(i => new OrderItemModel
            {
                Id = i.id,
                Name = i.name,
                Quantity = i.quantity ?? 0,
                Price = i.price_per_unit ?? 0,
                Sum = (i.price_per_unit ?? 0) * (i.quantity ?? 0),
                Tax = null,
                HasDiscounts = i._embedded.discounts.Any(),
                Comment = i.comment,
                Discounts = i._embedded?.discounts?.ToDiscounts(),
            }).ToList();
        }

        public static List<DiscountModel> ToDiscounts(this List<Service.Models.Omnivore.Ticket.Ticket.Discount> model)
        {
            return model.Select(i => new DiscountModel
            {
                Id = i.id,
                Name = i.name,
                Value = i.value ?? 0,
            }).ToList();
        }

        public static List<ShortOrderModel> ToApiShortOrderModel(this Service.Models.Omnivore.Ticket.Tickets.RootObject model, List<Domain.Models.Api.v4.PaymentModel> payments)
        {
            return model?._embedded?.tickets?.Select(t =>
            {
                var orderPayments = payments?.Where(p => p.OrderId == $"{t.id}-{t.opened_at}").ToList();

                return new ShortOrderModel
                {
                    Id = $"{t.id}-{t.opened_at}",
                    OpenedAt = (t.opened_at ?? 0).ToDateTime(),
                    EmployeeId = t._embedded?.employee?.id,
                    Number = t.ticket_number ?? 0,
                    Name = t.name ?? "",
                    GuestCount = t.guest_count ?? 0,
                    Status = GetOrderStatus(t.open ?? false, t.totals.due ?? 0, t.totals.total ?? 0, orderPayments),
                    PaymentsStatus = GetOrderPaymentStatus(orderPayments),

                    Total = t.totals?.ToApiOrderTotalModel(),
                    Table = t._embedded?.table?.ToApiOrderTableModel(),
                };
            }).ToList();
        }

        public static OrderStatusEnum GetOrderStatus(bool isOpen, long due, long total, List<Domain.Models.Api.v4.PaymentModel> payments)
        {
            var result = OrderStatusEnum.Closed;
            if (isOpen)
            {
                if (due == total)
                {
                    result = !payments.Any() ? OrderStatusEnum.Open : OrderStatusEnum.PartiallyPaid;
                }
                if (due > 0 && due != total)
                {
                    result = OrderStatusEnum.PartiallyPaid;
                }
                if (due == 0 && total != 0)
                {
                    result = OrderStatusEnum.Closed;
                }
            }
            return result;
        }

        public static OrderPaymentStatusEnum GetOrderPaymentStatus(List<Domain.Models.Api.v4.PaymentModel> payments)
        {
            var result = payments.Any(p => p.Status == PaymentStatusEnum.New || p.Status == PaymentStatusEnum.IsProcessing || p.Status == PaymentStatusEnum.Confirmed);

            return result ? OrderPaymentStatusEnum.Pending : OrderPaymentStatusEnum.Processed;
        }

        public static OrderTotalModel ToApiOrderTotalModel(this Service.Models.Omnivore.Ticket.Tickets.Totals model)
        {
            return new OrderTotalModel
            {
                Total = model.total ?? 0,
                Tax = model.tax ?? 0,
                Due = model.due ?? 0,
                OtherCharges = model.other_charges ?? 0,
                ServiceCharges = model.service_charges ?? 0,
                ItemsCost = model.sub_total ?? 0,
            };
        }

        public static OrderTableModel ToApiOrderTableModel(this Service.Models.Omnivore.Ticket.Tickets.Table model)
        {
            return new OrderTableModel
            {
                Id = model.id,
                Name = model.name,
                IsAvailable = model.available ?? false,
                Number = model.number ?? 0,
                Seats = model.seats ?? 0,
            };
        }

        public static List<OrderItemModel> ToApiOrderItems(this List<Service.Models.Omnivore.Ticket.Tickets.Item> model)
        {
            return model.Select(i => new OrderItemModel
            {
                Id = i.id,
                Name = i.name,
                Quantity = i.quantity ?? 0,
                Price = i.price_per_unit ?? 0,
                HasDiscounts = i._embedded.discounts.Any(),
                Comment = i.comment,
            }).ToList();
        }
    }
}