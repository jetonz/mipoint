﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onlinico.MiPoint.Web.Data.DataModel;
using Onlinico.MiPoint.Web.Domain.Models.Web;

namespace Onlinico.MiPoint.Web.Service.Mapper.Web
{
    public static class TenderTypeMapper
    {
        public static List<Domain.Models.Web.TenderTypeModel> ToDomainTenderTypes(
            this IEnumerable<Data.DataModel.TenderType> tenderTypes)
        {
            return tenderTypes.Select(t => new Domain.Models.Web.TenderTypeModel
            {
                Id = t.Id,
                Name = t.Name,
                Type = t.Type,
                Card = t.Card.ToDomainCardModel()
            }).ToList();
        }


        public static List<Data.DataModel.TenderType> ToEntityTenderTypes(
            this List<Domain.Models.Web.TenderTypeModel> tenderTypes)
        {
            return tenderTypes.Select(t => new Data.DataModel.TenderType
            {
                Id = t.Id,
                Name = t.Name,
                Type = t.Type,
                Card = t.Card.ToEntityCard()
            }).ToList();
        }

        public static void UpdateTenderTypeEntity(this Data.DataModel.TenderType entity,
            Domain.Models.Web.TenderTypeModel model)
        {
            entity.Name = model.Name;
            entity.Type = model.Type;
        }

        public static List<AvailableTenderTypeModel> ToDomainAvailableTenderTypes(
            this IEnumerable<AvailableTenderType> models)
        {
            return models.Select(m => m.ToDomainAvailableTenderTypeModel()).ToList();
        }

        public static AvailableTenderTypeModel ToDomainAvailableTenderTypeModel(this AvailableTenderType model)
        {
            return new AvailableTenderTypeModel
            {
                Id = model.Id,
                Name = model.Name,
                Type = model.Type,
            };
        }

    }
}
