﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Api.v2;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Api.v2
{
    public interface IReceiptService
    {
        Task<ResultModel<Domain.Models.Api.v2.ReceiptModel>> GetAsync(string receiptId);

        Task<ResultModel<List<Domain.Models.Api.v2.ReceiptModel>>> GetByTicketIdAsync(string locationId, string ticketId);

        Task<ResultModel> AddAsync(Domain.Models.Api.v2.ReceiptModel model);
    }
}
