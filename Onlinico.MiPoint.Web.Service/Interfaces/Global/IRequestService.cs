﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Onlinico.MiPoint.Web.Domain.Models.Global;

namespace Onlinico.MiPoint.Web.Service.Interfaces.Global
{
    public interface IRequestService
    {
        Task<ResultModel> AddAsync(RequestModel model);
    }
}
