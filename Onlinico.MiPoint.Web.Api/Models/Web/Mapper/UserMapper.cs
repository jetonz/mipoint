﻿using System.Collections.Generic;
using System.Linq;

namespace Onlinico.MiPoint.Web.Api.Models.Web.Mapper
{
    public static class UserMapper
    {
        public static UserModel ToUserModel(this Domain.Models.Web.UserModel model)
        {
            return new UserModel
            {
                Id = model.Id,
                FullName = model.FullName,
                Login = model.Login
            };
        }
        
        public static UserModel ToUserModel(this Data.DataModel.User model)
        {
            return new UserModel
            {
                Id = model.Id,
                FullName = model.FullName,
                Login = model.UserName
            };
        }

        public static List<UserModel> ToUserViewModelList(this List<Domain.Models.Web.UserModel> models)
        {
            return models.Select(u=> new UserModel
            {
                Id = u.Id,
                FullName = u.FullName,
                Login = u.Login
            }).ToList();
        }

        public static Domain.Models.Web.UserModel  ToDomainUserModel(this UserModel model)
        {
            return new Domain.Models.Web.UserModel
            {
                Id = model.Id,
                FullName = model.FullName,
                Login = model.Login
            };
        }
    }
}